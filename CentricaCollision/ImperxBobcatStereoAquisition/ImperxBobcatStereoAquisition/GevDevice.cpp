#include "StdAfx.h"
#include "GevDevice.h"
#include <list>
#include <algorithm>
#include <iostream>

#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>

// catch the ackward min max macros which some header is pulling in
#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

namespace
{
	//helper function for rendering text to the images
	cv::Size RenderText(cv::Mat& image, cv::Point origin, const std::string& text, double scale = 1.0)
	{
		int font = cv::FONT_HERSHEY_PLAIN;
		int line_type = CV_AA;
		int base_line;
		cv::putText(image, text, origin, font, scale, cv::Scalar(0), 2, line_type);
		cv::putText(image, text, origin, font, scale, cv::Scalar(255), 1, line_type);
		cv::Size size = cv::getTextSize(text, font, scale, 2, &base_line);
		size.height += base_line;
		return size;
	}
}


//
// Constructor
//

GevDevice::GevDevice()
	: pipeline_(&stream_)
	, gen_link_recovery_(0)
	, gen_transport_layer_params_locked_(0)
	, gen_payload_size_(0)
	, gen_width_(0)
	, gen_height_(0)
	, gen_command_start_(0)
	, gen_command_stop_(0)
	, gen_count_(0)
	, gen_framerate_(0)
	, gen_bandwidth_(0)
	, gen_command_reset_timestamp_(0)
	, gen_aquisition_mode_(0)
	, gen_auto_gain_enable_(0)
	, gen_auto_gain_max_(0)
	, gen_auto_gain_speed_(0)
	, gen_auto_exposure_enable_(0)
	, gen_auto_exposure_max_(0)
	, gen_auto_exposure_speed_(0)
	, gen_auto_luminance_level_(0)
	, gen_auto_luminance_type_(0)
	, gen_auto_offset_x_(0)
	, gen_auto_offset_y_(0)
	, gen_auto_width_(0)
	, gen_auto_height_(0)
	, gen_auto_gain_(0)
	, gen_auto_exposure_(0)
	, gen_auto_luminance_(0)
	, message_queue_(0)
	, quit_(false)
	, is_master_(false)
	, ready_(false)
	, other_(0)
	, stereo_handler_(0)
{
	memset(camera_id_, 0, sizeof(camera_id_));
}


//
// Destructor
//
GevDevice::~GevDevice()
{
	WrapUp();
}



//
// Public function to starts recording a stream
//
bool GevDevice::Run()
{
	bool success = false;
	if (OpenStream())
	{

		if(StartAcquisition())
		{ 
			success = true;
			AcquisitionLoop();
		}		
		StopAcquisition();


	}
	WrapUp();
	return success;	
}


//
// Public function to stops recording a stream
//
void GevDevice::Quit()
{
	quit_ = true;
}


//
// Helper function to display/log PvApi errors
//
bool logPvError(PvResult result)
{
	if(!result.IsOK())	
	{
		printf("%s: %s\n" 
			, result.GetCodeString().GetAscii()
			, result.GetDescription().GetAscii() );
	}
	return result.IsOK();
}



bool GevDevice::Connect(const char * device_identifier, StereoHandler * save_buffer, GevDevice * other, bool is_master)
{
	printf("Connecting to %s camera\n", is_master ? "Master" : "Slave");

	stereo_handler_ = save_buffer;
	is_master_ = is_master;
	memset(camera_id_, 0, sizeof(camera_id_));

	if(other != NULL)
	{
		other_ = other;
		other->other_ = this;
	}	

	PvString identifier(device_identifier); // camera ip address or DeviceUserID parameter
	PvString device_user_id; 
	PvResult result;

	if (device_identifier == NULL) // if no device id given...
	{
		printf("Please select the %s camera\n", is_master? "Master":"Slave" );

		// ...create and show a Gev Device finder dialog		
		PvDeviceFinderWnd  device_finder;
		device_finder.ShowModal();

		// Get the connectivity information for the selected Gev Device
		PvDeviceInfo * device_info = device_finder.GetSelected();

		if (device_info == NULL) //nothing selected, can't continue
			return false;

		// use ip for the device id
		identifier = device_info->GetIPAddress();
	}
	

	// connect to the Gev Device
	if (!device_.Connect(identifier).IsOK())
	{
		printf("Unable to connect to %s\n", identifier.GetAscii());
		return false;
	}


	// get communication link and camera device parameters 
	PvGenParameterArray *comm_params(device_.GetGenLink());
	PvGenParameterArray *gen_params(device_.GetGenParameters());

	gen_params->GetString("DeviceUserID", device_user_id);
	memcpy(&camera_id_, device_user_id.GetAscii(), std::min(device_user_id.GetLength(), unsigned(sizeof(camera_id_))));
		
	printf("Successfully connected to %s (%s)\n", identifier.GetAscii(), device_user_id.GetAscii());

	// negociate packet size
	// (based on gev player example code from the SDK)
	{
		bool auto_negotiation_enabled = true;
		PvGenBoolean *auto_negotiation = comm_params->GetBoolean("AutoNegotiation");
		if (auto_negotiation != NULL)
		{
			auto_negotiation->GetValue(auto_negotiation_enabled);
		}

		PvInt64 user_packet_size_value = 1476;
		PvGenInteger *user_packet_size = comm_params->GetInteger("DefaultPacketSize");
		if ((user_packet_size != NULL) && user_packet_size->IsReadable())
		{
			user_packet_size->GetValue(user_packet_size_value);
		}

		if ( auto_negotiation_enabled )
		{
			// Perform automatic packet size negociation
			printf( "Optimizing streaming packet size...\n" ) ;
			result = device_.NegotiatePacketSize( 0, 1476 );
			if ( !result.IsOK() )
			{
				printf( "WARNING: streaming packet size optimization failure, using 1476 bytes!\n"  );
				::Sleep( 3000 );
			}
	
		}
		else
		{
			bool manual_packet_size_success = false;

			// Start by figuring out if we can use GenICam to set the packet size
			bool use_genicam = false;
			PvGenInteger *packet_size = comm_params->GetInteger("GevSCPSPacketSize");
			if ( packet_size != NULL)
			{
				bool is_writable = false;
				result = packet_size->IsWritable(is_writable);
				if ( result.IsOK() && is_writable )
				{
					use_genicam = true;
				}
			}
                    
			if (use_genicam)
			{
				// Setting packet size through the GenICam interface
				result = packet_size->SetValue( user_packet_size_value );
				if ( result.IsOK() )
				{
					manual_packet_size_success = true;
				}
				else
				{
					// Last resort default...
					packet_size->SetValue( 576 );
					manual_packet_size_success = false;
				}
			}
			else
			{
				// Direct write, for GenICam challenged devices...
				result = device_.WriteRegister(0x0D04, (PvUInt32)user_packet_size_value );
				if ( result.IsOK() )
				{
					manual_packet_size_success = true;
				}
				else
				{
					// Last resort default...
					device_.WriteRegister( 0x0D04, 576 );
					manual_packet_size_success = false;
				}
			}
                    
			if (manual_packet_size_success)
			{
				printf("A packet of size of %i bytes was configured for streaming.\nYou may experience issues if your system configuration cannot support this packet size.\n"
					, user_packet_size_value);
			}
			else
			{                        
				printf("WARNING: could not set streaming packet size to %i bytes, using %i bytes!\n"
					, user_packet_size_value
					, 576 );
			}
			::Sleep( 3000 ); 
	
		}
		printf("Using packet size of %ull \n", user_packet_size_value);
	}
	printf("Finding camera parameters \n");

	// gather all the camera parameters we want
	if ((gen_link_recovery_ = comm_params->GetBoolean("LinkRecoveryEnabled")) == nullptr ||
		(gen_transport_layer_params_locked_ = gen_params->GetInteger("TLParamsLocked")) == nullptr ||
		(gen_payload_size_ = gen_params->GetInteger("PayloadSize")) == nullptr ||
		(gen_width_ = gen_params->GetInteger("Width")) == nullptr ||
		(gen_height_ = gen_params->GetInteger("Height")) == nullptr ||
		(gen_command_start_ = gen_params->GetCommand("AcquisitionStart")) == nullptr ||
		(gen_command_stop_ = gen_params->GetCommand("AcquisitionStop")) == nullptr ||
		(gen_command_reset_timestamp_ = gen_params->GetCommand("GevTimestampControlReset")) == nullptr ||
		(gen_aquisition_mode_ = gen_params->GetEnum("AcquisitionMode")) == nullptr ||

		(gen_auto_gain_enable_ = gen_params->GetBoolean("AgcEnable")) == nullptr ||
		(gen_auto_gain_max_ = gen_params->GetInteger("AgcMax")) == nullptr ||
		(gen_auto_gain_speed_ = gen_params->GetEnum("AgcSpeed")) == nullptr ||
		(gen_auto_exposure_enable_ = gen_params->GetBoolean("AecEnable")) == nullptr ||
		(gen_auto_exposure_max_ = gen_params->GetInteger("AecMax")) == nullptr ||
		(gen_auto_exposure_speed_ = gen_params->GetEnum("AecSpeed")) == nullptr ||

		(gen_auto_luminance_level_ = gen_params->GetInteger("AgcAecLuminanceLevel")) == nullptr ||
		(gen_auto_luminance_type_ = gen_params->GetEnum("AgcAecLuminanceType")) == nullptr ||
		(gen_auto_offset_x_ = gen_params->GetInteger("AgcAecOffsetX")) == nullptr ||
		(gen_auto_offset_y_ = gen_params->GetInteger("AgcAecOffsetY")) == nullptr ||
		(gen_auto_width_ = gen_params->GetInteger("AgcAecWidth")) == nullptr ||
		(gen_auto_height_ = gen_params->GetInteger("AgcAecHeight")) == nullptr ||

		(gen_auto_gain_ = gen_params->GetInteger("CurrentAgcGain")) == nullptr ||
		(gen_auto_exposure_ = gen_params->GetInteger("CurrentAecExposure")) == nullptr ||
		(gen_auto_luminance_ = gen_params->GetInteger("CurrentAvgOrPeakLuminance")) == nullptr ||
		(gen_auto_exposure_ = gen_params->GetInteger("CurrentAecExposure")) == nullptr)
	{
		printf("Unable to find all required camera parameters.\n");
		return false;
	}

	logPvError(gen_params->SetEnumValue("PixelFormat", "Mono8"));

	PvInt64 width, height;
	logPvError(gen_params->SetEnumValue("BinningVertical", "x1"));
	logPvError(gen_params->SetEnumValue("BinningHorizontal", "x1"));

	logPvError(gen_params->SetIntegerValue("OffsetX", 0));
	logPvError(gen_params->SetIntegerValue("OffsetY", 0));
	logPvError(gen_params->GetIntegerValue("WidthMax", width));
	logPvError(gen_params->GetIntegerValue("HeightMax", height));

	// ensure width and height are even numbers (drop the last pixel if odd)
	width &= -2;
	height &= -2;

	logPvError(gen_width_->SetValue(width));
	logPvError(gen_height_->SetValue(height));

	
	if(is_master_)//set the master camera to signal the slave camera 
	{
		printf("Setting master camera strobe.\n");
		logPvError( gen_params->SetEnumValue("IN1Selector", "None"));
		logPvError( gen_params->SetEnumValue("OUT1Selector", "VSync"));
		logPvError( gen_params->SetEnumValue("TriggerMode", "Off"));
	}
	else //set the slave camera to trigger using signals from the master camera 
	{		
		printf("Setting Slave camera trigger.\n");
		logPvError( gen_params->SetEnumValue("IN1Selector", "ExternalTrigger"));
		logPvError( gen_params->SetEnumValue("OUT1Selector", "None"));
		logPvError( gen_params->SetEnumValue("TriggerMode", "On"));
		logPvError( gen_params->SetEnumValue("TriggerType", "Fast"));
		logPvError( gen_params->SetEnumValue("TriggerSource", "External"));
	}

	printf("Setting exposure parameters.\n");

	// set the exposure/gain roi
	logPvError(gen_auto_offset_x_->SetValue(0));
	logPvError(gen_auto_offset_y_->SetValue(0));
	logPvError(gen_auto_width_->SetValue(width));
	logPvError(gen_auto_height_->SetValue(height));

	// set exposure/gain parameters
	logPvError(gen_auto_gain_max_->SetValue(600));
	logPvError(gen_auto_gain_speed_->SetValue("x2"));
	logPvError(gen_auto_exposure_max_->SetValue(100000));
	logPvError(gen_auto_exposure_speed_->SetValue("x2"));
	logPvError(gen_auto_luminance_level_->SetValue(1600));
	logPvError(gen_auto_luminance_type_->SetValue("Average")); // "Peak"

	// enable auto exposure and gain
	logPvError(gen_auto_exposure_enable_->SetValue(true));
	logPvError(gen_auto_gain_enable_->SetValue(true));

	// Register this class as an event sink for PvDevice callbacks
	device_.RegisterEventSink( this );

	// Save IP address (used when opening the stream object)
	PvInt64 ipAddress;
	logPvError( gen_params->GetIntegerValue("GevCurrentIPAddress", ipAddress));

	char ip[64];
	sprintf(ip,"%u.%u.%u.%u"
		, (ipAddress & 0xff000000)>>24
		, (ipAddress & 0xff0000)>>16
	    , (ipAddress & 0xff00)>>8
		, (ipAddress & 0xff));

	device_ip_ = PvString(ip);
	
    // enable link recovery 
    gen_link_recovery_->SetValue( true );
    
	return true;
}



// Opens stream and pipeline and allocates buffers
bool GevDevice::OpenStream()
{
	// Create, open the PvStream object
	printf( "Opening stream to device\n" );
	PvResult lResult = stream_.Open( device_ip_ );
	if ( !lResult.IsOK() )
	{
		printf( "Stream failed to open\n" );
		return false;
	}
    
    // Reading payload size from device
    PvInt64 size = 0;
    gen_payload_size_->GetValue( size );
	printf("Payload size of %I64u\n", size);

    // Create, init the PvPipeline object
    pipeline_.SetBufferSize( static_cast<PvUInt32>( size ) );
	pipeline_.SetBufferCount( 12 ); // was 16 

	// The pipeline needs to be "armed", or started before we instruct the device to send us images
	printf( "Starting pipeline\n" );
	lResult = pipeline_.Start();
	if ( !lResult.IsOK() )
	{
		printf( "Pipeline failed to start\n" );
		return false;
	}
	

	// Get stream parameters/stats
	PvGenParameterArray *stream_params = stream_.GetParameters();	
	PvUInt32 ct = stream_params->GetCount();
	
	gen_count_ = stream_params->GetInteger( "ImagesCount" );
	gen_framerate_ = stream_params->GetFloat( "AcquisitionRateAverage" );
	gen_bandwidth_ = stream_params->GetFloat( "BandwidthAverage" );

	stream_params->SetBooleanValue("IgnoreMissingPackets", true);

	return true;
}



// Closes the stream and pipeline
void GevDevice::CloseStream()
{
	gen_count_ = NULL;
	gen_framerate_ = NULL;
	gen_bandwidth_ = NULL;

	if ( pipeline_.IsStarted() )
	{
		printf( "Stopping pipeline\n" );		
		if(!pipeline_.Stop().IsOK())
		{
			printf( "Unable to stop the pipeline\n" );
		}
	}

	if ( stream_.IsOpen() )
	{
		printf( "Closing stream\n" );
		if(!stream_.Close().IsOK())
		{
			printf( "Unable to close stream\n" );
		}
	}
}



// Starts image acquisition
bool GevDevice::StartAcquisition()
{
	logPvError(gen_transport_layer_params_locked_->SetValue( 0 ));
	logPvError(gen_aquisition_mode_->SetValue("Continuous"));

	// set the Device IP destination to the Stream
	if (!logPvError(device_.SetStreamDestination(stream_.GetLocalIPAddress(), stream_.GetLocalPort())))
	{
		printf("Setting stream destination failed\n");
		return false;
	}
	
	// signal readiness
	{  // scope for lock
		std::unique_lock<std::mutex> lock(mutex_);
		ready_ = true;
		condition_.notify_one();
	}

	// wait for other thread
	{  // scope for lock
		std::unique_lock<std::mutex> lock(other_->mutex_);
		while (!other_->ready_) { other_->condition_.wait(lock); }
	}

	logPvError(gen_command_reset_timestamp_->Execute());
	logPvError(gen_transport_layer_params_locked_->SetValue( 1 ));
    logPvError(gen_command_start_->Execute());
	
	return true;
}


//
// Stops acquisition
//
bool GevDevice::StopAcquisition()
{
	ready_ = false;

	// Tell the device to stop sending images
	PvResult lResult = gen_command_stop_->Execute();
	if ( !lResult.IsOK() )
	{
		printf( "Unable to stop acquisition\n" );
		// Ignore failure, may be called when device is lost
	}

	// If present reset TLParamsLocked to 0. Must be done AFTER the 
	// streaming has been stopped
	if ( gen_transport_layer_params_locked_ != NULL )
	{
		printf( "Resetting TLParamsLocked to 0\n" );
		gen_transport_layer_params_locked_->SetValue( 0 );
		// Ignore failure, may be called when device is lost
	}

	// Reset streaming destination (optional...)
	lResult = device_.ResetStreamDestination();
	if ( !lResult.IsOK() )
	{
		printf( "Unable to reset stream destination\n" );
		// Ignore failure, may be called when device is lost
	}

	// Important: abort all currently queued buffer, otherwise timeouts
	// are going to be reported before we can successfully resume streaming
	// if this method is called in the context of a recovery attempt
	stream_.AbortQueuedBuffers();
	stream_.FlushPacketQueue();

	return true;
}



//

// Acquisition loop
//
void GevDevice::AcquisitionLoop()
{
	int frame_index = 0; 
	bool first_timeout = true;
	PvInt64 gain, exposure, luminance, width, height;
	double framerate = 0.0;
	double bandwidth = 0.0;	
	char char_buf[512];

	std::string winname = is_master_ ? "Master" : "Slave";
	char cam = is_master_ ? 'M' : 'S';

	MapFile::FrameInfo frameInfo;

	// fill out the non changing fields of cameraInfo
	memcpy(frameInfo.cameraID, camera_id_, std::min(sizeof(frameInfo.cameraID), sizeof(camera_id_)));
	frameInfo.IsMaster = is_master_;
	frameInfo.bayerPattern = -1;	

	gen_width_->GetValue(width);
	gen_height_->GetValue(height);

	cv::Mat image(height >> 4, width >> 4, CV_8UC1);


	
	// get the handle to the console (winapi)
	//HANDLE hconsole = GetStdHandle(STD_OUTPUT_HANDLE);
	
	//GetConsoleScreenBufferInfo(hconsole, &console_info); // get info including current cursor position
	//COORD cursor_origin = console_info.


	while ( !quit_)
	{
		// Do all recovery tasks requested through message queue
		HandleMsgQueue();
        if ( quit_ )
        {
            printf( "Quit flag set, terminating application\n" );
            break;
        }
		if ( stream_.IsOpen() && pipeline_.IsStarted() )
		{
			// Retrieve next buffer		
			PvBuffer *buffer = NULL;
            PvResult  operation_result;
			PvResult result = pipeline_.RetrieveNextBuffer( &buffer, 1000, &operation_result );

			{ // fill out the timestamp field in cameraInfo
				using namespace std::chrono;
				system_clock::duration system_time = system_clock::now().time_since_epoch();
				seconds secs = duration_cast<seconds>(system_time);
				nanoseconds nsecs = system_time - secs;

				frameInfo.systemTime.sec = secs.count();
				frameInfo.systemTime.nsec = nsecs.count();
			}

			
			if ( result.IsOK() ) //has not timed out
			{
	    		gen_framerate_->GetValue( framerate );
		    	gen_bandwidth_->GetValue( bandwidth );
				gen_auto_gain_->GetValue( gain );
				gen_auto_exposure_->GetValue( exposure );
				gen_auto_luminance_->GetValue( luminance );
				

                if (operation_result.IsOK()) //has succeeded
                {
	    			first_timeout = true;
                }				

		
				// If the buffer contains an image, pass it to  the frame handler
				if (buffer->GetPayloadType() == PvPayloadTypeImage)
				{
					PvImage *gevImg = buffer->GetImage();
					Img::ImageRef bayer(
						gevImg->GetDataPointer(),
						gevImg->GetWidth(),
						gevImg->GetHeight(),
						gevImg->GetWidth(),
						gevImg->GetBitsPerPixel());

					frameInfo.exposure = uint32_t(exposure);
					frameInfo.frameStatus = buffer->GetLostPacketCount();
					frameInfo.cameraTick = buffer->GetTimestamp();
					frameInfo.frameNumber = frame_index++;

					// We have an image - do some processing 
					// frameHandler will return null until it has enough frame history (handled in the Add method)	
					stereo_handler_->Add(bayer, frameInfo);// , std::move(frameHandler.HandleFrame(bayer, frameInfo)) );
					
					// rendering code
					int half_width = image.cols >> 1;

					// copy image from camera to the display image
					cv::resize(cv::Mat(height, width, CV_8UC1, bayer.data), image, image.size(), 0, 0, cv::INTER_NEAREST);

					// write some text on the image
					cv::Point origin(16, 16);
					sprintf(char_buf, "counter: %d", frameInfo.frameNumber);
					origin.y += RenderText(image, origin, char_buf, 1.0).height;
					sprintf(char_buf, "framerate: %0.3f", framerate);
					origin.y += RenderText(image, origin, char_buf, 1.0).height;
					sprintf(char_buf, "bandwidth: %0.0f", bandwidth);
					origin.y += RenderText(image, origin, char_buf, 1.0).height;
					sprintf(char_buf, "exposure: %d", frameInfo.exposure);
					origin.y += RenderText(image, origin, char_buf, 1.0).height;
					sprintf(char_buf, "luminance: %d", uint32_t(luminance));
					origin.y += RenderText(image, origin, char_buf, 1.0).height;
					sprintf(char_buf, "gain: %d", uint32_t(gain));
					origin.y += RenderText(image, origin, char_buf, 1.0).height;

					// display
					cv::imshow(winname, image);
					cv::waitKey(1);

				}
				// VERY IMPORTANT, release the buffer back to the pipeline
				pipeline_.ReleaseBuffer(buffer);

            }
			else
			{
				if ( first_timeout )
				{
					printf(is_master_ ? "Master" : "Slave");
					printf( " camera image timeout\n" );
					first_timeout = false;
				}	
			}
	
		}
		else //!pipeline_.RetrieveNextBuffer().IsOK()
		{
			// No stream/pipeline, must be in recovery. Wait a bit...
			printf("%c recovery\n", is_master_?"M":"S");
			::Sleep( 100 );
		}


	}

	printf( "\n" );
}


//
// Wrap-up: closes, disconnects, etc.
//
void GevDevice::WrapUp()
{
	printf( "GevDevice::WrapUp()\n" );

	CloseStream();

	gen_transport_layer_params_locked_ = NULL;
	gen_command_start_ = NULL;
	gen_command_stop_ = NULL;
    gen_command_reset_timestamp_ = NULL;

	if ( device_.IsConnected() )
	{
		// Unregister event sink (callbacks)
		device_.UnregisterEventSink( this );

		printf( "Disconnecting device\n" );
		device_.Disconnect();
	}
	printf( "Device Disconnected\n" );
}



//
// Processes the message queue.
//
// A message queue is used to have the PvDevice recovery callbacks
// perform operations in the context of the main application thread.
//
void GevDevice::HandleMsgQueue()
{
	std::lock_guard<std::mutex> lock(mutex_);

		while ( message_queue_.size() > 0 )
		{
			int lMsg = message_queue_.front();
			message_queue_.pop_front();

			switch ( lMsg )
			{
			case MSG_RESTOREACQ:

				printf( "** Main thread processing MSG_RESTARTACQ\n" );

				OpenStream();
				StartAcquisition();

				break;

			case MSG_STOPACQ:

				printf( "** Main thread processing MSG_STOPACQ\n" );
				StopAcquisition();
				CloseStream();

				break;

            case MSG_QUIT:

				printf( "** Main thread processing MSG_QUIT\n" );

                quit_ = true;
                        
                break;
			default:
				printf("** unknown message\n");
				break;

			}
		}
}


//
// PvDeviceEventSink callback
//
// Notification that the device just got disconnected.
//
void GevDevice::OnLinkDisconnected( PvDevice *aDevice )
{
	printf( "=====> Recovery callback: OnLinkDisconnected\n" );

    bool lEnabled = false;
    gen_link_recovery_->GetValue( lEnabled );
    if ( lEnabled )
    {
        // We lost the device, stop acquisition
		std::lock_guard<std::mutex> lock(mutex_);
	    message_queue_.push_back( MSG_STOPACQ );
    }
    else
    {
        // If recovery is not enabled, just signal to terminate the application
		std::lock_guard<std::mutex> lock(mutex_);
	    message_queue_.push_back( MSG_QUIT );
    }
}


//
// PvDeviceEventSink callback
//
// Notification that the device just got reconnected. Restart acquisition...
//
void GevDevice::OnLinkReconnected( PvDevice *aDevice )
{
	printf( "=====> Recovery callback: OnLinkReconnected\n" );
	std::lock_guard<std::mutex> lock(mutex_);
	message_queue_.push_back( MSG_RESTOREACQ );
}





