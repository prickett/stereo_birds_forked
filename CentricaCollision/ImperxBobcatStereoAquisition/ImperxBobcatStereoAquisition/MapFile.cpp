#include "StdAfx.h"
#include "MapFile.h"
#include <algorithm>
//#include <chrono>


MapFile::MapFile(const Img::ImageRef& bayer, const Img::ImageRef& mask, unsigned char *map, int order, const FrameInfo& camera)
{
	int mapSize = Img::MapSize(bayer,order);
	int outBufSize = Img::MappedBufferSize(bayer,order,map);
	int maskBufSize = Img::MappedBufferSize(mask,order-1,map);

	this->size = sizeof(Header) + mapSize + outBufSize + maskBufSize; 
	this->buffer = new unsigned char[this->size]; //we maintain one buffer for everything
	this->header = (Header*)this->buffer;  //set header to point to the start of the buffer
	this->isMaster = camera.IsMaster;

	header->magicNumber = 0xC0FFEE;
	header->width = bayer.width;
	header->height = bayer.height;
	header->bitsPerPixel = (short)bayer.bpp;
	header->bayerPattern = camera.bayerPattern;

	header->mapWidth = Img::MapWidth(bayer,order);
	header->mapHeight = Img::MapHeight(bayer,order);
	header->mapUnit = 1<<order;

	header->mapOffset = sizeof(Header);//!!!!!!!!!!!! realign
	header->dataOffset = header->mapOffset + mapSize;//!!!!!!!!!! realign
	header->maskOffset = header->dataOffset + outBufSize;

	memcpy(header->cameraID, camera.cameraID, std::min(sizeof(header->cameraID), sizeof(camera.cameraID)));
	header->exposure = camera.exposure;
	header->frameStatus = camera.frameStatus;
	header->cameraTick = camera.cameraTick;
	header->systemTime = camera.systemTime;
	header->FrameCount = camera.frameNumber;

	std::copy(map, map + mapSize, buffer + header->mapOffset);
	Img::MapToBuffer(bayer, order, map, buffer + header->dataOffset);
	Img::MapToBuffer(mask, order-1, map, buffer + header->maskOffset);
}

MapFile::MapFile(const MapFile &other)
	: size(other.size)
	, buffer(new unsigned char[other.size])
	, isMaster(other.isMaster)
{
	printf("MapFile copy constructor!");
	header = (Header*)buffer;
	std::copy( other.buffer, other.buffer + other.size, this->buffer );
}	


MapFile::~MapFile(void)
{
	header = NULL;
	delete[] this->buffer;
}

MapFile::Header * MapFile::GetHeader()
{
	return this->header;
}

bool MapFile::IsMaster()
{
	return this->isMaster != 0;
}

bool MapFile::Save(void)
{
	char path[64];
	
	sprintf(path,"C:/Capture/%s_%u_%06u.map",
		isMaster ? "M" : "S",
		header->systemTime,
		header->FrameCount);

	return MapFile::Save(path);
}

bool MapFile::Save(const char * path)
{
	FILE *file = fopen(path,"w+b");
	bool success = false;	
	if (file != 0)
	{
		success = MapFile::Save(file);
		fclose(file);
	}
	return success;
}

bool MapFile::Save(FILE * file)
{
	return fwrite(buffer,1,size,file) == size;
}
