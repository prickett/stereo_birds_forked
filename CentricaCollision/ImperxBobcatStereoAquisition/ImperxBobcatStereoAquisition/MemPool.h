#pragma once

///   Development code  
#include <vector>
#include <list>

template <typename T>
class Pool;

template <typename T>
struct uninitialized
{
	uninitialized() { }
	T value;
};

template <typename T>
struct Pool_
{
	std::vector< std::vector<uninitialized<T> > > pool;
	std::list< int > free;
};

template <typename T>
class Block
{
public:
	T* data(void){ return data_; };
	Block(void) :Block(nullptr, 0){}
private:
	Block(void)
		:data_(data), index_(index){}
	T* data_;
	int index_;
	static Pool_<T> pool_;
};


template <typename T>
class MemoryPool
{
public:
	Pool() :Pool(0, 0){}
	Pool(int block_size, int block_count)
		: pool_(block_count)
	{
		for (int i = 0; i < block_count; ++i)
		{
			pool_[i] = std::vector< uninitialized<T> >(block_size);
			free_.push_back(i);
		}
	}

	Block<T> Acquire(void)
	{
		Block<T> block(nullptr, 0);
		if (!free_.empty())
		{
			int i = free_.front();
			free_.pop_front();
			block.index_ = i;
			block.data_ = &pool_[i][0].value;
		}
		return block;
	}

	void Release(Block<T>& block)
	{
		if (block.data_)
		{
			free_.push_back(block.index_);
			block.data_ = nullptr;
		}
	}

	int BlockSize()
	{
		return pool_.empty() ? 0 : (int)pool_.front().size();
	}

	int BlockCount()
	{
		return pool_.size();
	}

	int AvailableCount()
	{
		return free_.size();
	}

private:

	std::vector< std::vector<uninitialized<T> > > pool_;
	std::list< int > free_;
	friend class Block<T>;
};
