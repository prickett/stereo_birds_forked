
#ifndef _TIMEDTOKENS_H_
#define _TIMEDTOKENS_H_


#include <list>
#include <string>
#include <chrono>

class TimedTokens
{
private:
	typedef std::chrono::high_resolution_clock clock;

	clock timer;
	clock::time_point start_time;
	std::list<std::pair<clock::time_point, std::string>> tokens;
public:

	TimedTokens(void)
		:start_time(timer.now())
	{}

	void Add(const char * token)
	{
		tokens.push_back({ timer.now(), std::string(token) });
	}

	void Display(void)
	{
		clock::time_point prev = start_time;
		for (std::pair<clock::time_point, std::string>& token : tokens)
		{
			clock::duration duration = token.first - prev;
			printf("%llu us\t%s\n",
				std::chrono::duration_cast<std::chrono::microseconds>(duration).count(),
				token.second.c_str());
			prev = token.first;
		}
	}
};

#endif // _TIMEDTOKENS_H_
