// ImperxBobcatStereoAquisition.cpp : Defines the entry point for the console application.
#include "stdafx.h"


#include "GevDevice.h"
#include "GevEnumerators.h"

#include <memory>
#include <thread>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

// Bobcat PoE-B6620C-TF00
// Bobcat_GEV_3_1_3_453 


void camera_thread_function(GevDevice* camera)
{
	try
	{ 
		camera->Run();
	}
	catch (std::exception const& e)
	{
		printf(e.what());
	}
}


int main(int argc, char* argv[])
{
	// show the command arguments
	for (int i = 1; i < argc; ++i)
		printf("%3d %s\n", i, argv[i]);
	
	const char* save_path = argc > 1 ? argv[1] : "D:\\Capture\\";
	const char* master_identifier = argc > 2 ? argv[2] : "Master";
	const char* slave_identifier = argc > 3 ? argv[3] : "Slave";
	
	chrono::system_clock::duration start_time = chrono::system_clock::now().time_since_epoch();
	 
	stringstream ss;
	ss << "StereoPairAcquisition_" << chrono::duration_cast<chrono::seconds>(start_time).count() << ".log";

	string log_file_name = ss.str();
		

	try
	{
		GevDevice master_camera, slave_camera;	
		//std::unique_ptr<StereoHandler> save_buffer(new StereoHandler(save_path));
		StereoHandler save_buffer(save_path, "SPC", 540); 
	
		bool master_ok = master_camera.Connect(master_identifier, &save_buffer, &slave_camera, true);
		bool slave_ok = slave_camera.Connect(slave_identifier, &save_buffer, &master_camera, false);

		cout << "Redirecting output to " << log_file_name << endl;
		if (!freopen(log_file_name.c_str(), "w", stdout))
			cout << "Unable to redirect standard out" << endl;

		if (master_ok & slave_ok)
		{ 
			std::thread master_thread(camera_thread_function, &master_camera);
			std::thread slave_thread(camera_thread_function, &slave_camera);

			bool quit = false;
			while (!quit)
			{ 
				int key = _getch();
				switch (key)
				{
				case 'q':
				case 'Q':
					quit = true;
					break;
				default:
					printf("%c (%d) not assigned\n", key, key);
					break;
				}				
			}
			

			// signal threads to finish
			master_camera.Quit(); 
			slave_camera.Quit();

			master_thread.join();
			slave_thread.join();
		}
	}
	catch (std::exception e)
	{
		printf(e.what());
		return -1;
	}
	//cout.rdbuf(coutbuf); //reset to standard output again
	return 0;
}






