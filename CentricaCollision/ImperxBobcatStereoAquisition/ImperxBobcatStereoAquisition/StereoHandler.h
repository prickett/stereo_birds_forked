#pragma once

//#include <windows.h>

#include <thread>
#include <mutex>
#include <list>
#include <deque>
#include <vector>
#include <stdio.h>

#include "MapFile.h"
#include "FrameHandler.h"
#include "MemoryPool.h"


struct BayerMapPair
{
	BayerMapPair(void)
		:map_file(nullptr)
	{}


	BayerMapPair(BayerMapPair && other)
		: bayer(std::move(other.bayer))
		, bayer_data(std::move(other.bayer_data))
		, map_file(std::move(other.map_file))
	{}

	BayerMapPair& operator=(BayerMapPair && other)
	{
		if (this != &other)
		{
			bayer = std::move(other.bayer);
			bayer_data = std::move(other.bayer_data);
			map_file = std::move(other.map_file);
		}
		return *this;
	}


	Img::ImageRef bayer;
	std::unique_ptr<MapFile> map_file;
	Block<uint8_t> bayer_data;
private:
	BayerMapPair(BayerMapPair const &);
	BayerMapPair& operator=(BayerMapPair const &);
};

class StereoHandler
{
public:
	StereoHandler(const char * save_folder, const char * save_prefix = 0, int frames_per_file = 540); 
	~StereoHandler(void);
	void Add(const Img::ImageRef& bayer, const MapFile::FrameInfo& frame_info);//  std::unique_ptr<MapFile>&& map_file);

private:
	enum 
	{ 
		MAGIC_NUMBER = 0xF00D5AC, // Chunk file identifier bytes
		SECTOR_ALIGNMENT = 0x200, // (512 bytes) all frames are aligned to sector sizes in an attempt improve disk access speed
		MAX_FILESIZE = 0x79ffffff, // ~ 100 meg short of 2 gig (if this limit is increased then the file handling code will need adjusting)
		DROP_THRESHOLD =  10000, // Difference error to trigger frame pop (typical difference error ~3)
		POOL_SIZE = 12, // the number controls the number of images that can be remembered at any one time
	};

	struct Header
	{
		uint32_t magic_number;
		uint32_t start_time;
		uint32_t end_time;
		uint32_t first_frame;
		uint32_t last_frame;
		uint32_t master_fullframe_offset;
		uint32_t master_fullframe_size;
		uint32_t slave_fullframe_offset;
		uint32_t slave_fullframe_size;
		uint32_t count; // number of frames in the file
	} header_;

	StereoHandler(const StereoHandler &); //not implemented (not copyable)
	void pairAndSave(void);
	bool handle( const Img::ImageRef& bayer, const MapFile::FrameInfo& frame_info, FrameHandler& handler, std::list<BayerMapPair>& frames);

	bool savePairedFrame(BayerMapPair& master_frame, BayerMapPair& slave_frame);
	bool saveDroppedFrame(MapFile* frame);
	void purgeFrames(std::deque<std::unique_ptr<MapFile>>& frames);
	void purgeFrames(std::list<BayerMapPair>& frames);

	void flush(void); //finish writing the current file
		
	std::string save_prefix_;
	std::string save_path_;
	uint32_t frames_per_file_;	
	uint32_t frame_count_;
	uint32_t file_count_;
	uint64_t master_tic_;
	uint64_t slave_tic_;

	FILE *file_;
	std::mutex access_mutex_;
	std::vector<uint32_t> offsets_;
	
	Pool<uint8_t> pool_;

	std::list<BayerMapPair> master_list_;
	std::list<BayerMapPair> slave_list_;

	FrameHandler master_handler_;
	FrameHandler slave_handler_;
	
	static const std::string temp_suffix_;
	static const std::string suffix_;
	static uint32_t sectorAlign(uint32_t pos);
	
};

