#include "StdAfx.h"
#include "GEVDeviceFinder.h"




//
// To find GEV Devices on a network
//

int GEVDeviceFinding()
{
	PvResult lResult;	
	PvDeviceInfo *lDeviceInfo = 0;

	// Create an GEV system and an interface.
	PvSystem lSystem;

	// Find all GEV Devices on the network.
	lSystem.SetDetectionTimeout( 2000 );
	lResult = lSystem.Find();
	if( !lResult.IsOK() )
	{
		printf( "PvSystem::Find Error: %s", lResult.GetCodeString().GetAscii() );
		return -1;
	}

	// Get the number of GEV Interfaces that were found using GetInterfaceCount.
	PvUInt32 lInterfaceCount = lSystem.GetInterfaceCount();

	// Display information about all found interface
	// For each interface, display information about all devices.
	for( PvUInt32 x = 0; x < lInterfaceCount; x++ )
	{
		// get pointer to each of interface
		PvInterface * lInterface = lSystem.GetInterface( x );

		printf( "Interface %i\nMAC Address: %s\nIP Address: %s\nSubnet Mask: %s\n\n",
			x,
			lInterface->GetMACAddress().GetAscii(),
			lInterface->GetIPAddress().GetAscii(),
			lInterface->GetSubnetMask().GetAscii() );

		// Get the number of GEV devices that were found using GetDeviceCount.
		PvUInt32 lDeviceCount = lInterface->GetDeviceCount();

		for( PvUInt32 y = 0; y < lDeviceCount ; y++ )
		{
			lDeviceInfo = lInterface->GetDeviceInfo( y );
			printf( "Device %i\nMAC Address: %s\nIP Address: %s\nSerial number: %s\n\n",
				y,
				lDeviceInfo->GetMACAddress().GetAscii(),
				lDeviceInfo->GetIPAddress().GetAscii(),
				lDeviceInfo->GetSerialNumber().GetAscii() );
		}
	}

	// Connect to the last GEV Device found.
	if( lDeviceInfo != NULL )
	{
		printf( "Connecting to %s\n",
			lDeviceInfo->GetMACAddress().GetAscii() );

		PvDevice lDevice;
		lResult = lDevice.Connect( lDeviceInfo );
		if ( !lResult.IsOK() )
		{
			printf( "Unable to connect to %s\n", 
				lDeviceInfo->GetMACAddress().GetAscii() );
		}
		else
		{
			printf( "Successfully connected to %s\n", 
				lDeviceInfo->GetMACAddress().GetAscii() );
		}
	}
	else
	{
		printf( "No device found\n" );
	}

	return 0;
}
