#pragma once
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>


class winThread
{
public:
	winThread(LPTHREAD_START_ROUTINE func, LPVOID param)
		:mHandle(CreateThread(0,0,func,param,0,0)) {} 
	~winThread(void){ CloseHandle(mHandle); }

	DWORD Suspend() { return SuspendThread(mHandle); }
	DWORD Resume() { return ResumeThread(mHandle); }
	DWORD Terminate(void) { return TerminateThread(mHandle, 0); }

private:
	HANDLE mHandle;
	winThread(const winThread &);
};

