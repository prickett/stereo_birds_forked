#pragma once
#include <memory>
#include <cstdint>
//#include <cinttypes>
#include "Img.h" 

//#include <OaIdl.h>
//#include "FrameHandler.h"


class MapFile
{
public:
	struct Timestamp
	{
		uint32_t sec;
		uint32_t nsec;
	};
	//struct Header
	//{
	//	unsigned long magicNumber;
	//	unsigned long width;
	//	unsigned long height;
	//	unsigned short bitsPerPixel;
	//	unsigned short bayerPattern;

	//	unsigned short mapWidth;
	//	unsigned short mapHeight;
	//	unsigned long mapUnit;
	//	unsigned long mapOffset;
	//	unsigned long dataOffset;
	//	unsigned long maskOffset;

	//	unsigned long cameraID;
	//	unsigned long exposure;
	//	unsigned long frameStatus;
	//	unsigned long long cameraTick;
	//	//unsigned long systemTick;


	//	__time64_t systemTime; //64bit 
	//	unsigned long FrameCount;
	//};
	struct MapTileSequence
	{
		uint32_t Scale;
		uint32_t Width;
		uint32_t Height;
		uint32_t Offset; //relative to start of header???
		uint32_t Size;
	};

	struct Header
	{
		uint32_t magicNumber;
		uint32_t width;  // *** was commented out, not sure why
		uint32_t height; // *** was commented out, not sure why
		uint16_t bitsPerPixel; //<-- how does this work then?? should this be part of MapTileSequence??
		uint16_t bayerPattern;

		int8_t cameraID[16];
		uint32_t exposure;
		uint32_t frameStatus;
		uint64_t cameraTick;
		Timestamp systemTime; //64bit 
		uint32_t FrameCount; // at 32 bits and ~4fps the counter will overflow after 34 years of continuous usage

		uint16_t mapWidth;
		uint16_t mapHeight;
		uint32_t mapUnit; // *** was commented out, not sure why
		//unsigned long mapSize; //!!		? <-- aways 1bpp  and size is always ((w + 7)& -8) * h <-- too strange?? width always rounded to nearest byte... bitmaps use DWord -> 4 bytes
		uint32_t mapStride; // better than storing Size, which can be calculated as mapHeight * mapStride
		uint32_t mapOffset; //relative to start of header
		uint32_t count; //number of map tile sequences following

		//MapTileSequence bayer;
		//MapTileSequence mask;

		//unsigned long dataWidth; //!!	?
		//unsigned long dataHeight; //!!	?
		//unsigned long dataScale; //!!	??
		uint32_t dataOffset;  // *** was commented out, not sure why
		//unsigned long dataSize; //!!	? currently calculated

		//unsigned long maskWidth; //!!	?
		//unsigned long maskHeight; //!!	?
		//unsigned long maskScale; //!!	??
		uint32_t maskOffset; // *** was commented out, not sure why
		//unsigned long maskSize; //!!	? currently calculated
	};

	//SAFEARRAY x;


	struct FrameInfo
	{		
		int8_t cameraID[16];
		uint32_t exposure;
		uint32_t frameStatus;
		uint64_t cameraTick;
		Timestamp systemTime;
		uint32_t frameNumber;
		uint16_t bayerPattern;
		uint8_t IsMaster;
	};

	//MapFile(Img::ImageRef *bayer, FrameHandler *frameHandler, FrameInfo *camera);
	MapFile(const Img::ImageRef& bayer, const Img::ImageRef& mask, unsigned char *map, int order, const FrameInfo& camera);
	MapFile(const MapFile& other);  
	~MapFile(void);

	Header* GetHeader(void);
	bool IsMaster(void);
	bool Save(void);
	bool Save(FILE * file);
	bool Save(const char * path);

	


private:
	int size;
	unsigned char * buffer;
	int8_t isMaster;
	Header* header;
};

