#pragma once


#include <vector>
#include <list>

template <typename T>
class Pool;

template <typename T>
struct uninitialized
{
	uninitialized() { }
	T value;
};


template <typename T>
class Block
{
public:
	T* data(void){ return data_; };
	Block(void) :data_(0), index_(0){}//:Block(nullptr, 0){}
private:
	Block(T* data, int index)
		:data_(data), index_(index){}
	T* data_;
	int index_;
	friend Pool<T>;
};


template <typename T>
class Pool
{
public:
	Pool(void) 
		: pool_(0){}
	Pool(int block_size, int block_count)
		: pool_(block_count)
	{
		for (int i = 0; i < block_count; ++i)
		{
			pool_[i] = std::vector< uninitialized<T> >(block_size);
			free_.push_back(i);
		}
	}

	Block<T> Acquire(void)
	{
		Block<T> block(nullptr, 0);
		if (!free_.empty())
		{
			int i = free_.front();
			free_.pop_front();
			block.index_ = i;
			block.data_ = &pool_[i][0].value;			
		}
		return block;
	}

	void Release(Block<T>& block)
	{
		if (block.data_)
		{
			free_.push_back(block.index_);
			block.data_ = nullptr;
		}
	}

	int BlockSize()
	{
		return pool_.empty() ? 0 :(int) pool_.front().size();
	}

	int BlockCount()
	{
		return pool_.size();
	}

	int AvailableCount()
	{
		return int(free_.size());
	}

private:

	std::vector< std::vector<uninitialized<T> > > pool_;
	std::list< int > free_;
	friend class Block<T>;
};


