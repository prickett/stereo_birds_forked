#pragma once
// helper classes to ease camera finding without using the GUI. slp.
// enumerates interfaces and devices

#include <conio.h>
#include <process.h>


#include <stdio.h>
#include <list>
#include <PvDeviceFinderWnd.h>
#include <PvSystem.h> 
#include <PvDevice.h>

template <class T>
class BaseEnumerator abstract
{
public:
	BaseEnumerator(void);

	void Reset();
	virtual bool MoveNext();
	virtual T * Current() = 0 {};

protected:
	PvUInt32 count;
	PvUInt32 index;
};


class GevDeviceEnumerator :public BaseEnumerator<PvDeviceInfo>
{
public:
	GevDeviceEnumerator(PvInterface * pInterface);
	PvDeviceInfo * Current();

private:
	PvInterface * pInterface;	
};


class GevInterfaceEnumerator :public BaseEnumerator<PvInterface>
{
public:
	GevInterfaceEnumerator(void);
	PvInterface * Current();

private:
	PvSystem pSystem;
};


class GevDeviceFinder
{
public:
	void FindAll(std::list<PvDeviceInfo*> &devicesOut);
	PvDeviceInfo * GUIFind();
	PvDeviceInfo * FindBySerial(const char * serial);
protected:
	GevInterfaceEnumerator interfaces;
	PvDeviceFinderWnd finderWin;
};




