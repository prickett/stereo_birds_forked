// PostProcess.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "..\ImperxBobcatStereoAquisition\Img.h"
#include "..\ImperxBobcatStereoAquisition\FrameHandler.h"
#include <conio.h>


void saveBitmapX(Img::ImageRef *image, char* filename)
{
	FILE *file = fopen(filename,"w+b");
	if (file != NULL)
	{
		Img::Bitmap::SaveAsGreyscale(*image,file);
		fclose(file);
	}
}


void doSomeStuff(int count, char* filenames[],bool isMaster)
{
	Img::ImageRef bayer;	
	Img::Bitmap::Load(filenames[0], bayer); //as bayer is currently null Bitmap::Load fills the header
	bayer.data = new unsigned char[bayer.size()];

	FrameHandler frameHandler;// (bayer.width, bayer.height);
	MapFile::FrameInfo cameraInfo;
				
	for(int i=0;i<count;i++)
	{
		printf("loading %s\n",filenames[i]);
		Img::Bitmap::Load(filenames[i],bayer);//get the next image

		cameraInfo.IsMaster = isMaster; 
		memset(cameraInfo.cameraID,0,sizeof(cameraInfo.cameraID));
		cameraInfo.exposure = 0;
		cameraInfo.frameStatus = 0;
		cameraInfo.cameraTick = i;
		cameraInfo.frameNumber = i;
	
		frameHandler.HandleFrame(bayer,cameraInfo);
	}
	
	delete [] bayer.data;
}






void doSomeStuffXX(int count, char* filenames[])
{
	//prepare some local buffers based on the dimensions of the first image
	using namespace Img;
	ImageRef bayer;	
	Bitmap::Load(filenames[0], bayer); //as bayer is currently null Bitmap::Load fills the header
	bayer.data = new unsigned char[bayer.size()];

	ImageRef target, blur, previous;
	ImageRef current(NULL,bayer.width>>1,bayer.height>>1,bayer.stride>>1,bayer.bpp);

	target = blur = previous = current;
	int sz = current.size();
	int simdSz = sz>>4;

	current.data = new unsigned char[sz];
	previous.data = new unsigned char[sz];	
	blur.data = new unsigned char[sz];
	target.data = new unsigned char[sz];
	unsigned char *swp;

	//map variables
	int order = 3;
	int mapsize = Img::MapSize(target, order);
	unsigned char *map = new unsigned char[mapsize];
	char path[64];

	//tPvFrame frame;
	//frame.ImageBuffer = bayer.image;
	//frame.Width= bayer.width;
	//frame.Height=bayer.height;
	//frame.BitDepth = bayer.bpp;

	////start the process
	//if(!Bitmap::Load(filenames[0],bayer))//get the data
	//	printf("error loading bitmap");
	//Img::ExtractChannel(&bayer, &previous, CHANNELOFINTEREST);
	//subBlur(&previous,&blur,5);

	Bitmap::Load(filenames[1],bayer);//get the data
	Img::ExtractChannel(bayer,current,1);
	//subBlur(&current,&blur,5);

	Img::AbsoluteDifference(
		(__m128i*) current.data, 
		(__m128i*) previous.data,
		(__m128i*) target.data,
		simdSz);				

	for(int i=2;i<count;i++)
	{
		swp = current.data;
		current.data = previous.data;
		previous.data = swp;

		Bitmap::Load(filenames[i],bayer);//get the next image
		Img::ExtractChannel(bayer, current, 1);//CHANNELOFINTEREST);

		sprintf(path,"C:\\Capture\\Tgt\\src%.5u.bmp",i-1);
		saveBitmapX(&current,path);

		//subBlur(&current,&blur,5);

		Img::TernaryDifference(
			(__m128i*) current.data,
			(__m128i*) previous.data,
			(__m128i*) target.data,
			(__m128i*) target.data,
			simdSz);

		//memcpy(blur.image , current.image, sz);
		//exagerateImage(&blur,5);
		//sprintf(maskPath,"C:\\Capture\\Tgt\\blur%.5u.bmp",i-1);
		//saveBitmapX(&blur,maskPath);

		memcpy(blur.data, target.data, sz);
		//exagerateImage(&blur,5);
		sprintf(path,"C:\\Capture\\Tgt\\mask%.5u.bmp",i-1);
		saveBitmapX(&blur,path);


		Img::MapThresholds(target,order,map,4);//4==16, 5 = 32, 6 = 64...

		//SaveFrameDebugging(&bayer,&target,map,order+1);
	}
	

	delete [] map;
	delete[] current.data;
	delete[] previous.data;
	delete[] blur.data;
	delete[] target.data;
	delete[] bayer.data;

}

int main(int argc, char* argv[])
{
	for(int i=1;i<argc;i++)
	{
		printf("%s\n",argv[i]);
	}
	while ( !_kbhit() );
	return 0;

	if(argc>4)
	{		
		doSomeStuff(argc-1,argv+1,true);
	}
 	return 0;
}


