#pragma once
#include <cstdint>
#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>
#include "StereoBirdsUtility.h"

void mosaic(const cv::Mat& src, cv::Mat& tgt, BayerPattern pattern);
bool demosaic(const cv::Mat& src, cv::Mat& tgt, BayerPattern pattern);
void demosaic_BGGR(const cv::Mat& src, cv::Mat& tgt);
void demosaic_GBRG(const cv::Mat& src, cv::Mat& tgt);
void demosaic_GRBG(const cv::Mat& src, cv::Mat& tgt);
void demosaic_RGGB(const cv::Mat& src, cv::Mat& tgt);

// interpolate between two values
inline uchar interp(const uchar& a, const uchar& b)
{
	return ((uint32_t)a + b) / 2u;
}

// interpolate between three values
inline uchar interp(const uchar& a, const uchar& b, const uchar& c)
{
	return ((uint32_t)a + b + c) / 3u;
}

// interpolate between the two values with the lowest gradiant
inline uchar interp_lowest_gradiant(const uchar& a, const uchar& b, const uchar& c, const uchar& d)
{
	int ab = (int)a - b;
	int cd = (int)c - d;
	return ab*ab<cd*cd ? interp(a, b) : interp(c, d);
}

inline uchar interp_NSEW(const uchar* src, int stride)
{
	return interp_lowest_gradiant(src[-stride], src[stride], src[-1], src[1]);
}

inline uchar interp_NwSeNeSw(const uchar* src, int stride)
{
	return interp_lowest_gradiant(src[-stride - 1], src[stride + 1], src[-stride + 1], src[stride - 1]);
}

inline uchar interp_NS(const uchar* src, int stride)
{
	return interp(src[-stride], src[stride]);
}

inline uchar interp_EW(const uchar* src)
{
	return interp(src[-1], src[1]);
}

inline void interp_blue(const uchar* src, uchar* tgt, int stride)
{
	tgt[0] = src[0];
	tgt[1] = interp_NSEW(src, stride);
	tgt[2] = interp_NwSeNeSw(src, stride);
}
inline void interp_blue_N(const uchar* src, uchar* tgt, int stride)
{
	tgt[0] = src[0];
	tgt[1] = interp(src[-1], src[+1], src[stride]);
	tgt[2] = interp(src[stride - 1], src[stride + 1]);
}
inline void interp_blue_E(const uchar* src, uchar* tgt, int stride, int step = 1)
{
	tgt[0] = src[0];
	tgt[1] = interp(src[-stride], src[step], src[stride]);
	tgt[2] = interp(src[step - stride], src[step + stride]);
}
inline void interp_blue_S(const uchar* src, uchar* tgt, int stride)
{
	interp_blue_N(src, tgt, -stride);
}
inline void interp_blue_W(const uchar* src, uchar* tgt, int stride)
{
	interp_blue_E(src, tgt, stride, -1);
}
inline void interp_blue_corner(const uchar* src, uchar* tgt, int stride, int step)
{
	tgt[0] = src[0];
	tgt[1] = interp(src[step], src[stride]);
	tgt[2] = src[stride + step];
}



inline void interp_green1(const uchar* src, uchar* tgt, int stride)
{
	tgt[0] = interp_EW(src);
	tgt[1] = src[0];
	tgt[2] = interp_NS(src, stride);
}
inline void interp_green1_N(const uchar* src, uchar* tgt, int stride)
{
	tgt[0] = interp_EW(src);
	tgt[1] = src[0];
	tgt[2] = src[stride];
}
inline void interp_green1_E(const uchar* src, uchar* tgt, int stride, int step = 1)
{
	tgt[0] = src[step];
	tgt[1] = src[0];
	tgt[2] = interp_NS(src, stride);
}
inline void interp_green1_S(const uchar* src, uchar* tgt, int stride)
{
	interp_green1_N(src, tgt, -stride);
}
inline void interp_green1_W(const uchar* src, uchar* tgt, int stride)
{
	interp_green1_E(src, tgt, stride, -1);
}
inline void interp_green1_corner(const uchar* src, uchar* tgt, int stride, int step)
{
	tgt[0] = src[step];
	tgt[1] = src[0];
	tgt[2] = src[stride];
}

inline void interp_green2(const uchar* src, uchar* tgt, int stride)
{
	tgt[0] = interp_NS(src, stride);
	tgt[1] = src[0];
	tgt[2] = interp_EW(src);
}
inline void interp_green2_N(const uchar* src, uchar* tgt, int stride)
{
	tgt[0] = src[stride];
	tgt[1] = src[0];
	tgt[2] = interp_EW(src);
}
inline void interp_green2_E(const uchar* src, uchar* tgt, int stride, int step = 1)
{
	tgt[0] = interp_NS(src, stride);
	tgt[1] = src[0];
	tgt[2] = src[step];
}
inline void interp_green2_S(const uchar* src, uchar* tgt, int stride)
{
	interp_green2_N(src, tgt, -stride);
}
inline void interp_green2_W(const uchar* src, uchar* tgt, int stride)
{
	interp_green2_E(src, tgt, stride, -1);
}
inline void interp_green2_corner(const uchar* src, uchar* tgt, int stride, int step)
{
	tgt[0] = src[stride];
	tgt[1] = src[0];
	tgt[2] = src[step];
}


inline void interp_red(const uchar* src, uchar* tgt, int stride)
{
	tgt[0] = interp_NwSeNeSw(src, stride);
	tgt[1] = interp_NSEW(src, stride);
	tgt[2] = src[0];
}
inline void interp_red_N(const uchar* src, uchar* tgt, int stride)
{
	tgt[0] = interp(src[stride - 1], src[stride + 1]);
	tgt[1] = interp(src[-1], src[+1], src[stride]);
	tgt[2] = src[0];
}
inline void interp_red_E(const uchar* src, uchar* tgt, int stride, int step = 1)
{
	tgt[0] = interp(src[-stride + step], src[stride + step]);
	tgt[1] = interp(src[-stride], src[+step], src[stride]);
	tgt[2] = src[0];
}
inline void interp_red_S(const uchar* src, uchar* tgt, int stride)
{
	interp_red_N(src, tgt, -stride);
}
inline void interp_red_W(const uchar* src, uchar* tgt, int stride)
{
	interp_red_E(src, tgt, stride, -1);
}
inline void interp_red_corner(const uchar* src, uchar* tgt, int stride, int step)
{
	tgt[0] = src[stride + step];
	tgt[1] = interp(src[step], src[stride]);
	tgt[2] = src[0];
}