
#include "demosaic.h"


bool demosaic(const cv::Mat& src, cv::Mat& tgt, BayerPattern pattern)
{
	//assert(src.type() == CV_8UC1 && tgt.size() == src.size() && (tgt.type() == CV_8UC3 || tgt.type() == CV_8UC4));
	// target image is assumed to be in BGR order

	bool success = true;
	switch (pattern)
	{
	case BayerPattern::BGGR:
		demosaic_BGGR(src, tgt);
		break;
	case BayerPattern::GBRG:
		demosaic_GBRG(src, tgt);
		break;
	case BayerPattern::GRBG:
		demosaic_GRBG(src, tgt);
		break;
	case BayerPattern::RGGB:
		demosaic_RGGB(src, tgt);
		break;
	default:
		success = false;
	}
	return success;
}




void demosaic_BGGR(const cv::Mat& src, cv::Mat& tgt)
{
	int stride = src.step;
	int step = tgt.elemSize();
	int rows = src.rows - 2;
	int cols = src.cols - 2;
	int x, y;

	uchar* pt = tgt.ptr(0);
	const uchar* ps = src.ptr(0);
	interp_blue_corner(ps, pt, stride, 1);
	for (x = 1; x < cols; ++x)
	{
		interp_green1_N(ps + x, pt + x*step, stride);
		++x;
		interp_blue_N(ps + x, pt + x*step, stride);
	}
	interp_green1_corner(ps + x, pt + x*step, stride, -1);

	for (y = 1; y < rows; ++y)
	{
		pt = tgt.ptr(y);
		ps = src.ptr(y);
		interp_green2_E(ps, pt, stride);
		for (x = 1; x < cols; ++x)
		{
			interp_red(ps + x, pt + x*step, stride);
			++x;
			interp_green2(ps + x, pt + x*step, stride);
		}
		interp_red_W(ps + x, pt + x*step, stride);

		++y;

		pt = tgt.ptr(y);
		ps = src.ptr(y);
		interp_blue_E(ps, pt, stride);
		for (x = 1; x < cols; ++x)
		{
			interp_green1(ps + x, pt + x*step, stride);
			++x;
			interp_blue(ps + x, pt + x*step, stride);
		}
		interp_green1_W(ps + x, pt + x*step, stride);
	}

	pt = tgt.ptr(y);
	ps = src.ptr(y);
	interp_green2_corner(ps, pt, -stride, 1);
	for (x = 1; x < cols; ++x)
	{
		interp_red_S(ps + x, pt + x*step, stride);
		++x;
		interp_green2_S(ps + x, pt + x*step, stride);
	}
	interp_red_corner(ps + x, pt + x*step, -stride, -1);
}

void demosaic_GBRG(const cv::Mat& src, cv::Mat& tgt)
{
	int stride = src.step;
	int step = tgt.elemSize();
	int rows = src.rows - 2;
	int cols = src.cols - 2;
	int x, y;

	uchar* pt = tgt.ptr(0);
	const uchar* ps = src.ptr(0);
	interp_green1_corner(ps, pt, stride, 1);
	for (x = 1; x < cols; ++x)
	{
		interp_blue_N(ps + x, pt + x*step, stride);
		++x;
		interp_green1_N(ps + x, pt + x*step, stride);
	}
	interp_blue_corner(ps + x, pt + x*step, stride, -1);


	for (y = 1; y < rows; ++y)
	{
		pt = tgt.ptr(y);
		ps = src.ptr(y);
		interp_red_E(ps, pt, stride);
		for (x = 1; x < cols; ++x)
		{
			interp_green2(ps + x, pt + x*step, stride);
			++x;
			interp_red(ps + x, pt + x*step, stride);
		}
		interp_green2_W(ps + x, pt + x*step, stride);

		++y;

		pt = tgt.ptr(y);
		ps = src.ptr(y);
		interp_green1_E(ps, pt, stride);
		for (x = 1; x < cols; ++x)
		{
			interp_blue(ps + x, pt + x*step, stride);
			++x;
			interp_green1(ps + x, pt + x*step, stride);
		}
		interp_blue_W(ps + x, pt + x*step, stride);
	}

	pt = tgt.ptr(y);
	ps = src.ptr(y);
	interp_red_corner(ps, pt, -stride, 1);
	for (x = 1; x < cols; ++x)
	{
		interp_green2_S(ps + x, pt + x*step, stride);
		++x;
		interp_red_S(ps + x, pt + x*step, stride);
	}
	interp_green2_corner(ps + x, pt + x*step, -stride, -1);

}
void demosaic_GRBG(const cv::Mat& src, cv::Mat& tgt)
{
	int stride = src.step;
	int step = tgt.elemSize();
	int rows = src.rows - 2;
	int cols = src.cols - 2;
	int x, y;

	uchar* pt = tgt.ptr(0);
	const uchar* ps = src.ptr(0);
	interp_green2_corner(ps, pt, stride, 1);
	for (x = 1; x < cols; ++x)
	{
		interp_red_N(ps + x, pt + x*step, stride);
		++x;
		interp_green2_N(ps + x, pt + x*step, stride);
	}
	interp_red_corner(ps + x, pt + x*step, stride, -1);

	for (y = 1; y < rows; ++y)
	{
		pt = tgt.ptr(y);
		ps = src.ptr(y);
		interp_blue_E(ps, pt, stride);
		for (x = 1; x < cols; ++x)
		{
			interp_green1(ps + x, pt + x*step, stride);
			++x;
			interp_blue(ps + x, pt + x*step, stride);
		}
		interp_green1_W(ps + x, pt + x*step, stride);

		++y;

		pt = tgt.ptr(y);
		ps = src.ptr(y);
		interp_green2_E(ps, pt, stride);
		for (x = 1; x < cols; ++x)
		{
			interp_red(ps + x, pt + x*step, stride);
			++x;
			interp_green2(ps + x, pt + x*step, stride);
		}
		interp_red_W(ps + x, pt + x*step, stride);
	}

	pt = tgt.ptr(y);
	ps = src.ptr(y);
	interp_blue_corner(ps, pt, -stride, 1);
	for (x = 1; x < cols; ++x)
	{
		interp_green1_S(ps + x, pt + x*step, stride);
		++x;
		interp_blue_S(ps + x, pt + x*step, stride);
	}
	interp_green1_corner(ps + x, pt + x*step, -stride, -1);

}

void demosaic_RGGB(const cv::Mat& src, cv::Mat& tgt)
{
	int stride = src.step;
	int step = tgt.elemSize();
	int rows = src.rows - 2;
	int cols = src.cols - 2;
	int x, y;

	uchar* pt = tgt.ptr(0);
	const uchar* ps = src.ptr(0);
	interp_red_corner(ps, pt, stride, 1);
	for (x = 1; x < cols; ++x)
	{
		interp_green2_N(ps + x, pt + x*step, stride);
		++x;
		interp_red_N(ps + x, pt + x*step, stride);
	}
	interp_green2_corner(ps + x, pt + x*step, stride, 1);

	for (y = 1; y < rows; ++y)
	{
		pt = tgt.ptr(y);
		ps = src.ptr(y);
		interp_green1_E(ps, pt, stride);
		for (x = 1; x < cols; ++x)
		{
			interp_blue(ps + x, pt + x*step, stride);
			++x;
			interp_green1(ps + x, pt + x*step, stride);
		}
		interp_blue_W(ps + x, pt + x*step, stride);

		++y;

		pt = tgt.ptr(y);
		ps = src.ptr(y);
		interp_red_E(ps, pt, stride);
		for (x = 1; x < cols; ++x)
		{
			interp_green2(ps + x, pt + x*step, stride);
			++x;
			interp_red(ps + x, pt + x*step, stride);
		}
		interp_green2_W(ps + x, pt + x*step, stride);
	}

	pt = tgt.ptr(y);
	ps = src.ptr(y);
	interp_green1_corner(ps, pt, -stride, 1);
	for (x = 1; x < cols; ++x)
	{
		interp_blue_S(ps + x, pt + x*step, stride);
		++x;
		interp_green1_S(ps + x, pt + x*step, stride);
	}
	interp_blue_corner(ps + x, pt + x*step, -stride, -1);

}

void mosaic(const cv::Mat& src, cv::Mat& tgt, BayerPattern pattern)
{
	assert(src.type() == CV_8UC3);

	// source image is assumed to be in BGR order
	// r holds the respective channel of interest offsets for even and odd rows
	// j1 and j2 hold the offsets to the next channel of interest for even and odd columns
	int r[2], j1, j2;

	switch (pattern)
	{
	case BayerPattern::BGGR:
		r[0] = 0; r[1] = 1;	j1 = 4;	j2 = 2;
		break;
	case BayerPattern::GBRG:
		r[0] = 1; r[1] = 2; j1 = 2; j2 = 4;
		break;
	case BayerPattern::GRBG:
		r[0] = 1; r[1] = 0; j1 = 4; j2 = 2;
		break;
	case BayerPattern::RGGB:
		r[0] = 2; r[1] = 1; j1 = 2;	j2 = 4;
		break;
	}

	tgt.create(src.size(), CV_8UC1);
	const int cols = src.cols & -2;

	for (int y = 0; y < src.rows; y++)
	{
		uchar* t = tgt.ptr(y);
		const uchar* s = src.ptr(y) + r[y & 1]; // get a pointer to the channel of interest
		for (int x = 0; x < cols; x += 2)
		{
			t[x] = *s;
			s += j1;
			t[x + 1] = *s;
			s += j2;
		}
		if (cols < src.cols) // handle odd number of columns
			t[cols] = *s;
	}
}





