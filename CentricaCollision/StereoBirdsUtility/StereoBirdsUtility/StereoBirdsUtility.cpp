
#define _COMPILING_STEREO_BIRDS_UTILITY

#include "StereoBirdsUtility.h"
#include "demosaic.h"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>	

#include <iostream>
using namespace std;

#define EXPORT extern "C" __declspec(dllexport) 

namespace
{
	cv::Vec3d find_centroid(cv::Vec3d* p, int count)
	{
		cv::Vec3d m = p[0];
		for (int i = 1; i < count; ++i)
			m += p[i];

		return m * 1.f / count;
	}

	void translate(cv::Vec3d t, cv::Vec3d* p, int count)
	{
		for (int i = 0; i < count; ++i)
			p[i] += t;
	}

}


EXPORT int Demosaic(int width, int height, uchar* source_data, uchar* target_data, int source_stride, int target_stride, BayerPattern pattern, int target_bits_per_pixel)
{
	int type;

	if ( !source_data || !target_data 
		|| source_stride < width 
		|| target_stride < width*(target_bits_per_pixel/CHAR_BIT)
		|| width<1
		|| height<1)
		return -1;

	if (target_bits_per_pixel == 24)
		type = CV_8UC3;
	else if (target_bits_per_pixel == 32)
		type = CV_8UC4;
	else
		return -1;

	cv::Mat src(height, width, CV_8UC1, source_data, source_stride);
	cv::Mat tgt(height, width, type, target_data, target_stride);
	demosaic(src, tgt, pattern);

	return 0;
}



EXPORT int FindTransform(point3d* pointsA, point3d* pointsB, int count, double matrix4x4_out[16])
{
	if (!pointsA || !pointsB || !matrix4x4_out || count < 3)
		return -1;
	
	// copy the input because we need to alter it.
	cv::Mat A = cv::Mat(count, 3, CV_64FC1, pointsA).clone();
	cv::Mat B = cv::Mat(count, 3, CV_64FC1, pointsB).clone();

	// get Vec3d pointers to the data
	cv::Vec3d* pA = A.ptr<cv::Vec3d>(0);
	cv::Vec3d* pB = B.ptr<cv::Vec3d>(0);

	// find centroids
	cv::Vec3d Ac =  find_centroid(pA, count);
	cv::Vec3d Bc = find_centroid(pB, count);

	// adjust points
	translate(-Ac, pA, count);
	translate(-Bc, pB, count);
		 
	// find the rotation 
	cv::Matx33d U, Vt;
	cv::SVD::compute(B.t()*A, cv::Matx31d(), U, Vt, cv::SVD::MODIFY_A);
	cv::Matx33d R = U * Vt;

	// reflection check
	if (cv::determinant(R) < 0)
		R.col(2) = -R.col(2);

	// find translation
	cv::Vec3d t = -R * Ac + Bc;

	matrix4x4_out[0] = R(0);
	matrix4x4_out[1] = R(1);
	matrix4x4_out[2] = R(2);
	matrix4x4_out[3] = t(0);

	matrix4x4_out[4] = R(3);
	matrix4x4_out[5] = R(4);
	matrix4x4_out[6] = R(5);
	matrix4x4_out[7] = t(1);

	matrix4x4_out[8] = R(6);
	matrix4x4_out[9] = R(7);
	matrix4x4_out[10] = R(8);
	matrix4x4_out[11] = t(2);

	matrix4x4_out[12] = 0;
	matrix4x4_out[13] = 0;
	matrix4x4_out[14] = 0;
	matrix4x4_out[15] = 1;

	return 0;
}




EXPORT int FindRotation(point3d* pointsA, point3d* pointsB, int count, double matrix3x3_out[9])
{
	if (!pointsA || !pointsB || !matrix3x3_out || count < 3)
		return -1;

	// copy the input because we need to alter it.
	cv::Mat A = cv::Mat(count, 3, CV_64FC1, pointsA).clone();
	cv::Mat B = cv::Mat(count, 3, CV_64FC1, pointsB).clone();

	// get Vec3d pointers to the data
	cv::Vec3d* pA = A.ptr<cv::Vec3d>(0);
	cv::Vec3d* pB = B.ptr<cv::Vec3d>(0);

	// find the rotation 
	cv::Matx33d U, Vt;
	cv::SVD::compute(B.t()*A, cv::Matx31d(), U, Vt, cv::SVD::MODIFY_A);
	cv::Matx33d R = U * Vt;

	// reflection check
	if (cv::determinant(R) < 0)
		R.col(2) = -R.col(2);

	matrix3x3_out[0] = R(0);
	matrix3x3_out[1] = R(1);
	matrix3x3_out[2] = R(2);

	matrix3x3_out[3] = R(3);
	matrix3x3_out[4] = R(4);
	matrix3x3_out[5] = R(5);

	matrix3x3_out[6] = R(6);
	matrix3x3_out[7] = R(7);
	matrix3x3_out[8] = R(8);

	return 0;
}