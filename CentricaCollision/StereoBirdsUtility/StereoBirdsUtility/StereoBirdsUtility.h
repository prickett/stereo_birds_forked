#pragma once  

#ifdef __cplusplus 
extern "C" {
#endif // __cplusplus  

#ifdef _COMPILING_STEREO_BIRDS_UTILITY
#define LIBSPEC __declspec(dllexport)
#else
#define LIBSPEC __declspec(dllimport)
#endif 

	enum BayerPattern{ BGGR, GBRG, GRBG, RGGB };

	// prototypes go here

	struct point3d
	{
		double x, y, z;
	};

	typedef unsigned char uchar;

	LIBSPEC int FindTransform(point3d* pointsA, point3d* pointsB, int count, double matrix4x4_out[16]);

	LIBSPEC int FindRotation(point3d* pointsA, point3d* pointsB, int count, double matrix3x3_out[9]);

	LIBSPEC int Demosaic(int width, int height, uchar* source_data, uchar* target_data, int source_stride, int target_stride, BayerPattern pattern, int target_bits_per_pixel);

#undef LIBSPEC  

#ifdef __cplusplus   
}
#endif // __cplusplus  
