﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace IsolatedRemoteStationMonitor
{
    class TargetDrives
    {
        private DriveInfo[] excludedDrives;
        private DriveInfo[] targetDrives;
        private int currentIndex;
        private long minimumSize;
        private long minimumSpace;


        public TargetDrives(long targetMinimumSize, long targetMinimumFreeSpace, params string[] exclude)
            : this(targetMinimumSize, targetMinimumFreeSpace, exclude.Select(c => new DriveInfo(c)).ToArray())        {        }

        public TargetDrives(long targetDriveMinimumSize, long targetDriveMinimumFreeSpace, params DriveInfo[] exclude)
        {
            excludedDrives = exclude.ToArray();//make a copy
            minimumSize = targetDriveMinimumSize;
            minimumSpace = targetDriveMinimumFreeSpace;
            findTargetDrives();


            Console.WriteLine("found {0} writeable drives", targetDrives.Length);
            foreach (var drive in targetDrives)
            {
                Console.WriteLine(drive);
            }
        }

        public DriveInfo[] Drives
        {
            get { return targetDrives.ToArray(); }
        }

        /// <summary>
        /// Returns the current target drive or null if no drives are available.
        /// </summary>
        public DriveInfo Current
        {
            get
            {
                return (currentIndex < targetDrives.Length) ? targetDrives[currentIndex] : null;
            }
        }

        //public void Reset()
        //{

        //}

        public bool MoveToNext()
        {
            currentIndex++;
            return currentIndex < targetDrives.Length;
        }

        private void findTargetDrives()
        {
            var drives = DriveInfo.GetDrives().Where(d =>
                d.IsReady &&
                d.TotalFreeSpace > minimumSize &&
                d.AvailableFreeSpace > minimumSpace &&
                excludedDrives.FirstOrDefault(e => e.Name.ToUpper() == d.Name.ToUpper()) == null &&
                (d.DriveType == DriveType.Fixed || d.DriveType == DriveType.Removable));

            targetDrives = drives.OrderBy(d => d.AvailableFreeSpace).ToArray();
            currentIndex =-1;

            //following is for debugging purposes, might be useful to keep if changes are planned

            //Console.WriteLine("\nDrives available for writing: {0}.", targetDrives.Length);
            //foreach (var drive in targetDrives)
            //{
            //    Console.WriteLine("found {0} (freespace:{1} size:{2})", drive.Name, drive.AvailableFreeSpace, drive.TotalSize);
            //}
        }

        /// <summary>
        /// Compares the available free space of two DriveInfo objects.
        /// No null checks are made and the accuracy is in MB.
        /// </summary>
        private static int compareFreeSpace(DriveInfo a, DriveInfo b)
        {
            return (int)((a.AvailableFreeSpace - b.AvailableFreeSpace) >> 20);
        }

    }
}
