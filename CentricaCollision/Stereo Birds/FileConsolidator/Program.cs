﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Createc.StereoBirds;


namespace FileConsolidator
{
    class Program
    {
        static void Main(string[] args)
        {
            string dir = args.Length > 0 ?
                args[0] : Directory.GetCurrentDirectory();

            if (Directory.Exists(dir))
            {
                if (askUser(string.Format("Consolodate files in {0}?", dir)))
                {
                    var sorter = new MapFileConsolidator(dir, dir);
                    sorter.SortFiles();
                }
            }
        }


        private static bool askUser(string question)
        {
            Console.WriteLine(question);
            char key = '\0';
            while (key != 'y' || key != 'n' || !char.IsControl(key))
            {
                key = char.ToLower(Console.ReadKey(true).KeyChar);
                Console.WriteLine("y/n? ");
            }
            return key == 'y';

        }
    }
}
