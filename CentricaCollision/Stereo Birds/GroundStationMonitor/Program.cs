﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Createc.StereoBirds;

namespace Createc.StereoBirds.GroundStation
{
    class Program
    {
        private static string SourceFolder = @"C:\Upload";
        private static Scheduler scheduler;

        static void Main(string[] args)
        {
            scheduler = new Scheduler();
            scheduler.Add(new ScheduledTask(Transfer));

            Console.WriteLine("Any key to quit!");
            Console.ReadKey(true);
            scheduler.Quit();
        }

        static void Transfer()
        {
            const long ONE_MB = 1024*1024;
            const long MIN_SIZE = ONE_MB * 100; 
            const long MIN_FREE = ONE_MB * 20;

            var targets = new TargetDrives(MIN_SIZE, MIN_FREE,"C");
            if (targets.MoveToNext())
            {
                var target = targets.Current;
                Console.WriteLine("Transfering to {0}", target.Name);
                var sorter = new MapFileConsolidator(SourceFolder, targets.Current.Name);
                do
                {
                    try
                    {
                        sorter.SortFiles();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                } while (sorter.TransferedCount != 0);

                Console.WriteLine("Transfered {0} files", sorter.TransferedCount);
            }

            
            //schedule to run again in 5 minutes
            scheduler.Add(  new ScheduledTask(DateTime.Now + TimeSpan.FromMinutes(5),Transfer));
        }



        private static bool AreYouSure()
        {
            Console.WriteLine("Are you sure? y/n ");
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.Y)
                {
                    Console.WriteLine("Okay.");
                    return true;
                }
                if (keyInfo.Key == ConsoleKey.N)
                {
                    Console.WriteLine("Canceled.");
                    return false;
                }
                Console.WriteLine("y/n? ");
            }

        }


    }
}
