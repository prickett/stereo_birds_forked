﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO.Ports;

namespace RemoteStationMonitor
{
    class USBRelayControl
    {

        #region static stuff
        private const byte RLY_MOD_ID = 0x09; //used to identify the device   
        private const byte VERSION = 0x5A;
        private const byte GETSTATE = 0x5B;
        private const byte SETSTATE = 0x5C;
        private const byte VOLTAGE = 0x5D;
        private const byte ALLON = 0x64;
        private const byte BASEON = 0x65;
        private const byte ALLOFF = 0x6E;
        private const byte BASEOFF = 0x6F;

        public static string[] AvailableControllers()
        {
            int ct = 0;
            USBRelayControl ctrl;
            string[] portNames = SerialPort.GetPortNames();
            foreach (var name in portNames)
            {
                ctrl = new USBRelayControl(name);
                if (ctrl.IsAvailable)
                {
                    portNames[ct++] = name;
                }
                ctrl.Dispose();
            }
            Array.Resize<string>(ref portNames, ct);
            return portNames;
        }
        #endregion

        private SerialPort USB_PORT;
        private byte[] SerBuf = new byte[5];
        private byte version = 0;
        private bool available = false;

        public string PortName { get; private set; }

        public USBRelayControl(string portName)
        {
            PortName = portName;
            USB_PORT = new SerialPort(PortName, 19200, 0, 8, StopBits.Two);
            USB_PORT.ReadTimeout = 50;
            USB_PORT.WriteTimeout = 50;
            USB_PORT.Open();
        }

        public bool IsAvailable
        {            
            get
            {
                if (available == false)
                {
                    sendByte(VERSION);// get version command for RLY16, returns module id and software version
                    i2c_recieve(2);
                    if (SerBuf[0] == RLY_MOD_ID)  // if the module id is that of the rly16  
                    {
                        version = SerBuf[1];  //remeber the software version
                        available = true;
                    }
                }
                return available;
            }
        }

        public float Voltage
        {
            get
            {
                float voltage = 0;
                if (IsAvailable)
                {
                    sendByte(VOLTAGE);
                    i2c_recieve(1);
                    voltage = (float)SerBuf[0] * 0.1f;
                }
                return voltage;
            }
        }

        public void AllOff()
        {
            if (IsAvailable)
            {
                sendByte(ALLOFF);
            }
        }

        public void AllOn()
        {
            if (IsAvailable)
            {
                sendByte(ALLON);
            }
        }

        public bool this[int index]
        {
            get
            {
                return ((int)State & (1 << index)) != 0;
            }
            set
            {
                index &= 7;
                sendByte((byte)(value ? BASEON + index : BASEOFF + index));
            }
        }

        public byte State
        {
            get
            {
                SerBuf[0] = 0;
                if (IsAvailable)
                {
                    sendByte(GETSTATE);
                    i2c_recieve(1);
                }
                return SerBuf[0];
            }
            set
            {
                if (IsAvailable)
                {
                    SerBuf[0] = SETSTATE; // set states command for RLY16
                    SerBuf[1] = value;
                    i2c_transmit(2);
                }

            }
        }


        private void sendByte(byte value)
        {
            SerBuf[0] = value;
            i2c_transmit(1);
        }

        private void i2c_transmit(byte write_bytes)
        {
            try
            {
                USB_PORT.Write(SerBuf, 0, write_bytes);      // writes specified amount of SerBuf out on COM port
            }
            catch (Exception)
            {
            }
        }

        private void i2c_recieve(byte read_bytes)
        {
            for (byte x = 0; x < read_bytes; x++)  // this will call the read function for the passed number times, 
            {                                      // this way it ensures each byte has been correctly recieved while
                try                                // still using timeouts
                {
                    USB_PORT.Read(SerBuf, x, 1);     // retrieves 1 byte at a time and places in SerBuf at position x
                }
                catch (Exception)                   // timeout or other error occured, set lost comms indicator
                {
                    SerBuf[0] = 255;
                    available = false;
                }
            }
        }


        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            if (USB_PORT != null)
            {
                USB_PORT.Close();
                USB_PORT.Dispose();
                USB_PORT = null;
            }
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }






    }
}

