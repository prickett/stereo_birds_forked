﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;

namespace RemoteStationMonitor
{
    class FileUpload
    {
        public string FtpServer { get; private set; }
        public string SourceDirectory { get; private set; }
        public bool Running { get; set; }

        private NetworkCredential identity;

        public FileUpload(string sourceDirectory, Uri ftpServer, string username, string password)
        {
            FtpServer = ftpServer.ToString();
            SourceDirectory = sourceDirectory;
            identity = new NetworkCredential(username, password);
        }

        public void Upload()
        {


            var folders = Directory.EnumerateDirectories(SourceDirectory,"_*",SearchOption.TopDirectoryOnly);
            foreach (var folder in folders)
            {
                Console.WriteLine("getting list of files in folder {0}", folder);
                int counter = 0;
                var minutesAgo = DateTime.Now - TimeSpan.FromMinutes(5);
                var files = Directory.EnumerateFiles(folder, "*.*", SearchOption.AllDirectories)
                    .Where(f=> File.GetLastWriteTime(f)<minutesAgo);
                foreach (var file in files)
                {
                    Uri target = new Uri(FtpServer + Path.GetFileName(file));
                    try
                    {
                        bool success;
                        using (var fs = new FileStream(file, FileMode.Open))
                        {
                            success = FTPUpload(fs, target, identity);
                        }
                        if (success)
                        {
                            //Console.Write('.');
                            counter++;
                            File.Delete(file);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
                Console.WriteLine("uploaded {0} files from folder {1}",counter, folder);

            }
           
        }
 

        static bool FTPUpload(FileStream source, Uri targetUri, NetworkCredential identity)
        {
            FtpWebRequest request;
            FtpWebResponse response = null;
            bool retval = false;
            request = (FtpWebRequest)FtpWebRequest.Create(targetUri);
            request.Credentials = identity;
            request.Method = WebRequestMethods.Ftp.UploadFile;
            request.UseBinary = true;
            request.Proxy = null;
            request.KeepAlive = false;
            request.UsePassive = false;
            request.ContentLength = source.Length;

            try
            {
                Stream target = request.GetRequestStream();
                source.CopyTo(target);
                target.Close();
                response = (FtpWebResponse)request.GetResponse();
                retval = response.StatusCode == FtpStatusCode.ClosingData;
                if (!retval)
                    Console.WriteLine("{0} {1}", response.StatusCode, response.StatusDescription);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return retval;
        }
    }
}
