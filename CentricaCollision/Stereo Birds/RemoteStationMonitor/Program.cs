﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Createc.StereoBirds;

namespace RemoteStationMonitor
{
    class Program
    {
        private static Scheduler scheduler;
        private static USBRelayControl relayControl;
        private static FileUpload uploader;

        static class Relays
        {
            public const int LeftWasherOn = 0;
            public const int RightWasherOn = 1;
            public const int LeftWiperOn = 2;
            public const int RightWiperOn = 3;
            public const int PTPLinkOff = 7;
        }


        static void Main(string[] args)
        {
            if (args.Length < 4)
            {
                Console.WriteLine("Usage: FileTransfer [SourceDirectory] [FtpServer] [Username] [Password]");
                return;
            }
            uploader = new FileUpload(args[0], new Uri(string.Format("ftp://{0}/", args[1])), args[2], args[3]);

            InitSchedule();
            ProgramLoop();
        }

        private static void InitSchedule()
        {
            var now = DateTime.Now;
            var midnight = DateTime.Today;
            var sixAm = midnight + TimeSpan.FromHours(6);

            scheduler = new Scheduler();
            ScheduledTask task;

            task = new ScheduledTask(sixAm, WashCycle, TimeSpan.FromHours(8));
            task.Reschedule(); //if due in the past the task fires instantly
            scheduler.Add(task);

            task = new ScheduledTask(now + TimeSpan.FromSeconds(20), CheckRelayControllerStatus, TimeSpan.FromHours(1));
            scheduler.Add(task);

            scheduler.Add(new ScheduledTask(DateTime.Now + TimeSpan.FromSeconds(3), SortFiles));

            scheduler.Add(new ScheduledTask(DateTime.Now + TimeSpan.FromSeconds(10), Upload));
        }

        static void Upload()
        {
            uploader.Upload();
            scheduler.Add(new ScheduledTask(DateTime.Now + TimeSpan.FromSeconds(3), Upload));
        }

        static void SortFiles()
        {
            Console.WriteLine("Sorting");
            var sorter = new MapFileConsolidator(@"C:\Capture\", @"C:\Capture\");
            do
            {
                try
                {
                    sorter.SortFiles();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            } while (sorter.TransferedCount != 0);

            Console.WriteLine("Sorted {0} files", sorter.TransferedCount);

            //schedule to run again in 5 minutes
            //scheduler.Add(new ScheduledTask(DateTime.Now + TimeSpan.FromSeconds(3), SortFiles));
            scheduler.Add(new ScheduledTask(DateTime.Now + TimeSpan.FromMinutes(2), SortFiles));
        }

        private static void ProgramLoop()
        {
            Welcome();
            while (true)
            {
                char key = Console.ReadKey(true).KeyChar;
                switch (char.ToLower(key))
                {
                    case 'q':
                        Console.WriteLine("\nQuit?");
                        if (AreYouSure())
                        {
                            scheduler.Quit();
                            return;
                        }
                        break;
                    case 'w':
                        scheduler.Add(new ScheduledTask(WashCycle));
                        break;
                    case 's':
                        scheduler.ShowTasks();
                        break;
                    case 'l':
                        scheduler.Add(new ScheduledTask(SquirtLeft));
                        break;
                    case 'r':
                        scheduler.Add(new ScheduledTask(SquirtRight));
                        break;
                    case 'x':
                        Welcome();
                        break;
                    case '!':
                        Console.WriteLine("\nPower cycle ptp link?");
                        if (AreYouSure())
                            scheduler.Add(new ScheduledTask(PowerCyclePTPLink));
                        break;
                    case '?':
                        Console.WriteLine("\nKey commands");
                        Console.WriteLine("? - Display this message.");
                        Console.WriteLine("s - Show schedule.");
                        Console.WriteLine("w - Run a wash cycle.");
                        Console.WriteLine("r - Squirt right washer.");
                        Console.WriteLine("l - Squirt left washer.");
                        Console.WriteLine("x - Clear screen.");
                        Console.WriteLine("! - Power cycle ptp link.");
                        Console.WriteLine("q - Quit.");
                        break;
                    default:
                        Console.WriteLine("Unknown command! Hit '?' for help.");
                        break;
                }
            }
        }

        private static void Welcome()
        {
            Console.Clear();
            Console.WriteLine("Stereo pair watchdog and scheduler.");
            Console.WriteLine("Hit '?' for help.");
        }

        private static bool AreYouSure()
        {
            Console.WriteLine("Are you sure? y/n ");
            while (true)
            {
                var keyInfo = Console.ReadKey(true);
                if (keyInfo.Key == ConsoleKey.Y)
                {
                    Console.WriteLine("Okay.");
                    return true;
                }
                if (keyInfo.Key == ConsoleKey.N)
                {
                    Console.WriteLine("Canceled.");
                    return false;
                }
                Console.WriteLine("y/n? ");
            }

        }


        /// <summary>
        /// runs a basic wash cycle
        /// </summary>
        private static void WashCycle()
        {
            Console.WriteLine("Starting wash cycle...");
            relayControl[Relays.LeftWiperOn] = true;
            relayControl[Relays.RightWiperOn] = true;
            for (int i = 0; i < 3; i++)
            {
                relayControl[Relays.LeftWasherOn] = true;
                relayControl[Relays.RightWasherOn] = true;
                Thread.Sleep(2000);
                Console.WriteLine("...washing...");

                relayControl[Relays.LeftWasherOn] = false;
                relayControl[Relays.RightWasherOn] = false;
                Thread.Sleep(3000);
            }
            relayControl[Relays.LeftWiperOn] = false;
            relayControl[Relays.RightWiperOn] = false;
            Console.WriteLine("...finished wash cycle");
        }

        /// <summary>
        /// squirt left washer
        /// </summary>
        private static void SquirtLeft()
        {
            relayControl[Relays.LeftWasherOn] = true;
            Thread.Sleep(3000);
            relayControl[Relays.LeftWasherOn] = false;
        }

        /// <summary>
        /// squirt right washer
        /// </summary>
        private static void SquirtRight()
        {
            relayControl[Relays.RightWasherOn] = true;
            Thread.Sleep(3000);
            relayControl[Relays.RightWasherOn] = false;
        }

        /// <summary>
        /// power cycle the point-to-point link
        /// </summary>
        private static void PowerCyclePTPLink()
        {
            //power cycle
            relayControl[Relays.PTPLinkOff] = true;
            Thread.Sleep(30000); //wait 30 seconds
            relayControl[Relays.PTPLinkOff] = false;
        }

        /// <summary>
        /// If the relay controller goes down try to get it back
        /// </summary>
        private static void CheckRelayControllerStatus()
        {
            if (relayControl == null || relayControl.IsAvailable == false)
            {
                if (relayControl != null)
                    relayControl.Dispose();

                var portNames = USBRelayControl.AvailableControllers();
                if (portNames.Length == 1)
                {
                    Console.WriteLine("Found relay controler on {0}", portNames[0]);
                    relayControl = new USBRelayControl(portNames[0]);
                }
                else if (portNames.Length == 0)//not too clever, might need a rethink
                {
                    Console.WriteLine("No relay controler found!");
                    relayControl = new USBRelayControl("com1");
                }
                else //there is more than one controller to choose from
                {
                    Console.WriteLine("Found more than one relay controller");
                    Console.WriteLine("Please select from the following ports");
                    char key = 'a';
                    foreach (var name in portNames)
                    {
                        Console.WriteLine("'{0}' - {1}", key++, name);
                    }
                    char keyMax = key;
                    while (true)
                    {
                        key = char.ToLower(Console.ReadKey(true).KeyChar);
                        if (key >= 'a' && key < keyMax)
                        {
                            relayControl = new USBRelayControl(portNames[(key - 'a')]);
                            break;
                        }
                        Console.WriteLine("choose 'a' - '{0}' ", keyMax);
                    }
                    relayControl.State = 0;
                }
            }
        }






        static void TestRelayControl()
        {
            USBRelayControl relayControl;
            var controlers = USBRelayControl.AvailableControllers();
            Console.WriteLine("found {0} controller(s)", controlers.Length);
            if (controlers.Length > 0)
            {
                relayControl = new USBRelayControl(controlers[0]);

                Random rnd = new Random();
                var states = new byte[10];
                rnd.NextBytes(states);
                const int INTERVAL = 100;

                Console.WriteLine("voltage = {0}", relayControl.Voltage);

                foreach (byte state in states)
                {
                    relayControl.State = state;
                    Console.WriteLine("{0} {1}", bitString(state), bitString(relayControl.State));
                    System.Threading.Thread.Sleep(INTERVAL);
                }


                relayControl.AllOn();
                Console.WriteLine(bitString(relayControl.State));
                System.Threading.Thread.Sleep(INTERVAL);

                relayControl.AllOff();
                Console.WriteLine(bitString(relayControl.State));
                System.Threading.Thread.Sleep(INTERVAL);

                for (int i = 0; i < 8; i++)
                {
                    relayControl[i] = true;
                    System.Threading.Thread.Sleep(INTERVAL);
                    Console.WriteLine(bitString(relayControl.State));
                    relayControl[i] = false;
                }

                for (int i = 6; i >= 0; i--)
                {
                    relayControl[i] = true;
                    System.Threading.Thread.Sleep(INTERVAL);
                    Console.WriteLine(bitString(relayControl.State));
                    relayControl[i] = false;
                }

                relayControl[7] = true;

                Console.WriteLine("voltage = {0}", relayControl.Voltage);
            }
        }

        static string bitString(byte value)
        {
            int n = value;
            char[] sout = new char[8];
            for (int i = 0; i < 8; i++)
            {
                sout[i] = (n & (1 << i)) != 0 ? '1' : '0';
            }
            return new string(sout);
        }
    }
}
