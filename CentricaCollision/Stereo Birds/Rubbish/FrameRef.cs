﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.IO;

namespace Createc.StereoBirds.GroundStation
{
    public enum FrameType
    {
        None,
        Map,
        Fullframe,
        Unknown
    }
    public enum FrameStatus
    {
        UnVerified,
        Verified,
        Bad_Path,
        Bad_File
    }


    public class FrameRef:IComparable<FrameRef>
    {
        public bool IsMaster { get; private set; }
        public bool IsSlave { get; private set; }
        public long FrameNumber { get; private set; }
        public long SystemTime { get; private set; }
        public FrameType Type { get; private set; }
        public string Path { get; private set; }
        public FrameStatus Status { get; set; }


        public FrameRef(long systemTime)
        {
            Type = FrameType.None;
            SystemTime = systemTime;
            FrameNumber = 0;
        }

        public FrameRef(string path)
        {
            var filename = System.IO.Path.GetFileNameWithoutExtension(path);
            var extension = System.IO.Path.GetExtension(path);

            switch (extension)
            {
                case ".map":
                    Type = FrameType.Map;
                    break;
                case ".bmp":
                case ".jpg":
                    Type = FrameType.Fullframe;
                    break;
                default:
                    Console.WriteLine(extension);
                    Type = FrameType.Unknown;
                    break;
            }


            int offset = 0;
            string result;
            int number = 0;

            IsMaster = (filename.ToLower().IndexOf('m') == 0);
            IsSlave = (filename.ToLower().IndexOf('s') == 0);

            result = InnerString(filename, '_', '_', ref offset);
            SystemTime = int.TryParse(result, out number) ? number : -1;

            if (offset != -1)
            {
                result = filename.Substring(offset);
                //result = InnerString(filename, '_', '.', ref offset);
                FrameNumber = int.TryParse(result, out number) ? number : -1;
            }

            Path = path;

        }

        private static string InnerString(string text, char opener, char closer,ref int offset)
        {
            int p, q;
            if ((p = text.IndexOf(opener,offset) + 1) >= 1 && (q = text.IndexOf(closer, p)) >= 1)
            {
                offset = q + 2;
                return text.Substring(p, q - p);
            }
            else
            {
                offset = -1;
                return null;
            }
        }


        public int CompareTo(FrameRef other)
        {
            if (other == null)
                return 1;
            int tDif = (int)(SystemTime - other.SystemTime);
            if (Math.Abs(tDif) < 20)
            {
                int fDif = (int)(FrameNumber - other.FrameNumber);
                if (Math.Abs(fDif) < 100)
                    return fDif;
            }
            return tDif;
        }
    }
}
