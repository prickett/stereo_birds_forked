﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using Createc.Imaging.Thresholds;

namespace Createc.StereoBirds.GroundStation
{
    public class MapFileHeader
    {
        public const int HEADERSIZE = 68;
        public MapFileHeader(BinaryReader reader)
        {
            if (reader.BaseStream.Length > HEADERSIZE)
            {
                MagicNumber = reader.ReadUInt32();
                Width = reader.ReadUInt32();
                Height = reader.ReadUInt32();
                BitsPerPixel = reader.ReadUInt16();
                BayerPattern = reader.ReadUInt16();

                MapWidth = reader.ReadUInt16();
                MapHeight = reader.ReadUInt16();
                MapUnitSize = reader.ReadInt32();
                MapOffset = reader.ReadInt32();
                DataOffset = reader.ReadInt32();
                MaskOffset = reader.ReadInt32();

                CameraID = reader.ReadUInt32();
                Exposure = reader.ReadUInt32();
                FrameStatus = reader.ReadUInt32();
                CameraTick = reader.ReadUInt32();
                SystemTick = reader.ReadUInt32();
                SystemTime = reader.ReadUInt32();
                reader.ReadUInt32();
                FrameCount = reader.ReadUInt32();
            }
        }
        public uint MagicNumber { get; private set; }
        public uint Width { get; private set; }
        public uint Height { get; private set; }
        public int BitsPerPixel { get; private set; }
        public int BayerPattern { get; private set; }
        public int MapWidth { get; private set; }
        public int MapHeight { get; private set; }
        public int MapUnitSize { get; private set; }
        public int MapOffset { get; private set; }
        public int DataOffset { get; private set; }
        public int MaskOffset { get; private set; }
        public uint CameraID { get; private set; }
        public uint Exposure { get; private set; }
        public uint FrameStatus { get; private set; }
        public uint CameraTick { get; private set; }
        public uint SystemTick { get; private set; }
        public ulong SystemTime { get; private set; }
        public uint FrameCount { get; private set; }
        //public int HeaderSize { get { return HEADERSIZE; } }
    }
    class MapFile : IDisposable//:IEnumerable<Location>
    {
        private const uint MAGICNUMBER = 0xC0FFEE;
        private static readonly DateTime SYSTEMTIME0 = new DateTime(1970, 1, 1);




        byte[] map;
        byte[] bayer;
        byte[] mask;
        int[,] offsets;

        Stream stream;
        BinaryReader reader;
        MapFileHeader header;
        FrameRef frameRef;

        public bool isOpen { get { return stream != null; } }
        public MapFileHeader Header { get { return header; } }
        public FrameRef FrameReference { get { return frameRef; } }


        //static readonly byte[] maskLut;
        static readonly int[] bitCtLut;
        static MapFile()
        {
            //maskLut = buildBrightnessLUT(3f);
            bitCtLut = buildBitCountLUT();
        }

        public MapFile()
        {
        }

        public MapFile(FrameRef frameRef)
        {
            Load(frameRef);
        }

        public bool Load(FrameRef frameRef)
        {
            loadHeader(frameRef);
            return frameRef.Status == FrameStatus.Verified;
        }

        private void handleLoadException(Exception e)
        {
            if (e is ArgumentException || 
                e is FileNotFoundException || 
                e is UnauthorizedAccessException  || 
                e is DirectoryNotFoundException)
            {
                frameRef.Status = FrameStatus.Bad_Path;
            }
            else if (e is IOException)
            {
                frameRef.Status = FrameStatus.Bad_File;
            }
            else
            {
                Console.WriteLine(e);
            }
        }
        private void loadHeader(FrameRef frameRef)
        {
            Close();
            clear();

            this.frameRef=frameRef;

            try
            {
                frameRef.Status = FrameStatus.UnVerified;
                stream = new FileStream(frameRef.Path, FileMode.Open, FileAccess.Read);
                reader = new BinaryReader(stream);
                header = new MapFileHeader(reader);                
            }
            catch (Exception e)
            {
                handleLoadException(e);
            }

            if (header != null && header.MagicNumber == MAGICNUMBER)//!!!!More checking here!!!
            {
                frameRef.Status = FrameStatus.Verified;
            }
            else if(frameRef.Status == FrameStatus.UnVerified)
            {
                frameRef.Status = FrameStatus.Bad_File;
            }
        }


        public byte[] GetMap()
        {
            if (isOpen)
            {
                if (map==null)
                {
                    reader.BaseStream.Seek(header.MapOffset, SeekOrigin.Begin);
                    map = reader.ReadBytes(mapSize());
                }
                return map;
            }
            return null;
        }

        public byte[] GetBayer()
        {
            if (isOpen)
            {
                if (bayer==null)
                {
                    reader.BaseStream.Seek(header.DataOffset, SeekOrigin.Begin);
                    bayer = reader.ReadBytes(bayerSize());
                }
                return bayer;
            }
            return null;
        }

        public byte[] GetMask()
        {
            if (isOpen)
            {
                if (mask==null)
                {
                    reader.BaseStream.Seek(header.MaskOffset, SeekOrigin.Begin);
                    mask = reader.ReadBytes(maskSize());
                }
                return mask;
            }
            return null;
        }

        public int[,] GetBayerOffsetsYX()
        {
            if (isOpen)
            {
                if (offsets == null)
                    offsets = buildBayerOffsetsYX();
                return offsets;
            }
            return null;
        }

        private int mapSize()
        {
            return ((header.MapWidth + 7) >> 3) * header.MapHeight;
        }
        private int bayerSize()
        { 
            return (int)(header.MaskOffset - header.DataOffset); 
        }
        private int maskSize()
        {
            return bayerSize() >> 1;
        }

        public int GetBayerRegion(ref byte[] target, int x, int y, int w, int h)
        {
            int mSz = header.MapUnitSize;
            int TileSz = mSz * mSz;
            int lastY = header.MapHeight - 1;

            byte[] bayer = GetBayer();
            int[,] offsets = GetBayerOffsetsYX();

            int szy = y + h;
            int szx = x + w;
            int t = 0;
            int sz = w * h * TileSz;
            int stride = mSz * w;

            if (target == null || target.Length < sz)
                target = new byte[sz];
            else
                Array.Clear(target, 0, sz);

            for (int yy = y; yy < szy; yy++)
            {
                t = (yy - y) * TileSz * w;
                if (yy == lastY)
                    TileSz = mSz * ((int)header.Height - lastY * mSz);
                for (int xx = x; xx < szx; xx++, t += mSz)
                {
                    int i = offsets[yy, xx];
                    if (i >= 0)
                        copyTile(bayer, i, target, t, stride, mSz);
                    //Array.Copy(mask, i , target, t, maskTileSz);
                }
            }
            return sz;

        }

        public void GetMaskRegion(ref byte[] target,int x,int y,int w,int h)
        {
            //int uSz = (int);
            int mSz = header.MapUnitSize >> 1;
            int maskTileSz = mSz * mSz;
            int halfHeight = (int)header.Height >> 1;
            int lastY = header.MapHeight - 1;

            byte[] mask = GetMask();
            int[,] offsets = GetBayerOffsetsYX();

            int szy = y + h;
            int szx = x + w;
            int t = 0;
            int sz=w*h*maskTileSz;
            int stride = mSz*w;

            if (target==null || target.Length < sz)
                target = new byte[sz];
            else
                Array.Clear(target, 0, sz);

            for (int yy = y; yy < szy; yy++)
            {
                t = (yy - y) * maskTileSz * w;
                if (yy==lastY)
                    maskTileSz=mSz * (halfHeight - lastY * mSz);
                for (int xx = x; xx < szx; xx++, t += mSz)
                {
                    int i = offsets[yy,xx]>>2;//divide by four
                    if (i >= 0)
                        copyTile(mask, i, target, t, stride, mSz);
                        //Array.Copy(mask, i , target, t, maskTileSz);
                }
            }

        }

        private static void copyTile(byte[] source, int srcOffset, byte[] target, int tgtOffset, int tgtStride, int size)
        {
            for (int y = 0; y < size; y++)
            {
                Array.Copy(source, srcOffset, target, tgtOffset, size);
                srcOffset += size;
                tgtOffset += tgtStride;
            }
        }



        private int[,] buildBayerOffsetsYX()
        {
            int usz = (int)header.MapUnitSize;
            int mw = (header.MapWidth+ 7) & -8;
            int mh = header.MapHeight;

            int[,] offsets = new int[mh, mw];
            int last = header.MapHeight - 1;
            int sz = usz * usz;
            int offset = 0;
            

            byte[] map = GetMap();
            //mw--;//adjust by 1
            for (int y = 0, i = 0; y < mh; y++)
            {
                if(y==last)
                    sz=usz * mh - (int)header.Height;
                for (int x = 0; x < mw; x += 8, i++)
                {
                    int bits = map[i];

                    int b = 0, n = 1;
                    while (b < 8)
                    {
                        if ((n & bits) != 0)
                        {
                            offsets[y, x + b] = offset;
                            offset += sz;
                        }
                        else
                        {
                            offsets[y, x + b] = -1;
                        }
                        n <<= 1;
                        b++;
                    }
                }
            }
            

            //foreach (var loc in new MapEnumerator(map, mw))
            //{
            //    int i = loc.Offset;
            //    offset += loc.Y < last ? sz : lsz;
            //    offsets[loc.Y,loc.X] = offset;
            //}
            return offsets;
        }

        public int MapTileCount()
        {
            return countTiles();
        }

        private int countTiles()
        {
            GetMap();
            int ct = 0;
            foreach (byte byt in map)
                ct += bitCtLut[byt];
            return ct;
        }


 


        //public override string ToString()
        //{
        //    string format = "magicNumber = {0:x}\nwidth = {1}\nheight = {2}\nbpp = {3}\nbayer pattern = {4}\n";
        //    format += "gridWidth = {5}\ngridHeight = {6}\ngridSize = {7}\nGrid offset = {8}\nData Offset = {9}\nMask Offset = {10}\n";
        //    format += "Camera Id = {11}\nExposure = {12}\nframeStatus = {13}\ncameraTick = {14}\nsystemTick = {15}\nsystemTime = {16}\nFrameNumber = {17}";
        //    return string.Format(format,
        //        header.magicNumber,
        //        header.width,
        //        header.height,
        //        header.bitsPerPixel,
        //        header.bayerPattern,
        //        header.mapWidth,
        //        header.mapHeight,
        //        header.mapUnitSize,
        //        header.gridOffset,
        //        header.dataOffset,
        //        header.maskOffset,
        //        header.cameraID,
        //        header.exposure,
        //        header.frameStatus,
        //        header.cameraTick,
        //        header.systemTick,
        //        header.systemTime,
        //        header.FrameCount);
        //}

        private void clear()
        {
            map = null;
            mask = null;
            bayer = null;
            header = null;
            offsets = null;
        }
        public void Close()
        {
            if (reader != null)
            {
                reader.Dispose();
                reader = null;
            }

            if (stream != null)
            {
                stream.Dispose();
                stream = null;
            }
        }

        public static DateTime SystemTimeToDateTime(ulong systemTime)
        {
            return SYSTEMTIME0 + TimeSpan.FromSeconds(systemTime);
        }
        public static ulong DateTimeToSystemTime(DateTime time)
        {
            return (ulong)(time - SYSTEMTIME0).TotalSeconds;
        }

        //public byte[] GetBuiltMask()
        //{
        //    int uSz = (int)header.mapUnitSize;
        //    int mSz = uSz >> 1;
        //    int maskIndex = 0;
        //    int maskTileSz = mSz * mSz;
        //    Bitmap bmpTile = createGreyscale(mSz, mSz);
        //    var rect = new Rectangle(0, 0, mSz, mSz);
        //    int halfHeight = (int)header.height >> 1;

        //    var interpolation = g.InterpolationMode;
        //    g.InterpolationMode = InterpolationMode.NearestNeighbor;

        //    int size = mSz * mSz;
        //    int lastY = header.mapHeight - 1;
        //    int lastSize = mSz * (halfHeight - lastY * mSz);
        //    //Console.WriteLine("starting...");

        //    foreach (var loc in new MapEnumerator(map, header.mapWidth))
        //    {

        //        //Console.Write("#");
        //        var bd = bmpTile.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

        //        maskTileSz = loc.Y < lastY ? size : lastSize;
        //        System.Runtime.InteropServices.Marshal.Copy(mask, maskIndex, bd.Scan0, maskTileSz);
        //        maskIndex += maskTileSz;

        //        bmpTile.UnlockBits(bd);

        //        bmpTile.RotateFlip(RotateFlipType.Rotate270FlipNone);//transposed

        //        g.DrawImage(bmpTile, loc.Y * uSz, header.width - (loc.X * uSz), uSz + 1, uSz + 1); //transposed

        //    }




        //    int w = (int)(header.width>>1);
        //    int h = (int)(header.height>>1);
        //    var build = new byte[];
        //}



        public List<Blob> TestBlobs()
        {

            if (isOpen)
            {
                //var bmp = CreateBitmap();
                //int w = bmp.Width;
                //int h = bmp.Height;
                //var rect = new Rectangle(0, 0, w,h);
                //var pix = bmp.LockBits(rect, ImageLockMode.ReadOnly, bmp.PixelFormat);
                //int sz = h * pix.Stride;
                //var byts = new byte[sz];
                //System.Runtime.InteropServices.Marshal.Copy(pix.Scan0, byts, 0, sz);
                //bmp.UnlockBits(pix);
                //bmp.Dispose();

                //var tmr = new System.Diagnostics.Stopwatch();
                //tmr.Start();
                //var ba = new Thresholder(true);

                //tmr.Stop();

                //Console.WriteLine("finding {0} blobs took {1}ms", ba.BlobList.Count, tmr.ElapsedMilliseconds);
                //return ba.BlobList;
            }
            return null;
        }


        //private static byte[] buildBrightnessLUT(float m)
        //{
        //    byte[] lut = new byte[256];
        //    int r = (int)(m * 65536f);
        //    for (int i = 0; i < 256; i++)
        //    {
        //        int n = i * r >> 16;
        //        lut[i] = (byte)(n < 256 ? n : 255);
        //    }
        //    return lut;
        //}
        private static int[] buildBitCountLUT()
        {
            int[] lut = new int[256];
            for (int i = 0; i < 256; i++)
            {
                int n = i, ct = 0;
                while (n != 0)
                {
                    n &= n - 1;
                    ct++;
                }
                lut[i] = ct;
            }
            return lut;
        }


        public void Dispose()
        {
            Close();
            clear();
        }


        T ByteArrayToStructure<T>(byte[] bytes) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(),
                typeof(T));
            handle.Free();
            return stuff;
        }
    }

    struct Location
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public int Offset { get; private set; }
        public Location(int x, int y, int offset)
            :this()
        {
            X = x;
            Y = y;
            Offset = offset;
        }
    }
    [StructLayout(LayoutKind.Explicit)]
    struct FileHeaderStructure
    {
        [FieldOffset(0)]
        public uint magicNumber;
        [FieldOffset(4)]
        public uint width;
        [FieldOffset(8)]
        public uint height;
        [FieldOffset(12)]
        public ushort bitsPerPixel;
        [FieldOffset(14)]
        public ushort bayerPattern;

        [FieldOffset(16)]
        public ushort mapWidth;
        [FieldOffset(18)]
        public ushort mapHeight;
        [FieldOffset(20)]
        public uint mapUnitSize;
        [FieldOffset(24)]
        public uint gridOffset;
        [FieldOffset(28)]
        public uint dataOffset;
        [FieldOffset(32)]
        public uint maskOffset;

        [FieldOffset(36)]
        public uint cameraID;
        [FieldOffset(40)]
        public uint exposure;
        [FieldOffset(44)]
        public uint frameStatus;
        [FieldOffset(48)]
        public uint cameraTick;
        [FieldOffset(52)]
        public uint systemTick;
        [FieldOffset(56)]
        public ulong systemTime; //64bit
        [FieldOffset(64)]
        public uint FrameCount;
    }
        

    class MapEnumerator:IEnumerator<Location>,IEnumerable<Location>
    {
        int width;
        int height;

        int index;
        int y;
        int x;
        int b;
        int offset;
        int bits;
        byte[] map;

        public MapEnumerator(byte[] map, int width)
        {
            this.map = map;
            this.width = width;
            int stride = (width + 7) >> 3; //width in bytes
            height = map.Length / stride;
            //count = map.Length * 8;
            Reset();
        }
                
        public bool MoveNext()
        {
            b++;
            while (y < height)
            {
                while (b < 8 & (bits & (1 << b)) == 0)
                    b++;

                if (b < 8) //we found a bit
                {
                    offset++;
                    return true;
                }
                else
                {
                    b = 0;
                    bits = 0;
                    while (y < height & bits == 0)
                    {
                        do
                        { 
                            index++; 
                            x += 8; 
                        } while (x < width && (bits = map[index]) == 0);

                        if (bits != 0)
                            break;

                        x = 0;
                        y++;
                    }
                }
            }
            return false;
        }

        public void Reset()
        {
            x = 0;
            y = 0;
            bits = 0;
            b = -1;
            offset = -1;
            index = -1;
        }

        public Location Current
        {
            get { return new Location(x+b, y, offset); }
        }
        object System.Collections.IEnumerator.Current
        {
            get { return new Location(x+b, y, offset); }
        }

        public void Dispose(){}




        public IEnumerator<Location> GetEnumerator()
        {
            return this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this;
        }
    }



}
