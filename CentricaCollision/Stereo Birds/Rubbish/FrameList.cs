﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Createc.StereoBirds.GroundStation
{
    
    public class FrameList : IEnumerator<FramePair>,IEnumerable<FramePair>
    {
        private List<int> pairIndexer = new List<int>();
        private List<int> frameIndexer = new List<int>();
        private List<FramePair> frames = new List<FramePair>();
        private List<int> indexer;
        private int index;

        public FrameList(List<FrameRef> masters, List<FrameRef> slaves)
        {
            pairFrames(masters, slaves);
            indexer = pairIndexer;
        }

        public FrameList(FrameList other)
        {
            pairIndexer = other.pairIndexer;
            frameIndexer = other.frameIndexer;
            frames = other.frames;
            indexer = other.indexer;
            index = other.index;
        }

        public bool SkipUnMatched
        {
            get { return indexer == pairIndexer; }
            set
            {
                if (SkipUnMatched != value)
                {
                    if (value)
                    {
                        indexer = pairIndexer;
                    }
                    else
                    {
                        indexer = frameIndexer;
                    }
                }
            }
        }

        public void MoveTo(long systemTime)
        {
            var fr = new FrameRef(systemTime);
            var fp = new FramePair(fr, fr);

            int i = frames.BinarySearch(fp);
            if (i < 0)
                i = -i - 1;

            i = indexer.BinarySearch(i);
            if (i < 0)
                i = -i - 1;

            if(i>=0)
                index=i;
        }

        //class FramePairComparer : IComparer<FramePair>
        //{
        //    public FramePairComparer(long systemTime,int[] indexer, List<FramePair> frames)
        //    {
        //    }
        //    //public int Compare(int x, int y)
        //    //{
        //    //    throw new NotImplementedException();
        //    //}

        //    public int Compare(FramePair x, FramePair y)
        //    {
        //        throw new NotImplementedException();
        //    }
        //}


        private void pairFrames(List<FrameRef> masters, List<FrameRef> slaves)
        {
            masters.Sort();//these should always be sorted....
            slaves.Sort();//...but there is no harm in making sure

            int m = 0;
            int s = 0;
            int msz = masters.Count;
            int ssz = slaves.Count;

            while (s < ssz & m < msz)
            {
                while (m < msz && masters[m].CompareTo(slaves[s]) < 0)
                {
                    frameIndexer.Add(frames.Count);
                    frames.Add(new FramePair(masters[m++], null));
                }
                while (s < ssz && slaves[s].CompareTo(masters[m]) < 0)
                {
                    frameIndexer.Add(frames.Count);
                    frames.Add(new FramePair(null, slaves[s++]));
                }
                while (s < ssz & m < msz && slaves[s].CompareTo(masters[m]) == 0)
                {
                    pairIndexer.Add(frames.Count);
                    frameIndexer.Add(frames.Count);
                    frames.Add(new FramePair(masters[m++], slaves[s++]));
                }
            }
        }

        public int Count
        {
            get { return indexer.Count; }
        }
        public int PairedFrameCount
        {
            get { return pairIndexer.Count; }
        }
        public int TotalFrameCount
        {
            get { return frameIndexer.Count; }
        }


        public FramePair Current
        {
            get { return frames[indexer[index]]; }
        }

        public void Dispose()
        {

        }

        object System.Collections.IEnumerator.Current
        {
            get { return frames[indexer[index]]; }
        }

        public bool MoveNext()
        {
            index++;
            return index < indexer.Count;
        }

        public void Reset()
        {
            index = -1;
        }





        public IEnumerator<FramePair> GetEnumerator()
        {
            return this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this;
        }
    }
}
