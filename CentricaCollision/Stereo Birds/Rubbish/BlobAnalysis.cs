﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace MapViewerOld
{
    class BlobAnalysis
    {
        private int height;
        private int width;

        private List<BlobX> blobList;
        private Slice[] current;
        private Slice[] previous;
        private int currentCt = 0;
        private int previousCt = 0;

        public List<BlobX> BlobList
        {
            get { return blobList; }
        }
        

        private BlobAnalysis(int width)
        {
            this.width = width;
            int sz = maxSlice(width);
            blobList = new List<BlobX>();
            current = new Slice[sz];
            previous = new Slice[sz];
        }

        public BlobAnalysis(byte[] image8bpp, int width,int threshold)
            :this(width)
        {
            height = image8bpp.Length / width;

            for (int y = 0, i = 0; y < height; y++)
            {
                
                bool over = false;
                for (int x = 0; x < width; x++, i++)
                {
                    if (over != image8bpp[i] > threshold)//threshold change
                    {
                        over = !over;
                        if (over) //starting a new slice
                        {
                            current[currentCt].x0 = x;
                        }
                        else //finishing slice
                        {
                            current[currentCt++].x1 = x;
                        }
                    }
                }
                if (over)//the scanline ended on a slice
                {
                    current[currentCt++].x1 = width;
                }
                matchSlices(y  );
            }
            CommitPrevious(height);//add the remaining blobs to the blob list
        }

        public BlobAnalysis(byte[] image1bpp, int width)
            : this(width)
        {
            int stride = (width + 7) >> 3;
            height = image1bpp.Length / stride;

            for (int y = 0, i = 0; y < height; y++)
            {

                bool over = false;
                for (int x = 0; x < width; x+=8, i++)
                {

                    int bits = image1bpp[i];
                    if (bits != 0 | over)
                    {
                        int n = 1, b = 0;
                        while (b < 8)
                        {
                            if (over != ((n & bits) != 0))//threshold change
                            {
                                over = !over;
                                if (over) //starting a new slice
                                {
                                    //current[currentCt].y = y;
                                    current[currentCt].x0 = x + b;
                                }
                                else //finishing slice
                                {
                                    current[currentCt++].x1 = x + b;
                                }
                            }
                            b++;
                            n <<= 1;
                        }
                    }
                }
                if (over)//the scanline ended on a slice
                {
                    current[currentCt++].x1 = width;
                }
                matchSlices(y);
            }
            CommitPrevious(height);//add the remaining blobs to the blob list
        }



        //private void threshold8bppScanline(int y)
        //{
        //    bool over = false;
        //    for (int x = 0; x < width; x++, i++)
        //    {
        //        if (over != image8bpp[i] > threshold)//threshold change
        //        {
        //            over = !over;
        //            if (over) //starting a new slice
        //            {
        //                //current[currentCt].y = y;
        //                current[currentCt].x0 = x;
        //            }
        //            else //finishing slice
        //            {
        //                current[currentCt++].x1 = x;
        //            }
        //        }
        //    }
        //    if (over)//the scanline ended on a slice
        //    {
        //        current[currentCt++].x1 = width;
        //    }
        //}


        private int maxSlice(int width)
        {
            return width + 1 >> 1;
        }



        void matchSlices(int y)
        {
            
            for (int c = 0, p = 0; c < currentCt; c++)
            {
                int x0 = current[c].x0;
                int x1 = current[c].x1;

                //we need to match up the slices from the previous scan with the slices on the current scan

                while (p < previousCt && previous[p].x1 <= x0) //while previous ends before current starts...
                    p++;//...increment previous

                //if we have not run out and the previous starts before the current ends we have a hit
                if (p < previousCt && previous[p].x0 < x1)//if previous starts before current ends (and previous ends before current starts)...
                {
                    //...we have a hit
                    BlobReference parent = previous[p].parent;
                    if (parent.Blob.Area == 0)
                    {
                        Console.Write('?');
                    }
                    current[c].parent = parent;
                    parent.Blob.Add(y, x0, x1);
                    p++;
                    
                    //any further previous slices that overlap get absorbed
                    while (p < previousCt && previous[p].x1 <= x1) //while previous finishes equal or before the current finish
                    {
                        //absorb and move to next previous
                        parent.Blob.Absorb(previous[p].parent.Blob);
                        previous[p].parent.Blob = parent.Blob; //update the blob not the parent 
                        p++;
                    }

                    if (p < previousCt && previous[p].x0 < x1) //if previous ends after current ends and starts before current finishes... 
                    {
                        //absorb but don't increment, previous might hit the next current
                        parent.Blob.Absorb(previous[p].parent.Blob);
                        previous[p].parent.Blob = parent.Blob; //update the blob not the parent 
                    }

                }
                else //no overlap with previous slices... 
                {
                    //...create a new blob and reference
                    BlobX blob = new BlobX();
                    blob.Add(y,x0,x1);
                    current[c].parent = new BlobReference(blob);
                }

            }

            CommitPrevious(y);

            Swap(ref current, ref previous);
            previousCt = currentCt;
            currentCt = 0;
        }


        /// <summary>
        /// Adds any finished blobs to the blob list 
        /// </summary>
        /// <param name="y">the current scanline</param>
        private void CommitPrevious(int y)
        {
            //int ct=0;
            for (int i = 0; i < previousCt; i++)
            {
                if (previous[i].parent != null)
                {
                    BlobX blob = previous[i].parent.Blob;

                    if (blob.YMax < y)
                    {
                        previous[i].parent.Blob = null;//!!!!!!!prob won't work!
                        if (blob.Area != 0)
                            blobList.Add(blob);
                        else
                            Console.Write('!');
                    }
                    //else
                    //{
                    //    ct++;
                    //}
                    //Console.WriteLine(" {0}", ct);
                }
                else
                {
                    Console.Write('?');
                }

            }
            //for (int i = 0; i < blobCt; i++)
            //{
            //    //if (blobBuffer[i].YMax < y)
            //    //{

            //    //}
            //}
        }


        void Swap<T>(ref T a, ref T b)
        {
            T t = a;
            a = b;
            b = t;
        }


        struct Slice
        {
            public int x0;
            public int x1;
            public BlobReference parent;
        }


        class BlobReference
        {
            public BlobX Blob { get; set; }
            public BlobReference(BlobX blob)
            {
                Blob = blob;
            }
        }


    }
    class BlobX
    {
        private int xMin;
        private int xMax;
        private int yMin;
        private int yMax;
        private int xSum;
        private int ySum;
        private int area;

        public BlobX()
        {
            Clear();
        }

        //public Blob(int y, Slice slice)
        //    : this()
        //{
        //    Add(y, slice.x0, slice.x1);
        //}

        //public void Add(int y, Slice slice)
        //{
        //    Add(y, slice.xStart, slice.xStop);
        //}

        public void Add(int y, int xStart, int xStop)
        {
            if (y < yMin)
                yMin = y;

            if (y > yMax)
                yMax = y;

            if (xStart < xMin)
                xMin = xStart;

            if (xStop > xMax)
                xMax = xStop;

            int w = xStop - xStart;
            ySum += w * y;
            xSum += w * xStart + ((w * (w - 1)) >> 1);
            area += w;
        }

        public void Absorb(BlobX other)
        {

            if (other.area == 0 || other == this)
                return;

            if (other.yMin < yMin)
                yMin = other.yMin;

            if (other.yMax > yMax)
                yMax = other.yMax;

            if (other.xMin < xMin)
                xMin = other.xMin;

            if (other.xMax > xMax)
                xMax = other.xMax;

            ySum += other.ySum;
            xSum += other.xSum;
            area += other.area;

            other.Clear();
        }

        public void Clear()
        {
            xMin = int.MaxValue;
            yMin = int.MaxValue;
            xMax = 0;
            yMax = 0;
            xSum = 0;
            ySum = 0;
            area = 0;
        }

        public int X
        {
            get { return xMin; }
        }
        public int Y
        {
            get { return yMin; }
        }
        public int XMax
        {
            get { return xMax; }
        }
        public int YMax
        {
            get { return yMax; }
        }
        public int Width
        {
            get { return xMax - xMin; }
        }
        public int Height
        {
            get { return yMax - yMin+1; }
        }
        public int Area
        {
            get { return area; }
        }
        public float CX
        {
            get { return (float)xSum / area; }
        }
        public float CY
        {
            get { return (float)ySum / area; }
        }
        public PointF Center
        {
            get { return new PointF(CX, CY); }
        }
        public Rectangle Bounds
        {
            get { return new Rectangle(xMin, yMin, Width, Height); }
        }
    }
}
