﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Createc.StereoBirds.GroundStation
{
    public struct FramePair : IComparable<FramePair>
    {
        private FrameRef master;
        private FrameRef slave;
        public FramePair(FrameRef master, FrameRef slave)
        {
            this.master = master;
            this.slave = slave;
        }
        public static readonly FramePair Empty = new FramePair();

        public FrameRef Master
        {
            get { return master; }
        }
        public FrameRef Slave
        {
            get { return slave; }
        }
        public long SystemTime
        {
            get { return master != null ? master.SystemTime : slave != null ? slave.SystemTime : 0; }
        }
        public long FrameNumber
        {
            get { return master != null ? master.FrameNumber : slave != null ? slave.FrameNumber : 0; }
        }

        public int CompareTo(FramePair other)
        {
            FrameRef o = other.master != null ? other.master : other.slave;
            FrameRef t = this.master != null ? this.master : other.slave;

            if (t != null)
                return t.CompareTo(o);
            else if (o != null)
                return -o.CompareTo(t);
            else
                return 0;
        }
    }
}
