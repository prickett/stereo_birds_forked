﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Createc.StereoBirds;
using Createc.Net.Rotation;
using Createc.Net.Vector;
using Createc.Net.Imaging;

//using Emgu.CV;
//using Emgu.CV.CvEnum;


namespace Createc.StereoBirds.Viewer
{
    public class StereoPair
    {

        StereoCalibration cal;
        Matrix3x3 matrixTrans;

        public StereoPair(StereoCalibration calibatration)
        {
            cal = calibatration;
        }

        public Coord2d ToSlavePt(Coord2d masterPt)
        {
            var q = pixel2normal(masterPt, cal.MasterCentre, cal.AngleOfResolution);
            return normal2pixel(matrixTrans.Hit(q), cal.SlaveCentre, cal.AngleOfResolution);
        }
        public Coord2d ToMasterPt(Coord2d slavePt)
        {
            var q = pixel2normal(slavePt, cal.SlaveCentre, cal.AngleOfResolution);
            return normal2pixel(cal.Matrix.Hit(q), cal.MasterCentre, cal.AngleOfResolution);
        }


        static Vector3d pixel2normal(Coord2d src, Coord2d centre, double aor)
        {
            double x, y, z, d;

            x = src.X - centre.X;
            y = src.Y - centre.Y;

            d = Math.Sqrt(x * x + y * y);
            if (d != 0)
            {
                var m = Math.Sin(d * aor) / d;
                z = Math.Cos(d * aor);
                x *= m;
                y *= m;
            }
            else
                z = 1;

            return new Vector3d(x, y, z);
        }

        static private Coord2d normal2pixel(Vector3d src, Coord2d centre, double aor)
        {
            double d = src.x * src.x + src.y * src.y;
            if (d != 0)
            {
                d = Math.Acos(src.z) / (Math.Sqrt(d) * aor);
            }
            return new Coord2d(
                centre.X + src.x * d,
                centre.Y + src.y * d);
        }
    }
}
