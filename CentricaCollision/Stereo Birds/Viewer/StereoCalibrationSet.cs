﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;



namespace Createc.StereoBirds.Viewer
{

    
    [Serializable]
    public class StereoCalibrationSet
    {
        List<StereoCalibration> set;

        public StereoCalibrationSet()
        {
            Console.WriteLine("Default set constructor");
            CameraPair = new CameraPair();

            //CameraPair = cameraPair;
            set = new List<StereoCalibration>();
        }

        public StereoCalibrationSet(CameraPair cameraPair)
        {
            CameraPair = cameraPair;
            set = new List<StereoCalibration>();
        }


        public CameraPair CameraPair { get; set; }

        public List<StereoCalibration> CalibrationList { get { return set; } set { set = value; } }

        /// <summary>
        /// Inserts a new calibration into the sorted set. Ensures that the set is always sorted and contains no null members
        /// </summary>
        /// <param name="calibration">The calibration to add</param>
        public void Add(StereoCalibration calibration)
        {
            if (calibration == null)
                throw new ArgumentNullException();


            int i = set.BinarySearch(calibration, new TimeComparer());

            if (i < 0) //not present in set ~i = next largest (or count if largest)
            {
                set.Insert(~i, calibration);
            }
            else //for now always replace... assume new calibration is better
            {
                set[i] = calibration;
            }
            //else //compare both calibrations... warning if different, replace with best
            //{
            //    Console.WriteLine("Calibration already exists for this timeframe");
            //    DebugMatrix(set[i].Matrix);
            //    DebugMatrix(calibration.Matrix);

            //    if (set[i].Error > calibration.Error)
            //        set[i] = calibration;

            //}

        }

        /// <summary>
        /// Finds the nearest calibration file to the given systemTime. Returns null if the set is empty.
        /// </summary>
        public StereoCalibration FindNearest(TimeStamp systemTime)
        {
            int i = FindNearestIndex(new StereoCalibration(systemTime));
            StereoCalibration c = null;
            if (i < set.Count)
            {
                if (i > 0)
                {

                    c = (systemTime.Seconds() - set[i - 1].SystemTime.Seconds() < set[i].SystemTime.Seconds() - systemTime.Seconds()) ? set[i - 1] : set[i];
                }
                else
                {
                    c = set[0];
                }
            }
            else if (i > 0)
            {
                c = set[i - 1];
            }
            return c;
        }

        int FindNearestIndex(StereoCalibration c)
        {
            int i = set.BinarySearch(c, new TimeComparer());
            return i < 0 ? ~i : i;
        }

        class TimeComparer : IComparer<StereoCalibration>
        {
            public int Compare(StereoCalibration a, StereoCalibration b)
            {
                return a.SystemTime.CompareTo(b.SystemTime);
            }
        }

        public void Debug()
        {
            Console.Write("\nCalibration Set\n");
            CameraPair.Debug();
            foreach (var c in set)
                c.Debug();
        }

        //public static void DebugMatrix(Matrix3x3 m)
        //{
        //    Console.Write("\n{0:f5}\t{1:f5}\t{2:f5}\n{3:f5}\t{4:f5}\t{5:f5}\n{6:f5}\t{7:f5}\t{8:f5}\n",
        //        m.m11, m.m21, m.m31, m.m12, m.m22, m.m32, m.m13, m.m23, m.m33);
        //}
    }
}
