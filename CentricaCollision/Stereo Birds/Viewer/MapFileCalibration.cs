﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Createc.StereoBirds;
using Createc.Net.Rotation;
using Createc.Net.Vector;
using Createc.Net.Imaging;
using Createc.Net.Imaging.Fisheye;


//using Emgu.CV;
//using Emgu.CV.CvEnum;



namespace Createc.StereoBirds.Viewer
{

    
    public class MapFileCalibration
    {
        //long timeFrom; //posix time
        //long timeTo;

        Vector3d offset;
        Matrix3x3 toSlave;
        Matrix3x3 toMaster;
        Fisheye master;
        Fisheye slave;
        bool calibrated;

        RegionOfInterest roi;


        public MapFileCalibration(Vector3d slaveOffset, int width, int height)
        {
            Width = width;
            Height = height;
            offset = slaveOffset;
            Coord2d c = new Coord2d(width >> 1, height >> 1);

            double a = (Math.PI / Math.Sqrt(width * width + height * height)) * .955;
            Console.WriteLine("aor {0}",a);
            slave = new Fisheye(c, a);
            master = new Fisheye(c, a);
            calibrated = false;

            roi = new RegionOfInterest(2024, 1024);
        }

        public int Width{get;private set;}
        public int Height { get; private set; }

        public Fisheye Master { get { return master; } }
        public Fisheye Slave { get { return slave; } }

        public void Calibrate(List<CoordPair> points)
        {
            //toSlave = FindRotation(master, slave, points);
            //
            //Console.WriteLine(Determinate(toSlave));
            //DebugMatrix (toSlave);
            //Console.WriteLine(Determinate(toMaster));
            //DebugMatrix(toMaster);
            //var m = toMaster * toSlave;
            //DebugMatrix(m);



           

            FindCentre(points);
            toMaster = toSlave.Transpose_();
            double error = RotationError(toSlave, master, slave, points);
            Console.WriteLine(error);
        }

        double GetRandom(Random random, double range)
        {
            return ((random.NextDouble() * 2.0) - 1.0) * range; 
        }

        public System.Drawing.Bitmap CreateCalbrationBitmap(List<CoordPair> points, int side)
        {

            

            int[] buf = new int[side * side];


            Fisheye m = master, s = slave;
            //m.Centre = new Coord2d(m.Centre.X - 4, m.Centre.Y +24);

            Coord2d mc = m.Centre, sc = s.Centre;

            Console.WriteLine("({0:f3},{1:f3})\t({2:f3},{3:f3})",mc.X,mc.Y,sc.X,sc.Y);
            Coord2d mBest = mc, sBest = sc;
            double scl = 64.0 / side;

            double error,min = double.MaxValue;

            for (int y = 0; y < side; y++)
            {
                int i = y * side;
                double oy = ((y << 1) - side) * scl;
                for (int x = 0; x < side; x++)
                {
                    double ox = ((x << 1) - side) * scl;
                    s.Centre = new Coord2d(sc.X + ox, sc.Y + oy);
                    error = CalibrationError(m, s, points) * 0.1;
                    if (error < 1)
                        buf[x + i] += (int)(255.0 * (1.0 - error)) * 0x10101;

                    if (error < min)
                    {
                        min = error;
                        sBest = s.Centre;
                    }
                }
            }


            s.Centre = sc = sBest;
            //min = double.MaxValue;

            //for (int y = 0; y < side; y++)
            //{
            //    int i = y * side;
            //    double oy = ((y << 1) - side) * scl;
            //    for (int x = 0; x < side; x++)
            //    {
            //        double ox = ((x << 1) - side) * scl;
            //        m.Centre = new Coord2d(mc.X + ox, mc.Y + oy);
            //        error = CalibrationError(m, s, points) * 0.1;
            //        if (error < 1)
            //            buf[x + i] += (int)(255.0 * (1.0 - error)) * 0x100;

            //        if (error < min)
            //        {
            //            min = error;
            //            mBest = m.Centre;
            //        }
            //    }
            //}
            //m.Centre = mc = mBest;
            Console.WriteLine("({0:f3},{1:f3})\t({2:f3},{3:f3})", mc.X, mc.Y, sc.X, sc.Y);
            Console.WriteLine("({0:f3},{1:f3})", mc.X-sc.X, mc.Y-sc.Y);


            var format = System.Drawing.Imaging.PixelFormat.Format32bppRgb;
            var bitmap = new System.Drawing.Bitmap(side, side, format);
            var rect = new System.Drawing.Rectangle(0, 0, side, side);

            var data = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.WriteOnly, format);
            System.Runtime.InteropServices.Marshal.Copy(buf, 0, data.Scan0, buf.Length);
            bitmap.UnlockBits(data);
            return bitmap;
        }

        public System.Drawing.Bitmap CreateCalbrationBitmap2(List<CoordPair> points, int side)
        {         
            int[] buf = new int[side*side];
            Fisheye m = master, s = slave;
            Coord2d mc = m.Centre, sc = s.Centre;
            Coord2d mBest = mc, sBest = sc;
            double scl = 128.0 / side;
            for (int y = 0; y < side; y++)
            {
                int i = y * side;
                double oy = ((y << 1) - side) * scl;
                for (int x = 0; x < side; x++)
                {
                    double ox = ((x << 1) - side) * scl;
                    s.Centre = new Coord2d(sc.X + ox, sc.Y + oy);
                    double error = CalibrationError(m, s, points);
                    error *= 0.01;
                    if (error < 1)
                        buf[x + i] =  (int)(255.0 * (1.0 - error)) * 0x10101;
                }
            }

            var format = System.Drawing.Imaging.PixelFormat.Format32bppRgb;
            var bitmap = new System.Drawing.Bitmap(side, side, format);
            var rect = new System.Drawing.Rectangle(0, 0, side, side);

            var data = bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.WriteOnly, format);
            System.Runtime.InteropServices.Marshal.Copy(buf, 0, data.Scan0, buf.Length);
            bitmap.UnlockBits(data);
            return bitmap;
        }



        void FindCentre(List<CoordPair> points, double distance = 64)
        {
            Fisheye m = master, s = slave;
            Coord2d mc = m.Centre, sc = s.Centre;
            Coord2d mBest = mc, sBest = sc;
            Coord2d[] offsets = getOffsets();
            double error, min = double.MaxValue;

            min = CalibrationError(m, s, points);
            do
            {
                bool found = false;
                foreach (var so in offsets)
                {
                    double x = so.X * distance, y = so.Y * distance;
                    m.Centre = new Coord2d(mc.X - x, mc.Y - y);
                    s.Centre = new Coord2d(sc.X + x, sc.Y + y);
                    if ((error = CalibrationError(m, s, points)) < min)
                    {
                        min = error;
                        sBest = s.Centre;
                        mBest = m.Centre;
                        found = true;
                    }
                }

                mc = mBest;
                sc = sBest;
                if (!found)
                    distance *= 0.5;

            } while (distance > 0.01);

            //Console.Write('\n');

            //Console.WriteLine(min);
            master.Centre = mBest;
            slave.Centre = sBest;

            Console.WriteLine("({0:f2},{1:f2}) ({2:f2},{3:f2})", mBest.X, mBest.Y, sBest.X, sBest.Y);
            Console.WriteLine("({0:f3},{1:f3})", mc.X - sc.X, mc.Y - sc.Y);
            toSlave = FindRotation(master, slave, points);
            toMaster = toSlave.Inverse();
        }


        //As Alan and John have said, your three-dimensional lines may not intersect. In general you can find the two points, 
        //A0 and B0, on the respective lines A1A2 and B1B2 which are closest together and determine if they coincide or else
        //see how far apart they lie.

        //The following is a specific formula using matlab for determining these closest points A0 and B0 in terms of vector
        //cross products and dot products. Assume that all four points are represented by three-element column vectors or by
        //three-element row vectors. Do the following:

        //nA = dot(cross(B2-B1,A1-B1),cross(A2-A1,B2-B1));
        //nB = dot(cross(A2-A1,A1-B1),cross(A2-A1,B2-B1));
        //d = dot(cross(A2-A1,B2-B1),cross(A2-A1,B2-B1));
        //A0 = A1 + (nA/d)*(A2-A1);
        //B0 = B1 + (nB/d)*(B2-B1);

        //As Alan mentioned, it is evident by its nature that A0 = A1+t*(A2-A1) with t = nA/d a scalar, must fall on the line
        //A1A2, and similarly for B0 = B1+s*(B2-B1) with scalar s = nB/d. You can easily verify that vector A0-B0 is orthogonal
        //to both A2-A1 and B2-B1, which establishes that A0 and B0 are the closest possible pair of points on the two lines. 
        //This can be done by evaluating dot(A0-B0,A2-A1) and dot(A0-B0,B2-B1) for various random four-point sets and showing
        //that each dot product is zero, allowing of course for very small round-off errors.

        //The one case where the above code becomes indeterminate occurs when the two lines are parallel, in which case there
        //are infinitely many possible pairs for A0 and B0. An arbitrary plane orthogonal to the lines will cut them in a valid
        //pair A0 and B0. For such a case you will get a zero divided by zero situation in the above code which produces a NaN.


        public Vector3d LineInterSection3D(Coord2d master, Coord2d slave, out double fit)
        {
            //offset *= -1.0;

            var mv = pixel2normal(master, this.master) * 1000.0;
            var sv = toMaster.Hit(pixel2normal(slave, this.slave) * 1000.0).Subtract(offset);
            var ms = offset * -1.0;
            var x = mv.Cross(sv);

            //offset *= -1.0;

            double m = mv.Cross(ms).Dot(x);
            double s = sv.Cross(ms).Dot(x);
            double d = mv.Cross(sv).Dot(x);

            var mi = mv * (m / d);
            var si = offset.Add(sv * (s / d));

            fit = mi.Subtract(si).DistanceSquared();
            return mi.Add(si) * 0.5;            
        }


        static void LineIntersect3D(Vector3d a1, Vector3d a2, Vector3d b1, Vector3d b2, out Vector3d a0, out Vector3d b0)
        {
            a2 = a2.Subtract(a1);
            b2 = b2.Subtract(b1);
            var ab = a1.Subtract(b1);
            var x = a2.Cross(b2);

            var na = b2.Cross(ab).Dot(x);
            var nb = a2.Cross(ab).Dot(x);
            var d = a2.Cross(b2).Dot(x);

            a0 = a1.Add(a2 * (na / d));
            b0 = b1.Add(b2 * (nb / d));
        }

 




        






    

        public Coord2d ToSlavePt(Coord2d masterPt)
        {
            var q = pixel2normal(masterPt, master);
            return normal2pixel(toSlave.Hit(q),slave);
        }
        public Coord2d ToMasterPt(Coord2d slavePt)
        {
            var q = pixel2normal(slavePt, slave);
            return normal2pixel(toMaster.Hit(q), master);
        }

        static double CalibrationError(Fisheye master, Fisheye slave, List<CoordPair> points)
        {
            return RotationError(FindRotation(master, slave, points), master, slave, points);
        }





        #region statics
        
        static Coord2d[] getOffsets()
        {
            double h = Math.Sqrt(0.75);
            return new Coord2d[] {           
                new Coord2d(1,0),               
                new Coord2d(0.5,h),               
                new Coord2d(-0.5,h),               
                new Coord2d(-1,0),               
                new Coord2d(-0.5,-h),               
                new Coord2d(0.5,-h)
            };
        }

        static double RotationError(Matrix3x3 matrix, Fisheye master, Fisheye slave, List<CoordPair> points)
        {
            if (points == null || points.Count == 0)
                return double.PositiveInfinity;
            double error = 0;
            foreach (var p in points)
            {
                var v = pixel2normal(p.Master, master);
                var t = normal2pixel(matrix.Hit(v), slave);
                double dx = t.X - p.Slave.X;
                double dy = t.Y - p.Slave.Y;
                error += dx * dx + dy * dy;
            }
            return error / points.Count;
        }

        static Matrix3x3 FindRotation(Fisheye master, Fisheye slave, List<CoordPair> points)
        {
            int count = points.Count;
            Matrix3x3 matrix;
            if (count < 3)
                throw new ArgumentException("Calibration requires at least three points from each frame");

            double[] masterPts = new double[3 * count];
            double[] slavePts = new double[3 * count];

            //Convert the passed points to 3d vectors and add to each
            for (int i = 0; i < count; i++)
            {
                Vector3d v;

                v = pixel2normal(points[i].Master, master);
                int j = i * 3;
                masterPts[j + 0] = v.x;
                masterPts[j + 1] = v.y;
                masterPts[j + 2] = v.z;

                v = pixel2normal(points[i].Slave, slave);
                slavePts[j + 0] = v.x;
                slavePts[j + 1] = v.y;
                slavePts[j + 2] = v.z;
            }

            // make our call to openCV SVD
            matrix = Unmanaged.FindRotation(masterPts, slavePts);




            ////The Emgu matrix object uses unmanaged resources and requires disposal
            //using (Matrix<double>
            //    mM = new Matrix<double>(3, count),
            //    mS = new Matrix<double>(count, 3),
            //    mW = new Matrix<double>(3, 3),
            //    mU = new Matrix<double>(3, 3),
            //    mV = new Matrix<double>(3, 3))
            //{
            //    //Convert the passed points to 3d vectors and add to each
            //    //matrix, the master vector matrix is built transposed.
            //    for (int i = 0; i < count; i++)
            //    {
            //        Vector3d v;

            //        v = pixel2normal(points[i].Master, master);
            //        mM[0, i] = v.x;
            //        mM[1, i] = v.y;
            //        mM[2, i] = v.z;

            //        v = pixel2normal(points[i].Slave, slave);
            //        mS[i, 0] = v.x;
            //        mS[i, 1] = v.y;
            //        mS[i, 2] = v.z;
            //    }

            //    using (var mA = mM * mS)//find the covariance matrix
            //    {
            //        //perform single value decomposition
            //        CvInvoke.cvSVD(mA, mW, mU, mV, SVD_TYPE.CV_SVD_DEFAULT);

            //    }
            //    using (var mUt = mU.Transpose())//transpose the U parameter
            //    {
            //        using (var mR = mV * mUt)//find the rotation matrix
            //        {                                              
            //            //DebugEmguMatrix(mR);
            //            //Console.WriteLine(mR.Det);

            //            matrix = new Matrix3x3(
            //                mR[0, 0], mR[0, 1], mR[0, 2],
            //                mR[1, 0], mR[1, 1], mR[1, 2],
            //                mR[2, 0], mR[2, 1], mR[2, 2]);
            //        }
            //    }
            //}
            return matrix;
        }


        static Vector3d pixel2normal(Coord2d src, Fisheye view)
        {
            double x, y, z, d;

            x = src.X - view.Centre.X;
            y = src.Y - view.Centre.Y;

            d = Math.Sqrt(x * x + y * y);
            if (d != 0)
            {
                var m = Math.Sin(d * view.AngleOfResolution) / d;
                z = Math.Cos(d * view.AngleOfResolution);
                x *= m;
                y *= m;
            }
            else
                z = 1;

            return new Vector3d(x, y, z);
        }

        static private Coord2d normal2pixel(Vector3d src, Fisheye view)
        {
            double d = src.x * src.x + src.y * src.y;
            if (d != 0)
            {
                d = Math.Acos(src.z) / (Math.Sqrt(d) * view.AngleOfResolution);
            }
            return new Coord2d(
                view.Centre.X + src.x * d,
                view.Centre.Y + src.y * d);
        }

        #endregion
        #region debugging

        public System.Drawing.Bitmap GetView(Coord2d p, System.Drawing.Bitmap bitmap)
        {
            var buf = Get8ppBytes(bitmap);
            var i = new Image(bitmap.Width, bitmap.Height, PixelTypes.Grey8, buf);
            var f = new FishEyeImage(i, master.AngleOfResolution, master.Centre);
            roi.SetLook(p, f);
            var img = roi.GetROI(f);
            int w = roi.Size.Width;
            int h = roi.Size.Height;
            var bmp = new System.Drawing.Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format8bppIndexed);
            var d = bmp.LockBits(new System.Drawing.Rectangle(0, 0, w, h), System.Drawing.Imaging.ImageLockMode.ReadWrite, bmp.PixelFormat);
            System.Runtime.InteropServices.Marshal.Copy(img.Buffer, 0, d.Scan0, img.Size);
            bmp.UnlockBits(d);

            var pal = bmp.Palette;
            var arr = pal.Entries;
            for (int j = 0; j < 256; j++)
                arr[j] = System.Drawing.Color.FromArgb((j * 0x10101) | -16777216);
            bmp.Palette = pal;

            return bmp;
        }

        static byte[] Get8ppBytes(System.Drawing.Bitmap b)
        {
            if (b != null)
            {
                int w = b.Width;
                int h = b.Height;
                int s = (w + 3) & -4;
                var r = new System.Drawing.Rectangle(0, 0, w, h);
                var d = b.LockBits(r, System.Drawing.Imaging.ImageLockMode.ReadWrite, b.PixelFormat);

                int sz = 0, chan = 0;
                var buffer = new byte[s * h];
                switch (d.PixelFormat)
                {
                    case System.Drawing.Imaging.PixelFormat.Format8bppIndexed:
                        sz = 1;
                        chan = 0;
                        break;
                    case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
                        sz = 3;
                        chan = 1;
                        break;
                    case System.Drawing.Imaging.PixelFormat.Format32bppRgb:
                        sz = 4;
                        chan = 1;
                        break;
                    default:
                        return null;
                }

                unsafe
                {
                    fixed (byte* target = buffer)
                    {
                        byte* source = (byte*)d.Scan0;
                        for (int y = 0; y < h; y++)
                        {
                            var ps = source + (chan + y * d.Stride);
                            var pt = target + (y * s);
                            for (int x = 0; x < w; x++, pt++, ps += sz)
                            {
                                *pt = *ps;
                            }
                        }
                    }
                }

                b.UnlockBits(d);
                return buffer;
            }
            return null;
        }

        public static void DebugMatrix(Matrix3x3 m)
        {
            Console.Write("\n{0:f5}\t{1:f5}\t{2:f5}\n{3:f5}\t{4:f5}\t{5:f5}\n{6:f5}\t{7:f5}\t{8:f5}\n",
                m.m11, m.m21, m.m31, m.m12, m.m22, m.m32, m.m13, m.m23, m.m33);
        }

        //public static void DebugEmguMatrix<T>(Matrix<T> m) where T : new()
        //{
        //    var d = m.Data;
        //    Console.Write("\n");
        //    for (int y = 0, h = d.GetLength(0); y < h; y++)
        //    {
        //        for (int x = 0, w = d.GetLength(1); x < w; x++)
        //        {
        //            Console.Write("{0:F5}\t", d[y, x]);
        //        }
        //        Console.Write("\n");
        //    }
        //}

        #endregion
        #region scratch

        void FindCentre3(List<CoordPair> points, double distance = 32)
        {
            Fisheye m = master, s = slave;
            Coord2d mc = m.Centre, sc = slave.Centre;
            Coord2d mBest = mc, sBest = sc;
            Coord2d[] offsets = getOffsets();
            double md = distance, sd = distance;

            double error, min = double.MaxValue;

            for (int i = 0; i < 10; i++)
            {
                min = double.MaxValue;
                md = distance;
                do
                {
                    foreach (var mo in offsets)
                    {
                        m.Centre = new Coord2d(mc.X + mo.X * md, mc.Y + mo.Y * md);
                        if ((error = CalibrationError(m, s, points)) < min)
                        {
                            min = error;
                            mBest = m.Centre;
                        }
                    }
                    if (mBest == mc) md *= 0.5; else mc = mBest;
                } while (md > 1);


                min = double.MaxValue;
                sd = distance;
                do
                {
                    foreach (var so in offsets)
                    {
                        s.Centre = new Coord2d(sc.X + so.X * sd, sc.Y + so.Y * sd);
                        if ((error = CalibrationError(m, s, points)) < min)
                        {
                            min = error;
                            sBest = s.Centre;
                        }
                    }

                    if (sBest == sc) sd *= 0.5; else sc = sBest;
                } while (sd > 1);

                distance *= 0.5;
            }


            master.Centre = mBest;
            slave.Centre = sBest;
            Console.WriteLine("({0:f2},{1:f2}) ({2:f2},{3:f2})\n", mBest.X, mBest.Y, sBest.X, sBest.Y);
            toSlave = FindRotation(master, slave, points);


        }
        void FindCentre2(List<CoordPair> points, double distance = 32)
        {
            Fisheye m = master, s = slave;
            Coord2d mc = m.Centre, sc = slave.Centre;
            Coord2d mBest = mc, sBest = sc;
            Coord2d[] offsets = getOffsets();
            double md = distance, sd = distance;

            double error, min = double.MaxValue;

            do
            {
                foreach (var mo in offsets)
                {
                    m.Centre = new Coord2d(mc.X + mo.X * md, mc.Y + mo.Y * md);
                    foreach (var so in offsets)
                    {
                        s.Centre = new Coord2d(sc.X + so.X * sd, sc.Y + so.Y * sd);
                        if ((error = CalibrationError(m, s, points)) < min)
                        {
                            //Console.Write('#');
                            min = error;
                            mBest = m.Centre;
                            sBest = s.Centre;
                        }
                        //Console.Write("{0:f1}\t", error);
                    }
                    //Console.Write('\n');
                }
                //Console.WriteLine("({0:f2},{1:f2}) ({2:f2},{3:f2})\n", mBest.X, mBest.Y, sBest.X, sBest.Y);

                if (mBest == mc) md *= 0.5; else mc = mBest;
                if (sBest == sc) sd *= 0.5; else sc = sBest;
            } while (md > 1 || sd > 1);



            master.Centre = mBest;
            slave.Centre = sBest;
            Console.WriteLine("({0:f2},{1:f2}) ({2:f2},{3:f2})\n", mBest.X, mBest.Y, sBest.X, sBest.Y);
            toSlave = FindRotation(master, slave, points);

        }

        #endregion



    }

    



    public struct CoordPair
    {
        public CoordPair(Coord2d master, Coord2d slave)
        {
            m = master;
            s = slave;
        }
        Coord2d m, s;
        public Coord2d Master { get { return m; } set { m = value; } }
        public Coord2d Slave { get { return s; } set { s = value; } }
    }

    public struct Fisheye
    {
        public Fisheye(Coord2d centre, double angleOfResolution)
        {
            cen = centre;
            aor = angleOfResolution;
        }
        Coord2d cen;
        double aor;
        public Coord2d Centre { get { return cen; } set { cen = value; } }
        public double AngleOfResolution { get { return aor; } set { aor = value; } }
    }
}
