﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.InteropServices;

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

using Createc.Net.Vector;
using Createc.Net.Rotation;

namespace Createc.StereoBirds.Viewer
{
    class Unmanaged
    {
        public enum BayerPattern { BGGR, GBRG, GRBG, RGGB };


        [DllImport("StereoBirdsUtility64.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint="FindRotation")]
        private static unsafe extern int FindRotation64(double* pointsA, double* pointsB, int count, double* matrix3x3);

        [DllImport("StereoBirdsUtility32.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "FindRotation")]
        private static unsafe extern int FindRotation32(double* pointsA, double* pointsB, int count, double* matrix3x3);

        [DllImport("StereoBirdsUtility64.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "Demosaic")]
        private static unsafe extern int Demosaic64(int width, int height, byte* source_data, byte* target_data, 
            int source_stride, int target_stride, BayerPattern pattern, int target_bits_per_pixel);

        [DllImport("StereoBirdsUtility32.dll", CallingConvention = CallingConvention.Cdecl, EntryPoint = "Demosaic")]
        private static unsafe extern int Demosaic32(int width, int height, byte* source_data, byte* target_data,
            int source_stride, int target_stride, BayerPattern pattern, int target_bits_per_pixel);


        public static unsafe Matrix3x3 FindRotation(double[] pointsA, double[] pointsB)
        {  
            int count = Math.Min(pointsA.Length,pointsB.Length) / 3; 
            int retval;
            double[] rotation = new double[9];

            fixed (double* S = pointsA)
            fixed (double* M = pointsB)
            fixed (double* R = rotation)
            { 
                if(IntPtr.Size == 8)
                    retval = Unmanaged.FindRotation64(M, S, count, R);
                else
                    retval = Unmanaged.FindRotation32(M, S, count, R);        
            }

            if(retval < 0)
                throw new ArgumentException();

            

            return new Matrix3x3(
                rotation[0], rotation[3], rotation[6],
                rotation[1], rotation[4], rotation[7],
                rotation[2], rotation[5], rotation[8]);     
        }


        public static unsafe bool Demosaic(int width, int height, byte* source_data, byte* target_data, int source_stride, int target_stride, BayerPattern pattern, int target_bits_per_pixel)
        {
            int retval;
            if (IntPtr.Size == 8)
                retval = Demosaic64(width, height, source_data, target_data, source_stride, target_stride, pattern, target_bits_per_pixel);
            else // assuming 32 bit 
                retval = Demosaic32(width, height, source_data, target_data, source_stride, target_stride, pattern, target_bits_per_pixel);
            return retval >=0;
        }

        //public static unsafe bool Demosaic(Bitmap source, Bitmap target, BayerPattern pattern, int target_bits_per_pixel)
        //{
        //    int retval;
        //    if (IntPtr.Size == 8)
        //        retval = Demosaic64(width, height, source_data, target_data, source_stride, target_stride, pattern, target_bits_per_pixel);
        //    else
        //        retval = Demosaic64(width, height, source_data, target_data, source_stride, target_stride, pattern, target_bits_per_pixel);
        //    return retval >= 0;
        //}


        // Demosaic(d.Scan0,            d.Width,    d.Height,   d.Stride,   image.Buffer,       image.Stride,    0);
        // Demosaic((byte*)sd.Scan0,    sd.Width,   sd.Height,  sd.Stride,  (byte*)bd.Scan0,    bd.Stride,   0);
        // Demosaic(d.Scan0,            d.Width,    d.Height,   d.Stride,   image.Buffer,       image.Stride,    0);

        // bayerTile = Demosaic8bpp(bayerTile, uSz, tileHeight, bayerOrder); //Avt
    }
}
