﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using CustomControls;

using Createc.Net.Imaging;
using Createc.Imaging.Thresholds;
using Createc.StereoBirds;


namespace Createc.StereoBirds.Viewer
{
    enum Mode
    {
        Main,
        CalibrationSelect,
        CalibrationCheck
    }

    public partial class mainFormOld : Form
    {
        static Random random = new Random();

        MapFileCalibration calibration;
        MapFileAcquisition aquisition; 
        MapFileBirdSearch birdSearch;
        StereoCalibrationSet calibrationSet;

        public mainFormOld()
        {
            InitializeComponent();

            playImageButton.Tag = MapFileRenderer.RenderModes.Image;
            playBayerButton.Tag = MapFileRenderer.RenderModes.Bayer;
            playMaskButton.Tag = MapFileRenderer.RenderModes.Mask;
            playMapButton.Tag = MapFileRenderer.RenderModes.Map;
            
            //renderer = new MapFileRenderer();
            bayerUpDown.Value = 2;
            //renderer.RenderMode = MapFileRenderer.RenderModes.Image;

            aquisition = new MapFileAcquisition();
            aquisition.OnFrameBatch += OnFrameBatch;
            aquisition.OnProgressChange += OnAquisitionProgressChange;
            aquisition.OnFinish += OnAquisitionFinish;
            SetAquisitionButtons(false);

            //birdSearch = new MapFileBirdSearch();
            birdSearch.OnBirdPossible += OnBirdPossible;
            birdSearch.OnProgressChange += OnBirdSearchProgressChange;
            birdSearch.OnFinish += OnBirdSearchFinish;

            
            birdConfirmPanel.Visible = false;
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            calibrationSet = UserData.LoadCalibrationSet();
        }

        private void mainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            UserData.SaveCalibrationSet(calibrationSet);
        }

        private void mainForm_Closing(object sender, FormClosingEventArgs e)
        {
            if (birdSearch.Busy || aquisition.Busy)
            {
                var result = MessageBox.Show("The application is busy.\nDo you want to force quit?",
                    "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                if (result == DialogResult.OK)
                {
                    aquisition.Quit(); //close any active threads
                    birdSearch.Quit();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }



        #region mess
        private void button1_Click(object sender, EventArgs e)
        {
            var p = @"D:\_1340000000\_1346700000\_1346741000\";
            var m = new FrameRef(p + "S_1346741240_1005568718_2432032_[2581].bmp");
            var s = new FrameRef(p + "M_1346741240_1005568711_2432032_[3032].bmp");
            var fp = new FramePair(m, s);

            renderFullFramePair(fp);

            calibration = new MapFileCalibration(new Net.Vector.Vector3d(1.395, 0, 0), 
                calibrationPanel.MasterView.Image.Width, 
                calibrationPanel.MasterView.Image.Height);

            var mm = calibrationPanel.MasterMarkers;
            mm.Clear();
            mm.Add(1469.46 * 2, 1640.37 * 2);
            mm.Add(1465.02 * 2, 725.93 * 2);
            mm.Add(320.57 * 2, 1450.41 * 2);
            mm.Add(34.88 * 2, 1398.65 * 2);
            mm.Add(194.69 * 2, 951.31 * 2);
            mm.Add(1337.35 * 2, 456.67 * 2);

            var sm = calibrationPanel.SlaveMarkers;
            sm.Clear();
            sm.Add(1474.46 * 2, 1663.65 * 2);
            sm.Add(1481.66 * 2, 745.18 * 2);
            sm.Add(330.62 * 2, 1457.44 * 2);
            sm.Add(43.94 * 2, 1405.51 * 2);
            sm.Add(207.10 * 2, 959.61 * 2);
            sm.Add(1354.23 * 2, 473.39 * 2);

            calibrationPanel.Invalidate();
        }

        private void CalibrateButton_Click(object sender, EventArgs e)
        {
            var slaveMarks = calibrationPanel.SlaveMarkers;
            var masterMarks = calibrationPanel.MasterMarkers;

            var s = slaveMarks.CurrentMarkers.ToArray();
            var m = masterMarks.CurrentMarkers.ToArray();
            var ct = Math.Min(s.Length,m.Length);
            var pts = new List<CoordPair>(ct);
            for (int i = 0; i < ct; i++)
            {
                pts.Add(new CoordPair(m[i], s[i]));
            }

            //AssignView(slaveView, calibration.CreateCalbrationBitmap(pts, 64));
            
          
            calibration.Calibrate(pts);
            //slaveMarks.Clear();

            for (int i = 0; i < ct; i++)
            {
                var mp = pts[i].Master;
                var sp = calibration.ToSlavePt(mp);
                slaveMarks.Add(sp);
            }

            //var mc = calibration.Master.Centre;
            //masterMarks.Add(mc.X, mc.Y);

            //var sc = calibration.Slave.Centre;
            //slaveMarks.Add(sc.X, sc.Y);



            //slaveView.Invalidate();

            Thread.Sleep(500);
            masterMarks.Clear();
            slaveMarks.Clear();
            //masterView.Invalidate();
            //slaveView.Invalidate();
            calibrationPanel.Invalidate();

        }

        private void CheckSyncButton_Click(object sender, EventArgs e)
        {
            var mm = calibrationPanel.MasterMarkers;
            var sm = calibrationPanel.SlaveMarkers;
            var ma = mm.CurrentMarkers.ToArray();
            var sa = sm.CurrentMarkers.ToArray();
            Console.WriteLine("{0} markers", ma.Length + sa.Length);

            mm.Clear();
            sm.Clear();

            foreach (var p in ma)
            {
                var m = new Coord2d(p.X, p.Y);
                var s = calibration.ToSlavePt(new Coord2d(p.X,p.Y));
                sm.Add(s.X, s.Y);
                mm.Add(m.X, m.Y);
            }

            foreach (var p in sa)
            {
                var s = new Coord2d(p.X, p.Y);
                Console.WriteLine("{0}\t",s);
                var m = calibration.ToMasterPt(new Coord2d(p.X, p.Y));
                Console.WriteLine(m);
                sm.Add(s.X, s.Y);
                mm.Add(m.X, m.Y);
            }

            Console.WriteLine("{0} and markers", mm.CurrentMarkers.Count() + sm.CurrentMarkers.Count());
            calibrationPanel.Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var mm = calibrationPanel.MasterMarkers;
            var sm = calibrationPanel.SlaveMarkers;
            var ma = mm.CurrentMarkers.ToArray();
            var sa = sm.CurrentMarkers.ToArray();

            int count = Math.Min(ma.Length, sa.Length);
            for (int i = 0; i < count; i++)
            { 
                var mp = new Coord2d(ma[i].X,ma[i].Y);
                var sp = new Coord2d(sa[i].X,sa[i].Y);
                double error;
                var h = calibration.LineInterSection3D(mp, sp, out error);

                Console.WriteLine("{5}\t({0:f3},{1:f3},{2:f3})\tDist:{3:f2}\tError:{4:f3}",
                    h.x, h.y, h.z, h.Distance(), error,i);
            }
        }

        private void sortFilesButton_Click(object sender, EventArgs e)
        {
            var ff = aquisition.FullFrames;

            if (ff.MoveNext())
            {
                renderFullFramePair(ff.Current);
            }
            else
            {
                ff.Reset();
                if (ff.MoveNext())
                    renderFullFramePair(ff.Current);
            }

            //var fp = ff.Current;
            //var bmp = renderer.CreateBitmap(fp.Master);
            //masterView.Image = calibration.GetView(new Coord2d(1600, 3750), bmp);
            //masterView.Invalidate();
        }

        #endregion
        #region aquisition

        private void SetAquisitionButtons(bool isBusy)
        {
            fileSearchCancelButton.Enabled = isBusy;
            aquisitionProgressBar.Enabled = isBusy;
            openFilesButton.Enabled = !isBusy;
            openFolderButton.Enabled = !isBusy;
        }

        private void OpenFolder_Click(object sender, EventArgs e)
        {
            string folder;
            if (!aquisition.Busy && (folder = getFolderFromDialog()) != null)
            {
                aquisition.LoadFromFolder(new FolderSearchParameters(folder));
                SetAquisitionButtons(true);
            }
        }

        private void OpenFiles_Click(object sender, EventArgs e)
        {
            string[] paths;
            if (!aquisition.Busy && (paths = getFilesFromDialog()) != null)
            {
                aquisition.LoadFromFiles(paths);
                SetAquisitionButtons(true);
            }
        }

        private void fileSearchCancelButton_Click(object sender, EventArgs e)
        {
            aquisition.Quit();
        }

        private void OnAquisitionFinish(object sender, EventArgs e)
        {
            aquisitionProgressBar.Value = 0;
            SetAquisitionButtons(false);
        }

        private void OnAquisitionProgressChange(object sender, OnProgressChangeEventArgs e)
        {
            aquisitionProgressBar.Value = Math.Min(100, e.PercentDone);
        }

        private void OnFrameBatch(object sender, OnFrameBatchEventArgs e)
        {            
            var batch = e.MapFrames;
            //if (calibration == null && aquisition.FullFrameCount>0)
            //{
            //    using (var map = new MapFile(batch[0].Master))
            //    {
            //        var h = map.Header;
                    
            //        calibration = new MapFileCalibration(new Net.Vector.Vector3d(1.395, 0, 0), (int)h.Height, (int)h.Width);
            //        renderFullFramePair(aquisition.FullFrames.FindNearest((long)h.SystemTime));
            //    }
                
            //}

            //birdSearch.Add(batch);

            Console.WriteLine("gui got batch {0}", batch.Count());
            this.Text = string.Format("{0} {1}", aquisition.MapFrameCount, aquisition.FullFrameCount);
        }

        #endregion
        #region bird search

        private void birdSearchCancelButton_Click(object sender, EventArgs e)
        {
            birdSearch.Quit();
        }

        private void OnBirdSearchFinish(object sender, EventArgs e)
        {
            Console.WriteLine("finished birdsearch");
            //aquisitionProgressBar.Value = 0;
            //SetAquisitionButtons(false);
        }

        private void OnBirdSearchProgressChange(object sender, OnProgressChangeEventArgs e)
        {

            birdSearchProgressBar.Value = Math.Min(100, e.PercentDone);
        }

        private void OnBirdPossible(object sender, OnBirdPossibleEventArgs e)
        {
            //fileFinderlabel.Text = string.Format("{0} pairs processed", aquisition.PairedCount);
            //Console.WriteLine("gui got batch {0}", e.FrameBatch.Count());
        }

        private void birdSearchButton_Click(object sender, EventArgs e)
        {
            var b = birdSearchButton;
            var bp = birdSearch.GetNextBirdPossible();
            if (bp != null)
            {
                renderFramePair(bp.FramePair);
                var mm = calibrationPanel.MasterMarkers;
                var sm = calibrationPanel.SlaveMarkers;
                double h = calibrationPanel.MasterView.Image.Height;
                foreach (var p in bp.Hits)
                {
                    mm.Add(new Coord2d(p.Master.Y, h-p.Master.X));
                    sm.Add(new Coord2d(p.Slave.Y, h-p.Slave.X));
                }
                calibrationPanel.Invalidate();
            }
        }

        #endregion


        #region debug functions

        //public void DebugPairs(List<BlobPair> pairs)
        //{
        //    var sp = new List<Blob>();
        //    var mp = new List<Blob>();
        //    foreach (var p in pairs)
        //    {
        //        mp.Add(p.Master);
        //        sp.Add(p.Slave);
        //    }
        //    DebugBlobs(mp, sp);
        //}

        //public void DebugBlobs(List<Blob> masters, List<Blob> slaves)
        //{
        //    int colour = (random.Next(0x1000000) ^ -1) | 0x404040;
        //    using (var pen = new Pen(Color.FromArgb(colour)))
        //    {
        //        BlobDebug(masterView, masters, pen);
        //        BlobDebug(slaveView, slaves, pen);
        //    }
        //}

        public void BlobDebug(ZoomBox view, List<Blob> blobs, Pen pen )
        {
            if (blobs != null)
            {
                int m = view.Image.Height + 32;
                using (var g = Graphics.FromImage(view.Image))
                {

                    foreach (var blob in blobs)
                    {

                        int x = blob.Y << 1;
                        int y = m - ((blob.X + blob.Width) << 1);
                        int w = blob.Height << 1;
                        int h = blob.Width << 1;
                        float cx = (int)blob.CY << 1;
                        float cy = m - ((int)(blob.CX) << 1);

                        if (x > view.Image.Width || y > view.Image.Height || x < 0 || y < 0)
                            Console.Write('!');
                        Console.WriteLine("({0}x{1} {2})", cx, cy, blob.Area);
                        g.DrawRectangle(pen, x, y, w + 1, h + 1);
                        drawCircle(new PointF(cx, cy), (float)Math.Sqrt((double)blob.Area / Math.PI) * 2f, null, pen, g);
                    }

                }
            }

        }
       // public void DebugBayerBuf(byte[] buffer, int x, int y, int w, int h)
       // {
       //     var view = leftView;// rightView;
       //     //int m = view.Image.Height ;
       //     //m = (m + 31) & -32;
       //     //x += w;

       //     var bmp = (Bitmap)view.Image;
       //     int bw = bmp.Width, bh = bmp.Height;
       //    // Console.Write("[{0},{1} {2}x{3}]",x,y,w,h);

       //     //Color color = Color.FromArgb(rnd.Next(0x1000000 )^ -1);
       //     int k = 0;
       //     for (int i = 0; i < h; i++)
       //     {
       //         for (int j = 0; j < w; j++, k++)
       //         {
                   
       //             int px = y + i;
       //             int py = bh - (x + j) + 31;
       //             if (((i+j) & 1)!=1 && px >= 0 && px < bw && py >= 0 && py < bh)
       //             { int byt = buffer[k];// *6;
       //             //if (byt > 255) byt = 255;
       //             Color colour = Color.FromArgb(byt, byt, 80);
       //                 bmp.SetPixel(px,py, colour);
       //             }
       //             //else
       //             //{
       //             //    Console.Write('?');
       //             //}
       //             //catch (Exception e)
       //             //{
       //             //    Console.Write("({0}x{1})", (y + i), m - ((x + j)));
       //             //}
       //         }
       //     }

       //}


        //public void DebugMaskBuf(byte[] buffer, int x,int y, int w,int h)
        //{
        //    var view = rightView;
        //    int m = view.Image.Height +32;
        //    //m = (m + 31) & -32;
        //    //x += w;
            
        //    var bmp = (Bitmap)view.Image;

        //        //Color color = Color.FromArgb(rnd.Next(0x1000000 )^ -1);
        //        int k=0;
        //        for (int i = 0; i < h; i++)
        //        {
        //            for (int j = 0; j < w; j++,k++)
        //            {
        //                int byt = buffer[k] * 6;
        //                if (byt > 255) byt = 255;
        //                Color color = Color.FromArgb(255, byt, byt);
        //                try
        //                {
        //                    bmp.SetPixel((y + i) << 1, m - ((x + j) << 1), color);
        //                }
        //                catch //(Exception e)
        //                {
        //                    Console.Write("({0}x{1})",(y + i) << 1, m - ((x + j) << 1));
        //                }
        //            }
        //        }
        //        //foreach (var blob in blobs)
        //        //{
        //        //    int colour = rnd.Next(0x1000000) ^ -1;
        //        //    using (var pen = new Pen(Color.FromArgb(colour)))
        //        //    {
        //        //        int x = blob.Y << 1;
        //        //        int y = m - ((blob.X - blob.Width) << 1);
        //        //        int w = blob.Height << 1;
        //        //        int h = blob.Width << 1;

        //        //        if (x > view.Image.Width || y > view.Image.Height || x < 0 || y < 0)
        //        //            Console.Write('!');
        //        //        Console.WriteLine("({0} {1} {2} {3})", x, y, w, h);
        //        //        g.DrawRectangle(Pens.Red, x, y, w + 1, h + 1);
        //        //    }
        //        //}
           

        //}
        #endregion

        private void zoomBox1_Click(object sender, EventArgs e)
        {


        }

        #region birdsearch


        //private void birdSearchCancelButton_Click(object sender, EventArgs e)
        //{
        //    birdSearcher.CancelAsync();
        //}

        private void birdSearcher_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //e.Result
            //birdSearchButton.Enabled = mapFilePairs.PotentialCount != 0;
            birdSearchButton.Enabled = true; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            birdSearchCancelButton.Enabled = false;
            birdSearchProgressBar.Value = 0;
        }
        private void birdSearcher_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            birdSearchProgressBar.Value = e.ProgressPercentage;
        }

        #endregion

        //private void fileFinder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    fullFrames = fileFinder.FullFrames;
        //    mapFilePairs = fileFinder.MapFrames;
        //    mapFileEnumerator = (MapFileEnumerator) mapFilePairs.GetEnumerator();

        //    fileSearchCancelButton.Enabled = false;
        //    openFilesButton.Enabled = true;
        //    openFolderButton.Enabled = true;
        //    birdSearchButton.Enabled=mapFilePairs.PotentialCount!=0;

        //    fileFinderlabel.Text = string.Format("{0} pottential pairs found", mapFilePairs.PotentialCount);
        //    if (e.Cancelled == false && e.Error == null)
        //    {
        //    }
        //}


        static string[] getFilesFromDialog()
        {
            //frameTimer.Enabled = false;
            string[] filelist = new string[0]; //the return if the dialog fails or is canceled
            using (var dialog = new OpenFileDialog())
            {
                dialog.Multiselect = true;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    filelist = dialog.FileNames;
                }
            }
            return filelist;
        }


        static FolderBrowserDialog folderDialog; //retain an instance to remember last chosen folder
        static string getFolderFromDialog()
        {
            if (folderDialog == null)
                folderDialog = new FolderBrowserDialog() { ShowNewFolderButton = false };

            var fd = folderDialog;
            return fd.ShowDialog() == DialogResult.OK ? fd.SelectedPath : null;
        }


        #region graphics






        private void view_Paint(object sender, PaintEventArgs e)
        {
            var view = (ZoomBox)sender;
            //this.Text  = view.Zoom.ToString();
            var markerData = (MarkerData) view.Tag;
            markerData.Render(view, e.Graphics);

            invalidateOtherView(view);            
        }








        private void invalidateOtherView(ZoomBox view)
        {
            //var otherView = view == masterView ? rightView : leftView;

            //if (otherView.Zoom != view.Zoom || otherView.Offset != view.Offset)
            //{
            //    otherView.Zoom = view.Zoom;
            //    otherView.Offset = view.Offset;
            //    otherView.Invalidate();
            //}
        }





        //private bool renderMapFileX(ZoomBox view, MapFile mapFile, Bitmap background)
        //{

        //}

        void renderFullFramePair(FramePair framePair)
        {
            calibrationPanel.Render(framePair);
        }

        //void renderFullFrame(ZoomBox view, FrameRef frameRef)
        //{
        //    if (frameRef.Type != FrameType.Fullframe || !File.Exists(frameRef.Path))
        //        return;
        //    ((MarkerData)view.Tag).FrameReference=frameRef;
        //    AssignView(view, renderer.CreateBitmap(frameRef));
        //}

        private static void AssignView(ZoomBox view,Bitmap image)
        {
            var old = view.Image;
            view.Image = image;
            if(old!=null)
                old.Dispose();
        }


        //private bool renderMapFile(ZoomBox view, MapFile mapFile)
        //{
        //    if (mapFile != null)//&& mapFile.isOpen
        //    {
        //        //update markerData with the new frame
        //        var markerData = (MarkerData)view.Tag;
        //        markerData.FrameReference = mapFile.FrameReference;

        //        view.Image = renderer.CreateBitmap(mapFile);
        //        mapFile.Close();
        //        return true;
        //    }
        //    return false;
        //}


        private void renderFramePair(FramePair pair)
        {
            using (MapFile master = new MapFile(pair.Master), slave = new MapFile(pair.Slave))
            {
                renderFramePair(master, slave);
            }
        }

        private void renderFramePair(MapFilePair pair)
        {
            renderFramePair(pair.Master, pair.Slave);
        }


        private void renderFramePair(MapFile master, MapFile slave)
        {
            //renderMapFile(leftView, master);
            //renderMapFile(rightView, slave);

            //if (master.Header != null)
            //{
            //    ulong tm = (ulong)master.Header.SystemTime;
            //    long fm = master.Header.FrameCount;
            //    timeLabel.Text = string.Format("{0} ({1}:{2})", TimeConverter.ToDateTime(tm), tm,fm);
            //}
        }



        private void renderNextFramePair(IEnumerator<MapFilePair> mapFileEnumerator)
        {
            if (mapFileEnumerator != null)
            {
                if (mapFileEnumerator.MoveNext())
                {
                    renderFramePair(mapFileEnumerator.Current);
                }
                else //auto cycle
                {
                    mapFileEnumerator.Reset();
                }
            }
        }

        //private void frameTimer_Tick(object sender, EventArgs e)
        //{
        //    renderNextFramePair(mapFileEnumerator);
        //}

        private bool LoadImage(ZoomBox view, string imagepath)
        {            
            var old = view.Image;
            bool ret = false;
            try
            {
                var bmp = new Bitmap(imagepath);
                bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);
                view.Image = bmp;
                if (old != null)
                    old.Dispose();
                ret=true;
                Console.WriteLine("good");
            }
            catch
            {
            }
            return ret;
        }

        #endregion


        private void playPauseButton_Click(object sender, EventArgs e)
        {
            frameTimer.Enabled = !frameTimer.Enabled;
            playPauseButton.Text = frameTimer.Enabled ? "Pause" : "Play";
        }

        private void DisgardUnmatchedCheck_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void LoadBmp(ZoomBox view, string path)
        {
            Bitmap bmp;
            System.Drawing.Image old;
            try
            {
                bmp = new Bitmap(path);
                if (bmp.Width > bmp.Height)
                    bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);
                old = view.Image;
                view.Image = bmp;
                if (old != null)
                    old.Dispose();
            }
            catch //(Exception e)
            {
                Console.WriteLine("{0} {1}", path, File.Exists(path));
            }
        }






        private void renderMode_CheckedChanged(object sender, EventArgs e)
        {
            //calibrationPanel.Renderer.RenderMode= (MapFileRenderer.RenderModes)((RadioButton)sender).Tag;

        }

        private void drawCircle(PointF center, float radius, Brush brush, Pen pen, Graphics graphics)
        {
            float diam = radius * 2f;
            float x = center.X - radius;
            float y = center.Y - radius;
            //graphics.FillEllipse(brush, x, y, diam, diam);
            graphics.DrawEllipse(pen, x, y, diam, diam);
        }

        private void drawBlob(Graphics g, ZoomBox view, Blob blob, Pen pen)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

   

        private void AllDirectoriesCheck_CheckedChanged(object sender, EventArgs e)
        {

        }


        //Thread sorterThread;
        //MapFileConsolidator sorter;
        //private void sortFilesButton_Click(object sender, EventArgs e)
        //{
        //    if (sorterThread == null || sorterThread.IsAlive == false)
        //    {
        //        string folder = getFolderFromDialog();
        //        sorter = new MapFileConsolidator(folder, folder);
        //        sorterThread = new Thread(sorter.SortFiles);
        //        sorterThread.Start();
        //    }
        //}



        private string generateFileName(MapFile mapFile, string suffix)
        {
            if (mapFile != null)
            {
                var tm = TimeConverter.ToDateTime(mapFile.Header.SystemTime);
                return string.Format("{0}-{1}-{2}-{3}-{4}-{5}-{6}-{7}",
                    tm.Year,
                    tm.Month,
                    tm.Day,
                    tm.Hour,
                    tm.Minute,
                    tm.Second,
                    mapFile.Header.FrameCount,
                    suffix);
            }
            else
            {
                return suffix;
            }
        }





        private void SaveFrameButton_Click(object sender, EventArgs e)
        {
            //string lazyArsedHardCodedPath = @"C:\Capture\SavedFrames\";
            //var mapPair = mapFileEnumerator.Current;
            //if (mapPair != null)
            //{
            //    string prefix = lazyArsedHardCodedPath;
            //    string suffix = renderer.RenderMode.ToString() + ".bmp";
            //    if (mapPair.Master != null && rightView.Image != null)
            //    {
            //        //todo, investigate ImageCodecInfo with params over ImageFormat
            //        var fileName = prefix + generateFileName(mapPair.Master, "R-" + suffix);
            //        rightView.Image.Save(fileName, ImageFormat.Bmp);
            //    }
            //    if (mapPair.Slave != null && leftView.Image != null)
            //    {
            //        var fileName = prefix + generateFileName(mapPair.Slave, "L-" + suffix);
            //        leftView.Image.Save(fileName, ImageFormat.Bmp);
            //    }
            //}
        }

        private void fwdButton_Click(object sender, EventArgs e)
        {
            //if (frameTimer.Enabled)
            //    playPauseButton.PerformClick();
            //renderNextFramePair(mapFileEnumerator);
        }

        private void bakButton_Click(object sender, EventArgs e)
        {
            //if (frameTimer.Enabled)
            //    playPauseButton.PerformClick();


            //if (mapFileEnumerator != null)
            //{
            //    if (mapFileEnumerator.MovePrev())
            //    {
            //        renderFramePair(mapFileEnumerator.Current);
            //    }
            //}
            ////mapFileEnumerator.MovePrev();
            ////renderNextFramePair(mapFileEnumerator);
        }





        private void saveMarkersButton_Click(object sender, EventArgs e)
        {
            using (var dialog = new SaveFileDialog())
            {
                dialog.FileName = "BirdTrack";
                try
                {
                    if (dialog.ShowDialog() == DialogResult.OK)
                    {
                        MarkerData.SaveMarkerData(dialog.FileName, calibrationPanel.MasterMarkers,calibrationPanel.SlaveMarkers,null);
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        private void clearMarksButton_Click(object sender, EventArgs e)
        {
            calibrationPanel.MasterMarkers.Clear();
            calibrationPanel.SlaveMarkers.Clear();
            calibrationPanel.Invalidate();
        }

        private void bayerUpDown_ValueChanged(object sender, EventArgs e)
        {
            //renderer.BayerOrder = (int)bayerUpDown.Value;
            //if (!frameTimer.Enabled &&  renderer.RenderMode == MapFileRenderer.RenderModes.Image)
            //{
            //    renderFramePair(mapFileEnumerator.Current);
            //    //leftView.Invalidate();
            //    //rightView.Invalidate();
            //}
        }



        private void button3_Click(object sender, EventArgs e)
        {
            


        }

        private void main_KeyPress(object sender, KeyPressEventArgs e)
        {
            Console.Write(e.KeyChar);
        }

        private void main_KeyDown(object sender, KeyEventArgs e)
        {
            
            switch (e.KeyCode)
            {
                case Keys.F1:
                    calibrationPanel.Visible = true;
                    birdConfirmPanel.Visible = false;
                    e.Handled = true;
                    break;
                case Keys.F2:
                    birdConfirmPanel.Visible = true;
                    calibrationPanel.Visible = false;
                    e.Handled = true;
                    break;
            }
        }





  

 











        //private void zoomBox1_Paint(object sender, PaintEventArgs e)
        //{
        //    if (blobList == null)
        //        return;

        //    var g = e.Graphics;
        //    var brush = new SolidBrush(Color.FromArgb(unchecked((int)0x40404020)));
        //    var pen = new Pen(Color.FromArgb(unchecked((int)0xffff0000)));
        //    float zm = zoomBox1.Zoom;

         
        //    Console.WriteLine(blobList.Count);
        //    foreach (var blob in blobList)
        //    {
        //        //Console.WriteLine(blob.Bounds);  
        //        var p = zoomBox1.ToControlSpace(new PointF(blob.X - 0.5f, blob.Y - 0.5f));// new Point(blob.X, blob.Y))
        //        float w = blob.Width * zm;
        //        float h = blob.Height * zm;
        //        var c = zoomBox1.ToControlSpace(blob.Center);
        //        float r = (float)Math.Sqrt((double)blob.Area / Math.PI) * zm;

        //        //g.FillRectangle(brush, p.X, p.Y, w, h);

        //        drawCircle(c, r, brush, pen, g);
        //        g.DrawRectangle(pen, p.X, p.Y, w, h);
        //    }

        //    pen.Dispose();
        //    brush.Dispose();
        //}


        
    }

}
