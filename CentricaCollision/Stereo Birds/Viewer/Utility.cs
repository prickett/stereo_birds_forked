﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;

namespace Createc.StereoBirds.Viewer
{
    class Utility
    {
        public static string GetFrameRefText(FrameRef frameRef)
        {
            var dt = TimeConverter.ToDateTime(frameRef.SystemTime);
            return string.Format("{0}:{1}:{2} {3} ({4}) {5}",
                dt.Hour,
                dt.Minute,
                dt.Second,
                dt.ToLongDateString(),
                frameRef.SystemTime.sec,
                frameRef.FrameNumber);
        }
        public static string GetFileNameFromSaveDialog(string filename = null)
        {
            string fn = null; //the return if the dialog fails or is canceled
            using (var dialog = new SaveFileDialog())
            {
                dialog.FileName = filename;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    fn = dialog.FileName;
                }
            }
            return fn;
        }
        public static string[] GetFileNamesFromDialog()
        {
            string[] filelist = new string[0]; //the return if the dialog fails or is canceled
            using (var dialog = new OpenFileDialog())
            {
                dialog.Multiselect = true;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    filelist = dialog.FileNames;
                }
            }
            return filelist;
        }

        static FolderBrowserDialog folderDialog; //retain an instance to remember last chosen folder
        public static string GetFolderFromDialog(string description = null,bool showNewFolder = false)
        {
            if (folderDialog == null)
                folderDialog = new FolderBrowserDialog();
            //var fd = new FolderBrowserDialog()
            //{
            //    RootFolder = rootfolder;
            //    Description = description,
            //    ShowNewFolderButton = showNewFolder
            //};
            var fd = folderDialog;
            fd.Description = description;
            fd.ShowNewFolderButton = showNewFolder;
            return fd.ShowDialog() == DialogResult.OK ? fd.SelectedPath : null;
        }
    }
}
