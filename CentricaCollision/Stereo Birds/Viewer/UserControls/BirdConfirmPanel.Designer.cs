﻿namespace Createc.StereoBirds.Viewer
{
    partial class BirdConfirmPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.flowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.Dock = System.Windows.Forms.DockStyle.Top;
            this.label.Location = new System.Drawing.Point(0, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(614, 19);
            this.label.TabIndex = 2;
            this.label.Text = "Left click any images that show a bird.";
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayout
            // 
            this.flowLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayout.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayout.Location = new System.Drawing.Point(0, 19);
            this.flowLayout.Name = "flowLayout";
            this.flowLayout.Size = new System.Drawing.Size(614, 341);
            this.flowLayout.TabIndex = 3;
            this.flowLayout.Click += new System.EventHandler(this.flowLayout_Click);
            this.flowLayout.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.flowLayout_ControlAdded);
            this.flowLayout.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.flowLayout_ControlRemoved);
            this.flowLayout.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.flowLayout_PreviewKeyDown);
            this.flowLayout.Resize += new System.EventHandler(this.flowLayout_Resize);
            // 
            // BirdConfirmPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.flowLayout);
            this.Controls.Add(this.label);
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "BirdConfirmPanel";
            this.Size = new System.Drawing.Size(614, 360);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.birdConfirmPanel_KeyDown);
            this.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.BirdConfirmPanel_PreviewKeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label;
        private System.Windows.Forms.FlowLayoutPanel flowLayout;
    }
}
