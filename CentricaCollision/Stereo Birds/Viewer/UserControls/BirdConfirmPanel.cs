﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Createc.StereoBirds.Viewer
{
    public partial class BirdConfirmPanel : UserControl
    {
        const int UNDOLIMIT = 5;
        LinkedList<BirdConfirmUserControl> undo;

        public BirdConfirmPanel()
        {
            InitializeComponent();
            undo = new LinkedList<BirdConfirmUserControl>();
        }

        private void flowLayout_Click(object sender, EventArgs e)
        {
            var ctrl = new BirdConfirmUserControl();
            ctrl.Text = flowLayout.Controls.Count.ToString();
            ctrl.Size = new Size(256, 256);
            flowLayout.Controls.Add(ctrl);
        }
        private void flowLayout_ControlRemoved(object sender, ControlEventArgs e)
        {
            Console.WriteLine("control removed");
            if (undo.Count >= UNDOLIMIT)
            {
                var node = undo.First;
                node.Value.Dispose();
                undo.Remove(node);
            }

            undo.AddLast((BirdConfirmUserControl)e.Control);
        }

        private void birdConfirmPanel_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Space:
                    if (flowLayout.Controls.Count > 0)
                        flowLayout.Controls.RemoveAt(0);
                    break;
                case Keys.U:
                    if (undo.Count > 0)
                    {
                        var node = undo.Last;
                        undo.Remove(node);
                        flowLayout.Controls.Add(node.Value);
                        flowLayout.Controls.SetChildIndex(node.Value, 0);
                    }
                    break;
            }
        }

        private void flowLayout_ControlAdded(object sender, ControlEventArgs e)
        {

        }

        private void BirdConfirmPanel_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.KeyChar
        }

        private void BirdConfirmPanel_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            Console.WriteLine("preview");
        }

        private void flowLayout_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            Console.WriteLine("flo preview");
        }

        private void flowLayout_Resize(object sender, EventArgs e)
        {
            //label.Text = string.Format("{0}({1}) {2}({3})", Width, ClientSize.Width, flowLayout.Width, flowLayout.ClientSize.Width);
        }
        
    }

}
