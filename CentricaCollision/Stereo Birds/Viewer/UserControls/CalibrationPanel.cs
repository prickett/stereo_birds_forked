﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CustomControls;

using Createc.Net.Imaging;
using Createc.Imaging.Thresholds;
using Createc.StereoBirds;

namespace Createc.StereoBirds.Viewer
{

    public partial class CalibrationPanel : UserControl
    {
        public delegate void OnCalibrationCancelHandler(object sender, EventArgs e);
        public event OnCalibrationCancelHandler OnCalibrationCancel;

        public delegate void OnCalibrationHandler(object sender, OnCalibrationArgs e);
        public event OnCalibrationHandler OnCalibration;

        StereoCalibration calibration;
        MapFileRenderer renderer;
        CameraView master;
        CameraView slave;

        public CalibrationPanel()
        {
            InitializeComponent();

            master = new CameraView(rightView);
            slave = new CameraView(leftView);
            renderer = new MapFileRenderer();   
        }

        public ZoomBox MasterView { get { return master.Zoombox; } }
        public ZoomBox SlaveView { get { return slave.Zoombox; } }
        public MarkerData MasterMarkers { get { return master.Markers; } }
        public MarkerData SlaveMarkers { get { return slave.Markers; } }

        public InterpolationMode InterpolationMode
        {
            get { return master.Zoombox.InterpolationMode; }
            set
            {
                if (value != InterpolationMode)
                {
                    master.Zoombox.InterpolationMode = value;
                    slave.Zoombox.InterpolationMode = value;
                    Invalidate();
                }
            }
        }
        public SmoothingMode SmoothingMode
        {
            get { return master.Zoombox.SmoothingMode; }
            set
            {
                if (value != SmoothingMode)
                {
                    master.Zoombox.SmoothingMode = value;
                    slave.Zoombox.SmoothingMode = value;
                    Invalidate();
                }
            }
        }
        public StereoCalibration Calibration
        {
            get { return calibration; }
        }

        public void CalibrateImagePair(Pair<Createc.Net.Imaging.Image> imagePair, FramePair pair)
        {
            label.Text = Utility.GetFrameRefText(pair.Master);

            master.Markers.FrameReference = pair;
            slave.Markers.FrameReference = pair;

            Render(imagePair.A, pair.Master, master);
            Render(imagePair.B, pair.Slave, slave);
            calibration = new StereoCalibration(pair.SystemTime, imagePair.A.Width * 2, imagePair.A.Height * 2, new Net.Vector.Vector3d(1.395, 0, 0));
            //calibration = new StereoCalibration(pair.SystemTime, imagePair.A.Width, imagePair.A.Height, new Net.Vector.Vector3d(1.395, 0, 0));
            calibrationButton.Enabled = false;

            label.Text += string.Format("    ({0}x{1})", imagePair.A.Width, imagePair.A.Height);
        }



        void Render(Createc.Net.Imaging.Image image,FrameRef frame, CameraView view)
        {
            assignView(view.Zoombox, MapFileRenderer.CreateBitmapFromImage(image));
           
            //Console.WriteLine("{0} rendering", frame.IsMaster ? "Master" : "Slave");
            view.Zoombox.Invalidate();
        }

        public void Render(FramePair pair)
        {
            Utility.GetFrameRefText(pair.Master);

            master.Markers.FrameReference = pair;
            slave.Markers.FrameReference = pair;

            Render(pair.Master, master);
            Render(pair.Slave, slave);
            int w = master.Zoombox.Image.Width;
            int h = master.Zoombox.Image.Height;
            calibration = new StereoCalibration(pair.SystemTime,w*2,h*2,new Net.Vector.Vector3d(1.395, 0, 0));
            calibrationButton.Enabled = false;

            //calibration = new StereoCalibration(pair.SystemTime, h, w, new Net.Vector.Vector3d(1.395, 0, 0)); //inverted width/height
        }
        void Render(FrameRef frame, CameraView view)
        {
            assignView(view.Zoombox, renderer.CreateBitmap(frame));
            
            //Console.WriteLine("{0} rendering", frame.IsMaster ? "Master" : "Slave");
            view.Zoombox.Invalidate();
        }

        void RaiseOnCalibration(OnCalibrationArgs args)
        {
            var handler = OnCalibration;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        void RaiseOnCalibrationCancel()
        {
            var handler = OnCalibrationCancel;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }
        

        public void HandViews()
        {
            swap(ref master,ref slave);
        }

        #region mouse control

        //private void view_MouseDown(object sender, MouseEventArgs e)

        private void view_MouseDown(object sender, MouseEventArgs e)
        {
            var view = (ZoomBox)sender;
            var markers = getMarkers(view);
            if (e.Button == MouseButtons.Left) 
            {
                var point = view.ToPixelSpace(e.Location);
                var location = new Coord2d(point.X, point.Y);

                int index = markers.HitTest(location, 16f / view.Zoom);

                if (index < 0)
                {
                    index = markers.Add(location);
                    markers.HighLightIndex = index;
                    view.Invalidate();
                    checkForEnoughPoints();                    
                }
                markers.GrabPoint = e.Location;
                markers.GrabIndex = index;
            }
            else if (e.Button == MouseButtons.Right) //delete
            {
                int index = markers.HighLightIndex;
                if (index >= 0)
                {
                    markers.DeleteMarker(index);
                }
            }
        }

        private void view_MouseMove(object sender, MouseEventArgs e)
        {
            var view = (ZoomBox)sender;
            var markers = getMarkers(view);
            var point = view.ToPixelSpace(e.Location);
            var location = new Coord2d(point.X, point.Y);

            if (markers.GrabIndex < 0) //nothing being grabbed
            {
                int oldIndex = markers.HighLightIndex;
                int newIndex = markers.HitTest(location, 16f / view.Zoom);                
                if (oldIndex != newIndex)
                {
                    markers.HighLightIndex = newIndex;
                    view.Invalidate();
                }
            }
            else //grab in progress
            {
                var a = markers.GrabPoint;
                var b = e.Location;
                float x = (float)(b.X - a.X) / view.Zoom;
                float y = (float)(b.Y - a.Y) / view.Zoom;
                markers.MoveMarker(markers.GrabIndex, x, y);
                markers.GrabPoint = b;
                view.Invalidate();
            }

        }

        private void view_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var markers = getMarkers( (ZoomBox)sender);
                markers.GrabIndex = -1;
            }
        }

        MarkerData getMarkers(ZoomBox zoomBox)
        {
            if (zoomBox == master.Zoombox)
                return master.Markers;
            if (zoomBox == slave.Zoombox)
                return slave.Markers;
            else
                throw new ArgumentException("unknown zoomBox");
        }

        void checkForEnoughPoints()
        {
            int count = 0;

            if (master.Markers.CurrentMarkers != null)
                count = master.Markers.CurrentMarkers.Count;

            if (slave.Markers.CurrentMarkers != null)
                count = Math.Min(count, slave.Markers.CurrentMarkers.Count);

            //int mCt = master.Markers.CurrentMarkers != null ? 
            //    master.Markers.CurrentMarkers.Count : 0;

            //int sCt = slave.Markers.CurrentMarkers != null ? 
            //    slave.Markers.CurrentMarkers.Count : 0;

            calibrationButton.Enabled = count >= 3;
        }

        #endregion

        static void swap<T>(ref T a,ref T b)
        {
            T t = a;
            a=b;
            b =t;
        }

        static void assignView(ZoomBox view, Bitmap image)
        {
            var old = view.Image;
            view.Image = image;
            if (old != null)
                old.Dispose();
            else           
                view.ZoomToFit(); 
        }

        #region painting

        void view_Paint(object sender, PaintEventArgs e)
        {
            CameraView view = (ZoomBox)sender == master.Zoombox? master:slave; 
            view.Markers.Render(view.Zoombox, e.Graphics);
        }

        bool suppressOther = false;
        void view_ViewChanged(object sender, EventArgs e)
        { 
            //this updates the other frames Zoom and Offset
            if (!suppressOther)
            {
                suppressOther = true;
                var view = (ZoomBox)sender;
                var other = view == leftView ?rightView :leftView;
                other.Zoom = view.Zoom;
                other.Offset = view.Offset;
                other.Invalidate();
                suppressOther = false;
            }
        }

        #endregion



        private void calibrationButton_Click(object sender, EventArgs e)
        {
            if (master.Zoombox.Image == null || slave.Zoombox.Image == null)
                return;

            int w = master.Zoombox.Image.Width;
            int h = master.Zoombox.Image.Height;
            var s = SlaveMarkers.CurrentMarkers.ToArray();
            var m = MasterMarkers.CurrentMarkers.ToArray();
            var ct = Math.Min(s.Length, m.Length);

            if (ct < 3)
            {
                Console.WriteLine("At least 3 points are required for calibration");
                return;
            }

            var pts = new List<CoordPair>(ct);

            for (int i = 0; i < ct; i++)
            {
                //Console.WriteLine("({0},{1})\t({2},{3})", m[i].X, m[i].Y, s[i].X, s[i].Y);
                // points are scaled because fullframes are shown at half scale
                var mPt = new Coord2d(m[i].X * 2.0, m[i].Y * 2.0);
                var sPt = new Coord2d(s[i].X * 2.0, s[i].Y * 2.0);

                //var mPt = new Coord2d(m[i].X, m[i].Y );
                //var sPt = new Coord2d(s[i].X , s[i].Y );
                pts.Add(new CoordPair(mPt, sPt));
            }

            calibration.Calibrate(pts);
            RaiseOnCalibration(new OnCalibrationArgs(calibration));


            //Console.WriteLine("calibration error: {0}", calibration.Error);
            //var mc = calibration.MasterCentre;
            //var sc = calibration.SlaveCentre;
            //double x = mc.X - sc.X;
            //double y = mc.Y - sc.Y;
            //Console.WriteLine("master: ({0:f3},{1:f3})", x,y);
            //Console.WriteLine("slave:  ({0:f3},{1:f3})", mc.X + x * 0.5,mc.Y+y*0.5);


        }

        private void badImageButton_Click(object sender, EventArgs e)
        {
            RaiseOnCalibrationCancel();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var p = @"D:\_1340000000\_1346700000\_1346741000\";
            var m = new FrameRef(p + "S_1346741240_1005568718_2432032_[2581].bmp");
            var s = new FrameRef(p + "M_1346741240_1005568711_2432032_[3032].bmp");
            var fp = new FramePair(m, s);

            Render(fp);
            
            var mm = MasterMarkers;
            mm.Clear();
            mm.Add(1469.46 * 2, 1640.37 * 2);
            mm.Add(1465.02 * 2, 725.93 * 2);
            mm.Add(320.57 * 2, 1450.41 * 2);
            mm.Add(34.88 * 2, 1398.65 * 2);
            mm.Add(194.69 * 2, 951.31 * 2);
            mm.Add(1337.35 * 2, 456.67 * 2);

            var sm = SlaveMarkers;
            sm.Clear();
            sm.Add(1474.46 * 2, 1663.65 * 2);
            sm.Add(1481.66 * 2, 745.18 * 2);
            sm.Add(330.62 * 2, 1457.44 * 2);
            sm.Add(43.94 * 2, 1405.51 * 2);
            sm.Add(207.10 * 2, 959.61 * 2);
            sm.Add(1354.23 * 2, 473.39 * 2);

            Invalidate();
        }

    }
    class CameraView
    {
        public CameraView(ZoomBox view)
        {
            view.InterpolationMode = InterpolationMode.Bicubic;// InterpolationMode.NearestNeighbor;
            view.SmoothingMode = SmoothingMode.AntiAlias;
            Zoombox = view;

            Markers = new MarkerData();
        }
        public ZoomBox Zoombox { get; private set; }
        public MarkerData Markers { get; private set; }
        public MarkerData OldMarkers { get; private set; } //!!!!!!!!!!!!!!!!!!!
    }
    public class OnCalibrationArgs
    {
        public OnCalibrationArgs(StereoCalibration calibration)
        {
            Calibration = calibration;
        }
        public StereoCalibration Calibration { get; private set; }
    }
}
