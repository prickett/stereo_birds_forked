﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using Createc.Net.Imaging;
using Createc.Net.Imaging.Interpolation;
using Createc.Net.Rotation;
using Createc.Net.Vector;
using Createc.Net.Imaging.Fisheye;

namespace Createc.StereoBirds.Viewer
{
    public partial class VerifyCalibrationPanel : UserControl
    {
        public delegate void OnCalibrationPassHandler(object sender, OnCalibrationArgs e);
        public event OnCalibrationPassHandler OnCalibrationPass;

        public delegate void OnCalibrationFailHandler(object sender, OnCalibrationArgs e);
        public event OnCalibrationFailHandler OnCalibrationFail;


        static Random random = new Random();
        StereoCalibration calibration;
        Pair<Createc.Net.Imaging.Image> imagePair;
        double xm = 0.5, ym = 0.50;

        RegionOfInterestX roi;        
        
        public VerifyCalibrationPanel()
        {
            InitializeComponent();
            int w = 1024;
            int h = 192;
            roi = new RegionOfInterestX(w,h, 50);
            roi.Interpolator = new Linear();
            masterPictureBox.Image = new Bitmap(w, h, PixelFormat.Format32bppRgb);
            slavePictureBox.Image = new Bitmap(w, h, PixelFormat.Format32bppRgb);
            combinedPictureBox.Image = new Bitmap(w, h, PixelFormat.Format32bppRgb);
          
        }

  

        public StereoCalibration Calibration
        { 
            get { return calibration; } 
        }

        public Pair<Createc.Net.Imaging.Image> ImagePair 
        {
           get { return imagePair; }            
        }  

        public void Confirm(StereoCalibration calibration, Pair<Createc.Net.Imaging.Image> imagePair)
        {
            this.calibration = calibration;
            this.imagePair = imagePair;
            Render();
        }

        void Render()
        {
            RenderXY(xm, ym);
        }

        void RenderXY(double xm, double ym)
        {
            if (calibration == null || imagePair.A == null || imagePair.B == null)
                throw new InvalidOperationException("calibration or imagePair is null");

            //need to adjust calibration for full frames (half scale)
            double aor = calibration.AngleOfResolution * 2.0;
            FishEyeImage fishA = new FishEyeImage(imagePair.A, aor, Coord2dMult(calibration.MasterCentre, 0.5));
            FishEyeImage fishB = new FishEyeImage(imagePair.B, aor, Coord2dMult(calibration.SlaveCentre, 0.5));
            Coord2d look = new Coord2d(imagePair.A.Width * xm, imagePair.A.Height * ym);

            roi.SetLook(look, fishA);            
            var imageA = roi.GetROI(fishA);

            roi.Matrix = calibration.Matrix * roi.Matrix;
            var imageB = roi.GetROI(fishB);
            var imageC = AbsoluteDifference(imageA, imageB);

            CopyImageToBitmap(imageA, (Bitmap) masterPictureBox.Image);
            CopyImageToBitmap(imageB, (Bitmap)slavePictureBox.Image);
            CopyImageToBitmap(imageC, (Bitmap)combinedPictureBox.Image);

            var colour = Color.FromArgb(128,masterPictureBox.BackColor);
            DrawGrid((Bitmap)masterPictureBox.Image, 64, colour);
            DrawGrid((Bitmap)slavePictureBox.Image, 64, colour);
            DrawGrid((Bitmap)combinedPictureBox.Image, 64, colour);
   
            masterPictureBox.Invalidate();
            slavePictureBox.Invalidate();
            combinedPictureBox.Invalidate();
        }

        Coord2d Coord2dMult(Coord2d p, double m)
        {
            return new Coord2d(p.X * m, p.Y * m);
        }

        private void RenderRoi(FishEyeImage source, Bitmap target)
        {
            CopyImageToBitmap(roi.GetROI(source), target);
            DrawGrid(target, 64, SystemColors.ControlDarkDark);
        }

        Createc.Net.Imaging.Image AbsoluteDifference(Createc.Net.Imaging.Image a,Createc.Net.Imaging.Image b)
        {
            if (a.PixelFormat != b.PixelFormat)
                throw new ArgumentException("Images must have matching pixelformats");
            int w = Math.Min(a.Width, b.Width);
            int h = Math.Min(a.Height, b.Height);
            int s = w * (a.PixelFormat.Bpp>>3); // error if sub byte bpp 

            byte[] lut = new byte[512];
            for (int i = 1; i < 256; i++)
            {
                int v = i*5;
                if(v<256)
                {
                    lut[255 + i] =(byte) v;
                    lut[255 - i] = (byte)v;
                }
                else
                {
                    lut[255 + i] = 255;
                    lut[255 - i] = 255;
                }
            }


            var c = new Createc.Net.Imaging.Image(w, h, a.PixelFormat);
            var aB = a.Buffer;
            var bB = b.Buffer;
            var cB = c.Buffer;

            for (int y = 0; y < h; y++)
            {
                int ai = y * a.Stride;
                int bi = y * b.Stride;
                int ci = y * c.Stride;
                for (int x = 0; x < s; x++)
                {
                    cB[ci++] = lut[255 + aB[ai++] - bB[bi++]]; 
                }
            }
            return c;

            //var md = masterPictureBox.Image
        }

        static void CopyImageToBitmap(Createc.Net.Imaging.Image image, Bitmap bitmap)
        {
            if (image.Width != bitmap.Width || image.Height != bitmap.Height)
                throw new ArgumentException("bitmap and roi must be set to the same size");

            if (image.PixelFormat.Bpp != Bitmap.GetPixelFormatSize(bitmap.PixelFormat))
                throw new ArgumentException("pixel formats must match");

            var bd = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, bitmap.PixelFormat);

            if (image.Stride != bd.Stride)
            {
                bitmap.UnlockBits(bd);
                throw new ArgumentException("pixel formats must match");
            }

            System.Runtime.InteropServices.Marshal.Copy(image.Buffer, 0, bd.Scan0, image.Size);
            bitmap.UnlockBits(bd);
        }

        static void DrawGrid(Bitmap bmp,int size,Color colour)
        {
            var g = Graphics.FromImage(bmp);
            var pen = new Pen(colour,0.1f);

            int h = bmp.Height;
            int w = bmp.Width;
            int ct, sz;

            ct = w / size;
            sz = w / ct; 
            for (int i = 1; i < ct; i++)
                g.DrawLine(pen, i * sz, 0, i * sz, h);

            ct = h / size;
            sz = h / ct;
            for (int i = 1; i < ct; i++)
                g.DrawLine(pen, 0, i * sz, w, i * sz);

            pen.Dispose();
            g.Dispose();
        }

        //void Render(PictureBox pic, 

        public void GetFishEye(FrameRef frame, FishEyeImage image)
        {
            
        }

        private void tableLayout_Resize(object sender, EventArgs e)
        {

        }

        private void tableLayout_Click(object sender, EventArgs e)
        {

        }

        static PictureBox createPictureBox(int w,int h)
        {
            var bmp = new Bitmap(w, h, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            var pb = new PictureBox();
            pb.Image = bmp;
            pb.Dock = DockStyle.Fill;
            pb.SizeMode = PictureBoxSizeMode.Zoom;
            return pb;
        }





        void RaiseOnCalibrationPass(OnCalibrationArgs args)
        {
            var handler = OnCalibrationPass;
            if (handler != null)
            {
                handler(this, args);
            }
        }
        void RaiseOnCalibrationFail(OnCalibrationArgs args)
        {
            var handler = OnCalibrationFail;
            if (handler != null)
            {
                handler(this, args);
            }
        }

        private void goodButton_Click(object sender, EventArgs e)
        {
            RaiseOnCalibrationPass(new OnCalibrationArgs(calibration));
        }

        private void badButton_Click(object sender, EventArgs e)
        {
            RaiseOnCalibrationFail(new OnCalibrationArgs(calibration));
        }

        private void randomiseButton_Click(object sender, EventArgs e)
        {
            double x = 0.275 +  random.NextDouble() * 0.45;
            double y = 0.1 + random.NextDouble() * 0.8;
            RenderXY(x, y);
        }


    

    }


    public class OnCalibrationPassArgs : EventArgs
    {
        public OnCalibrationPassArgs(StereoCalibration calibration, bool isGood)
        {
            Calibration = calibration;
            IsGood = isGood;
        }
        public StereoCalibration Calibration { get; private set; }
        public bool IsGood { get; private set; }
    }
        

}
