﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Createc.StereoBirds.Viewer
{
    public partial class BirdConfirmUserControl : UserControl
    {
        public BirdConfirmUserControl()
        {
            InitializeComponent();
        }

        public bool Confirmed { get; private set; }

        void BirdConfirmUserControl_Click(object sender, EventArgs e)
        {
            Confirmed = !Confirmed;
            Invalidate();
        }

        public override string Text
        {
            get { return label.Text; }
            set { label.Text = value; }
        }

        private void BirdConfirmUserControl_Load(object sender, EventArgs e)
        {

        }

        private void BirdConfirmUserControl_Paint(object sender, PaintEventArgs e)
        {
            var sz = ClientSize;

            using (var pen = Confirmed ? new Pen(Color.Red, 2) : new Pen(SystemColors.ActiveBorder, 2))
            {
                e.Graphics.DrawRectangle(pen, 1, 1, sz.Width - 2, sz.Height - 2);
            }
            //if (Confirmed)
            //{
            //    Console.Write('*');
            //    e.Graphics.DrawRectangle(Pens.Red, new Rectangle(Point.Empty, Size));
            //    e.Graphics.DrawRectangle(Pens.Red, 24,24,48,48);
            //}
        }
    }
}
