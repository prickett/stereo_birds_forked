﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Createc.StereoBirds.Viewer
{
    public partial class LoadFilesPanel : UserControl
    {
        //public delegate void OnFrameBatchHandler(object sender, OnFrameBatchEventArgs e);
        //public event OnFrameBatchHandler OnFrameBatch;
        MapFileAcquisition aquisition;

        public LoadFilesPanel()
        {
            InitializeComponent();

            aquisition = new MapFileAcquisition();
            Attach();
            //aquisition.OnFrameBatch += OnFrameBatch;
            //aquisition.OnProgressChange += OnAquisitionProgressChange;
            //aquisition.OnFinish += OnAquisitionFinish;

            SetAquisitionButtons(false);
        }

        public MapFileAcquisition MapFileAcquisition 
        { 
            get { return aquisition; }
            set
            {
                Detach();
                aquisition = value;
                Attach();
            }
        }

        public void Quit()
        {
            if (aquisition != null)
                aquisition.Quit();
        }

        public bool Busy
        {
            get { return aquisition != null && aquisition.Busy; }
        }

        void Attach()
        {
            if (aquisition != null)
            {
                aquisition.OnFrameBatch += OnFrameBatch;
                aquisition.OnProgressChange += OnAquisitionProgressChange;
                aquisition.OnFinish += OnAquisitionFinish;
            }
        }

        void Detach()
        {
            if (aquisition != null)
            {
                aquisition.OnFrameBatch -= OnFrameBatch;
                aquisition.OnProgressChange -= OnAquisitionProgressChange;
                aquisition.OnFinish -= OnAquisitionFinish;
                aquisition = null;
            }
        }

        private void SetAquisitionButtons(bool isBusy)
        {
            fileSearchCancelButton.Enabled = isBusy;
            progressBar.Enabled = isBusy;
            openFilesButton.Enabled = !isBusy;
            openFolderButton.Enabled = !isBusy;
            progressBar.Visible = isBusy;
        }

        private void openFilesButton_Click(object sender, EventArgs e)
        {
            string[] paths;
            if (!aquisition.Busy && (paths = Utility.GetFileNamesFromDialog()) != null)
            {
                aquisition.LoadFromFiles(paths);
                SetAquisitionButtons(true);
            }
        }

        private void openFolderButton_Click(object sender, EventArgs e)
        {
            string folder;
            if (!aquisition.Busy && (folder = Utility.GetFolderFromDialog()) != null)
            {
                aquisition.LoadFromFolder(new FolderSearchParameters(folder));
                SetAquisitionButtons(true);
            }

        }

        private void fileSearchCancelButton_Click(object sender, EventArgs e)
        {
            aquisition.Quit();
        }

        #region aquisition events

        private void OnAquisitionFinish(object sender, EventArgs e)
        {
            progressBar.Value = 0;
            SetAquisitionButtons(false);
        }

        private void OnAquisitionProgressChange(object sender, OnProgressChangeEventArgs e)
        {
            progressBar.Value = Math.Min(100, e.PercentDone);
        }

        private void OnFrameBatch(object sender, OnFrameBatchEventArgs e)
        {
            foundLabel.Text = string.Format("found {0} frames", aquisition.MapFrameCount);
        }

        #endregion


    }
}
