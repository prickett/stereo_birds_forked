﻿namespace Createc.StereoBirds.Viewer
{
    partial class VerifyCalibrationPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.panel = new System.Windows.Forms.Panel();
            this.randomiseButton = new System.Windows.Forms.Button();
            this.badButton = new System.Windows.Forms.Button();
            this.goodButton = new System.Windows.Forms.Button();
            this.tableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.combinedPictureBox = new System.Windows.Forms.PictureBox();
            this.slavePictureBox = new System.Windows.Forms.PictureBox();
            this.masterPictureBox = new System.Windows.Forms.PictureBox();
            this.panel.SuspendLayout();
            this.tableLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.combinedPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.slavePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.masterPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.Dock = System.Windows.Forms.DockStyle.Top;
            this.label.Location = new System.Drawing.Point(0, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(263, 19);
            this.label.TabIndex = 3;
            this.label.Text = "Confirm Calibration";
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel
            // 
            this.panel.Controls.Add(this.randomiseButton);
            this.panel.Controls.Add(this.badButton);
            this.panel.Controls.Add(this.goodButton);
            this.panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel.Location = new System.Drawing.Point(0, 201);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(263, 32);
            this.panel.TabIndex = 6;
            // 
            // randomiseButton
            // 
            this.randomiseButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.randomiseButton.Location = new System.Drawing.Point(12, 6);
            this.randomiseButton.Name = "randomiseButton";
            this.randomiseButton.Size = new System.Drawing.Size(75, 23);
            this.randomiseButton.TabIndex = 9;
            this.randomiseButton.Text = "Randomise";
            this.randomiseButton.UseVisualStyleBackColor = true;
            this.randomiseButton.Click += new System.EventHandler(this.randomiseButton_Click);
            // 
            // badButton
            // 
            this.badButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.badButton.Location = new System.Drawing.Point(172, 6);
            this.badButton.Name = "badButton";
            this.badButton.Size = new System.Drawing.Size(75, 23);
            this.badButton.TabIndex = 8;
            this.badButton.Text = "Bad ";
            this.badButton.UseVisualStyleBackColor = true;
            this.badButton.Click += new System.EventHandler(this.badButton_Click);
            // 
            // goodButton
            // 
            this.goodButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.goodButton.Location = new System.Drawing.Point(92, 6);
            this.goodButton.Name = "goodButton";
            this.goodButton.Size = new System.Drawing.Size(75, 23);
            this.goodButton.TabIndex = 6;
            this.goodButton.Text = "Good";
            this.goodButton.UseVisualStyleBackColor = true;
            this.goodButton.Click += new System.EventHandler(this.goodButton_Click);
            // 
            // tableLayout
            // 
            this.tableLayout.ColumnCount = 1;
            this.tableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayout.Controls.Add(this.combinedPictureBox, 0, 2);
            this.tableLayout.Controls.Add(this.slavePictureBox, 0, 1);
            this.tableLayout.Controls.Add(this.masterPictureBox, 0, 0);
            this.tableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayout.Location = new System.Drawing.Point(0, 19);
            this.tableLayout.Name = "tableLayout";
            this.tableLayout.RowCount = 4;
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayout.Size = new System.Drawing.Size(263, 182);
            this.tableLayout.TabIndex = 7;
            this.tableLayout.Click += new System.EventHandler(this.tableLayout_Click);
            this.tableLayout.Resize += new System.EventHandler(this.tableLayout_Resize);
            // 
            // combinedPictureBox
            // 
            this.combinedPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.combinedPictureBox.Location = new System.Drawing.Point(3, 123);
            this.combinedPictureBox.Name = "combinedPictureBox";
            this.combinedPictureBox.Size = new System.Drawing.Size(257, 54);
            this.combinedPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.combinedPictureBox.TabIndex = 2;
            this.combinedPictureBox.TabStop = false;
            // 
            // slavePictureBox
            // 
            this.slavePictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.slavePictureBox.Location = new System.Drawing.Point(3, 63);
            this.slavePictureBox.Name = "slavePictureBox";
            this.slavePictureBox.Size = new System.Drawing.Size(257, 54);
            this.slavePictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.slavePictureBox.TabIndex = 1;
            this.slavePictureBox.TabStop = false;
            // 
            // masterPictureBox
            // 
            this.masterPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.masterPictureBox.Location = new System.Drawing.Point(3, 3);
            this.masterPictureBox.Name = "masterPictureBox";
            this.masterPictureBox.Size = new System.Drawing.Size(257, 54);
            this.masterPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.masterPictureBox.TabIndex = 0;
            this.masterPictureBox.TabStop = false;
            // 
            // VerifyCalibrationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayout);
            this.Controls.Add(this.panel);
            this.Controls.Add(this.label);
            this.Name = "VerifyCalibrationPanel";
            this.Size = new System.Drawing.Size(263, 233);
            this.panel.ResumeLayout(false);
            this.tableLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.combinedPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.slavePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.masterPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button randomiseButton;
        private System.Windows.Forms.Button badButton;
        private System.Windows.Forms.Button goodButton;
        private System.Windows.Forms.TableLayoutPanel tableLayout;
        private System.Windows.Forms.PictureBox slavePictureBox;
        private System.Windows.Forms.PictureBox masterPictureBox;
        private System.Windows.Forms.PictureBox combinedPictureBox;
    }
}
