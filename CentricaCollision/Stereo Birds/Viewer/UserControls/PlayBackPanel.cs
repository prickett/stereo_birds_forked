﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.ComponentModel;
using System.Drawing.Imaging;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CustomControls;

using Createc.Net.Imaging;
using Createc.Imaging.Thresholds;
using Createc.StereoBirds;

namespace Createc.StereoBirds.Viewer
{
    public partial class PlayBackPanel : UserControl
    {
        MapFileAcquisition aquisition;
        StereoCalibrationSet calibrationSet;
        MapFileRenderer renderer;
        CameraView master;
        CameraView slave;

        public PlayBackPanel()
        {
            InitializeComponent();

            master = new CameraView(rightView);
            slave = new CameraView(leftView);
            renderer = new MapFileRenderer();
            renderer.RenderMode = MapFileRenderer.RenderModes.Image;

            renderModeComboBox.BeginUpdate();
            renderModeComboBox.Items.Add(MapFileRenderer.RenderModes.Image);
            renderModeComboBox.Items.Add(MapFileRenderer.RenderModes.Bayer);
            renderModeComboBox.Items.Add(MapFileRenderer.RenderModes.Map);
            renderModeComboBox.Items.Add(MapFileRenderer.RenderModes.Mask);
            renderModeComboBox.SelectedIndex = 0;
            renderModeComboBox.EndUpdate();
        }

        public ZoomBox MasterView { get { return master.Zoombox; } }
        public ZoomBox SlaveView { get { return slave.Zoombox; } }
        public MarkerData MasterMarkers { get { return master.Markers; } }
        public MarkerData SlaveMarkers { get { return slave.Markers; } }

        public InterpolationMode InterpolationMode
        {
            get { return master.Zoombox.InterpolationMode; }
            set
            {
                if (value != InterpolationMode)
                {
                    master.Zoombox.InterpolationMode = value;
                    slave.Zoombox.InterpolationMode = value;
                    Invalidate();
                }
            }
        }
        public SmoothingMode SmoothingMode
        {
            get { return master.Zoombox.SmoothingMode; }
            set
            {
                if (value != SmoothingMode)
                {
                    master.Zoombox.SmoothingMode = value;
                    slave.Zoombox.SmoothingMode = value;
                    Invalidate();
                }
            }
        }
        public StereoCalibrationSet CalibrationSet
        {
            get { return calibrationSet; }
            set { calibrationSet = value; }
        }
        public MapFileAcquisition MapFileAquisition
        {
            get { return aquisition; }
            set { aquisition = value; }
        }


        public void OnAquisition(object sender, OnFrameBatchEventArgs e)
        {
            if (MasterView.Image == null)
            {
                var f = aquisition.MapFrames;
                f.Reset();
                f.MoveNext();
                Render(f.Current);
            }
            
            trackBar.Maximum = aquisition.MapFrameCount;
            Console.WriteLine(aquisition.MapFrameCount);
        }

        private void previousButton_Click(object sender, EventArgs e)
        {            
            var f = aquisition.MapFrames;
            if (f.MovePrevious())
            {
                trackBar.Value = f.CurrentIndex;
                Render(f.Current);
            }
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            var f = aquisition.MapFrames;
            if (f.MoveNext())
            {
                trackBar.Value = f.CurrentIndex;
                Render(f.Current);                
            }
        }

        public void Render(FramePair pair)
        { 
            master.Markers.FrameReference = pair;
            slave.Markers.FrameReference = pair;
            Render(pair.Master, master);
            Render(pair.Slave, slave);

            label.Text = Utility.GetFrameRefText(pair.Master);
            label.Text += string.Format("    ({0}x{1})", master.Zoombox.Image.Width, master.Zoombox.Image.Height);
        }

        void Render(FrameRef frame, CameraView view)
        {
            assignView(view.Zoombox, renderer.CreateBitmap(frame));            
            //Console.WriteLine("{0} rendering", frame.IsMaster ? "Master" : "Slave");
            view.Zoombox.Invalidate();
        }

        static void assignView(ZoomBox view, Bitmap image)
        {
            var old = view.Image;
            view.Image = image;
            if (old != null)
                old.Dispose();
            else
                view.ZoomToFit();
        }

    




        #region painting

        void view_Paint(object sender, PaintEventArgs e)
        {
            CameraView view = (ZoomBox)sender == master.Zoombox ? master : slave;
            view.Markers.Render(view.Zoombox, e.Graphics);
        }

        bool suppressOther = false;
        void view_ViewChanged(object sender, EventArgs e)
        {
            //this updates the other frames Zoom and Offset
            if (!suppressOther)
            {
                suppressOther = true;
                var view = (ZoomBox)sender;
                var other = view == leftView ? rightView : leftView;
                other.Zoom = view.Zoom;
                other.Offset = view.Offset;
                other.Invalidate();
                suppressOther = false;
            }
        }

        #endregion

        #region mouse control

        //private void view_MouseDown(object sender, MouseEventArgs e)

        private void view_MouseDown(object sender, MouseEventArgs e)
        {
            var view = (ZoomBox)sender;
            var markers = getMarkers(view);
            if (e.Button == MouseButtons.Left)
            {
                var point = view.ToPixelSpace(e.Location);
                var location = new Coord2d(point.X, point.Y);

                int index = markers.HitTest(location, 16f / view.Zoom);

                if (index < 0)
                {
                    index = markers.Add(location);
                    markers.HighLightIndex = index;
                    view.Invalidate();
                }
                markers.GrabPoint = e.Location;
                markers.GrabIndex = index;
            }
            else if (e.Button == MouseButtons.Right) //delete
            {
                int index = markers.HighLightIndex;
                if (index >= 0)
                {
                    markers.DeleteMarker(index);
                }
            }
        }

        private void view_MouseMove(object sender, MouseEventArgs e)
        {
            var view = (ZoomBox)sender;
            var markers = getMarkers(view);
            var point = view.ToPixelSpace(e.Location);
            var location = new Coord2d(point.X, point.Y);

            if (markers.GrabIndex < 0) //nothing being grabbed
            {
                int oldIndex = markers.HighLightIndex;
                int newIndex = markers.HitTest(location, 16f / view.Zoom);
                if (oldIndex != newIndex)
                {
                    markers.HighLightIndex = newIndex;
                    view.Invalidate();
                }
            }
            else //grab in progress
            {
                var a = markers.GrabPoint;
                var b = e.Location;
                float x = (float)(b.X - a.X) / view.Zoom;
                float y = (float)(b.Y - a.Y) / view.Zoom;
                markers.MoveMarker(markers.GrabIndex, x, y);
                markers.GrabPoint = b;
                view.Invalidate();
            }

        }

        private void view_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                var markers = getMarkers((ZoomBox)sender);
                markers.GrabIndex = -1;
            }
        }

        MarkerData getMarkers(ZoomBox zoomBox)
        {
            if (zoomBox == master.Zoombox)
                return master.Markers;
            if (zoomBox == slave.Zoombox)
                return slave.Markers;
            else
                throw new ArgumentException("unknown zoomBox");
        }


        #endregion

        private void trackBar_ValueChanged(object sender, EventArgs e)
        {
            //Console.Write('_');
        }

        private void trackBar_Scroll(object sender, EventArgs e)
        {
            var f = aquisition.MapFrames;
            if (f.MoveToIndex(trackBar.Value))
            {
                Render(f.Current);
            }
        }

        private void saveMarksbutton_Click(object sender, EventArgs e)
        {           
            Console.WriteLine("saving...");

            string fn = Utility.GetFileNameFromSaveDialog(string.Format("{0}.txt", MasterMarkers.FrameReference.SystemTime));
            if(fn != null)
                MarkerData.SaveMarkerData(fn,MasterMarkers,SlaveMarkers, calibrationSet);

            MasterMarkers.Clear();
            SlaveMarkers.Clear();
            //master.Markers.Clear();
            //slave.Markers.Clear();

            master.Zoombox.Invalidate();
            slave.Zoombox.Invalidate();
        }


        private void renderModeComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            renderer.RenderMode = (MapFileRenderer.RenderModes)renderModeComboBox.SelectedItem;
            Render(aquisition.MapFrames.Current);
        }

        private void commitButton_Click(object sender, EventArgs e)
        {
            MasterMarkers.Commit();
            SlaveMarkers.Commit();
            master.Zoombox.Invalidate();
            slave.Zoombox.Invalidate();
        }

   



    }


}
