﻿namespace Createc.StereoBirds.Viewer
{
    partial class PlayBackPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.previousButton = new System.Windows.Forms.Button();
            this.rightView = new CustomControls.ZoomBox();
            this.panel = new System.Windows.Forms.Panel();
            this.renderModeComboBox = new System.Windows.Forms.ComboBox();
            this.saveMarksbutton = new System.Windows.Forms.Button();
            this.trackBar = new System.Windows.Forms.TrackBar();
            this.nextButton = new System.Windows.Forms.Button();
            this.leftView = new CustomControls.ZoomBox();
            this.stereoContainer = new System.Windows.Forms.SplitContainer();
            this.label = new System.Windows.Forms.Label();
            this.commitButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.rightView)).BeginInit();
            this.panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stereoContainer)).BeginInit();
            this.stereoContainer.Panel1.SuspendLayout();
            this.stereoContainer.Panel2.SuspendLayout();
            this.stereoContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // previousButton
            // 
            this.previousButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.previousButton.Location = new System.Drawing.Point(3, 6);
            this.previousButton.Name = "previousButton";
            this.previousButton.Size = new System.Drawing.Size(75, 23);
            this.previousButton.TabIndex = 9;
            this.previousButton.Text = "Previous";
            this.previousButton.UseVisualStyleBackColor = true;
            this.previousButton.Click += new System.EventHandler(this.previousButton_Click);
            // 
            // rightView
            // 
            this.rightView.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.rightView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightView.Image = null;
            this.rightView.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bilinear;
            this.rightView.Location = new System.Drawing.Point(0, 0);
            this.rightView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rightView.Name = "rightView";
            this.rightView.Offset = new System.Drawing.Point(0, 0);
            this.rightView.Size = new System.Drawing.Size(351, 260);
            this.rightView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.rightView.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            this.rightView.TabIndex = 0;
            this.rightView.TabStop = false;
            this.rightView.Zoom = 1F;
            this.rightView.ViewChanged += new System.EventHandler(this.view_ViewChanged);
            this.rightView.Paint += new System.Windows.Forms.PaintEventHandler(this.view_Paint);
            this.rightView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.view_MouseDown);
            this.rightView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.view_MouseMove);
            this.rightView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.view_MouseUp);
            // 
            // panel
            // 
            this.panel.BackColor = System.Drawing.SystemColors.Control;
            this.panel.Controls.Add(this.commitButton);
            this.panel.Controls.Add(this.renderModeComboBox);
            this.panel.Controls.Add(this.saveMarksbutton);
            this.panel.Controls.Add(this.trackBar);
            this.panel.Controls.Add(this.previousButton);
            this.panel.Controls.Add(this.nextButton);
            this.panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel.Location = new System.Drawing.Point(0, 246);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(698, 32);
            this.panel.TabIndex = 8;
            // 
            // renderModeComboBox
            // 
            this.renderModeComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.renderModeComboBox.FormattingEnabled = true;
            this.renderModeComboBox.Location = new System.Drawing.Point(647, 8);
            this.renderModeComboBox.Name = "renderModeComboBox";
            this.renderModeComboBox.Size = new System.Drawing.Size(48, 21);
            this.renderModeComboBox.TabIndex = 13;
            this.renderModeComboBox.SelectionChangeCommitted += new System.EventHandler(this.renderModeComboBox_SelectionChangeCommitted);
            // 
            // saveMarksbutton
            // 
            this.saveMarksbutton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.saveMarksbutton.Location = new System.Drawing.Point(246, 6);
            this.saveMarksbutton.Name = "saveMarksbutton";
            this.saveMarksbutton.Size = new System.Drawing.Size(75, 23);
            this.saveMarksbutton.TabIndex = 12;
            this.saveMarksbutton.Text = "Save";
            this.saveMarksbutton.UseVisualStyleBackColor = true;
            this.saveMarksbutton.Click += new System.EventHandler(this.saveMarksbutton_Click);
            // 
            // trackBar
            // 
            this.trackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBar.LargeChange = 10;
            this.trackBar.Location = new System.Drawing.Point(327, 6);
            this.trackBar.Maximum = 0;
            this.trackBar.Name = "trackBar";
            this.trackBar.Size = new System.Drawing.Size(314, 45);
            this.trackBar.TabIndex = 11;
            this.trackBar.Scroll += new System.EventHandler(this.trackBar_Scroll);
            this.trackBar.ValueChanged += new System.EventHandler(this.trackBar_ValueChanged);
            // 
            // nextButton
            // 
            this.nextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.nextButton.Location = new System.Drawing.Point(84, 6);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 6;
            this.nextButton.Text = "Next";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // leftView
            // 
            this.leftView.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.leftView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftView.Image = null;
            this.leftView.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bilinear;
            this.leftView.Location = new System.Drawing.Point(0, 0);
            this.leftView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.leftView.Name = "leftView";
            this.leftView.Offset = new System.Drawing.Point(0, 0);
            this.leftView.Size = new System.Drawing.Size(346, 260);
            this.leftView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.leftView.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            this.leftView.TabIndex = 0;
            this.leftView.TabStop = false;
            this.leftView.Zoom = 1F;
            this.leftView.ViewChanged += new System.EventHandler(this.view_ViewChanged);
            this.leftView.Paint += new System.Windows.Forms.PaintEventHandler(this.view_Paint);
            this.leftView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.view_MouseDown);
            this.leftView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.view_MouseMove);
            this.leftView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.view_MouseUp);
            // 
            // stereoContainer
            // 
            this.stereoContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stereoContainer.IsSplitterFixed = true;
            this.stereoContainer.Location = new System.Drawing.Point(0, 18);
            this.stereoContainer.Margin = new System.Windows.Forms.Padding(3, 3, 3, 60);
            this.stereoContainer.Name = "stereoContainer";
            // 
            // stereoContainer.Panel1
            // 
            this.stereoContainer.Panel1.Controls.Add(this.leftView);
            // 
            // stereoContainer.Panel2
            // 
            this.stereoContainer.Panel2.Controls.Add(this.rightView);
            this.stereoContainer.Size = new System.Drawing.Size(698, 260);
            this.stereoContainer.SplitterDistance = 346;
            this.stereoContainer.SplitterWidth = 1;
            this.stereoContainer.TabIndex = 7;
            // 
            // label
            // 
            this.label.Dock = System.Windows.Forms.DockStyle.Top;
            this.label.Location = new System.Drawing.Point(0, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(698, 18);
            this.label.TabIndex = 6;
            this.label.Text = "Playback";
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // commitButton
            // 
            this.commitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.commitButton.Location = new System.Drawing.Point(165, 6);
            this.commitButton.Name = "commitButton";
            this.commitButton.Size = new System.Drawing.Size(75, 23);
            this.commitButton.TabIndex = 14;
            this.commitButton.Text = "Commit";
            this.commitButton.UseVisualStyleBackColor = true;
            this.commitButton.Click += new System.EventHandler(this.commitButton_Click);
            // 
            // PlayBackPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel);
            this.Controls.Add(this.stereoContainer);
            this.Controls.Add(this.label);
            this.Name = "PlayBackPanel";
            this.Size = new System.Drawing.Size(698, 278);
            ((System.ComponentModel.ISupportInitialize)(this.rightView)).EndInit();
            this.panel.ResumeLayout(false);
            this.panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leftView)).EndInit();
            this.stereoContainer.Panel1.ResumeLayout(false);
            this.stereoContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stereoContainer)).EndInit();
            this.stereoContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button previousButton;
        private CustomControls.ZoomBox rightView;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button nextButton;
        private CustomControls.ZoomBox leftView;
        private System.Windows.Forms.SplitContainer stereoContainer;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.TrackBar trackBar;
        private System.Windows.Forms.Button saveMarksbutton;
        private System.Windows.Forms.ComboBox renderModeComboBox;
        private System.Windows.Forms.Button commitButton;
    }
}
