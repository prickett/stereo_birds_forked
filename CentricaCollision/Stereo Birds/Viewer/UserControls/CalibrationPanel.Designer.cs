﻿namespace Createc.StereoBirds.Viewer
{
    partial class CalibrationPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.stereoContainer = new System.Windows.Forms.SplitContainer();
            this.leftView = new CustomControls.ZoomBox();
            this.rightView = new CustomControls.ZoomBox();
            this.panel = new System.Windows.Forms.Panel();
            this.badImageButton = new System.Windows.Forms.Button();
            this.calibrationButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.stereoContainer)).BeginInit();
            this.stereoContainer.Panel1.SuspendLayout();
            this.stereoContainer.Panel2.SuspendLayout();
            this.stereoContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightView)).BeginInit();
            this.panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.Dock = System.Windows.Forms.DockStyle.Top;
            this.label.Location = new System.Drawing.Point(0, 0);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(344, 18);
            this.label.TabIndex = 3;
            this.label.Text = "Calibration";
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // stereoContainer
            // 
            this.stereoContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.stereoContainer.IsSplitterFixed = true;
            this.stereoContainer.Location = new System.Drawing.Point(0, 18);
            this.stereoContainer.Name = "stereoContainer";
            // 
            // stereoContainer.Panel1
            // 
            this.stereoContainer.Panel1.Controls.Add(this.leftView);
            // 
            // stereoContainer.Panel2
            // 
            this.stereoContainer.Panel2.Controls.Add(this.rightView);
            this.stereoContainer.Size = new System.Drawing.Size(344, 154);
            this.stereoContainer.SplitterDistance = 171;
            this.stereoContainer.SplitterWidth = 1;
            this.stereoContainer.TabIndex = 4;
            // 
            // leftView
            // 
            this.leftView.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.leftView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftView.Image = null;
            this.leftView.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bilinear;
            this.leftView.Location = new System.Drawing.Point(0, 0);
            this.leftView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.leftView.Name = "leftView";
            this.leftView.Offset = new System.Drawing.Point(0, 0);
            this.leftView.Size = new System.Drawing.Size(171, 154);
            this.leftView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.leftView.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            this.leftView.TabIndex = 0;
            this.leftView.TabStop = false;
            this.leftView.Zoom = 1F;
            this.leftView.ViewChanged += new System.EventHandler(this.view_ViewChanged);
            this.leftView.Paint += new System.Windows.Forms.PaintEventHandler(this.view_Paint);
            this.leftView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.view_MouseDown);
            this.leftView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.view_MouseMove);
            this.leftView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.view_MouseUp);
            // 
            // rightView
            // 
            this.rightView.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.rightView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightView.Image = null;
            this.rightView.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bilinear;
            this.rightView.Location = new System.Drawing.Point(0, 0);
            this.rightView.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rightView.Name = "rightView";
            this.rightView.Offset = new System.Drawing.Point(0, 0);
            this.rightView.Size = new System.Drawing.Size(172, 154);
            this.rightView.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.rightView.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            this.rightView.TabIndex = 0;
            this.rightView.TabStop = false;
            this.rightView.Zoom = 1F;
            this.rightView.ViewChanged += new System.EventHandler(this.view_ViewChanged);
            this.rightView.Paint += new System.Windows.Forms.PaintEventHandler(this.view_Paint);
            this.rightView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.view_MouseDown);
            this.rightView.MouseMove += new System.Windows.Forms.MouseEventHandler(this.view_MouseMove);
            this.rightView.MouseUp += new System.Windows.Forms.MouseEventHandler(this.view_MouseUp);
            // 
            // panel
            // 
            this.panel.Controls.Add(this.badImageButton);
            this.panel.Controls.Add(this.calibrationButton);
            this.panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel.Location = new System.Drawing.Point(0, 172);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(344, 32);
            this.panel.TabIndex = 5;
            // 
            // badImageButton
            // 
            this.badImageButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.badImageButton.Location = new System.Drawing.Point(176, 6);
            this.badImageButton.Name = "badImageButton";
            this.badImageButton.Size = new System.Drawing.Size(75, 23);
            this.badImageButton.TabIndex = 8;
            this.badImageButton.Text = "Bad Image";
            this.badImageButton.UseVisualStyleBackColor = true;
            this.badImageButton.Click += new System.EventHandler(this.badImageButton_Click);
            // 
            // calibrationButton
            // 
            this.calibrationButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.calibrationButton.Location = new System.Drawing.Point(90, 6);
            this.calibrationButton.Name = "calibrationButton";
            this.calibrationButton.Size = new System.Drawing.Size(75, 23);
            this.calibrationButton.TabIndex = 6;
            this.calibrationButton.Text = "Calibrate";
            this.calibrationButton.UseVisualStyleBackColor = true;
            this.calibrationButton.Click += new System.EventHandler(this.calibrationButton_Click);
            // 
            // CalibrationPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.stereoContainer);
            this.Controls.Add(this.label);
            this.Controls.Add(this.panel);
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "CalibrationPanel";
            this.Size = new System.Drawing.Size(344, 204);
            this.stereoContainer.Panel1.ResumeLayout(false);
            this.stereoContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.stereoContainer)).EndInit();
            this.stereoContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leftView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightView)).EndInit();
            this.panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label;
        private System.Windows.Forms.SplitContainer stereoContainer;
        private CustomControls.ZoomBox leftView;
        private CustomControls.ZoomBox rightView;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Button calibrationButton;
        private System.Windows.Forms.Button badImageButton;
    }
}
