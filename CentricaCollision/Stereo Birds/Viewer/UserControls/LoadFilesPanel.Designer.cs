﻿namespace Createc.StereoBirds.Viewer
{
    partial class LoadFilesPanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.fileSearchCancelButton = new System.Windows.Forms.Button();
            this.foundLabel = new System.Windows.Forms.Label();
            this.openFilesButton = new System.Windows.Forms.Button();
            this.openFolderButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(45, 172);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(60, 14);
            this.progressBar.TabIndex = 14;
            this.progressBar.Visible = false;
            // 
            // fileSearchCancelButton
            // 
            this.fileSearchCancelButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.fileSearchCancelButton.Enabled = false;
            this.fileSearchCancelButton.Location = new System.Drawing.Point(24, 125);
            this.fileSearchCancelButton.Name = "fileSearchCancelButton";
            this.fileSearchCancelButton.Size = new System.Drawing.Size(101, 27);
            this.fileSearchCancelButton.TabIndex = 10;
            this.fileSearchCancelButton.Text = "Cancel";
            this.fileSearchCancelButton.UseVisualStyleBackColor = true;
            this.fileSearchCancelButton.Click += new System.EventHandler(this.fileSearchCancelButton_Click);
            // 
            // foundLabel
            // 
            this.foundLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.foundLabel.Location = new System.Drawing.Point(0, 31);
            this.foundLabel.Name = "foundLabel";
            this.foundLabel.Size = new System.Drawing.Size(149, 24);
            this.foundLabel.TabIndex = 9;
            this.foundLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // openFilesButton
            // 
            this.openFilesButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.openFilesButton.Location = new System.Drawing.Point(24, 59);
            this.openFilesButton.Name = "openFilesButton";
            this.openFilesButton.Size = new System.Drawing.Size(101, 27);
            this.openFilesButton.TabIndex = 8;
            this.openFilesButton.Text = "Open files";
            this.openFilesButton.UseVisualStyleBackColor = true;
            this.openFilesButton.Click += new System.EventHandler(this.openFilesButton_Click);
            // 
            // openFolderButton
            // 
            this.openFolderButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.openFolderButton.Location = new System.Drawing.Point(24, 92);
            this.openFolderButton.Name = "openFolderButton";
            this.openFolderButton.Size = new System.Drawing.Size(101, 27);
            this.openFolderButton.TabIndex = 7;
            this.openFolderButton.Text = "Open folder";
            this.openFolderButton.UseVisualStyleBackColor = true;
            this.openFolderButton.Click += new System.EventHandler(this.openFolderButton_Click);
            // 
            // LoadFilesPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.foundLabel);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.openFilesButton);
            this.Controls.Add(this.fileSearchCancelButton);
            this.Controls.Add(this.openFolderButton);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MinimumSize = new System.Drawing.Size(150, 148);
            this.Name = "LoadFilesPanel";
            this.Size = new System.Drawing.Size(150, 223);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Button fileSearchCancelButton;
        private System.Windows.Forms.Label foundLabel;
        private System.Windows.Forms.Button openFilesButton;
        private System.Windows.Forms.Button openFolderButton;
    }
}
