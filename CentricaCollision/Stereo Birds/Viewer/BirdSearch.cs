﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using CustomControls;
using Createc.Imaging.Thresholds;
using Createc.StereoBirds;

namespace Createc.StereoBirds.Viewer
{
    class BackgroundBirdSearch:BackgroundWorker
    {
        private const long MEAN_TIC_BETWEEN_FRAMES = 18730910; //based on rough sample of 50
        private const long CAMERA_TIC_TOLERANCE = MEAN_TIC_BETWEEN_FRAMES >> 2;//<-- any difference below this is ignored
        private const long ROLLOVER_VERIFY = MEAN_TIC_BETWEEN_FRAMES << 5;
        private const long BIT32 = 1L << 32;


        private const int MAPBLOB_AREAMAX = 64;
        private const int MAPBLOB_COUNTMAX = 128;
        private const int MAPBLOB_DISTANCEMAX = 6;
        private const int MASKBLOB_THRESHOLD = 16;
        private const int MASKBLOB_JOINDISTANCE = 12;
        private const int MASKBLOB_DISTANCEMAX = 6 * 32;
        private const int MIN_COMPARE_PIXELS = 16;
        private const float COMPARE_PASS = 0.7f;

        private IEnumerable<MapFilePair>  frames;//MapFilePairCollection
        private Thresholder masterThresh = new Thresholder();
        private Thresholder slaveThresh = new Thresholder();
        private byte[] masterBBuf = new byte[0];
        private byte[] slaveBBuf = new byte[0];
        private float[] masterFBuf = new float[0];
        private float[] slaveFBuf = new float[0];
        private float[] mNBuf = new float[0];
        private float[] sNBuf = new float[0];
        private float[] kBuf = new float[0];

        private BirdPossibleOld current;
        private List<BirdPossibleOld> hits;



        public BirdPossibleOld[] Hits
        {
            get { return hits.ToArray(); }
        }


        public void SaveResults(String path)
        {
            using (var fs = new FileStream(path,FileMode.OpenOrCreate,FileAccess.Write))
            {
                SaveResults(fs);
            }
        }
        public void SaveResults(Stream stream)
        {
            using (var bw = new BinaryWriter(stream))
            {
                SaveResults(bw);
            }
        }
        public void SaveResults(BinaryWriter writer)
        {
            writer.Write(hits.Count);
            foreach (var bsr in Hits)
            {
                writer.Write(bsr.FramePair.SystemTime.sec);
                writer.Write(bsr.FramePair.SystemTime.nsec);
                writer.Write(bsr.FramePair.FrameNumber);
                writer.Write(bsr.FramePair.Master.Path);
                writer.Write(bsr.FramePair.Slave.Path);
                writer.Write(bsr.Hits.Count);
                foreach (var hit in bsr.Hits)
                {
                    writer.Write(hit.Master.X);
                    writer.Write(hit.Master.Y);
                    writer.Write(hit.Slave.X);
                    writer.Write(hit.Slave.Y);
                    writer.Write(hit.Score);
                    writer.Write(hit.PixelCount);
                }
            }
        }


        //private static Form1 debugForm;

        public class PossiblePairs 
        {
            public PossiblePairs(FramePair framePair, List<BlobPairOld> pairs)
            {
                FramePair = framePair;
                Pairs = pairs;
            }
            public FramePair FramePair {get;private set;}
            public List<BlobPairOld> Pairs {get;private set;}
        }


        public BackgroundBirdSearch(mainFormOld debugForm)
        {
            //BackgroundBirdSearch.debugForm = debugForm;
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
            DoWork += new DoWorkEventHandler(birdSearch_DoWork);
            ProgressChanged += new ProgressChangedEventHandler(birdSearch_ProgressChganged);
            RunWorkerCompleted += new RunWorkerCompletedEventHandler(birdSearch_RunWorkerCompleted);
        }

        public bool Search(IEnumerable<MapFilePair> mapPairs) //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! MapFilePairCollection
        {
            if (!IsBusy && mapPairs != null)
            {
                hits = new List<BirdPossibleOld>();
                frames = mapPairs;/// new FrameCollection(mapPairs);
                RunWorkerAsync(frames);
                return true;
            }
            return false;
        }

        public void birdSearch_DoWork(object sender, DoWorkEventArgs e)
        {
            const int MASK = (1 << 5) - 1; //power of 2 -1, 31
            const int SAVEMASK = (1 << 10) - 1; //power of 2 -1, 31
            const int COUNTMASK = (1<<10)-1;
            BackgroundWorker worker = sender as BackgroundWorker;
            IEnumerable<MapFilePair> frames = (IEnumerable<MapFilePair>)e.Argument;//MapFilePairCollection

            int counter = 0;
            
            foreach (var pair in frames)
            {
                if ((counter++ & MASK) == 0)
                {
                    worker.ReportProgress(((counter & COUNTMASK) * 100) / (COUNTMASK+1));
                }


                if (pair.Master.isOpen && pair.Slave.isOpen)
                {
                    current = new BirdPossibleOld(new FramePair(pair.Master.FrameReference, pair.Master.FrameReference));
                    SearchMap(pair.Master, pair.Slave);
                    hits.Add(current);
                }
                else
                {
                    Console.Write(" Bad Pair ");
                }

                if (pair.Master.isOpen)
                    pair.Master.Close();
                if (pair.Slave.isOpen)
                    pair.Slave.Close();

                if ((worker.CancellationPending == true))
                {
                    e.Cancel = true;
                    break;
                }

                if ((counter & SAVEMASK) == 0)
                {
                    saveCurrentResults();
                }
            }

            saveCurrentResults();
            worker.ReportProgress(100);
            e.Result = counter;
        }

        private void saveCurrentResults()
        {
            int ct = hits.Count;
            if (ct > 0)
            {
                string path = string.Format("{0}_{1}.birdPoss", hits[0].FramePair.SystemTime, hits[ct - 1].FramePair.SystemTime);
                SaveResults(path);
            }
            hits.Clear();
        }


        public void birdSearch_ProgressChganged(object sender, ProgressChangedEventArgs e)
        {
            //Console.WriteLine(e.ProgressPercentage);
        }

        public void birdSearch_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            saveCurrentResults();

            if ((e.Cancelled == true))
            {
                Console.WriteLine("Canceled!");
            }
            else if (!(e.Error == null))
            {
                Console.WriteLine("Error: " + e.Error.Message);
            }
            else
            {
                Console.WriteLine("Done!");
            }
        }



        public void SearchMap(MapFile master, MapFile slave)
        {
            if (!master.isOpen | !slave.isOpen)
            {
                Console.WriteLine("both files must be open");
                return;
            }

            if (thresholdMap(master, masterThresh) && thresholdMap(slave, slaveThresh))
            {
                var mapPairs = pairMapBlobs(masterThresh.BlobList, slaveThresh.BlobList, MAPBLOB_DISTANCEMAX);
                int mct = masterThresh.BlobList.Count;
                int sct = slaveThresh.BlobList.Count;

                if (mapPairs.Count > 0)
                {
                    foreach (var poss in mapPairs)
                    {                     
                        thresholdMask(ref masterBBuf, master, poss.Master, masterThresh);
                        thresholdMask(ref slaveBBuf, slave, poss.Slave, slaveThresh);

                        joinCloseBlobs(masterThresh.BlobList,MASKBLOB_JOINDISTANCE);
                        joinCloseBlobs(slaveThresh.BlobList, MASKBLOB_JOINDISTANCE);

                        //joinAll(masterThresh.BlobList);
                        //joinAll(slaveThresh.BlobList);
                        bayerCompare(masterThresh.BlobList, slaveThresh.BlobList, master, slave);                    
                    }
                }


                //Console.Write("[{0} {1}] {2} pairs found",  mct,  sct, mapPairs.Count);
            }
            //else if(masterThresh.BlobList 
            //{
            //    Console.Write("rejecting on map blob counts {2} {3}", master.Header.SystemTime, master.Header.FrameCount, masterThresh.BlobList.Count, slaveThresh.BlobList.Count);
            //}
            return;
        }

        private static bool thresholdMap(MapFile frame, Thresholder thresholder)
        {
            thresholder.StickyCorners = true;
            thresholder.Threshold1bit(frame.GetMap(), frame.Header.MapWidth);
            filter(thresholder.BlobList, MAPBLOB_AREAMAX);
            int ct = thresholder.BlobList.Count;
            return ct > 0 && ct < MAPBLOB_COUNTMAX;
        }
        private static bool thresholdMask(ref byte[] frameBuffer, MapFile frame, Blob blob, Thresholder thresholder)
        {
            thresholder.StickyCorners = true;
            int m = frame.Header.MapUnitSize >> 1;
            frame.GetMaskRegion(ref frameBuffer, blob.X, blob.Y, blob.Width, blob.Height);

            thresholder.Threshold8bit(frameBuffer, blob.Width * m, blob.Height * m, 0, blob.Width * m, MASKBLOB_THRESHOLD);
            offsetBlobs(thresholder.BlobList, blob.X * m, blob.Y * m);
            return true;//!!!!!!!!!
        }


        private void bayerCompare(List<Blob> masters, List<Blob> slaves, MapFile master, MapFile slave)
        {
            int m = master.Header.MapUnitSize;
            int d = m >> 1;
            int mx, my, mw, mh;
            int sx, sy, sw, sh;
            float mcx, mcy, scx, scy;
            int ox, oy;
            int size;

            foreach (var mb in masters)
            {
                mx = mb.X / d;
                my = mb.Y / d;
                mw = (mb.Width + d - 1) / d;
                mh = (mb.Height + d - 1) / d;
                mcx = mb.CX / d - mx;
                mcy = mb.CY / d - my;

                size = master.GetBayerRegion(ref masterBBuf, mx, my, mw, mh);
                //if (size != mw * m * mh * m)
                //    Console.WriteLine("??");

                convertToFloat(ref masterBBuf, ref masterFBuf, size);
                //debugForm.DebugBayerBuf(masterBuf, mx * m, my * m, mw * m, mh * m);
                foreach (var sb in slaves)
                {
                    sx = sb.X / d;
                    sy = sb.Y / d;
                    sw = (sb.Width + d - 1) / d;
                    sh = (sb.Height + d - 1) / d;
                    scx = sb.CX / d - sx;
                    scy = sb.CY / d - sy;

                    ox = (int)((mcx - scx) * m);
                    oy = (int)((mcy - scy) * m);
                    size = slave.GetBayerRegion(ref slaveBBuf, sx, sy, sw, sh);
                    convertToFloat(ref slaveBBuf, ref slaveFBuf, size);

                    int ct,x=ox,y=oy;
                    float score= bayerCompareCore(mw*m, mh*m, sw*m, sh*m, ref x, ref y,out ct);
                    if (score >= COMPARE_PASS)
                    {
                        var hit = new HitOld((int)mb.CX << 1, (int)mb.CY << 1, (int)sb.CX << 1, (int)sb.CY << 1, score, ct);
                        current.Hits.Add(hit);
                        //Console.Write("\n(Hit {0},{1} [{2}  {3}])", ((int)mb.CX << 1) + ox, ((int)mb.CY << 1) + oy, score, ct);
                    }
                    else
                    {
                        //Console.Write("*");
                    }
                    //debugForm.DebugBayerBuf(slaveBBuf, mx * m + ox, my * m + oy, sw * m, sh * m);
                }
            }
        }

        private void convertToFloat(ref byte[] source, ref float[] target,int count)
        {
            if (source.Length < count)
                throw new ArgumentOutOfRangeException("count cannot exceed source.Length");
            sizeBuffer(ref target, count);
            for (int i = 0; i < count; i++)
            {
                target[i] = source[i]; 
            }
        }

        private void sizeBuffer<T>(ref T[] buffer, int size)
        {
            if (buffer==null || buffer.Length < size)
                buffer = new T[size];
        }


        private float bayerCompareCore(int mw, int mh, int sw, int sh, ref int x, ref int y,out int pixelCount)
        {
            x &= -2;
            y &= -2;

            const int STEP = 2;

            float score;
            float best = int.MinValue;
            int ox, oy, bx = -1, by = -1;
            int ct;
            pixelCount = 0;

            var xs = new int[] { 0, 1, 1, 1, 0, -1, -1, -1, 0 };
            var ys = new int[] { 0, 1, 0, -1, -1, -1, 0, 1, 1 };

            for (int i = 0; i < 9; i += 2)
            {
                ox = x + xs[i] * STEP;
                oy = y + ys[i ] * STEP;
                score = bayerCompareCoreX(mw, mh, sw, sh, ox, oy,out ct);
                if (score > best)
                {
                    pixelCount = ct;
                    best = score;
                    bx = ox; by = oy;
                }
            }
            x = bx;
            y = by;
            return best;
        }


        //if (a > aMx)
        //    aMx = a;
        //else if (a < aMn)
        //    aMn = a;

        //if (b > bMx)
        //    bMx = b;
        //else if (b < bMn)
        //    bMn = b;
        private float bayerCompareCoreX(int mw, int mh, int sw, int sh, int ox, int oy, out int pixelCount)
        {
            int x0, x1, y0, y1;
            x0 = Math.Max(0, -ox);
            y0 = Math.Max(0, -oy);
            x1 = Math.Min(mw, -ox + sw);
            y1 = Math.Min(mh, -oy + sh);

            int size = Math.Max(mw * mh, sw * sh);
            sizeBuffer(ref kBuf,size ); //size out utility buffer to largest possible
            sizeBuffer(ref mNBuf, size); //size out utility buffer to largest possible
            sizeBuffer(ref sNBuf, size); //size out utility buffer to largest possible

            int count = 0;
            int aSum = 0, bSum = 0;
            float repCt;
            float a, b, aλ, bλ, aσ, bσ;

            //build mask and get count;
            {
                int ai, bi;
                for (int y = y0, i = 0; y < y1; y++)
                {
                    int mi = y * mw;
                    int si = (y + oy) * sw + ox;
                    for (int x = x0; x < x1; x++, i++)
                    {
                        ai = masterBBuf[mi + x];
                        bi = slaveBBuf[si + x];
                        if ((int)ai * bi != 0)
                        {
                            count++;
                            kBuf[i] = 1f;
                            aSum += ai;
                            bSum += bi;
                        }
                        else //might be better to just clear the array??
                        {
                            kBuf[i] = 0f;
                        }
                    }
                }
            }
            pixelCount = count;

            if (count < MIN_COMPARE_PIXELS)
            {                
                return -1;
            }

            repCt = 1f / (float)count;
            aλ = (float)aSum * repCt;
            bλ = (float)bSum * repCt;

            //subtract mean and multiply mask

            aσ = 0;
            bσ = 0;

            for (int y = y0, i = 0; y < y1; y++)
            {
                int mi = y * mw;
                int si = (y + oy) * sw + ox;
                for (int x = x0; x < x1; x++, i++)
                {
                    a = kBuf[i] * (masterFBuf[mi + x] - aλ);
                    mNBuf[i] = a;
                    aσ += a*a;

                    b = kBuf[i] * (slaveFBuf[si + x] - bλ);
                    sNBuf[i] = b;
                    bσ += b*b;

                }
            }
            aσ = 1f / (float)Math.Sqrt(aσ * repCt);
            bσ = 1f / (float)Math.Sqrt(bσ * repCt);

            float ab = 0;
            for (int y = y0, i = 0; y < y1; y++)
            {
                for (int x = x0; x < x1; x++, i++)
                {
                    a = mNBuf[i] * aσ;
                    b = sNBuf[i] * bσ;
                    ab += a * b;
                }
            }

            return ab * repCt;
        }


        //private void buildMask()
        //{
        //}
        private static void bayerGet(ref byte[] frameBuffer, MapFile frame, Blob b)
        {
            int m = frame.Header.MapUnitSize >> 1;
            frame.GetBayerRegion(ref frameBuffer, b.X / m, b.Y / m, b.Width/m, b.Height/m);
        }

        private static void offsetBlobs(List<Blob> blobs, int x, int y)
        {
            foreach (var blob in blobs)
            {
                blob.Offset(x, y);
            }
        }

        private static void joinAll(List<Blob> blobs)
        {
            for (int i = blobs.Count - 1; i > 0; i--)
            {
                blobs[0].Absorb(blobs[i]);
                blobs.RemoveAt(i);
            }
            
        }

        private static void joinCloseBlobs(List<Blob> blobs, int distance)
        {
            
            int ub = blobs.Count - 1;
            if (ub > 0)
            {
                var hits = new bool[(ub*(ub+1))>>1];

                for (int i = 0,h=0; i < ub; i++)
                {
                    var a = new Blob(blobs[i]);
                    for (int j = i + 1; j <= ub; j++)
                    {
                        var b = new Blob(blobs[j]);
                        if (a.XMax + distance > b.X &&
                            b.XMax + distance > a.X &&
                            a.YMax + distance > b.Y &&
                            b.YMax + distance > a.Y) // hit
                        { 
                            hits[h]=true;
                        }
                        h++;    
                    }
                }


                for (int i = 0,h=0; i < ub; i++)
                {
                    bool absorbed = false;
                    for (int j = i + 1; j <= ub; j++)
                    {                        
                        if (hits[h])
                        {
                            blobs[j].Absorb(blobs[i]);
                            blobs[i] = blobs[j];
                            absorbed = true;
                        }
                        h++;
                    }
                    if (absorbed)
                        blobs[i] = Blob.Empty;
                }

                for (int i = ub,j=ub; i >= 0; i--)
                {
                    if (blobs[i].Area == 0)
                    {
                        blobs[i] = blobs[j];
                        blobs.RemoveAt(j);
                        j--;
                    }

                }
            }
        }




        private static List<BlobPairOld> pairMapBlobs( List<Blob> masterList, List<Blob> slaveList, float maxDistance)
        {
            float max = maxDistance * maxDistance;
            var pairs = new List<BlobPairOld>();
            
            List<Blob> bigList, smallList;

            if (masterList.Count <= slaveList.Count)
            {
                smallList = masterList;
                bigList = slaveList;
            }
            else
            {
                smallList = slaveList;
                bigList = masterList;
            }

            int ct = 0;
            foreach (var blob in smallList)
            {
                Blob hitZone = new Blob();
                foreach (var blobOther in bigList)
                {
                    ct++;
                    if (areaCompare(blob, blobOther) > 0 && squareDistance(blob.CX - blobOther.CX, blob.CY - blobOther.CY) < max)
                    {
                        hitZone.Absorb(new Blob(blobOther));
                    }
                }

                if (hitZone.Area > 0)
                {
                    if(smallList == masterList)
                        pairs.Add(new BlobPairOld() { Master = blob, Slave = hitZone });
                    else
                        pairs.Add(new BlobPairOld() { Master = hitZone, Slave = blob });
                    //Console.Write("[{0}]",  closestBlobs.Count);
                }
            }
            //Console.Write("(Calls->{0})", ct);
            return pairs;
        }

        //private static PossiblePairs pairBlobs(List<Blob> masterList, List<Blob> slaveList,float maxDistance)
        //{
        //    float max = maxDistance * maxDistance;
        //    var pairs = new PossiblePairs(masterList.Count <= slaveList.Count);
        //    List<Blob> bigList, smallList;

        //    if (pairs.IsMaster)
        //    {
        //        smallList = masterList;
        //        bigList = slaveList;
        //    }
        //    else
        //    {
        //        smallList = slaveList;
        //        bigList = masterList;
        //    }

        //    int ct = 0;
        //    foreach (var blob in smallList)
        //    {
        //        List<Blob> closestBlobs = new List<Blob>();
        //        foreach (var blobOther in bigList)
        //        {
        //            ct++;
        //            if (areaCompare(blob, blobOther) > 0 &&  squareDistance(blob.CX - blobOther.CX, blob.CY - blobOther.CY) < max)
        //            {
        //                closestBlobs.Add(blobOther);
        //            }
        //        }

        //        if (closestBlobs.Count > 0)
        //        {
        //            pairs.Add(Tuple.Create(blob, closestBlobs));
        //            //Console.Write("[{0}]",  closestBlobs.Count);
        //        }
        //        else
        //        {
        //            //Console.Write("[{0}x{1} {2}]", blob.CX, blob.CY, closestBlobs.Count);
        //        }
        //    }
        //    Console.Write("(Calls->{0})", ct);
        //    return pairs;

 
            
        //}

        private static int areaCompare(Blob a, Blob b)
        {
            int m = a.Area + 1; //1 added to allow a larger ratio with small areas 1:4 match is fine
            int n = b.Area + 1; //example accepted ratio upper bounds
            int d = m - n;
            return m * n - d * d;
        }



        private static float squareDistance(float x, float y)
        {
            return x * x + y * y;
        }




        private static void filter(List<Blob> blobs, int maxArea)
        {
            int last = blobs.Count - 1;
            for (int i = last; i >= 0; i--)
            {
                if (blobs[i].Area > maxArea)
                {
                    blobs[i] = blobs[last];
                    blobs.RemoveAt(last);
                    last--;
                }
            }
        }

        //private void swap<T>(ref T a, ref T b)
        //{
        //    T swp = a;
        //    a = b;
        //    b = swp;
        //}
       
    }


    public class BirdPossibleOld
    {
        public BirdPossibleOld(FramePair framePair)
        {
            FramePair = framePair;
            Hits = new List<HitOld>();
        }
        public FramePair FramePair { get; private set; }
        public List<HitOld> Hits { get; private set; }
    }

    public struct HitOld
    {
        public HitOld(int mx, int my, int sx, int sy, float score, int pixelCount)
            : this()
        {
            Master = new Point(mx, my);
            Slave = new Point(sx, sy);
            Score = score;
            PixelCount = pixelCount;
        }
        public Point Master { get; private set; }
        public Point Slave { get; private set; }
        public float Score { get; private set; }
        public int PixelCount { get; private set; }
    }

    public struct BlobPairOld
    {
        public Blob Master;
        public Blob Slave;
    }
}
