﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Createc.Net.Imaging;
using Createc.Net.Rotation;
using Createc.Net.Vector;

namespace Createc.StereoBirds.Viewer
{
    [Serializable()]
    public class MapFileCalibrationSet
    {
        List<MapFileCalibrationData> set;

        public MapFileCalibrationSet()
        {
            //var r = new Random();
            //int w = r.Next(100, 1000);
            //int h = r.Next(100, 1000);
            //Width = w;
            //Height = h;
            //Id = GetHashCode();
            //set = new List<MapFileCalibrationData>();
            //for (int i = 0; i < 100; i++)
            //{

            //    var md = new MapFileCalibrationData()
            //    {
            //        AngleOfResolution = r.NextDouble(),
            //        Matrix = new Matrix3x3(new Vector3d(r.NextDouble(), r.NextDouble(), r.NextDouble()), r.NextDouble() * Math.PI * 2),
            //        MasterCentre = new Coord2d(r.NextDouble() * w, r.NextDouble() * h),
            //        SlaveCentre = new Coord2d(r.NextDouble() * w, r.NextDouble() * h),
            //        Error = r.NextDouble()
            //    };
            //    set.Add(md);
            //}
        }

        public int MasterId { get; set; }
        public int SlaveId { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public MapFileCalibrationData[] Set
        {
            get { return set.ToArray(); }
            set { set = new List<MapFileCalibrationData>(value); }
        }
    }

    public class MapFileCalibrationData
    {
        public long SystemTime { get; set; }
        public double AngleOfResolution { get; set; }
        public Coord2d MasterCentre { get; set; }
        public Coord2d SlaveCentre { get; set; }
        public Matrix3x3 Matrix { get; set; }
        public double Error { get; set; }
    }
}
