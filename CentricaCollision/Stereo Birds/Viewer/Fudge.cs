﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using Createc.StereoBirds;

namespace Createc.StereoBirds.Viewer
{
    class Fudge
    {
        private static FolderBrowserDialog folderDialog = new FolderBrowserDialog();

        public static List<FramePair> LoadPairs()
        {
            string[] paths = GetPathsFromDialogue();            
            return paths!=null?  PairFrames(paths):null;
        }

        public static List<FramePair> LoadPairsFromFolder()
        {
            Console.WriteLine("select folder");
            string[] paths = GetFilesFromFolderDialogue();

            List<FramePair> result;
            if (paths != null)
            {
                Console.WriteLine("got list of {0} files", paths.Length);
                result = PairFrames(paths);// : null;
            }
            else
            {
                result = null;
            }
            
            Console.WriteLine("finished ordering", paths.Length);
            return result;
        }



        public static List<FramePair> PairFrames(string[] paths)
        {
            List<FrameRef> masters, slaves;
            splitMastersAndSlaves(paths, out masters, out slaves);

            GetCameraTicks(masters);
            GetCameraTicks(slaves);

            return FooBarry(masters, slaves);
        }

        static void splitMastersAndSlaves(string[] paths, out List<FrameRef> masters, out  List<FrameRef> slaves)
        {
            masters = new List<FrameRef>();
            slaves = new List<FrameRef>();
            foreach (var path in paths)
            {
                var fRef = new FrameRef(path);
                if (fRef.Type == FrameType.Map)
                {
                    if (fRef.IsMaster)
                        masters.Add(fRef);
                    else
                        slaves.Add(fRef);
                }
            }
        }


        static int Compare(FrameRef a, FrameRef b)
        {
            int c = a.CompareTo(b);
            if (c == 0 && a != b)
            {
                ulong d = a.CameraTick - b.CameraTick;

                //if (d > 0x40000000)
                //    d -= 0x100000000;
               // else if(-d > 0x40000000)
                //    d += 0x100000000;

                //Console.WriteLine("{0}\t{1}\t{2}",d,a.FrameNumber,b.FrameNumber);

                //return (int) d;


                //Console.WriteLine("{0} {1}", a.Path, b.Path);
                //Console.WriteLine("{0} {1}", a.CameraTick ,b.CameraTick);
                //int f = (int)d;
                //if (f == d)
                    //c = f;
            }
            return c;
        }


        static List<FramePair> BananaBox(List<FrameRef> masters, List<FrameRef> slaves)
        {
            var pairs = new List<FramePair>(slaves.Count);

            slaves.Sort(Compare);
            syncFrames(masters, slaves, 12);

            if (slaves == null || masters == null)
                throw new ArgumentNullException();

            if (slaves.Count == 0 || masters.Count == 0)
                return pairs;

            FrameRef master, slave;
            master = masters[0];
            slave = slaves[0];

            int dropped = 0;
            ulong md, m0, m1 = master.CameraTick;
            ulong sd, s0, s1 = slave.CameraTick;

            pairs.Add(new FramePair(master, slave));

            for (int s = 1, m = 1; s < slaves.Count && m < masters.Count; s++)
            {
                s0 = s1;
                slave = slaves[s];
                s1 = slave.CameraTick;
                if (s0 > s1)
                {
                    if ((s0 - s1 & 0x80000000) != 0) //overflow
                    {
                        s0 -= 0x100000000;
                        //Console.WriteLine("^{0}:{1}", s0, s1);
                        dropped = 5;
                    }
                    else //swap slave, not because
                    {
                        swap(ref s0, ref s1);
                        int ub = pairs.Count - 1;
                        FrameRef t = pairs[ub].Slave;
                        pairs[ub] = new FramePair(master, slave);
                        slave = t;
                        dropped = 5;
                        //Console.WriteLine("<>{0}:{1}", s0, s1);
                    }
                }

                sd = s1 - s0;
                m0 = m1;
                do
                {
                    master = masters[m];
                    m1 = master.CameraTick;
                    if (m0 > m1)
                        m0 -= 0x100000000;
                    md = m1 - m0;
                    md = md & 0xffffffff;
                } while (++m < masters.Count && Math.Round((double)sd / md) > 1);

                if (dropped > 0)
                {
                    dropped--;
                    long df = master.FrameNumber - slave.FrameNumber;
                    //Console.WriteLine("{0}:{1}\t{2}:{3} {4} {5}", 
                    //    sd, md,
                    //    master.FrameNumber, 
                    //    slave.FrameNumber,
                    //    df,
                    //    Math.Abs(df) < 2? "Good":"Bad");
                }

                if (m < masters.Count)
                {
                    pairs.Add(new FramePair(master, slave));
                }
            }
            pairs.TrimExcess();//get rid of any extra capacity
            Console.WriteLine("done");
            return pairs;
        }






        static List<FramePair> FooBarry(List<FrameRef> masters, List<FrameRef> slaves)
        {
            if (slaves == null || masters == null)
                throw new ArgumentNullException();

            var pairs = new List<FramePair>(slaves.Count);

            if (slaves.Count == 0 || masters.Count == 0)
                return pairs;

            slaves.Sort();
            syncFrames(masters, slaves, 5);

            int masterIndex = 0;
            int slaveIndex = 0;
            PairUpRun(masters, ref masterIndex, slaves, ref slaveIndex, pairs);
            Console.WriteLine("{0} {1}", masterIndex, slaveIndex);

            pairs.TrimExcess();//get rid of any extra capacity 
            Console.WriteLine("done");
            return pairs;
        }

        static ulong getTick(FrameRef frame, ref ulong previousTick, out ulong tick)
        {
            tick = frame.CameraTick;
            if (previousTick > tick) //handle overflow
                previousTick -= 0x100000000; 
            return tick - previousTick;
        }


        

        static void Sync(List<FrameRef> masters, ref int masterIndex, List<FrameRef> slaves, ref int slaveIndex)
        {
            //local copys to save repeated calls to getter
            int masterCount = masters.Count;
            int slaveCount = slaves.Count;

            while (masterIndex < masterCount && masters[masterIndex].CompareTo(slaves[slaveIndex]) < 0)
                masterIndex++;

            while (slaveIndex < slaveCount && slaves[slaveIndex].CompareTo(masters[masterIndex]) < 0)
                slaveIndex++;

            if (masterIndex < masterCount && slaveIndex < slaveCount)
            {

            }

            

        }

        static void debugFrame(List<FrameRef> masters, List<FrameRef> slaves, int masterIndex, int slaveIndex, long mDif, long sDif)
        {
            FrameRef m = masters[masterIndex];
            FrameRef s = slaves[slaveIndex];
            Console.WriteLine("{0}:{1}\t{2}:{3}\t{4}\t{5}\t{6}",
                masterIndex,slaveIndex,
                mDif,sDif,
                m.SystemTime.Seconds() - s.SystemTime.Seconds(),
                m.FrameNumber - s.FrameNumber,
                m.CameraTick - s.CameraTick);
        }

        static void debugFrames(List<FrameRef> masters, List<FrameRef> slaves)
        {
            int count = Math.Min(masters.Count, slaves.Count);
            for (int i = 0; i < count; i++)
                Console.WriteLine("{0}\t{1}:{2}", i, masters[i].CameraTick, slaves[i].CameraTick);
        }

        static void PairUpRun(List<FrameRef> masters, ref int masterIndex, List<FrameRef> slaves, ref int slaveIndex, List<FramePair> pairs)
        {
            const long MAXDIF = 500;
            const long MAXDROP = 2;

            ulong mDif, mTic, mOld = masters[masterIndex].CameraTick;
            ulong sDif, sTic, sOld = slaves[slaveIndex].CameraTick;

            //local copys to save repeated calls to getter
            int masterCount = masters.Count;
            int slaveCount = slaves.Count;

            //debugFrames(masters, slaves);

            while (masterIndex < masterCount && slaveIndex < slaveCount)
            {
                mDif = getTick(masters[masterIndex], ref mOld, out mTic);
                sDif = getTick(slaves[slaveIndex], ref sOld, out sTic);

                //debugFrame(masters, slaves, masterIndex, slaveIndex, mDif, sDif);

                for (int i = 0; i < MAXDROP; i++) //allow for MAXDROP dropped frames 
                {
                    if ((sDif - mDif) > MAXDIF && ++masterIndex < masterCount) //common case
                    {
                        mDif = getTick(masters[masterIndex], ref mOld, out mTic);
                    }
                    else if ((mDif - sDif) > MAXDIF && ++slaveIndex < slaveCount) //rare case
                    {
                        sDif = getTick(slaves[slaveIndex], ref sOld, out sTic);
                    }
                    else //all good exit
                    {
                        break;
                    }
                }

                ulong d = mDif - sDif;
                if (d * d > MAXDIF * MAXDIF)
                {
                    Console.WriteLine("Error > Max {0}>{1}",d,MAXDIF);
                    break;
                }

                pairs.Add(new FramePair(masters[masterIndex++], slaves[slaveIndex++]));
                sOld = sTic;
                mOld = mTic;
            }

            int ct = Math.Min(5, (pairs.Count+1) >> 1);
            for (int i = 0; i < ct; i++)
                Console.Write("{0}\t{1}\t{2}\n",
                    pairs[i].FrameNumber,
                    pairs[i].Master.FrameNumber - pairs[i].Slave.FrameNumber,
                    pairs[i].SystemTime);

            Console.Write("...\n");

            for (int i = pairs.Count - ct; i < pairs.Count; i++)
                Console.Write("{0}\t{1}\t{2}\n",
                    pairs[i].FrameNumber,
                    pairs[i].Master.FrameNumber - pairs[i].Slave.FrameNumber,
                    pairs[i].SystemTime);


        }


        static void PairUpRunX(List<FrameRef> masters, ref int masterIndex, List<FrameRef> slaves, ref int slaveIndex, List<FramePair> pairs)
        {
            FrameRef master, slave;
            master = masters[masterIndex++];
            slave = slaves[slaveIndex++];

            ulong mDif, mTic, mOld = master.CameraTick;
            ulong sDif, sTic, sOld = slave.CameraTick;

            int masterCount = masters.Count;
            int slaveCount = slaves.Count;

            pairs.Add(new FramePair(master, slave));

            while(masterIndex < masterCount && slaveIndex < slaveCount)
            {
                mDif = getTick(master = masters[masterIndex],ref mOld, out mTic);
                sDif = getTick(slave = slaves[slaveIndex], ref sOld, out sTic);

                if ((mDif - sDif) > 100000 && ++slaveIndex < slaveCount)
                {
                    sDif = getTick(slave = slaves[slaveIndex], ref sOld, out sTic);
                }
                
                if ((sDif - mDif) > 100000 && ++masterIndex < masterCount)
                {
                    mDif = getTick(master = masters[masterIndex], ref mOld, out mTic);
                }

                if (sDif - mDif < 100000)
                {
                    long df = master.FrameNumber - slave.FrameNumber;
                    Console.WriteLine("{0}:{1}\t{2}:{3} {4} {5}",
                        sDif,
                        mDif,
                        master.FrameNumber,
                        slave.FrameNumber,
                        Math.Abs(df) < 2 ? "Good" : "Bad",
                        sDif - mDif);

                    pairs.Add(new FramePair(master, slave));

                    sOld = sTic;
                    mOld = mTic;

                    masterIndex++;
                    slaveIndex++;
                }
                else
                    break;
            }


        }




        static List<FramePair> FooBarryX(List<FrameRef> masters, List<FrameRef> slaves)
        {
            if (slaves == null || masters == null)
                throw new ArgumentNullException();

            var pairs = new List<FramePair>(slaves.Count);
            if (slaves.Count == 0 || masters.Count == 0)
                return pairs;

            slaves.Sort();
            syncFrames(masters, slaves, 5);

            FrameRef master, slave;
            master = masters[0];
            slave = slaves[0];

            ulong md, m0, m1 = master.CameraTick;
            ulong sd, s0, s1 = slave.CameraTick;
            m0 = m1;
            s0 = s1;

            pairs.Add(new FramePair(master, slave));



            //md = 0;//???????


            for (int s = 1, m = 1; s < slaves.Count && m < masters.Count;)
            {
                master = masters[m];
                m1 = master.CameraTick;
                if (m0 > m1) { m0 -= 0x100000000; }
                md = m1 - m0;
                
                do
                {
                    slave = slaves[s];
                    s1 = slave.CameraTick;
                    if (s0 > s1) { s0 -= 0x100000000; }
                    sd = s1 - s0;
                    Console.Write('*');
                } while ((md - sd) > 100000 && ++s < slaves.Count);//while (Math.Round((double)md / sd) > 1 && ++s < slaves.Count);


                while ((sd - md) > 100000 && ++m < masters.Count) //while (Math.Round((double)sd / md) > 1 && ++m < masters.Count) 
                {
                    master = masters[m];
                    m1 = master.CameraTick;
                    if (m0 > m1) { m0 -= 0x100000000; }
                    md = m1 - m0;
                    Console.Write('+');
                    //md = md & 0xffffffff; //<--keep (why is it not above??)
                }


                //Console.WriteLine("{0:F3}",Math.Abs(1.0 - (double)sd / md));

                if (sd - md < 100000)//if (m < masters.Count && Math.Round((double)sd / md) == 1.0)//Math.Abs(1.0 - (double)sd / md) < 0.1 
                {
                    long df = master.FrameNumber - slave.FrameNumber;
                    Console.WriteLine("{0}:{1}\t{2}:{3} {4} {5} {6}",
                        sd, md,
                        master.FrameNumber,
                        slave.FrameNumber,
                        df,
                        Math.Abs(df) < 2 ? "Good" : "Bad",
                        (double)sd / md
                         );

                    pairs.Add(new FramePair(master, slave));
                    s0 = s1;
                    m0 = m1;
                    m++;
                    s++;  
                }
            }


            pairs.TrimExcess();//get rid of any extra capacity
            Console.WriteLine("done");
            return pairs;
        }










        static void GetCameraTicks(List<FrameRef> frames)
        {
            frames.Sort();
            MapFile mapFile = new MapFile();
            foreach (var frame in frames)
            {
                mapFile.Open(frame);
                frame.CameraTick = mapFile.Header.CameraTick;
                //if (frame.CameraTick == 0)
                //{
                //    Console.WriteLine(mapFile.FrameReference.Path);
                //    Console.WriteLine("CameraID\t{0}",mapFile.Header.CameraID);
                //    Console.WriteLine("CameraTick\t{0}",mapFile.Header.CameraTick);
                //    Console.WriteLine("FrameCt\t{0}", mapFile.Header.FrameCount);
                //    Console.WriteLine("Status\t{0}", mapFile.Header.FrameStatus);
                //    Console.WriteLine("Exposure\t{0}", mapFile.Header.Exposure);
                //    Console.WriteLine();
                //}
                mapFile.Close();
            }
            mapFile.Dispose();
        }

        static void syncFrames(List<FrameRef> masters, List<FrameRef> slaves,int sampleSize)
        {
            int tot = Math.Min(Math.Min(masters.Count, slaves.Count) - 1, sampleSize);
            
            var mdiffs = new ulong[sampleSize];
            var sdiffs = new ulong[sampleSize];

            for (int i = 0; i < sampleSize; i++)
            {
                mdiffs[i] = masters[i + 1].CameraTick - masters[i].CameraTick;
                sdiffs[i] = slaves[i + 1].CameraTick - slaves[i].CameraTick;
            }

            for (int i = 0; i < sampleSize; i++)
                Console.WriteLine("{0}\t{1}", mdiffs[i], sdiffs[i]);
            Console.WriteLine();


            for (int i = 1; i < sampleSize; i++)
                Console.Write("{0}\t{1}\n", i, mdiffs[i] - sdiffs[i - 1]);
            Console.WriteLine();

            for (int i = 0; i < sampleSize; i++)
                Console.Write("{0}\t{1}\n", i, mdiffs[i] - sdiffs[i]);
            Console.WriteLine();

            for (int i = 0; i < sampleSize-1; i++)
                Console.Write("{0}\t{1}\n",i,mdiffs[i] - sdiffs[i+1]);
            Console.WriteLine();

            Console.WriteLine(sumSquares(mdiffs, sdiffs, -1));
            Console.WriteLine(sumSquares(mdiffs, sdiffs, 0));
            Console.WriteLine(sumSquares(mdiffs, sdiffs, 1));
        }

        static ulong sumSquares(ulong[] a, ulong[] b, int offset)
        {
            int count = Math.Min(a.Length,b.Length);
            int start;

            if(offset>0)
            {
                count -= offset;
                start = 0;
            }
            else
            {
                start = -offset;
            }

            ulong sum=0;

            for (int i = start ; i < count; i++)
            {
                ulong d = a[i] - b[i + offset];
                sum += d * d;
            }
            return sum;
        }


        static string[] GetPathsFromDialogue()
        {
            string[] paths = null;
            using (var dialogue = new OpenFileDialog() { Multiselect = true, Title = "Select files for conversion" })
            {
                if(dialogue.ShowDialog()==DialogResult.OK)
                    paths = dialogue.FileNames;
            }
            return paths; //can be null
        }


        static string[] GetFilesFromFolderDialogue()
        {
            string[] paths = null;

            if (folderDialog.ShowDialog() == DialogResult.OK)
            {
                paths = Directory.GetFiles(folderDialog.SelectedPath,"*.*",SearchOption.AllDirectories);
            }  
            return paths; //can be null
        }

        static void swap<T>(ref T a, ref T b)
        {
            T t = a;
            a = b;
            b = t;
        }
    }
}
