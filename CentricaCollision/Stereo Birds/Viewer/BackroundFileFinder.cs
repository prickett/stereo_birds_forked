﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using CustomControls;
using Createc.Imaging.Thresholds;
using Createc.StereoBirds;

namespace Createc.StereoBirds.Viewer
{
    class BackroundFileFinder:BackgroundWorker
    {
        public event EventHandler<GotFramesEventArgs> GotFrames;

        private MapFilePairCollection mapFrames;
        private MapFilePairCollection fullFrames;
        private MapFilePairCollection frames;
        //private FrameList mapFrames;
        //private FrameList fullFrames;
       // private FrameList frames;

        private MapFile masterMap = new MapFile();
        private MapFile slaveMap = new MapFile();

        public BackroundFileFinder()
        {
            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
            DoWork += new DoWorkEventHandler(fileFinder_DoWork);
            //ProgressChanged += new ProgressChangedEventHandler(fileFinder_ProgressChanged);
            RunWorkerCompleted += new RunWorkerCompletedEventHandler(fileFinder_RunWorkerCompleted);
        }

        public MapFilePairCollection Frames
        {
            get { return frames; }
        }
        public MapFilePairCollection MapFrames
        {
            get { return mapFrames; }
        }
        public MapFilePairCollection FullFrames
        {
            get { return fullFrames; }
        }

        public bool StartSearch(string directory,SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            if (!IsBusy && Directory.Exists(directory)) //&& directory!=null? exists okay with null?
            {
                RunWorkerAsync(Directory.EnumerateFiles(directory, "*.*", searchOption));
                return true;
            }
            return false;
        }

        public bool StartSearch(string[] files)
        {
            if (!IsBusy && files != null && files.Length!=0)
            {
                RunWorkerAsync(files);
                return true;
            }
            return false;
        }


        private void fileFinder_DoWork(object sender, DoWorkEventArgs e)
        {
            const int MASK = (1 << 10) - 1;
            BackgroundWorker worker = sender as BackgroundWorker;
            var fileEnumerator = (IEnumerable<string>)e.Argument;
            //string directory = (string)e.Argument;
            int counter = 0;
            MapFile mapFile = new MapFile();

            var masterFrames = new List<FrameRef>();
            var slaveFrames = new List<FrameRef>();
            var masterFullFrames = new List<FrameRef>();
            var slaveFullFrames = new List<FrameRef>();

            foreach (var f in fileEnumerator)
            {
                if ((counter++ & MASK) == 0)
                {
                    if ((worker.CancellationPending == true))
                    {
                        e.Cancel = true;
                        break;
                    }
                    else
                    {
                        worker.ReportProgress(counter);//can't return a percentage on a job of unknown length.
                    }
                }

                var frame = new FrameRef(f);
                switch (frame.Type)
                {

                    case FrameType.Map:
                        if (frame.IsMaster)
                            masterFrames.Add(frame);
                        else if (frame.IsSlave)
                            slaveFrames.Add(frame);
                        break;
                    case FrameType.Fullframe:
                        if (frame.IsMaster)
                            masterFullFrames.Add(frame);
                        else if (frame.IsSlave)
                            slaveFullFrames.Add(frame);
                        break;
                    default:
                        Console.WriteLine("Bad file {0}", frame.Path);
                        break;
                }

            }
            worker.ReportProgress(counter);

            masterFrames.Sort();
            slaveFrames.Sort();
            masterFullFrames.Sort();
            slaveFullFrames.Sort();

            mapFrames = new MapFilePairCollection(masterFrames, slaveFrames);
            fullFrames = new MapFilePairCollection(masterFullFrames, slaveFullFrames);
            frames = mapFrames;
            e.Result = frames.PotentialCount;
        }

        //private void addFrame(FrameRef frame, List<FrameRef> masters, List<FrameRef> slaves)
        //{
        //    switch (frame.Type)
        //    {
        //        case FrameType.Map:
        //            if (frame.IsMaster)
        //                masterFrames.Add(frame);
        //            else if (frame.IsSlave)
        //                slaveFrames.Add(frame);
        //            break;
        //        case FrameType.Fullframe:
        //            if (frame.IsMaster)
        //                masterFullFrames.Add(frame);
        //            else if (frame.IsSlave)
        //                slaveFullFrames.Add(frame);
        //            break;
        //        default:
        //            Console.WriteLine("Bad file {0}", frame.Path);
        //            break;
        //    }
        //}

        private static bool isSorted(List<FrameRef> files)
        {
            string lastpath = "";
            foreach (var f in files)
            {
                string path = f.Path;
                if (path.CompareTo(lastpath) < 0)
                    return false;
                lastpath = path;
            }
            return true;
        }


        private void fileFinder_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Cancelled == true))
            {
                Console.WriteLine("Canceled!");
            }

            else if (!(e.Error == null))
            {
                Console.WriteLine("Error: " + e.Error.Message);
            }

            else
            {
                OnGotFramesEvent(new GotFramesEventArgs(mapFrames,fullFrames));
                Console.WriteLine("Done!");
            }
        }

        // Wrap event invocations inside a protected virtual method
        // to allow derived classes to override the event invocation behavior
        protected virtual void OnGotFramesEvent(GotFramesEventArgs e)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            EventHandler<GotFramesEventArgs> handler = GotFrames;

            // Event will be null if there are no subscribers
            if (handler != null)
            {
                handler(this, e);
            }
        }

        //private void fileFinder_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //{
        //   // Console.WriteLine("{0} files found" , e.ProgressPercentage);
        //}


        //private string getFolderFromUser()
        //{
        //    string folder = null; //the return if the dialog fails or is canceled
        //    using (var dialog = new FolderBrowserDialog())
        //    {
        //        dialog.ShowNewFolderButton = false;
        //        if (dialog.ShowDialog() == DialogResult.OK)
        //        {
        //            //Console.WriteLine("calling GetFiles() {0}", DateTime.Now);
        //            folder = dialog.SelectedPath;//Slow call!
        //            //Console.WriteLine("GetFiles() return {0} files {1}",filelist.Length, DateTime.Now);
        //        }
        //    }
        //    return folder;
        //}

        //private string[] filesFromFolderDialog()
        //{
        //    string[] filelist = new string[0]; //the return if the dialog fails or is canceled
        //    using (var dialog = new FolderBrowserDialog())
        //    {
        //        dialog.ShowNewFolderButton = false;
        //        if (dialog.ShowDialog() == DialogResult.OK)
        //        {
        //            //Console.WriteLine("calling GetFiles() {0}", DateTime.Now);
        //            filelist = Directory.GetFiles(dialog.SelectedPath);//Slow call!
        //            //Console.WriteLine("GetFiles() return {0} files {1}",filelist.Length, DateTime.Now);
        //        }
        //    }
        //    return filelist;
        //}

        //private string[] filesFromFileDialog()
        //{
        //    string[] filelist = new string[0]; //the return if the dialog fails or is canceled
        //    using (var dialog = new OpenFileDialog())
        //    {
        //        dialog.Multiselect = true;
        //        if (dialog.ShowDialog() == DialogResult.OK)
        //        {
        //            filelist = dialog.FileNames;
        //        }
        //    }
        //    return filelist;
        //}




        //private int? FrameDifference()
        //{
        //    var mf = masterFrames.Current;
        //    var sf = slaveFrames.Current;
        //    int? dif;

        //    if (mf != null) // slaveFrame might be null
        //        dif = mf.CompareTo(sf);
        //    else if (sf != null) // masterFrame is null
        //        dif = -1;
        //    else// both slaveFrame and masterFrame are null
        //        dif = null; 

        //    return dif;
        //}

        //private void stripFrames(List<MapFileRef> frames, bool[] toKeep)
        //{
        //    int k = 0;
        //    for (int i = 0; i < toKeep.Length; i++)
        //    {
        //        if (toKeep[i])
        //        {
        //            frames[k] = frames[i];
        //            k++;
        //        }
        //    }
        //    int ct = frames.Count - k;
        //    Console.WriteLine("{0} out of {1} frames disgarded", ct, frames.Count);
        //    frames.RemoveRange(k, ct);
        //}


        //private void pairFrames()
        //{
        //    pairFrames(masterFullFrames, slaveFullFrames, ref pairedFullFrames);
        //    pairFrames(masterFrames, slaveFrames, ref pairedFrames);
        //}

        //private void pairFrames(ListThing<FrameRef> masters, ListThing<FrameRef> slaves, ref ListThing<FramePair> pairs)
        //{
        //    masters.Sort();//these should always be sorted....
        //    slaves.Sort();//...but there is no harm in making sure

        //    if (pairs == null)
        //        pairs = new ListThing<FramePair>();

        //    int m = 0;
        //    int s = 0;
        //    int msz = masters.Count;
        //    int ssz = slaves.Count;

        //    while (s < ssz & m < msz)
        //    {
        //        while (m < msz && masters[m].CompareTo(slaves[s]) < 0)
        //        {
        //            frameIndexer.Add(pairs.Count);
        //            pairs.Add(new FramePair(masters[m++], null)); 
        //        }
        //        while (s < ssz && slaves[s].CompareTo(masters[m]) < 0)
        //        {
        //            frameIndexer.Add(pairs.Count);
        //            pairs.Add(new FramePair(null, slaves[s++]));
        //        }
        //        while (s < ssz & m < msz && slaves[s].CompareTo(masters[m]) == 0)
        //        {
        //            pairIndexer.Add(pairs.Count);
        //            frameIndexer.Add(pairs.Count);
        //            pairs.Add(new FramePair(masters[m++], slaves[s++]));
        //        }
        //    }
        //}


        //public FramePair Current
        //{
        //    get { return frames.Current; }
        //}

        //FramePair IEnumerator<FramePair>.Current
        //{
        //    get { return frames.Current; }
        //}

        //object System.Collections.IEnumerator.Current
        //{
        //    get { return frames.Current; }
        //}

        //public bool MoveNext()
        //{
        //    return frames.MoveNext();
        //}

        //public void Reset()
        //{
        //    frames.Reset();
        //}
    }


    public class GotFramesEventArgs : EventArgs
    {
        public GotFramesEventArgs(MapFilePairCollection mapFrames, MapFilePairCollection fullFrames)
        {
            this.mapFrames = mapFrames;
            this.fullFrames = fullFrames;
        }

        private MapFilePairCollection mapFrames;
        private MapFilePairCollection fullFrames;

        public MapFilePairCollection MapFrames
        {
            get { return mapFrames; }
        }
        public MapFilePairCollection FullFrames
        {
            get { return fullFrames; }
        }
    }

    //class FrameListPair
    //{
    //    public FrameListPair(FrameList mapFrames,Fr
    //}
}
