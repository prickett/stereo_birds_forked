﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Createc.StereoBirds.Viewer
{
    class UserClicks
    {
        private List<BirdPossibleOld> hits;


        public void Test(FramePair framePair,int mx,int my,int sx,int sy)
        {
            var bsr = new BirdPossibleOld(framePair);
            bsr.Hits.Add(new HitOld(mx,my,sx,sy, -1f, -1));
        }

        
        public void SaveResults(String path)
        {
            using (var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
            {
                SaveResults(fs);
            }
        }

        public void SaveResults(Stream stream)
        {
            using (var bw = new BinaryWriter(stream))
            {
                SaveResults(bw);
            }
        }

        public void SaveResults(BinaryWriter writer)
        {
            writer.Write(hits.Count);
            foreach (var bsr in hits)
            {
                writer.Write(bsr.FramePair.SystemTime.sec);
                writer.Write(bsr.FramePair.SystemTime.nsec);
                writer.Write(bsr.FramePair.FrameNumber);
                writer.Write(bsr.FramePair.Master.Path);
                writer.Write(bsr.FramePair.Slave.Path);
                writer.Write(bsr.Hits.Count);
                foreach (var hit in bsr.Hits)
                {
                    writer.Write(hit.Master.X);
                    writer.Write(hit.Master.Y);
                    writer.Write(hit.Slave.X);
                    writer.Write(hit.Slave.Y);
                    writer.Write(hit.Score);
                    writer.Write(hit.PixelCount);
                }
            }
        }

        private void saveCurrentResults()
        {
            int ct = hits.Count;
            if (ct > 0)
            {
                string path = string.Format("c:\\Capture\\{0}_{1}.birdPoss", hits[0].FramePair.SystemTime, hits[ct - 1].FramePair.SystemTime);
                SaveResults(path);
            }
            hits.Clear();
        }
    }
}
