﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Createc.StereoBirds.Viewer
{
    public class FramePairList
    {
        Mutex mutex = new Mutex();
        List<FramePair> pairs = new List<FramePair>();
        int index;

        public int Count { get { return pairs.Count; } }

        public FramePair Current
        {
            get 
            {
                mutex.WaitOne();
                var fp = InRange(index) ? pairs[index] : new FramePair();
                mutex.ReleaseMutex();
                return fp;
            }
        }

        public int CurrentIndex
        {
            get { return index; }
        }


        public FramePair FindNearest(TimeStamp systemTime)
        {
            mutex.WaitOne();
            int i = Find(systemTime);
            if (i < 0) i = ~i;
            if (i >= pairs.Count) i = pairs.Count - 1;
            var fp = InRange(i) ? pairs[i] : new FramePair();
            mutex.ReleaseMutex();
            return fp;
        }

        ///public FramePair FindNearest(long systemTime)


        public bool MoveToIndex(int i)
        {
            if(InRange(i))
            {
                index = i;
                return true;
            }
            return false;
        }

        public bool MoveTo(TimeStamp systemTime)
        {
            mutex.WaitOne();
            int i = Find(systemTime);
            if (i >= 0)
                index = i;
            mutex.ReleaseMutex();
            return i >= 0;
        }

        int Find(TimeStamp systemTime)
        {
            return pairs.BinarySearch(new FramePair(new FrameRef(systemTime), null));
        }

      public bool MoveNext()
        {
            if (InRange(++index))
                return true;
            index--;
            return false;
        }

        public bool MovePrevious()
        {
            if (InRange(--index))
                return true;
            index++;
            return false;

        }

        public void Reset()
        {
            index = -1;
        }

        public void FilterAndAdd(ref List<FramePair> sortedPairs)
        {
            if (sortedPairs == null || sortedPairs.Count == 0)
                return;

            mutex.WaitOne();

            if (pairs.Count == 0 || sortedPairs[0].Master.CompareTo(pairs.Last().Master) > 0) //new pairs go after
            {
                pairs.AddRange(sortedPairs);
            }
            else if (sortedPairs.Last().Master.CompareTo(pairs[0].Master) < 0) //new pairs go before
            {
                pairs.InsertRange(0, sortedPairs);
                index += sortedPairs.Count;
            }
            else //new pairs merged
            {
                FramePair fp = InRange(index) ? pairs[index] : new FramePair(null, null);
                sortedPairs = new List<FramePair>(sortedPairs.Where(f => pairs.BinarySearch(f) < 0));
                pairs.AddRange(sortedPairs);
                pairs.Sort();
                index = pairs.BinarySearch(fp);
            }

            mutex.ReleaseMutex();
        
        }

        bool InRange(int index)
        {
            return index >= 0 && index < pairs.Count;
        }
    }
}
