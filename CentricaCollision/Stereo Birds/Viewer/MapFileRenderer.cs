﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

//using System.Windows.Media;
//using System.Windows.Media.Imaging;

using Createc.Imaging.Thresholds;
using Createc.StereoBirds;

using Createc.Net.Imaging;

namespace Createc.StereoBirds.Viewer
{
    class MapFileRenderer
    {
        public enum RenderModes
        {
            None = 0,
            Map,
            Mask,
            Bayer,
            Image
        }

        static readonly byte[] maskLut;
        static MapFileRenderer()
        {
            maskLut = buildBrightnessLUT(3f);
        }

        public MapFileRenderer(RenderModes renderMode = RenderModes.Bayer /*,int bayerOrder = 0*/)
        {
            RenderMode = renderMode;
        }

        public RenderModes RenderMode { get; set; }

        private static bool checkArgs(Graphics g, MapFile mapfile)
        {
            return g != null && mapfile!=null &&  mapfile.isOpen;
        }

        public Pair<Createc.Net.Imaging.Image> CreateImagePair(FramePair framePair)
        {
            var pair = new Pair<Createc.Net.Imaging.Image>();
            switch (framePair.Type)
            {
                case FrameType.Map:
                    throw new NotImplementedException();
                case FrameType.Fullframe:
                    pair.A = FullFrameImage(framePair.Master);
                    pair.B = FullFrameImage(framePair.Slave);
                    return pair;
                default:
                    throw new ArgumentException();
            }

        }

        public Createc.Net.Imaging.Image CreateImage(FrameRef frameRef)
        {
            switch (frameRef.Type)
            {
                case FrameType.Map:
                    throw new NotImplementedException();
                case FrameType.Fullframe:
                    return FullFrameImage(frameRef);
                default:
                    throw new ArgumentException();
            }
        }

        public Createc.Net.Imaging.Image FullFrameImage(FrameRef frameRef)
        {
            Createc.Net.Imaging.Image image = null;
            try
            {
                Console.WriteLine("FullFrameImage {0} {1} {2}", frameRef.IsMaster, frameRef.Offset, frameRef.FrameNumber);
                
                using (var fs = new FileStream(frameRef.Path, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    // System.Drawing does not support loading images from streams when the image is not at the start of the stream
                    // this is really dumb so we are forced to copy the data to a new stream in order to read it when we don't know
                    // how big the image is until we read it. 
                    fs.Seek(frameRef.Offset, SeekOrigin.Begin);
                    int size = frameRef.Size;  
                    int offset = 0;
                    byte[] bytes = new byte[size];
                    while(offset < size)
                    {
                        offset += fs.Read(bytes, offset, size - offset); // Read will read up to size bytes so it might need calling several times
                    }
                    MemoryStream ms = new MemoryStream(bytes);
                    
                    using (var bmp = new Bitmap(ms))
                    {
                        if (bmp.PixelFormat == PixelFormat.Format8bppIndexed)
                        {
                            if (bmp.Width > bmp.Height)
                            {
                                bmp.RotateFlip(RotateFlipType.Rotate270FlipNone);
                                Console.WriteLine("{0}x{1} (Rotating image)", bmp.Width, bmp.Height);
                            }

                            BitmapData d = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat);
                            int w = d.Width >> 1, h = d.Height >> 1;
                            image = new Createc.Net.Imaging.Image(w, h, w * 4, PixelTypes.BGRA32);
                            Demosaic(d.Scan0, d.Width, d.Height, d.Stride, image.Buffer, image.Stride, 0);

                            //image = new Createc.Net.Imaging.Image(d.Width, d.Height, d.Width * 4, PixelTypes.BGRA32);
                            //unsafe
                            //{
                            //    fixed (byte* tgt = image.Buffer)
                            //    { 
                            //        Unmanaged.Demosaic(d.Width, d.Height, (byte*)d.Scan0, tgt, 
                            //            d.Stride, image.Stride, Unmanaged.BayerPattern.RGGB, 32);
                            //    }
                            //}

                            bmp.UnlockBits(d);
                        }
                    }
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Error rendering fullframe: {0}", e.Message);
            }

            return image;
        }

        public Bitmap CreateBitmap(FrameRef frameRef)
        {
            Bitmap bmp = null;
            if(frameRef.Type == FrameType.Map)
            {
                using (var m = new MapFile(frameRef))
                {
                    bmp = CreateBitmap(m);
                }
            }
            else if(frameRef.Type == FrameType.Fullframe)
            {
                Bitmap src;
                try
                {
                    src = new Bitmap(frameRef.Path);
                    if (src.Width > src.Height)
                        src.RotateFlip(RotateFlipType.Rotate270FlipNone);
                }
                catch
                {
                    src = null;
                } 

                if(src != null && src.PixelFormat == PixelFormat.Format8bppIndexed)
                {
                    bmp = new Bitmap(src.Width, src.Height, PixelFormat.Format24bppRgb);
                    var sd = src.LockBits(new Rectangle(0, 0, src.Width, src.Height), ImageLockMode.ReadOnly, src.PixelFormat);
                    var bd = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, bmp.PixelFormat);

                    unsafe
                    {
                        Console.Write(" demosaic start");
                        Unmanaged.Demosaic(sd.Width, sd.Height, (byte*)sd.Scan0, (byte*)bd.Scan0, sd.Stride, bd.Stride, 
                            Unmanaged.BayerPattern.GRBG, System.Drawing.Image.GetPixelFormatSize(bmp.PixelFormat));
                        //Demosaic((byte*)sd.Scan0, sd.Width, sd.Height, sd.Stride, (byte*)bd.Scan0, bd.Stride, 0);
                        Console.Write(" ...demosaic end");
                    }

                    bmp.UnlockBits(bd);
                    src.UnlockBits(sd);
                    src.Dispose();

                    using (var g = Graphics.FromImage(bmp))
                    {
                        Console.Write(" ...resize start");
                        g.InterpolationMode = InterpolationMode.Bilinear;
                        g.DrawImage(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height), new Rectangle(0, 0, bmp.Width >> 1, bmp.Height >> 1), GraphicsUnit.Pixel);
                        Console.Write(" ...resize end\n");
                    }
                }
                else
                {
                    bmp = src;
                }
             
            }
            return bmp;
        }


        public Bitmap CreateBitmap(MapFile mapFile)
        {
            if (!mapFile.isOpen)
                return null;

            var header = mapFile.Header;
            //reject any sizes that are less than zero or greater than 65535
            if ((header.Width & -65536) != 0 || (header.Height & -65536) != 0) 
                return null;

            var bmp = new Bitmap((int)header.Height, (int)header.Width, PixelFormat.Format32bppRgb);//transposed
            //var bmp = new Bitmap((int)header.MapUnitSize * header.MapHeight, (int)header.MapUnitSize * header.MapWidth, PixelFormat.Format32bppRgb);//transposed
            bool ret;

            using (var g = Graphics.FromImage(bmp))
            {
                ret = Render(g, mapFile);
            }
            return bmp;
        }

        public bool Render(Graphics g, MapFile mapFile)
        {
            if(!checkArgs(g,mapFile))
                return false;

            
            switch (RenderMode)
            {
                case RenderModes.None:                   
                case RenderModes.Image:
                    return RenderImage(g, mapFile);
                case RenderModes.Bayer:
                    return RenderBayer(g, mapFile);
                case RenderModes.Mask:
                    return RenderMask(g, mapFile);
                case RenderModes.Map:
                    return RenderMap(g, mapFile);
                default:
                    Console.WriteLine("Unexpected render mode {0}", RenderMode);
                    return false;
            }
        }



        //public static bool RenderImage(Graphics g, MapFile mapFile)
        //{
        //    if (!checkArgs(g, mapFile))
        //        return false;

        //    int[] image = mapFile.GetImage();
        //    int[,] offsets = mapFile.GetBayerOffsetsYX();

        //    if (image == null)
        //        return false;

        //    var header = mapFile.Header;

        //    int uSz = (int)header.MapUnitSize;
        //    int iSz = uSz >> 1;
        //    int bayerTileSz = uSz * uSz;
        //    int imageTileSz = bayerTileSz >> 2;

        //    Bitmap bmpTile = new Bitmap(iSz, iSz, PixelFormat.Format32bppRgb);
            
        //    var rect = new Rectangle(0, 0, iSz, iSz);
        //    int lastY = header.MapHeight - 1;


        //    for (int y = 0; y < header.MapHeight; y++)
        //    {
        //        if (y == lastY)
        //            bayerTileSz = uSz * ((int)header.Height - lastY * uSz); //!!!!!!!!!!!!!!!!!!!!!!!!!!1
        //        for (int x = 0; x < header.MapWidth; x++)
        //        {
        //            int i = offsets[y, x];
        //            if (i >= 0)
        //            {  
        //                var bd = bmpTile.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format32bppRgb);
        //                System.Runtime.InteropServices.Marshal.Copy(image, i, bd.Scan0, imageTileSz);
        //                bmpTile.UnlockBits(bd);
        //                bmpTile.RotateFlip(RotateFlipType.Rotate270FlipNone);//transposed
        //                g.DrawImage(bmpTile, y * iSz, header.Width - (x * iSz), iSz, iSz);
        //            }
        //        }
        //    }

        //    bmpTile.Dispose();
        //    return true;

        //}
        public static bool RenderImage(Graphics g, MapFile mapFile)
        {
            if (!checkArgs(g, mapFile))
                return false;

            var header = mapFile.Header;

            int uSz = (int)header.MapUnitSize;
            int bayerTileSz = uSz * uSz;
            Bitmap bmpTile = new Bitmap(uSz, uSz, PixelFormat.Format32bppRgb);// createGreyscale(uSz, uSz);
            var rect = new Rectangle(0, 0, uSz, uSz);
            int lastY = header.MapHeight - 1;
            int tileHeight = uSz;

            byte[] bayer = mapFile.GetBayer();
            byte[] bayerTile = new byte[bayerTileSz*3];
            int[,] offsets = mapFile.GetBayerOffsetsYX();

            Unmanaged.BayerPattern bayerPattern = (Unmanaged.BayerPattern)mapFile.Header.BayerPattern;

            for (int y = 0; y < header.MapHeight; y++)
            {
                if (y == lastY)
                {
                    tileHeight = ((int)header.Height - lastY * uSz);
                    bayerTileSz = uSz * tileHeight;
                    //bayerTile = new byte[bayerTileSz];
                }
                for (int x = 0; x < header.MapWidth; x++)
                {
                    int i = offsets[y, x];
                    if (i >= 0)
                    {
                        var bd = bmpTile.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format32bppRgb);
                        Array.Copy(bayer, i, bayerTile, 0, bayerTileSz);

                        
                        bool retval = false;
                        unsafe
                        {                           
                            fixed (byte* src = bayerTile)
                            {
                                retval = Unmanaged.Demosaic(uSz, tileHeight, src, (byte*)bd.Scan0, uSz, bd.Stride,
                                    bayerPattern, System.Drawing.Image.GetPixelFormatSize(bd.PixelFormat));
                            }
                        }
                      
                        bmpTile.UnlockBits(bd);
                        bmpTile.RotateFlip(RotateFlipType.Rotate270FlipNone);//transposed
                        g.DrawImage(bmpTile, y * uSz, header.Width - (x+1) * uSz, uSz, uSz);
                    }
                }
            }

            bmpTile.Dispose();
            return true;

        }
        private static byte ByteSaturate(float value)
        {
            if (value < 0)
                return 0;
            else if (value <= 255f)
                return (byte)value;
            else
                return 255;
        }


        public static bool  RenderBayer(Graphics g, MapFile mapFile)
        {
            if (!checkArgs(g, mapFile))
                return false;

            var header = mapFile.Header;

            int uSz = (int)header.MapUnitSize;
            int bayerTileSz = uSz * uSz;
            Bitmap bmpTile =createGreyscale(uSz, uSz);
            var rect = new Rectangle(0, 0, uSz, uSz);
            int lastY = header.MapHeight - 1;

            byte[] bayer = mapFile.GetBayer();
            int[,] offsets = mapFile.GetBayerOffsetsYX();

            for (int y = 0; y < header.MapHeight; y++)
            {
                if(y==lastY)
                    bayerTileSz = uSz * ((int)header.Height - lastY * uSz);
                for (int x = 0; x < header.MapWidth; x++)
                {
                    int i = offsets[y,x];
                    if (i >= 0)
                    {
                        var bd = bmpTile.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
                        System.Runtime.InteropServices.Marshal.Copy(bayer,i, bd.Scan0, bayerTileSz);
                        bmpTile.UnlockBits(bd);
                        bmpTile.RotateFlip(RotateFlipType.Rotate270FlipNone);//transposed
                        g.DrawImage(bmpTile, y * uSz, header.Width - (x+1) * uSz , uSz, uSz);
                    }
                    
                }
            }

            bmpTile.Dispose();
            return true;

        }

        public static bool RenderMask(Graphics g, MapFile mapFile)
        {
            if (!checkArgs(g, mapFile))
                return false;

            var header = mapFile.Header;

            int uSz = (int)header.MapUnitSize;
            int mSz = uSz>>1;

            int maskTileSz = mSz * mSz;
            Bitmap bmpTile = createGreyscale(mSz, mSz);
            var rect = new Rectangle(0, 0, mSz, mSz);
            int lastY = header.MapHeight - 1;

            var interpolation = g.InterpolationMode;
            g.InterpolationMode = InterpolationMode.NearestNeighbor;

            byte[] mask = mapFile.GetMask();
            int[,] offsets = mapFile.GetBayerOffsetsYX();
            byte[] tempBuf = new byte[maskTileSz];

            for (int y = 0; y < header.MapHeight; y++)
            {
                if (y == lastY)
                    maskTileSz = mSz * (((int)header.Height>>1) - lastY * mSz);
                for (int x = 0; x < header.MapWidth; x++)
                {
                    int i = offsets[y, x]>>2;
                    if (i >= 0)
                    {
                        var bd = bmpTile.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
                        Array.Copy(mask, i, tempBuf, 0, maskTileSz);
                        
                        for (int j = 0; j < maskTileSz; j++)
                            tempBuf[j] = maskLut[tempBuf[j]];

                        System.Runtime.InteropServices.Marshal.Copy(tempBuf, 0, bd.Scan0, maskTileSz); 
                        bmpTile.UnlockBits(bd);
                        bmpTile.RotateFlip(RotateFlipType.Rotate270FlipNone);//transposed
                        g.DrawImage(bmpTile, y * uSz, header.Width - (x * uSz), uSz + 1, uSz + 1); //transposed
                    }
                }
            }

            bmpTile.Dispose();
            g.InterpolationMode = interpolation;
            return true;


            //if (!checkArgs(g, mapFile))
            //    return false;

            //var header = mapFile.Header;

            //int uSz = (int)header.MapUnitSize;
            //int mSz = uSz >> 1;
            //int maskIndex = 0;
            //int maskTileSz = mSz * mSz;
            //Bitmap bmpTile = createGreyscale(mSz, mSz);
            //var rect = new Rectangle(0, 0, mSz, mSz);
            //int halfHeight = (int)header.Height >> 1;

            //var interpolation = g.InterpolationMode;
            //g.InterpolationMode = InterpolationMode.NearestNeighbor;

            //int size = mSz * mSz;
            //int lastY = header.MapHeight - 1;
            //int lastSize = mSz * (halfHeight - lastY * mSz);

            //byte[] tempBuf = new byte[maskTileSz];
            ////Console.WriteLine("starting...");


            //byte[] mask = mapFile.GetMask();

            //foreach (var loc in new MapEnumerator(mapFile.GetMap(), header.MapWidth))
            //{

            //    //Console.Write("#");
            //    var bd = bmpTile.LockBits(rect, ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);

            //    maskTileSz = loc.Y < lastY ? size : lastSize;

            //    Array.Copy(mask, maskIndex, tempBuf, 0, maskTileSz);

            //    for (int i = 0; i < maskTileSz; i++)
            //        tempBuf[i] = maskLut[tempBuf[i]];

            //    System.Runtime.InteropServices.Marshal.Copy(tempBuf, 0, bd.Scan0, maskTileSz);


            //    maskIndex += maskTileSz;

            //    bmpTile.UnlockBits(bd);

            //    bmpTile.RotateFlip(RotateFlipType.Rotate270FlipNone);//transposed

            //    g.DrawImage(bmpTile, loc.Y * uSz, header.Width - (loc.X * uSz), uSz + 1, uSz + 1); //transposed

            //}

            //bmpTile.Dispose();
            //g.InterpolationMode = interpolation;
            //return true;
        }

        public bool RenderMap(Graphics g,MapFile mapFile)
        {
            if (!checkArgs(g, mapFile))
                return false;

            var header = mapFile.Header;

            int uSz = (int)header.MapUnitSize;

            int[,] offsets = mapFile.GetBayerOffsetsYX();
            int wid = (int)header.Width;
            for (int y = 0; y < header.MapHeight; y++)
            {
                for (int x = 0; x < header.MapWidth; x++)
                {
                    if (offsets[y, x] >= 0)
                    {
                        g.FillRectangle(Brushes.Gray, new Rectangle(y * uSz, wid - (x * uSz), uSz, uSz));
                    }
                }
            }
            return true;
        }

        private static Bitmap createGreyscale(int width, int height)
        {
            var bmp = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
            var pal = bmp.Palette;
            var cols = pal.Entries;
            for (int i = 0; i < 256; i++)
            {
                cols[i] = Color.FromArgb((i * 0x10101) | -16777216); //0xff000000;
            }
            bmp.Palette = pal;
            return bmp;
        }

        private static byte[] buildBrightnessLUT(float m)
        {
            byte[] lut = new byte[256];
            int r = (int)(m * 65536f);
            for (int i = 0; i < 256; i++)
            {
                int n = i * r >> 16;
                lut[i] = (byte)(n < 256 ? n : 255);
            }
            return lut;
        }



        //private static unsafe byte[] Demosaic8bpp(byte[] source, int width, int height,int bayerOrder)
        //{
        //    int redX = bayerOrder & 1;
        //    int redY = (bayerOrder>>1) & 1;
        //    int size = width * height;
        //    if (source.Length < size || size < 1)
        //        throw new ArgumentOutOfRangeException();

        //    float[] fSrc = toFloat(source);// source.Cast<float>().ToArray();
        //    float[] fTgt = new float[size * 3];
        //    fixed (float* pSrc = fSrc, pTgt = fTgt)
        //    {
        //        MalvarDemosaic(pTgt, pSrc, width, height, redX, redY);
        //    }

        //    byte[] output = new byte[size * 4];
        //    int redOff = 0;
        //    int greenOff = size;
        //    int blueOff = size * 2;
        //    for (int yy = 0, i = 0, j = 0; yy < height; yy++)
        //    {
        //        for (int xx = 0; xx < width; xx++, j++)
        //        {
        //            output[i++] = ByteSaturate(fTgt[j + blueOff]);
        //            output[i++] = ByteSaturate(fTgt[j + greenOff]);
        //            output[i++] = ByteSaturate(fTgt[j + redOff]);
        //            output[i++] = 255;
        //        }
        //    }
        //    return output;// fTgt.Cast<byte>().ToArray();
        //}



        //private static float[] toFloat(byte[] data)
        //{
        //    int sz = data.Length;
        //    var fData = new float[sz];
        //    //float m = 1f;// / 255f;
        //    for (int i = 0; i < sz; i++)
        //        fData[i] = (float)data[i];// *m;
        //    return fData;
        //}
        //private static byte[] toByte(float[] data)
        //{
        //    int sz = data.Length;
        //    var bData = new byte[sz];
        //    for (int i = 0; i < sz; i++)
        //        bData[i] = (byte)data[i];//(data[i] * 255f);
        //    return bData;
        //}

        static void GetDemosaicOffsets(int bayerOrder, int stride, out int r, out int g, out int gg, out int b)
        {
            switch (bayerOrder)
            {
                case 0: //0
                    r = 0;
                    g = 1;
                    gg = stride;
                    b = stride + 1;
                    break;
                case 1: //1
                    g = 0;
                    r = 1;
                    b = stride;
                    gg = stride + 1;
                    break;
                case 2: //2
                    g = 0;
                    b = 1;
                    r = stride;
                    gg = stride + 1;
                    break;
                case 3: //3
                    b = 0;
                    g = 1;
                    gg = stride;
                    r = stride + 1;
                    break;
                default:
                    goto case 0;
            }
        }

        //unsafe static void Demosaic(byte* source, int width, int height, int stride, byte* target, int targetStride, int bayerOrder)
        //{
        //    int w = width >> 1;
        //    int h = height >> 1;
        //    int r, b, g0, g1;
        //    GetDemosaicOffsets(bayerOrder, stride, out r, out g0, out g1, out b);

        //    for (int y = 0; y < h; y++)
        //    {
        //        byte* s = source + (y << 1) * stride;
        //        byte* t = target + y * targetStride;
        //        for (int x = 0; x < w; x++)
        //        {
        //            *t++ = s[b];
        //            *t++ = (byte)(((int)s[g0] + s[g1]) >> 1);
        //            *t++ = s[r];
        //            *t++ = 255;
        //            s += 2;
        //        }
        //    }
        //}

        unsafe static void Demosaic(IntPtr source, int width, int height, int stride, byte[] target, int targetStride, int bayerOrder)
        {
            int w = width >> 1;
            int h = height >> 1;
            int r, b, g0, g1;
            GetDemosaicOffsets(bayerOrder, stride, out r, out g0, out g1, out b);

            byte* offset = (byte*)source;
            for (int y = 0; y < h; y++)
            {
                byte* s = offset + (y << 1) * stride;
                int i = y * targetStride;
                for (int x = 0; x < w; x++, s += 2)
                {
                    target[i++] = s[b];
                    target[i++] = (byte)(((int)s[g0] + s[g1]) >> 1);
                    target[i++] = s[r];
                    target[i++] = 255;
                }
            }
        }

       

        //private static unsafe void MalvarDemosaic(float* output, float* input, int width, int height, int redX, int redY)
        //{
        //    int BlueX = 1 - redX;
        //    int BlueY = 1 - redY;
        //    float* OutputRed = output;
        //    float* OutputGreen = output + width * height;
        //    float* OutputBlue = output + 2 * width * height;
        //    /* Neigh holds a copy of the 5x5 neighborhood around the current point */
        //    float[,] Neigh = new float[5, 5];
        //    /* NeighPresence is used for boundary handling.  It is set to 0 if the 
        //       neighbor is beyond the boundaries of the image and 1 otherwise. */
        //    int[,] NeighPresence = new int[5, 5];
        //    int i, j, x, y, nx, ny;


        //    for (y = 0, i = 0; y < height; y++)
        //    {
        //        for (x = 0; x < width; x++, i++)
        //        {
        //            /* 5x5 neighborhood around the point (x,y) is copied into Neigh */
        //            for (ny = -2, j = x + width * (y - 2); ny <= 2; ny++, j += width)
        //            {
        //                for (nx = -2; nx <= 2; nx++)
        //                {
        //                    if (0 <= x + nx && x + nx < width
        //                         && 0 <= y + ny && y + ny < height)
        //                    {
        //                        Neigh[2 + nx, 2 + ny] = input[j + nx];
        //                        NeighPresence[2 + nx, 2 + ny] = 1;
        //                    }
        //                    else
        //                    {
        //                        Neigh[2 + nx, 2 + ny] = 0;
        //                        NeighPresence[2 + nx, 2 + ny] = 0;
        //                    }
        //                }
        //            }

        //            if ((x & 1) == redX && (y & 1) == redY)
        //            {
        //                /* Center pixel is red */
        //                OutputRed[i] = input[i];
        //                OutputGreen[i] = (2 * (Neigh[2, 1] + Neigh[1, 2]
        //                    + Neigh[3, 2] + Neigh[2, 3])
        //                    + (NeighPresence[0, 2] + NeighPresence[4, 2]
        //                    + NeighPresence[2, 0] + NeighPresence[2, 4]) * Neigh[2, 2]
        //                    - Neigh[0, 2] - Neigh[4, 2]
        //                   - Neigh[2, 0] - Neigh[2, 4])
        //                    / (2 * (NeighPresence[2, 1] + NeighPresence[1, 2]
        //                    + NeighPresence[3, 2] + NeighPresence[2, 3]));
        //                OutputBlue[i] = (4 * (Neigh[1, 1] + Neigh[3, 1]
        //                     + Neigh[1, 3] + Neigh[3, 3]) +
        //                     3 * ((NeighPresence[0, 2] + NeighPresence[4, 2]
        //                     + NeighPresence[2, 0] + NeighPresence[2, 4]) * Neigh[2, 2]
        //                     - Neigh[0, 2] - Neigh[4, 2]
        //                     - Neigh[2, 0] - Neigh[2, 4]))
        //                     / (4 * (NeighPresence[1, 1] + NeighPresence[3, 1]
        //                     + NeighPresence[1, 3] + NeighPresence[3, 3]));
        //            }
        //            else if ((x & 1) == BlueX && (y & 1) == BlueY)
        //            {
        //                /* Center pixel is blue */
        //                OutputBlue[i] = input[i];
        //                OutputGreen[i] = (2 * (Neigh[2, 1] + Neigh[1, 2]
        //                   + Neigh[3, 2] + Neigh[2, 3])
        //                    + (NeighPresence[0, 2] + NeighPresence[4, 2]
        //                    + NeighPresence[2, 0] + NeighPresence[2, 4]) * Neigh[2, 2]
        //                   - Neigh[0, 2] - Neigh[4, 2]
        //                    - Neigh[2, 0] - Neigh[2, 4])
        //                    / (2 * (NeighPresence[2, 1] + NeighPresence[1, 2]
        //                    + NeighPresence[3, 2] + NeighPresence[2, 3]));

        //                OutputRed[i] = (4 * (Neigh[1, 1] + Neigh[3, 1]
        //                    + Neigh[1, 3] + Neigh[3, 3]) +
        //                    3 * ((NeighPresence[0, 2] + NeighPresence[4, 2]
        //                    + NeighPresence[2, 0] + NeighPresence[2, 4]) * Neigh[2, 2]
        //                    - Neigh[0, 2] - Neigh[4, 2]
        //                    - Neigh[2, 0] - Neigh[2, 4]))
        //                    / (4 * (NeighPresence[1, 1] + NeighPresence[3, 1]
        //                    + NeighPresence[1, 3] + NeighPresence[3, 3]));
        //            }
        //            else
        //            {
        //                /* Center pixel is green */
        //                OutputGreen[i] = input[i];

        //                if ((y & 1) == redY)
        //                {
        //                    /* Left and right neighbors are red */
        //                    OutputRed[i] = (8 * (Neigh[1, 2] + Neigh[3, 2])
        //                        + (2 * (NeighPresence[1, 1] + NeighPresence[3, 1]
        //                        + NeighPresence[0, 2] + NeighPresence[4, 2]
        //                        + NeighPresence[1, 3] + NeighPresence[3, 3])
        //                        - NeighPresence[2, 0] - NeighPresence[2, 4]) * Neigh[2, 2]
        //                        - 2 * (Neigh[1, 1] + Neigh[3, 1]
        //                        + Neigh[0, 2] + Neigh[4, 2]
        //                        + Neigh[1, 3] + Neigh[3, 3])
        //                        + Neigh[2, 0] + Neigh[2, 4])
        //                        / (8 * (NeighPresence[1, 2] + NeighPresence[3, 2]));

        //                    OutputBlue[i] = (8 * (Neigh[2, 1] + Neigh[2, 3])
        //                        + (2 * (NeighPresence[1, 1] + NeighPresence[3, 1]
        //                        + NeighPresence[2, 0] + NeighPresence[2, 4]
        //                        + NeighPresence[1, 3] + NeighPresence[3, 3])
        //                        - NeighPresence[0, 2] - NeighPresence[4, 2]) * Neigh[2, 2]
        //                        - 2 * (Neigh[1, 1] + Neigh[3, 1]
        //                        + Neigh[2, 0] + Neigh[2, 4]
        //                        + Neigh[1, 3] + Neigh[3, 3])
        //                        + Neigh[0, 2] + Neigh[4, 2])
        //                        / (8 * (NeighPresence[2, 1] + NeighPresence[2, 3]));
        //                }
        //                else
        //                {
        //                    /* Left and right neighbors are blue */
        //                    OutputRed[i] = (8 * (Neigh[2, 1] + Neigh[2, 3])
        //                        + (2 * (NeighPresence[1, 1] + NeighPresence[3, 1]
        //                        + NeighPresence[2, 0] + NeighPresence[2, 4]
        //                        + NeighPresence[1, 3] + NeighPresence[3, 3])
        //                        - NeighPresence[0, 2] - NeighPresence[4, 2]) * Neigh[2, 2]
        //                        - 2 * (Neigh[1, 1] + Neigh[3, 1]
        //                        + Neigh[2, 0] + Neigh[2, 4]
        //                        + Neigh[1, 3] + Neigh[3, 3])
        //                        + Neigh[0, 2] + Neigh[4, 2])
        //                        / (8 * (NeighPresence[2, 1] + NeighPresence[2, 3]));

        //                    OutputBlue[i] = (8 * (Neigh[1, 2] + Neigh[3, 2])
        //                        + (2 * (NeighPresence[1, 1] + NeighPresence[3, 1]
        //                        + NeighPresence[0, 2] + NeighPresence[4, 2]
        //                        + NeighPresence[1, 3] + NeighPresence[3, 3])
        //                        - NeighPresence[2, 0] - NeighPresence[2, 4]) * Neigh[2, 2]
        //                        - 2 * (Neigh[1, 1] + Neigh[3, 1]
        //                        + Neigh[0, 2] + Neigh[4, 2]
        //                        + Neigh[1, 3] + Neigh[3, 3])
        //                        + Neigh[2, 0] + Neigh[2, 4])
        //                        / (8 * (NeighPresence[1, 2] + NeighPresence[3, 2]));
        //                }
        //            }
        //        }
        //    }
        //}

        public static Bitmap CreateBitmapFromImage(Createc.Net.Imaging.Image image)
        {
            if(image.PixelFormat != PixelTypes.BGRA32)
                throw new ArgumentException("PixelType unsupported");

            var fmt = PixelFormat.Format32bppRgb;
            var bmp = new Bitmap(image.Width, image.Height, fmt);
            var bd = bmp.LockBits(new Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.WriteOnly, fmt);
            System.Runtime.InteropServices.Marshal.Copy(image.Buffer, 0, bd.Scan0, image.Size);
            bmp.UnlockBits(bd);
            return bmp;
        }


    }

    public struct Pair<T>
    {
        public Pair(T a, T b)
            : this()
        {
            A = a;
            B = b;
        }
        public T A { get; set; }
        public T B { get; set; }
    }
}
