﻿namespace Createc.StereoBirds.Viewer
{
    partial class GraphicsOptionsBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.interpolationBox = new System.Windows.Forms.GroupBox();
            this.nearestNeighbourButton = new System.Windows.Forms.RadioButton();
            this.highQualityBicubicButton = new System.Windows.Forms.RadioButton();
            this.highQualityBilinearButton = new System.Windows.Forms.RadioButton();
            this.bicubicButton = new System.Windows.Forms.RadioButton();
            this.bilinearButton = new System.Windows.Forms.RadioButton();
            this.antiAliasCheck = new System.Windows.Forms.CheckBox();
            this.interpolationBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // interpolationBox
            // 
            this.interpolationBox.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.interpolationBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.interpolationBox.Controls.Add(this.nearestNeighbourButton);
            this.interpolationBox.Controls.Add(this.highQualityBicubicButton);
            this.interpolationBox.Controls.Add(this.highQualityBilinearButton);
            this.interpolationBox.Controls.Add(this.bicubicButton);
            this.interpolationBox.Controls.Add(this.bilinearButton);
            this.interpolationBox.Controls.Add(this.antiAliasCheck);
            this.interpolationBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.interpolationBox.Location = new System.Drawing.Point(0, 0);
            this.interpolationBox.Name = "interpolationBox";
            this.interpolationBox.Size = new System.Drawing.Size(150, 167);
            this.interpolationBox.TabIndex = 7;
            this.interpolationBox.TabStop = false;
            this.interpolationBox.Tag = "";
            this.interpolationBox.Text = "Graphics";
            // 
            // nearestNeighbourButton
            // 
            this.nearestNeighbourButton.AutoSize = true;
            this.nearestNeighbourButton.Checked = true;
            this.nearestNeighbourButton.Location = new System.Drawing.Point(7, 19);
            this.nearestNeighbourButton.Name = "nearestNeighbourButton";
            this.nearestNeighbourButton.Size = new System.Drawing.Size(114, 17);
            this.nearestNeighbourButton.TabIndex = 0;
            this.nearestNeighbourButton.TabStop = true;
            this.nearestNeighbourButton.Text = "Nearest Neighbour";
            this.nearestNeighbourButton.UseVisualStyleBackColor = true;
            this.nearestNeighbourButton.CheckedChanged += new System.EventHandler(this.option_CheckedChanged);
            // 
            // highQualityBicubicButton
            // 
            this.highQualityBicubicButton.AutoSize = true;
            this.highQualityBicubicButton.Location = new System.Drawing.Point(7, 111);
            this.highQualityBicubicButton.Name = "highQualityBicubicButton";
            this.highQualityBicubicButton.Size = new System.Drawing.Size(110, 17);
            this.highQualityBicubicButton.TabIndex = 4;
            this.highQualityBicubicButton.TabStop = true;
            this.highQualityBicubicButton.Text = "Prefiltered Bicubic";
            this.highQualityBicubicButton.UseVisualStyleBackColor = true;
            // 
            // highQualityBilinearButton
            // 
            this.highQualityBilinearButton.AutoSize = true;
            this.highQualityBilinearButton.Location = new System.Drawing.Point(7, 88);
            this.highQualityBilinearButton.Name = "highQualityBilinearButton";
            this.highQualityBilinearButton.Size = new System.Drawing.Size(109, 17);
            this.highQualityBilinearButton.TabIndex = 3;
            this.highQualityBilinearButton.TabStop = true;
            this.highQualityBilinearButton.Text = "Prefiltered Bilinear";
            this.highQualityBilinearButton.UseVisualStyleBackColor = true;
            // 
            // bicubicButton
            // 
            this.bicubicButton.AutoSize = true;
            this.bicubicButton.Location = new System.Drawing.Point(7, 65);
            this.bicubicButton.Name = "bicubicButton";
            this.bicubicButton.Size = new System.Drawing.Size(60, 17);
            this.bicubicButton.TabIndex = 2;
            this.bicubicButton.Text = "Bicubic";
            this.bicubicButton.UseVisualStyleBackColor = true;
            // 
            // bilinearButton
            // 
            this.bilinearButton.AutoSize = true;
            this.bilinearButton.Location = new System.Drawing.Point(7, 42);
            this.bilinearButton.Name = "bilinearButton";
            this.bilinearButton.Size = new System.Drawing.Size(59, 17);
            this.bilinearButton.TabIndex = 1;
            this.bilinearButton.TabStop = true;
            this.bilinearButton.Text = "Bilinear";
            this.bilinearButton.UseVisualStyleBackColor = true;
            // 
            // antiAliasCheck
            // 
            this.antiAliasCheck.AutoSize = true;
            this.antiAliasCheck.Checked = true;
            this.antiAliasCheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.antiAliasCheck.Location = new System.Drawing.Point(7, 142);
            this.antiAliasCheck.Name = "antiAliasCheck";
            this.antiAliasCheck.Size = new System.Drawing.Size(83, 17);
            this.antiAliasCheck.TabIndex = 2;
            this.antiAliasCheck.Text = "Anti Aliasing";
            this.antiAliasCheck.UseVisualStyleBackColor = true;
            this.antiAliasCheck.CheckedChanged += new System.EventHandler(this.antiAliasCheck_CheckedChanged);
            // 
            // GraphicsOptionsBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Controls.Add(this.interpolationBox);
            this.Name = "GraphicsOptionsBox";
            this.Size = new System.Drawing.Size(150, 167);
            this.interpolationBox.ResumeLayout(false);
            this.interpolationBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox interpolationBox;
        private System.Windows.Forms.RadioButton nearestNeighbourButton;
        private System.Windows.Forms.RadioButton highQualityBicubicButton;
        private System.Windows.Forms.RadioButton highQualityBilinearButton;
        private System.Windows.Forms.RadioButton bicubicButton;
        private System.Windows.Forms.RadioButton bilinearButton;
        private System.Windows.Forms.CheckBox antiAliasCheck;
    }
}
