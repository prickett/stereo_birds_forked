﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;

namespace Createc.StereoBirds.Viewer
{
    static class UserData
    {
        static string AppDataFolder
        {
            get { return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\Createc\CollisionMonitoring"; }
        }
        static string CalibrationPath
        {
            get { return AppDataFolder + @"\Calibration"; }
        }


        public static StereoCalibrationSet LoadCalibrationSet()
        {
            StereoCalibrationSet set = Deserialise<StereoCalibrationSet>(CalibrationPath + ".xml"); 

            if (set == null) // if no xml try loading the old binary version
            {
                Console.WriteLine("Calibration.xml file not found, attempting to load binary version");
                set = (StereoCalibrationSet)DeserialiseBinary(CalibrationPath + ".bin");
            }
                

            if (set == null)
            {
                set = new StereoCalibrationSet();
            }
            return set;
        }

        public static void SaveCalibrationSet(StereoCalibrationSet set)
        {
            Console.WriteLine("Saving calibration set");
            Serialise(CalibrationPath + ".xml", set);
        }


        static object DeserialiseBinary(string path)
        {
            object obj = null;

            try
            {
                if (File.Exists(path))
                {
                    using (var fs = new FileStream(path, FileMode.Open))
                    {
                        obj = new BinaryFormatter().Deserialize(fs);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to deserialise. {0}", e.Message);
            }

            return obj;
        }

        static void SerialiseBinary(string path, object obj)
        {
            if (obj == null)
                throw new ArgumentNullException();

            try
            {
                string folder = Path.GetDirectoryName(path);
                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);

                using (var fs = new FileStream(path, FileMode.Create))
                {
                    new BinaryFormatter().Serialize(fs,obj);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to serialise. {0}", e.Message);
            }
        }


        static T Deserialise<T>(string path)
        {
            object obj = null;

            try
            {
                if (File.Exists(path))
                {
                    using (var fs = new FileStream(path, FileMode.Open))
                    {
                        var xml = new XmlSerializer(typeof(T));
                        obj = xml.Deserialize(fs);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to deserialise. {0}", e.Message);
            }

            return (T)obj;
        }

        //static object Deserialise(string path)
        //{
        //    object obj = null;

        //    try
        //    {
        //        if (File.Exists(path))
        //        {
        //            using (var fs = new FileStream(path, FileMode.Open))
        //            {
        //                //obj = new BinaryFormatter().Deserialize(fs);
        //                XmlSerializer serializer = new XmlSerializer(typeof(StereoCalibrationSet));//.Deserialize(fs, obj);
                        
        //                //Console.WriteLine("{0}", obj.GetType().ToString());
        //                //var xml = new XmlSerializer(obj.GetType());
        //                //obj = xml.Deserialize(fs);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Failed to deserialise. {0}", e.Message);
        //    }

        //    return obj;
        //}

        static void Serialise(string path, object obj)
        {
            if (obj == null)
                throw new ArgumentNullException();

            try
            {
                string folder = Path.GetDirectoryName(path);
                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);

                using (var fs = new FileStream(path, FileMode.Create))
                {
                    new XmlSerializer(obj.GetType()).Serialize(fs, obj);
                }
            }
            catch (Exception e)
            {

                Console.WriteLine("Failed to serialise.");
                while(e != null)
                {
                    Console.WriteLine("{0}", e.Message);
                    e = e.InnerException;
                }

            }
        }




        //static void Serialise<T>(string path, T obj)
        //{
        //    if (obj == null)
        //        throw new ArgumentNullException();

        //    try
        //    {
        //        string folder = Path.GetDirectoryName(path);
        //        if (!Directory.Exists(folder))
        //            Directory.CreateDirectory(folder);

        //        using (var fs = new FileStream(path, FileMode.Create))
        //        {
        //            //new BinaryFormatter().Serialize(fs,obj);
        //            var xml = new XmlSerializer(typeof(T));
        //            xml.Serialize(fs, obj);
        //        } 
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine("Failed to serialise. {0}", e.Message);
        //    }
        //}


    }
}
