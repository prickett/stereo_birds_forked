﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Createc.StereoBirds.Viewer
{
    class MultiThreadList<T>
    {
        delegate int Comparer(T a, T b);

        Comparer compare;
        Mutex mutex = new Mutex();
        List<T> pairs = new List<T>();
        int index;

        public int Count { get { return pairs.Count; } }

        public T Current
        {
            get
            {
                mutex.WaitOne();
                var fp = InRange(index) ? pairs[index] : default(T);
                mutex.ReleaseMutex();
                return fp;
            }
        }


        public T FindNearest(T value)
        {
            mutex.WaitOne();
            int i = Find(value);
            if (i < 0) i = ~i;
            if (i >= pairs.Count) i = pairs.Count - 1;
            var fp = InRange(i) ? pairs[i] : default(T);
            mutex.ReleaseMutex();
            return fp;
        }
        public bool MoveTo(T value)
        {
            mutex.WaitOne();
            int i = Find(value);
            if (i >= 0)
                index = i;
            mutex.ReleaseMutex();
            return i >= 0;
        }

        int Find(T value)
        {
            return pairs.BinarySearch(value);
        }

        public bool MoveNext()
        {
            if (InRange(++index))
                return true;
            index--;
            return false;
        }

        public bool MovePrevious()
        {
            if (InRange(--index))
                return true;
            index++;
            return false;

        }

        public void Reset()
        {
            index = -1;
        }

        public void FilterAndAdd(ref List<T> sorted)
        {
            if (sorted == null || sorted.Count == 0)
                return;

            sorted.Sort();

            mutex.WaitOne();

            if (pairs.Count == 0 || compare(sorted[0], pairs.Last()) > 0) //new pairs go after
            {
                pairs.AddRange(sorted);
            }
            else if (compare(sorted.Last(), pairs[0]) < 0) //new pairs go before
            {
                pairs.InsertRange(0, sorted);
                index += sorted.Count;
            }
            else //new pairs merged
            {
                T fp = InRange(index) ? pairs[index] : default(T);
                sorted = new List<T>(sorted.Where(f => pairs.BinarySearch(f) < 0));
                pairs.AddRange(sorted);
                pairs.Sort();
                index = pairs.BinarySearch(fp);
            }

            mutex.ReleaseMutex();

        }

        bool InRange(int index)
        {
            return index >= 0 && index < pairs.Count;
        }

    }
}
