﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Createc.Net.Vector;

namespace Createc.StereoBirds.Viewer
{
    [Serializable]
    public class CameraPair
    {
        public CameraPair() 
        {
            FriendlyName = "Friendly name";
            MasterId = 10000001;
            SlaveId = 10000002;
            Location = new GlobalLocation();
            Azimuth = 0;
            Width = 1;
            Height = 1;
        }
        public CameraPair(string friendlyName, long masterId, long slaveId, GlobalLocation location, double azimuth, int width, int height)
        {
            FriendlyName = friendlyName;
            MasterId = masterId;
            SlaveId = slaveId;
            Location = location;
            Azimuth = azimuth;
            Width = width;
            Height = height;
        }

        public string FriendlyName { get;  set; }
        public long MasterId { get;set; }
        public long SlaveId { get;  set; }        
        public GlobalLocation Location { get;  set; }
        public double Azimuth { get;  set; }
        public int Width { get;  set; }
        public int Height { get;  set; }

        public static bool operator ==(CameraPair a, CameraPair b)
        {
            if (a == null || b == null)
                return a == null && b == null;
            else
                return
                    a.FriendlyName == b.FriendlyName &&
                    a.MasterId == b.MasterId &&
                    a.SlaveId == b.SlaveId &&
                    a.Location.Latitude == b.Location.Latitude &&
                    a.Location.Longitude == b.Location.Longitude &&
                    a.Location.Elevation == b.Location.Elevation &&
                    a.Azimuth == b.Azimuth &&
                    a.Width == b.Width &&
                    a.Height == b.Height;

        }

        public static bool operator !=(CameraPair a, CameraPair b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            return GetType() == obj.GetType() && this == (CameraPair)obj; //== checks for null
        }

        public override int GetHashCode()
        {   
            // fix to work with old binary
            if (FriendlyName == null)
                FriendlyName = "Friendly name";
            // end of fix

            return
                FriendlyName.GetHashCode() ^
                MasterId.GetHashCode() ^
                SlaveId.GetHashCode() ^
                Location.Latitude.GetHashCode() ^
                Location.Longitude.GetHashCode() ^
                Location.Elevation.GetHashCode() ^
                Azimuth.GetHashCode() ^
                Width ^ Height; 
        }

        public void Debug()
        {
            Console.Write("Camera Pair\n");
            Console.Write(" Name:       {0}\n", FriendlyName);
            Console.Write(" MasterId:   {0}\n", MasterId);
            Console.Write(" SlaveId:    {0}\n", SlaveId);
            Console.Write(" Width:      {0}\n", Width);
            Console.Write(" Height:     {0}\n", Height);
            Console.Write(" Lattitude:  {0}\n", Location.Latitude);
            Console.Write(" Longitude:  {0}\n", Location.Longitude);
            Console.Write(" Elevation:  {0}\n", Location.Elevation);
            Console.Write(" Azimuth:    {0}\n", Azimuth);
        }

    }

    [Serializable]
    public struct GlobalLocation
    {
        double lat, lng, ele;
        public GlobalLocation(double latitude, double longitude, double elevation)
        {
            lat = latitude;
            lng = longitude;
            ele = elevation;
        }
        public double Latitude { get { return lat; } set { lat = value; } }
        public double Longitude { get { return lng; } set { lng = value; } }
        public double Elevation { get { return ele; } set { ele = value; } }
    }
}
