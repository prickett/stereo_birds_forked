using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Createc.Imaging.Thresholds
{
    class Thresholder
    {
        private delegate void SliceMatcherDelegate(int y);
        private int height;
        private int width;
        private List<Blob> blobList;// = new List<Blob>(0);
        private Slice[] current = new Slice[0];
        private Slice[] previous = new Slice[0];
        private int currCt = 0;
        private int prevCt = 0;
        private int offsetX;
        private int offsetY;
        private SliceMatcherDelegate sliceMatcher;
        public int DebugCt { get; private set; }
        

        public List<Blob> BlobList
        {
            get { return blobList; }
        }


        public Thresholder(bool stickyCorners)
        {
            StickyCorners = stickyCorners;
        }
        public Thresholder() : this(false) { }

        public bool StickyCorners
        {
            get { return sliceMatcher == matchSlicesStickyCorners; }
            set
            {
                if (value)
                    sliceMatcher = matchSlicesStickyCorners;
                else
                    sliceMatcher = matchSlices;
            }
        }

        public List<Blob> Threshold8bit(byte[] image, int width, byte threshold)
        {
            return Threshold8bit(image, width, image.Length / width, 0, width, threshold);
        }

        public List<Blob> Threshold8bit(byte[] image, int width, int height, int offset, int stride, byte threshold)
        {
            offsetY = offset / stride;
            offsetX = offset - (offsetY * stride);
            initMembers(width, height);
            for (int y = 0; y < height; y++, offset += stride)
            {
                threshold8bppScanline(image, offset, threshold);
                sliceMatcher(y); //call our slice matcher delegate
                addFinishedBlobs(y);
                moveToNextScanline();
            }
            addFinishedBlobs(height);//add the remaining blobs to the blob list
            return blobList;
        }


        public List<Blob> Threshold1bit(byte[] image, int width)
        {
            int stride = (width + 7) >> 3;
            return Threshold1bit(image, width, image.Length / stride, 0, stride);
        }

        public List<Blob> Threshold1bit(byte[] image, int width, int height, int offset, int stride)
        {
            offsetY = offset / stride;
            offsetX = (offset - (offsetY * stride))<<3;
            initMembers(width, height);
            for (int y = 0; y < height; y++)//, i = 0
            {
                threshold1bppScanline(image, y * stride);
                sliceMatcher(y);
                addFinishedBlobs(y);
                moveToNextScanline();
            }
            addFinishedBlobs(height);//add the remaining blobs to the blob list

            return blobList;
        }

        private void initMembers(int width, int height)
        {
            this.height=height;
            this.width = width;
            int sz = maxSlice(width);
            if (sz != current.Length)
            {
                current = new Slice[sz];
                previous = new Slice[sz];
            }
            blobList = new List<Blob>();
            currCt = 0;
            prevCt = 0;
        }

        private Thresholder(int width)
        {
            this.width = width;
            int sz = maxSlice(width);
            blobList = new List<Blob>();
            current = new Slice[sz];
            previous = new Slice[sz];
        }

        //public Thresholder(byte[] image8bpp, int width, byte threshold)
        //    : this(width)
        //{
        //    height = image8bpp.Length / width;

        //    for (int y = 0; y < height; y++)//, i = 0
        //    {
        //        threshold8bppScanline(image8bpp, y * width, threshold);
        //        matchSlices(y);
        //        addFinishedBlobs(y);
        //        moveToNextScanline();
        //    }
        //    addFinishedBlobs(height);//add the remaining blobs to the blob list
        //}

        //public Thresholder(byte[] image1bpp, int width)
        //    : this(width)
        //{
        //    int stride = (width + 7) >> 3;
        //    height = image1bpp.Length / stride;

        //    for (int y = 0; y < height; y++)//, i = 0
        //    {
        //        threshold1bppScanline(image1bpp, y * stride);
        //        matchSlices(y);
        //        addFinishedBlobs(y);
        //        moveToNextScanline();
        //    }
        //    addFinishedBlobs(height);//add the remaining blobs to the blob list
        //}

        private void moveToNextScanline()
        {
            var tmp = previous;
            previous = current; //move back
            current = tmp;//swap back previous to over write 

            prevCt = currCt;
            currCt = 0;
        }

        private void threshold8bppScanline(byte[] image, int offset, byte threshold)
        {
            bool over = false;
            for (int x = 0; x < width; x++)
            {
                if (over != image[x + offset] >= threshold)//trips on threshold change
                {
                    over = !over;
                    if (over) //starting a new slice
                    {
                        current[currCt].x0 = x;
                    }
                    else //finishing slice
                    {
                        current[currCt++].x1 = x;
                    }
                }
            }
            if (over)//the scanline ended on a slice
            {
                current[currCt++].x1 = width;
            }
        }

        private void threshold1bppScanline(byte[] image, int offset)
        {
            bool over = false;
            for (int x = 0; x < width; x+=8)
            {
                int bits = image[offset++];
                if (bits != 0)
                {
                    int n = 1, b = 0;
                    while (b < 8)
                    {
                        if (over != ((n & bits) != 0))//if threshold change
                        {
                            over = !over;
                            if (over) //start of new slice
                            {
                                current[currCt].x0 = x + b; //start new slice
                            }
                            else //end of slice
                            {
                                current[currCt++].x1 = x + b; 
                            }
                        }
                        n <<= 1;
                        b++;
                    }
                }
                else if (over)//byte is zero and last byte ended on a bit
                {
                    current[currCt++].x1 = x; //close slice
                    over = false;
                }
            }
            if (over)//the scanline ended on a slice
            {
                current[currCt++].x1 = width; //close slice
            }
        }

        //public ThresholdBlobs(byte[] image1bpp, int width,bool foo,bool bar)
        //    : this(width)
        //{
        //    int stride = (width + 7) >> 3;
        //    height = image1bpp.Length / stride;

        //    for (int y = 0, i = 0; y < height; y++)
        //    {

        //        bool over = false;
        //        for (int x = 0; x < width; x += 8, i++)
        //        {
        //            if (image1bpp[i] != 0)
        //            {
        //                int bits = image1bpp[i];
        //                int n = 1, b = 0;
        //                while (b < 8)
        //                {
        //                    if (over != ((n & bits) != 0))//threshold change
        //                    {
        //                        over = !over;
        //                        if (over) //starting a new slice
        //                        {
        //                            current[currCt].x0 = x + b;
        //                        }
        //                        else //finishing slice
        //                        {
        //                            current[currCt++].x1 = x + b;
        //                        }
        //                    }
        //                    n <<= 1;
        //                    b++;
        //                }
        //            }
        //            else if (over)//byte is zero and last byte ended on a bit
        //            {
        //                current[currCt++].x1 = x;
        //            }
        //        }

        //        if (over)//the scanline ended on a slice
        //        {
        //            current[currCt++].x1 = width;
        //        }
        //        matchSlices(y);
        //    }
        //    addFinishedBlobs(height);//add the remaining blobs to the blob list
        //}






        private int maxSlice(int width)
        {
            return width + 1 >> 1;
        }

        void matchSlices(int y)
        {
            for (int c = 0, p = 0; c < currCt; c++)
            {
                int x0 = current[c].x0;
                int x1 = current[c].x1;

                //we need to match up the slices from the previous scan with the slices on the current scan

                while (p < prevCt && previous[p].x1 <= x0) //while previous ends before current starts
                    p++;//increment previous

                if (p < prevCt && previous[p].x0 < x1)//if previous starts before current ends (and previous ends before current starts)
                {
                    //we have our first hit, make this parent
                    Blob blob = previous[p].Blob;
                    //if (blob.Area == 0)
                    //    Console.Write('^');
                    blob.Add(y, x0, x1);
                    current[c].Blob = blob;

                    //while previous finishes equal or before current finish increment previous
                    //if the next previous starts before the current ends absorb it.
                    while (previous[p].x1 <= x1 && ++p < prevCt && previous[p].x0 < x1) 
                    {
                        if (previous[p].Blob != blob)
                        {
                            blob.Absorb(previous[p].Blob);
                            replaceAbsorbed(blob, p, c);
                        }
                    }                    
                }
                else //no overlap with previous slices... 
                {    //...create a new blob 
                    Blob blob = new Blob();
                    blob.Add(y, x0, x1);
                    current[c].Blob = blob;
                }
            }
        }


        void matchSlicesStickyCorners(int y)
        {
            for (int c = 0, p = 0; c < currCt; c++)
            {
                int x0 = current[c].x0;
                int x1 = current[c].x1;

                //we need to match up the slices from the previous scan with the slices on the current scan

                while (p < prevCt && previous[p].x1 < x0) //while previous ends before current starts
                    p++;//increment previous

                if (p < prevCt && previous[p].x0 <= x1)//if previous starts before current ends (and previous ends before current starts)
                {
                    //we have our first hit, make this parent
                    Blob blob = previous[p].Blob;
                    //if (blob.Area == 0)
                    //    Console.Write('^');
                    blob.Add(y, x0, x1);
                    current[c].Blob = blob;

                    //while previous finishes equal or before current finish increment previous
                    //if the next previous starts before the current ends absorb it.
                    while (previous[p].x1 <= x1 && ++p < prevCt && previous[p].x0 <= x1)
                    {
                        if (previous[p].Blob != blob)
                        {
                            blob.Absorb(previous[p].Blob);
                            replaceAbsorbed(blob, p, c);
                        }
                    }
                }
                else //no overlap with previous slices... 
                {    //...create a new blob 
                    Blob blob = new Blob();
                    blob.Add(y, x0, x1);
                    current[c].Blob = blob;
                }
            }
        }

        private void replaceAbsorbed(Blob replacement, int prev,int curr )
        {
            for (int i = prev; i < prevCt; i++)
            {
                if (previous[i].Blob.Area == 0)
                {
                    previous[i].Blob = replacement;
                }
            }
            for (int i = 0; i < curr; i++)
            {
                if (current[i].Blob.Area == 0)
                {
                    current[i].Blob = replacement;
                }
            }
        }




        /// <summary>
        /// Adds any finished blobs to the blob list 
        /// </summary>
        /// <param name="y">the current scanline</param>
        private void addFinishedBlobs(int y)
        {
            for (int i = 0; i < prevCt; i++)
            {
                Blob b = previous[i].Blob;
                if (b.YMax < y && b.Area != 0)
                {
                    Blob nb = new Blob(); //turn b into a copy of b
                    nb.Absorb(b);
                    nb.Offset(offsetX, offsetY);//adjust to image space (if region)
                    blobList.Add(nb);
                }
            }
        }

        void swap<T>(ref T a, ref T b)
        {
            T t = a;
            a = b;
            b = t;
        }

        struct Slice
        {
            public int x0;
            public int x1;
            public Blob Blob;
            //public BlobReference parent;
        }

        class BlobReference
        {
            public Blob Blob { get; set; }
            public BlobReference(Blob blob)
            {
                Blob = blob;
            }
        }
    }   
}
