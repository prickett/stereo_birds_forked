﻿namespace Createc.StereoBirds.Viewer
{
    partial class mainFormOld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.timeLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.aquisitionProgressBar = new System.Windows.Forms.ProgressBar();
            this.fileSearchCancelButton = new System.Windows.Forms.Button();
            this.fileFinderlabel = new System.Windows.Forms.Label();
            this.openFilesButton = new System.Windows.Forms.Button();
            this.openFolderButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.birdSearchCancelButton = new System.Windows.Forms.Button();
            this.birdSearchProgressBar = new System.Windows.Forms.ProgressBar();
            this.birdSearchButton = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bayerUpDown = new System.Windows.Forms.NumericUpDown();
            this.clearMarksButton = new System.Windows.Forms.Button();
            this.saveMarkersButton = new System.Windows.Forms.Button();
            this.bakButton = new System.Windows.Forms.Button();
            this.fwdButton = new System.Windows.Forms.Button();
            this.playImageButton = new System.Windows.Forms.RadioButton();
            this.SaveFrameButton = new System.Windows.Forms.Button();
            this.SkipUnmatchedCheck = new System.Windows.Forms.CheckBox();
            this.playBayerButton = new System.Windows.Forms.RadioButton();
            this.playMaskButton = new System.Windows.Forms.RadioButton();
            this.playBmpButton = new System.Windows.Forms.RadioButton();
            this.playMapButton = new System.Windows.Forms.RadioButton();
            this.playPauseButton = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.quitFrameAquisitionButton = new System.Windows.Forms.Button();
            this.CheckSyncButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.sortFilesButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.frameTimer = new System.Windows.Forms.Timer(this.components);
            this.birdConfirmPanel = new Createc.StereoBirds.Viewer.BirdConfirmPanel();
            this.graphicsOptionsBox1 = new Createc.StereoBirds.Viewer.GraphicsOptionsBox();
            this.calibrationPanel = new Createc.StereoBirds.Viewer.CalibrationPanel();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bayerUpDown)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Controls.Add(this.timeLabel);
            this.flowLayoutPanel1.Controls.Add(this.groupBox1);
            this.flowLayoutPanel1.Controls.Add(this.graphicsOptionsBox1);
            this.flowLayoutPanel1.Controls.Add(this.groupBox3);
            this.flowLayoutPanel1.Controls.Add(this.groupBox2);
            this.flowLayoutPanel1.Controls.Add(this.groupBox4);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(181, 600);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // timeLabel
            // 
            this.timeLabel.AutoSize = true;
            this.timeLabel.Location = new System.Drawing.Point(3, 0);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(0, 13);
            this.timeLabel.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.aquisitionProgressBar);
            this.groupBox1.Controls.Add(this.fileSearchCancelButton);
            this.groupBox1.Controls.Add(this.fileFinderlabel);
            this.groupBox1.Controls.Add(this.openFilesButton);
            this.groupBox1.Controls.Add(this.openFolderButton);
            this.groupBox1.Location = new System.Drawing.Point(9, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(147, 163);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(20, 140);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(87, 23);
            this.button3.TabIndex = 15;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // aquisitionProgressBar
            // 
            this.aquisitionProgressBar.Location = new System.Drawing.Point(7, 105);
            this.aquisitionProgressBar.Name = "aquisitionProgressBar";
            this.aquisitionProgressBar.Size = new System.Drawing.Size(133, 12);
            this.aquisitionProgressBar.TabIndex = 14;
            // 
            // fileSearchCancelButton
            // 
            this.fileSearchCancelButton.Enabled = false;
            this.fileSearchCancelButton.Location = new System.Drawing.Point(28, 77);
            this.fileSearchCancelButton.Name = "fileSearchCancelButton";
            this.fileSearchCancelButton.Size = new System.Drawing.Size(87, 23);
            this.fileSearchCancelButton.TabIndex = 10;
            this.fileSearchCancelButton.Text = "Cancel";
            this.fileSearchCancelButton.UseVisualStyleBackColor = true;
            this.fileSearchCancelButton.Click += new System.EventHandler(this.fileSearchCancelButton_Click);
            // 
            // fileFinderlabel
            // 
            this.fileFinderlabel.Location = new System.Drawing.Point(7, 118);
            this.fileFinderlabel.Name = "fileFinderlabel";
            this.fileFinderlabel.Size = new System.Drawing.Size(133, 36);
            this.fileFinderlabel.TabIndex = 9;
            this.fileFinderlabel.Text = "0 files found";
            this.fileFinderlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // openFilesButton
            // 
            this.openFilesButton.Location = new System.Drawing.Point(28, 19);
            this.openFilesButton.Name = "openFilesButton";
            this.openFilesButton.Size = new System.Drawing.Size(87, 23);
            this.openFilesButton.TabIndex = 8;
            this.openFilesButton.Text = "Open files";
            this.openFilesButton.UseVisualStyleBackColor = true;
            this.openFilesButton.Click += new System.EventHandler(this.OpenFiles_Click);
            // 
            // openFolderButton
            // 
            this.openFolderButton.Location = new System.Drawing.Point(28, 48);
            this.openFolderButton.Name = "openFolderButton";
            this.openFolderButton.Size = new System.Drawing.Size(87, 23);
            this.openFolderButton.TabIndex = 7;
            this.openFolderButton.Text = "Open folder";
            this.openFolderButton.UseVisualStyleBackColor = true;
            this.openFolderButton.Click += new System.EventHandler(this.OpenFolder_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.birdSearchCancelButton);
            this.groupBox3.Controls.Add(this.birdSearchProgressBar);
            this.groupBox3.Controls.Add(this.birdSearchButton);
            this.groupBox3.Location = new System.Drawing.Point(3, 345);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(147, 98);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Bird Search";
            // 
            // birdSearchCancelButton
            // 
            this.birdSearchCancelButton.Enabled = false;
            this.birdSearchCancelButton.Location = new System.Drawing.Point(28, 48);
            this.birdSearchCancelButton.Name = "birdSearchCancelButton";
            this.birdSearchCancelButton.Size = new System.Drawing.Size(87, 23);
            this.birdSearchCancelButton.TabIndex = 14;
            this.birdSearchCancelButton.Text = "Cancel";
            this.birdSearchCancelButton.UseVisualStyleBackColor = true;
            this.birdSearchCancelButton.Click += new System.EventHandler(this.birdSearchCancelButton_Click);
            // 
            // birdSearchProgressBar
            // 
            this.birdSearchProgressBar.Location = new System.Drawing.Point(7, 77);
            this.birdSearchProgressBar.Name = "birdSearchProgressBar";
            this.birdSearchProgressBar.Size = new System.Drawing.Size(133, 12);
            this.birdSearchProgressBar.TabIndex = 13;
            // 
            // birdSearchButton
            // 
            this.birdSearchButton.Location = new System.Drawing.Point(28, 19);
            this.birdSearchButton.Name = "birdSearchButton";
            this.birdSearchButton.Size = new System.Drawing.Size(87, 23);
            this.birdSearchButton.TabIndex = 13;
            this.birdSearchButton.Text = "Search";
            this.birdSearchButton.UseVisualStyleBackColor = true;
            this.birdSearchButton.Click += new System.EventHandler(this.birdSearchButton_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.bayerUpDown);
            this.groupBox2.Controls.Add(this.clearMarksButton);
            this.groupBox2.Controls.Add(this.saveMarkersButton);
            this.groupBox2.Controls.Add(this.bakButton);
            this.groupBox2.Controls.Add(this.fwdButton);
            this.groupBox2.Controls.Add(this.playImageButton);
            this.groupBox2.Controls.Add(this.SaveFrameButton);
            this.groupBox2.Controls.Add(this.SkipUnmatchedCheck);
            this.groupBox2.Controls.Add(this.playBayerButton);
            this.groupBox2.Controls.Add(this.playMaskButton);
            this.groupBox2.Controls.Add(this.playBmpButton);
            this.groupBox2.Controls.Add(this.playMapButton);
            this.groupBox2.Controls.Add(this.playPauseButton);
            this.groupBox2.Location = new System.Drawing.Point(3, 449);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(147, 352);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Playback";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(45, 326);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Bayer order";
            // 
            // bayerUpDown
            // 
            this.bayerUpDown.Location = new System.Drawing.Point(7, 324);
            this.bayerUpDown.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.bayerUpDown.Name = "bayerUpDown";
            this.bayerUpDown.Size = new System.Drawing.Size(31, 21);
            this.bayerUpDown.TabIndex = 16;
            this.bayerUpDown.ValueChanged += new System.EventHandler(this.bayerUpDown_ValueChanged);
            // 
            // clearMarksButton
            // 
            this.clearMarksButton.Location = new System.Drawing.Point(28, 135);
            this.clearMarksButton.Name = "clearMarksButton";
            this.clearMarksButton.Size = new System.Drawing.Size(87, 23);
            this.clearMarksButton.TabIndex = 15;
            this.clearMarksButton.Text = "Clear Marks";
            this.clearMarksButton.UseVisualStyleBackColor = true;
            this.clearMarksButton.Click += new System.EventHandler(this.clearMarksButton_Click);
            // 
            // saveMarkersButton
            // 
            this.saveMarkersButton.Location = new System.Drawing.Point(28, 105);
            this.saveMarkersButton.Name = "saveMarkersButton";
            this.saveMarkersButton.Size = new System.Drawing.Size(87, 23);
            this.saveMarkersButton.TabIndex = 14;
            this.saveMarkersButton.Text = "Save Marks";
            this.saveMarkersButton.UseVisualStyleBackColor = true;
            this.saveMarkersButton.Click += new System.EventHandler(this.saveMarkersButton_Click);
            // 
            // bakButton
            // 
            this.bakButton.Location = new System.Drawing.Point(10, 48);
            this.bakButton.Name = "bakButton";
            this.bakButton.Size = new System.Drawing.Size(58, 23);
            this.bakButton.TabIndex = 13;
            this.bakButton.Text = "Bak";
            this.bakButton.UseVisualStyleBackColor = true;
            this.bakButton.Click += new System.EventHandler(this.bakButton_Click);
            // 
            // fwdButton
            // 
            this.fwdButton.Location = new System.Drawing.Point(76, 48);
            this.fwdButton.Name = "fwdButton";
            this.fwdButton.Size = new System.Drawing.Size(58, 23);
            this.fwdButton.TabIndex = 12;
            this.fwdButton.Text = "Fwd";
            this.fwdButton.UseVisualStyleBackColor = true;
            this.fwdButton.Click += new System.EventHandler(this.fwdButton_Click);
            // 
            // playImageButton
            // 
            this.playImageButton.AutoSize = true;
            this.playImageButton.Checked = true;
            this.playImageButton.Location = new System.Drawing.Point(7, 255);
            this.playImageButton.Name = "playImageButton";
            this.playImageButton.Size = new System.Drawing.Size(107, 17);
            this.playImageButton.TabIndex = 11;
            this.playImageButton.TabStop = true;
            this.playImageButton.Text = "Render Image";
            this.playImageButton.UseVisualStyleBackColor = true;
            this.playImageButton.CheckedChanged += new System.EventHandler(this.renderMode_CheckedChanged);
            // 
            // SaveFrameButton
            // 
            this.SaveFrameButton.Location = new System.Drawing.Point(28, 77);
            this.SaveFrameButton.Name = "SaveFrameButton";
            this.SaveFrameButton.Size = new System.Drawing.Size(87, 23);
            this.SaveFrameButton.TabIndex = 10;
            this.SaveFrameButton.Text = "Save Frame";
            this.SaveFrameButton.UseVisualStyleBackColor = true;
            this.SaveFrameButton.Click += new System.EventHandler(this.SaveFrameButton_Click);
            // 
            // SkipUnmatchedCheck
            // 
            this.SkipUnmatchedCheck.AutoSize = true;
            this.SkipUnmatchedCheck.Location = new System.Drawing.Point(7, 300);
            this.SkipUnmatchedCheck.Name = "SkipUnmatchedCheck";
            this.SkipUnmatchedCheck.Size = new System.Drawing.Size(118, 17);
            this.SkipUnmatchedCheck.TabIndex = 9;
            this.SkipUnmatchedCheck.Text = "Skip unmatched";
            this.SkipUnmatchedCheck.UseVisualStyleBackColor = true;
            this.SkipUnmatchedCheck.CheckedChanged += new System.EventHandler(this.DisgardUnmatchedCheck_CheckedChanged);
            // 
            // playBayerButton
            // 
            this.playBayerButton.AutoSize = true;
            this.playBayerButton.Location = new System.Drawing.Point(7, 233);
            this.playBayerButton.Name = "playBayerButton";
            this.playBayerButton.Size = new System.Drawing.Size(104, 17);
            this.playBayerButton.TabIndex = 4;
            this.playBayerButton.Text = "Render Bayer";
            this.playBayerButton.UseVisualStyleBackColor = true;
            this.playBayerButton.CheckedChanged += new System.EventHandler(this.renderMode_CheckedChanged);
            // 
            // playMaskButton
            // 
            this.playMaskButton.AutoSize = true;
            this.playMaskButton.Location = new System.Drawing.Point(7, 209);
            this.playMaskButton.Name = "playMaskButton";
            this.playMaskButton.Size = new System.Drawing.Size(99, 17);
            this.playMaskButton.TabIndex = 3;
            this.playMaskButton.Text = "Render Mask";
            this.playMaskButton.UseVisualStyleBackColor = true;
            this.playMaskButton.CheckedChanged += new System.EventHandler(this.renderMode_CheckedChanged);
            // 
            // playBmpButton
            // 
            this.playBmpButton.AutoSize = true;
            this.playBmpButton.Enabled = false;
            this.playBmpButton.Location = new System.Drawing.Point(7, 278);
            this.playBmpButton.Name = "playBmpButton";
            this.playBmpButton.Size = new System.Drawing.Size(88, 17);
            this.playBmpButton.TabIndex = 2;
            this.playBmpButton.Text = "Full frames";
            this.playBmpButton.UseVisualStyleBackColor = true;
            // 
            // playMapButton
            // 
            this.playMapButton.AutoSize = true;
            this.playMapButton.Location = new System.Drawing.Point(7, 186);
            this.playMapButton.Name = "playMapButton";
            this.playMapButton.Size = new System.Drawing.Size(93, 17);
            this.playMapButton.TabIndex = 1;
            this.playMapButton.Text = "Render Map";
            this.playMapButton.UseVisualStyleBackColor = true;
            this.playMapButton.CheckedChanged += new System.EventHandler(this.renderMode_CheckedChanged);
            // 
            // playPauseButton
            // 
            this.playPauseButton.Location = new System.Drawing.Point(28, 19);
            this.playPauseButton.Name = "playPauseButton";
            this.playPauseButton.Size = new System.Drawing.Size(87, 23);
            this.playPauseButton.TabIndex = 0;
            this.playPauseButton.Text = "Play";
            this.playPauseButton.UseVisualStyleBackColor = true;
            this.playPauseButton.Click += new System.EventHandler(this.playPauseButton_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.button2);
            this.groupBox4.Controls.Add(this.quitFrameAquisitionButton);
            this.groupBox4.Controls.Add(this.CheckSyncButton);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.sortFilesButton);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Location = new System.Drawing.Point(3, 807);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(147, 246);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Status";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(17, 194);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(87, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // quitFrameAquisitionButton
            // 
            this.quitFrameAquisitionButton.Location = new System.Drawing.Point(23, 111);
            this.quitFrameAquisitionButton.Name = "quitFrameAquisitionButton";
            this.quitFrameAquisitionButton.Size = new System.Drawing.Size(87, 23);
            this.quitFrameAquisitionButton.TabIndex = 16;
            this.quitFrameAquisitionButton.Text = "cancel";
            this.quitFrameAquisitionButton.UseVisualStyleBackColor = true;
            this.quitFrameAquisitionButton.Click += new System.EventHandler(this.CalibrateButton_Click);
            // 
            // CheckSyncButton
            // 
            this.CheckSyncButton.Location = new System.Drawing.Point(22, 140);
            this.CheckSyncButton.Name = "CheckSyncButton";
            this.CheckSyncButton.Size = new System.Drawing.Size(82, 48);
            this.CheckSyncButton.TabIndex = 15;
            this.CheckSyncButton.Text = "Check sync";
            this.CheckSyncButton.UseVisualStyleBackColor = true;
            this.CheckSyncButton.Click += new System.EventHandler(this.CheckSyncButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "label2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "label1";
            // 
            // sortFilesButton
            // 
            this.sortFilesButton.Location = new System.Drawing.Point(23, 32);
            this.sortFilesButton.Name = "sortFilesButton";
            this.sortFilesButton.Size = new System.Drawing.Size(87, 23);
            this.sortFilesButton.TabIndex = 12;
            this.sortFilesButton.Text = "SortFiles";
            this.sortFilesButton.UseVisualStyleBackColor = true;
            this.sortFilesButton.Click += new System.EventHandler(this.sortFilesButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(23, 82);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(87, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "fudge Open";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(181, 600);
            this.panel1.TabIndex = 1;
            // 
            // frameTimer
            // 
            this.frameTimer.Interval = 50;
            // 
            // birdConfirmPanel
            // 
            this.birdConfirmPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.birdConfirmPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.birdConfirmPanel.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birdConfirmPanel.Location = new System.Drawing.Point(181, 0);
            this.birdConfirmPanel.Margin = new System.Windows.Forms.Padding(12, 9, 12, 9);
            this.birdConfirmPanel.Name = "birdConfirmPanel";
            this.birdConfirmPanel.Size = new System.Drawing.Size(941, 600);
            this.birdConfirmPanel.TabIndex = 3;
            // 
            // graphicsOptionsBox1
            // 
            this.graphicsOptionsBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.graphicsOptionsBox1.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            this.graphicsOptionsBox1.Location = new System.Drawing.Point(3, 172);
            this.graphicsOptionsBox1.Name = "graphicsOptionsBox1";
            this.graphicsOptionsBox1.Size = new System.Drawing.Size(150, 167);
            this.graphicsOptionsBox1.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            this.graphicsOptionsBox1.TabIndex = 15;
            // 
            // calibrationPanel
            // 
            this.calibrationPanel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.calibrationPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calibrationPanel.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.calibrationPanel.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            this.calibrationPanel.Location = new System.Drawing.Point(181, 0);
            this.calibrationPanel.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.calibrationPanel.Name = "calibrationPanel";
            this.calibrationPanel.Size = new System.Drawing.Size(941, 600);
            this.calibrationPanel.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            this.calibrationPanel.TabIndex = 4;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 600);
            this.Controls.Add(this.calibrationPanel);
            this.Controls.Add(this.birdConfirmPanel);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Name = "mainForm";
            this.Text = "Collision Monitoring";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_Closing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mainForm_FormClosed);
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.main_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.main_KeyPress);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bayerUpDown)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button openFolderButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button openFilesButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Timer frameTimer;
        private System.Windows.Forms.Button playPauseButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ProgressBar birdSearchProgressBar;
        private System.Windows.Forms.CheckBox SkipUnmatchedCheck;
        private System.Windows.Forms.RadioButton playBmpButton;
        private System.Windows.Forms.RadioButton playMapButton;
        private System.Windows.Forms.Button sortFilesButton;
        private System.Windows.Forms.Button birdSearchButton;
        private System.Windows.Forms.RadioButton playBayerButton;
        private System.Windows.Forms.RadioButton playMaskButton;
        private System.Windows.Forms.Label fileFinderlabel;
        private System.Windows.Forms.Button fileSearchCancelButton;
        private System.Windows.Forms.Button birdSearchCancelButton;
        private System.Windows.Forms.Button CheckSyncButton;
        private System.Windows.Forms.Button SaveFrameButton;
        private System.Windows.Forms.RadioButton playImageButton;
        private System.Windows.Forms.Button fwdButton;
        private System.Windows.Forms.Button bakButton;
        private System.Windows.Forms.Button saveMarkersButton;
        private System.Windows.Forms.Button clearMarksButton;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.NumericUpDown bayerUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button quitFrameAquisitionButton;
        private System.Windows.Forms.ProgressBar aquisitionProgressBar;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private BirdConfirmPanel birdConfirmPanel;
        private GraphicsOptionsBox graphicsOptionsBox1;
        private CalibrationPanel calibrationPanel;

    }
}

