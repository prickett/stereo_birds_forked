﻿namespace Createc.StereoBirds.Viewer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Createc.StereoBirds.Viewer.MapFileAcquisition mapFileAcquisition1 = new Createc.StereoBirds.Viewer.MapFileAcquisition();
            this.playBackPanel = new Createc.StereoBirds.Viewer.PlayBackPanel();
            this.calibrationPanel = new Createc.StereoBirds.Viewer.CalibrationPanel();
            this.loadFilesPanel = new Createc.StereoBirds.Viewer.LoadFilesPanel();
            this.confirmCalibrationPanel = new Createc.StereoBirds.Viewer.VerifyCalibrationPanel();
            this.birdConfirmPanel = new Createc.StereoBirds.Viewer.BirdConfirmPanel();
            this.SuspendLayout();
            // 
            // playBackPanel
            // 
            this.playBackPanel.CalibrationSet = null;
            this.playBackPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playBackPanel.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
            this.playBackPanel.Location = new System.Drawing.Point(0, 0);
            this.playBackPanel.MapFileAquisition = null;
            this.playBackPanel.Margin = new System.Windows.Forms.Padding(4);
            this.playBackPanel.Name = "playBackPanel";
            this.playBackPanel.Size = new System.Drawing.Size(976, 370);
            this.playBackPanel.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            this.playBackPanel.TabIndex = 5;
            // 
            // calibrationPanel
            // 
            this.calibrationPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calibrationPanel.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bilinear;
            this.calibrationPanel.Location = new System.Drawing.Point(0, 0);
            this.calibrationPanel.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.calibrationPanel.Name = "calibrationPanel";
            this.calibrationPanel.Size = new System.Drawing.Size(976, 370);
            this.calibrationPanel.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            this.calibrationPanel.TabIndex = 3;
            this.calibrationPanel.OnCalibrationCancel += new Createc.StereoBirds.Viewer.CalibrationPanel.OnCalibrationCancelHandler(this.calibrationPanel_OnCalibrationCancel);
            this.calibrationPanel.OnCalibration += new Createc.StereoBirds.Viewer.CalibrationPanel.OnCalibrationHandler(this.calibrationPanel_OnCalibration);
            // 
            // loadFilesPanel
            // 
            this.loadFilesPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.loadFilesPanel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadFilesPanel.Location = new System.Drawing.Point(0, 0);
            this.loadFilesPanel.MapFileAcquisition = mapFileAcquisition1;
            this.loadFilesPanel.Margin = new System.Windows.Forms.Padding(4);
            this.loadFilesPanel.MinimumSize = new System.Drawing.Size(172, 158);
            this.loadFilesPanel.Name = "loadFilesPanel";
            this.loadFilesPanel.Size = new System.Drawing.Size(976, 370);
            this.loadFilesPanel.TabIndex = 4;
            // 
            // confirmCalibrationPanel
            // 
            this.confirmCalibrationPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.confirmCalibrationPanel.Location = new System.Drawing.Point(0, 0);
            this.confirmCalibrationPanel.Margin = new System.Windows.Forms.Padding(4);
            this.confirmCalibrationPanel.Name = "confirmCalibrationPanel";
            this.confirmCalibrationPanel.Size = new System.Drawing.Size(976, 370);
            this.confirmCalibrationPanel.TabIndex = 2;
            this.confirmCalibrationPanel.OnCalibrationPass += new Createc.StereoBirds.Viewer.VerifyCalibrationPanel.OnCalibrationPassHandler(this.confirmCalibrationPanel_OnCalibrationPass);
            this.confirmCalibrationPanel.OnCalibrationFail += new Createc.StereoBirds.Viewer.VerifyCalibrationPanel.OnCalibrationFailHandler(this.confirmCalibrationPanel_OnCalibrationFail);
            // 
            // birdConfirmPanel
            // 
            this.birdConfirmPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.birdConfirmPanel.Location = new System.Drawing.Point(0, 0);
            this.birdConfirmPanel.Margin = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.birdConfirmPanel.Name = "birdConfirmPanel";
            this.birdConfirmPanel.Size = new System.Drawing.Size(976, 370);
            this.birdConfirmPanel.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(976, 370);
            this.Controls.Add(this.playBackPanel);
            this.Controls.Add(this.calibrationPanel);
            this.Controls.Add(this.loadFilesPanel);
            this.Controls.Add(this.confirmCalibrationPanel);
            this.Controls.Add(this.birdConfirmPanel);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.ResumeLayout(false);

        }

        #endregion

        private BirdConfirmPanel birdConfirmPanel;
        private VerifyCalibrationPanel confirmCalibrationPanel;
        private CalibrationPanel calibrationPanel;
        private LoadFilesPanel loadFilesPanel;
        private PlayBackPanel playBackPanel;




    }
}