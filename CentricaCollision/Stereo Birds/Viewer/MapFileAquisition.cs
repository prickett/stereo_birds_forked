﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading;
using System.IO;
using Createc.StereoBirds;

namespace Createc.StereoBirds.Viewer
{   
    public class MapFileAcquisition
    {
        public delegate void OnFrameBatchHandler(object sender, OnFrameBatchEventArgs e);
        public event OnFrameBatchHandler OnFrameBatch;

        public delegate void OnProgressChangeHandler(object sender,OnProgressChangeEventArgs e);
        public event OnProgressChangeHandler OnProgressChange;

        public delegate void OnFinishHandler(object sender, EventArgs e);
        public event OnFinishHandler OnFinish;

        SynchronizationContext syncContext;
        Thread thread;
        int chunkCount;
        int chunksDone;
        volatile bool quit;
        MapFilePairer pairer;

        FramePairList mapFrames = new FramePairList();
        FramePairList fullFrames = new FramePairList();

        public FramePairList MapFrames { get { return mapFrames; } }
        public FramePairList FullFrames { get { return fullFrames; } }
        public int MapFrameCount { get { return mapFrames.Count; } }
        public int FullFrameCount { get { return fullFrames.Count; } }
        public bool Busy { get; private set; }

        public MapFileAcquisition()
        {
            quit = false;
            pairer = new MapFilePairer();
            var sc = SynchronizationContext.Current;
            syncContext = sc != null ? sc : new SynchronizationContext();
        }

        public bool LoadFromFolder(FolderSearchParameters p)
        {
            if (!Busy)
            {
                Busy = true;
                thread = new Thread(LoadFromFolderThread);
                thread.Start(p);
                return true;
            }
            return false;
        }

        public bool LoadFromFiles(string[] paths)
        {
            if (!Busy)
            {
                Busy = true;
                thread = new Thread(LoadFromFilesThread);
                thread.Start(paths);
                return true;
            }
            return false;
        }
 
        public void Quit()
        {
            quit = true;
            while (Busy)
            {
                Thread.Sleep(0);
                System.Windows.Forms.Application.DoEvents();
            }
            quit = false;
        }


        void LoadFromFilesThread(object args)
        {
            chunkCount = 1;
            chunksDone = 0;

            // hack, if using new style files ToFrameRefArray returns null and adds the pairs directly
            var frames = ToFrameRefArray((string[])args);

            var masters = frames.Where(f => f.IsMaster).ToArray();
            var slaves = frames.Where(f => f.IsSlave).ToArray();

            AddToPairer(masters, slaves);
      
            Busy = false;
            syncContext.Post(ContextRaiseOnFinish, null);
        }



        void LoadFromFolderThread(object args)
        {
            var p = (FolderSearchParameters)args;
            syncContext.Post(ContextRaiseOnProgressChange, 0.01f); //GetDirectories can take some time...

            var folders = new List<string>();
            using (var e = Directory.EnumerateDirectories(p.RootFolder, p.FolderSearchPattern, p.SearchOption).GetEnumerator())
            {
                while (!quit && e.MoveNext())
                    folders.Add(e.Current);
            }

            chunkCount = (folders.Count + 1) * 2 + 1;
            chunksDone = 1;

            if (!quit)//get all files (if any) from the root folder     
                Acquire(p.RootFolder, p.MasterFrameSearchPattern, p.SlaveFrameSearchPattern);

            //get all files (if any) from each sub folder of the root folder
            for (int i = 0; !quit && i < folders.Count; i++)
                Acquire(folders[i], p.MasterFrameSearchPattern, p.SlaveFrameSearchPattern);

            //pairer.DebugRemainder();

            Busy = false;
            syncContext.Post(ContextRaiseOnFinish, null);
        }


        void Acquire(string folder, string masterPat, string slavePat)
        {
            Console.Write("{0}\n",folder);
            string[] masters = null, slaves = null;
            try
            {
                masters = Directory.GetFiles(folder, masterPat);
                slaves = Directory.GetFiles(folder, slavePat);
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            if (masters == null || slaves == null)
                return;

            AddToPairer(ToFrameRefArray(masters), ToFrameRefArray(slaves));           
        }




        FrameRef[] ToFrameRefArray(string[] paths)
        {
            var frames = paths.Select(p => new FrameRef(p)).ToArray();               
                //.Where(f => f.Type == FrameType.Map).ToArray();

            float done = (float)chunksDone / chunkCount;
            float frac = 1f / (frames.Length * chunkCount);
            int count = 0;
            bool old_style_files = false;

            //FrameRef pairedFrames[];

            using (var mapFile = new MapFile())
            using (var chunkFile = new ChunkFile())
            {
                foreach (var frame in frames)
                { 
                    if(frame.Type == FrameType.Multiframe) //new style file
                    {
                        if(chunkFile.Open(frame.Path))
                        {
                            var pairedMaps = chunkFile.GetFramePairs();
                            mapFrames.FilterAndAdd(ref pairedMaps);

                            var pairedFullFrames = new List<FramePair>() { chunkFile.GetFullFramePair() };
                            fullFrames.FilterAndAdd(ref pairedFullFrames);

                            if (pairedMaps.Count > 0 && !quit)
                            {
                                var args = new OnFrameBatchEventArgs(pairedMaps, pairedFullFrames);
                                syncContext.Post(ContextRaiseOnFrameBatch, args);
                            }
                            chunkFile.Close();
                        }
                        count++;
                        syncContext.Post(ContextRaiseOnProgressChange, done + count * frac);
                    }
                    else //old style files
                    {
                        old_style_files = true;
                        if (frame.Type == FrameType.Map)
                        {
                            mapFile.Open(frame);
                            frame.CameraTick = mapFile.Header.CameraTick;
                            mapFile.Close();
                        }

                        if ((count++ & 31) == 0)
                        {
                            syncContext.Post(ContextRaiseOnProgressChange, done + count * frac);
                        }
                   }



                    if (quit) break;
                }
            }

            //Console.WriteLine("got {0} frames from {1} paths", frames.Length, paths.Length);

            if (!quit)
            {
                chunksDone++;
                syncContext.Post(ContextRaiseOnProgressChange, (float)chunksDone / chunkCount);
            }

            return old_style_files ? 
                frames.Where(f => f.Type != FrameType.Multiframe).ToArray() 
                : new FrameRef[0];
            //return frames;
        }

        void AddToPairer(FrameRef[] masters, FrameRef[] slaves)
        {
            pairer.Add(masters, slaves);

            var pairedFullFrames = pairer.GetPairedFullFrames();
            fullFrames.FilterAndAdd(ref pairedFullFrames);

            var pairedMaps = pairer.GetPairedFrames();
            mapFrames.FilterAndAdd(ref pairedMaps);

            if (pairedMaps.Count > 0  && !quit)
            {
                var args = new OnFrameBatchEventArgs(pairedMaps,pairedFullFrames);
                syncContext.Post(ContextRaiseOnFrameBatch,args);
            }
        }

        #region sync context callbacks

        void ContextRaiseOnFinish(object param)
        {
            var handler = OnFinish;
            if (handler != null)
            {
                handler(this, new EventArgs());
            }
        }

        void ContextRaiseOnFrameBatch(object param)
        {
            var handler = OnFrameBatch;
            if (handler != null)
            {
                handler(this, (OnFrameBatchEventArgs)param);
            }
        }

        void ContextRaiseOnProgressChange(object param)
        {
            var handler = OnProgressChange;
            if (handler != null)
            {
                int done =  (int)((float)param * 100f);
                var args = new OnProgressChangeEventArgs(done);
                handler(this, args);
            }
        }

        #endregion

    }

    class MapFilePairer
    {
        class SequenceIterator
        {
            const ulong MAXDIF = 400;
            const long MAXDROP = 4;

            int mStart, sStart;
            int mIndex, sIndex;
            ulong mOld, sOld;
            ulong error=0;
            int count=0;

            List<FrameRef> masters;
            List<FrameRef> slaves;

            public int MasterIndex { get { return mIndex; } }
            public int SlaveIndex { get { return sIndex; } }
            public FramePair Current { get; private set; }

            public int Count { get { return count; } }
            public ulong Error { get { return count > 1 ? error / (ulong)(count - 1) : ulong.MaxValue; } }

            public SequenceIterator(List<FrameRef> masters, List<FrameRef> slaves, int masterIndex, int slaveIndex)
            {
                this.masters = masters;
                this.slaves = slaves;
                mStart = mIndex = masterIndex;
                sStart= sIndex = slaveIndex;

                if (mIndex < masters.Count && sIndex < slaves.Count)
                {
                    mOld = masters[mIndex].CameraTick;
                    sOld = slaves[sIndex].CameraTick;
                }
            }

            public bool Next()
            {
                int mCt = masters.Count;
                int sCt = slaves.Count;
                ulong mDif, sDif, mTic, sTic;

                if (mIndex < mCt && sIndex < sCt)
                {
                    mDif = getTick(masters[mIndex], ref mOld, out mTic);
                    sDif = getTick(slaves[sIndex], ref sOld, out sTic);

                    uint drop;
                    for (drop = 1; drop < MAXDROP; drop++) //allow for MAXDROP dropped frames 
                    {
                        if ((sDif - mDif) > MAXDIF * drop && ++mIndex < mCt) //common case
                        {
                            mDif = getTick(masters[mIndex], ref mOld, out mTic);
                        }
                        else if ((mDif - sDif) > MAXDIF * drop && ++sIndex < sCt) //rare case
                        {
                            sDif = getTick(slaves[sIndex], ref sOld, out sTic);
                        }
                        else //all good exit
                        {
                            break;
                        }
                    }

                    ulong dif = (mDif - sDif) / drop;

     
                    if (dif * dif < MAXDIF * MAXDIF  && mIndex < mCt && sIndex < sCt)
                    {
                        count++;
                        error += dif*dif;
                        Current = new FramePair(masters[mIndex++], slaves[sIndex++]);
                        mOld = mTic;
                        sOld = sTic;
                        return true;
                    }
                }
                return false;
            }

            /// <summary>
            /// remove all paired frames 
            /// </summary>
            public void TrimPaired()
            {
                if (mStart!=mIndex)
                {
                    masters.RemoveRange(mStart, mIndex - mStart);
                    slaves.RemoveRange(sStart, sIndex - sStart);
                    mIndex = mStart;
                    sIndex = sStart;
                }
            }

            static ulong getTick(FrameRef frame, ref ulong previousTick, out ulong tick)
            {
                tick = frame.CameraTick;
                if (previousTick > tick) //handle overflow
                    previousTick -= 0x100000000;
                return tick - previousTick;
            }
        }

        List<FrameRef> masterMaps;
        List<FrameRef> slaveMaps;
        //List<FramePair> pairList;
        //List<FramePair> ffPairList;
        List<FrameRef> masterFullFrames;
        List<FrameRef> slaveFullFrames;

        public MapFilePairer()
        {
            Clear();
        }

        //public int PairedCount
        //{
        //    get { return pairList.Count; }
        //}

        //public int UnpairedCount
        //{
        //    get { return masterMaps.Count + slaveMaps.Count; }
        //}

        public void Add(FrameRef[] masters, FrameRef[] slaves)
        {
            AddFrames(masterMaps,masterFullFrames, masters);
            AddFrames(slaveMaps, slaveFullFrames, slaves);
        }

        public List<FramePair> GetPairedFrames()
        {
            var pairs = new List<FramePair>();
            int mi = 0, si = 0;

            while (Sync(masterMaps,slaveMaps, ref mi, ref si))
            {
                PairUpRun(masterMaps, slaveMaps, ref mi, ref si, pairs);
            }

            return pairs;
        }


        public List<FramePair> GetPairedFullFrames()
        {
            var pairs = new List<FramePair>();
            int mi = 0, mo = 0;
            int si = 0, so = 0;
            var m = new List<FrameRef>();
            var s = new List<FrameRef>();

            while (RoughSync(masterFullFrames, slaveFullFrames, ref mi, ref si))
            {
                if (si > so && masterFullFrames[mi].CompareTo(slaveFullFrames[si - 1]) == 0)
                    si--;

                if (mi - mo > 1)
                     m.AddRange(masterFullFrames.GetRange(mo, mi - mo));

                if (si - so > 1)
                    s.AddRange(slaveFullFrames.GetRange(so, si - so));

                pairs.Add(new FramePair(masterFullFrames[mi], slaveFullFrames[si]));

                if (si + 1 < slaveFullFrames.Count && masterFullFrames[mi].CompareTo(slaveFullFrames[si + 1]) == 0)
                    si++;

                so = ++si;
                mo = ++mi;
            }
            masterFullFrames = m; //replace with any skipped sections
            slaveFullFrames = s; //replace with any skipped sections not including any double frames
            return pairs;
        }




        public void Clear()
        {            
            masterMaps = new List<FrameRef>();
            slaveMaps = new List<FrameRef>();

            masterFullFrames = new List<FrameRef>();
            slaveFullFrames = new List<FrameRef>();
        }





        #region debugging

        public void DebugRemainder()
        {
            DebugLists();
            //Console.Write("\nMaster List\n");
            //DebugList(masterList);
            //Console.Write("\nSlave List\n");
            //DebugList(slaveList);

            //Console.WriteLine("{0} master frames remaining", masterList.Count);
            //Console.WriteLine("{0} slave frames remaining", slaveList.Count);

        }

        void DebugLists()
        {
            int mi = 0, si = 0;

            while (mi < masterMaps.Count && si < slaveMaps.Count)
            {
                int comp = masterMaps[mi].CompareTo(slaveMaps[si]);
                Console.Write("{0}\t{1}\n",
                    comp <= 0 ? masterMaps[mi++].SystemTime.ToString() : "\t\t",
                    comp >= 0 ? slaveMaps[si++].SystemTime.ToString() : "");
            }

            while (mi < masterMaps.Count)
                Console.Write("{0}\n", masterMaps[mi++].SystemTime.ToString());

            while (si < slaveMaps.Count)
                Console.Write("\t\t\t{0}\n", slaveMaps[si++].SystemTime.ToString());
        }

        //static void DebugList(List<FrameRef> frames)
        //{
        //    ulong startTime = 0, time = 0;
        //    foreach (var frame in frames)
        //    {
        //        ulong tm = frame.SystemTime.sec;
        //        Console.Write('.');
        //        if (tm - time > 2L)
        //        {                    
        //            if (time != 0)
        //            {
        //                Console.Write("{0} -> {1}\n", startTime, time,
        //                    TimeConverter.ToDateTime(startTime),
        //                    TimeConverter.ToDateTime(time));
        //            }
        //            startTime = tm;
        //        }
        //        time = tm;
        //    }
        //    if (time != 0)
        //    {
        //        Console.Write("{0} -> {1}\n", startTime, frames.Last().SystemTime,
        //            TimeConverter.ToDateTime((ulong)startTime),
        //            TimeConverter.ToDateTime((ulong)time));
        //    }
        //}

        #endregion
        #region statics

        static void AddPairs(List<FramePair> pairs,List<FramePair> addition)
        {
            if (addition == null || addition.Count == 0)
                return;

            if (pairs.Count == 0 || Compare(addition[0].Master, pairs.Last().Master) > 0) //new pairs go after
            {
                pairs.AddRange(addition);
            }
            else if (Compare(addition.Last().Master, pairs[0].Master) < 0) //new pairs go before
            {
                pairs.InsertRange(0, addition);
            }
            else //new pairs merged
            {
                //rather that Exists extension method we are using BinarySearch to take advantage of being sorted
                pairs.AddRange(addition.Where(f => pairs.BinarySearch(f) < 0));
                pairs.Sort();
            }
        }

        static void AddFrames(List<FrameRef> maps, List<FrameRef> fullframes, FrameRef[] addition)
        {
            var mapAdd = new List<FrameRef>();
            var fullFrameAdd = new List<FrameRef>();
            foreach (var frame in addition)
            {
                if (frame.Type == FrameType.Map)
                    mapAdd.Add(frame);
                else if (frame.Type == FrameType.Fullframe)
                    fullFrameAdd.Add(frame);
            }

            mapAdd.Sort(Compare);
            fullFrameAdd.Sort();

            AddFrames(maps, mapAdd);
            AddFrames(fullframes, fullFrameAdd);
        }


        static void AddFrames(List<FrameRef> list, List<FrameRef> addition)
        {
            if (addition == null || addition.Count == 0)
                 return;

            //Array.Sort(addition, Compare);

            if (list.Count == 0 || Compare(addition[0], list.Last()) > 0) //new frames go after
            {
                list.AddRange(addition);
            }
            else if (Compare(addition.Last(), list[0]) < 0) //new frames go before
            {
                list.InsertRange(0, addition);
            }
            else //new frames merged
            {                
                list.AddRange(addition.Where(f=>list.BinarySearch(f)<0));
                list.Sort(); 
            }
        }

        

        /// <summary>
        /// Synchronises frames. Returns a pair of indexes marking the start of the first synchronised run encountered equal or greater than the passed indexes.
        /// If no run is encountered one or both of the returned indexes will be out of range of the associated lists. 
        /// </summary>
        /// <param name="masters">List of master frame references to synchronise.</param>
        /// <param name="slaves">List of slave frame references to synchronise.</param>
        /// <param name="masterIndex">Master index. Passed as start of search, returns at start of run or out of range if there is no run</param>
        /// <param name="slaveIndex">Slave index. Passed as start of search, returns at start of run or out of range if there is no run</param>
        /// <returns>True if a run was found.</returns>
        static bool Sync(List<FrameRef> masters, List<FrameRef> slaves, ref int masterIndex, ref int slaveIndex)
        {
            while (RoughSync(masters, slaves, ref masterIndex, ref slaveIndex))
            {
                int m = masterIndex;
                int s = slaveIndex;
                const int DEVIATION = 8;
                const int COUNT = 5;

                if (IsPaired(masters, slaves, masterIndex, slaveIndex, COUNT))
                    return true;

                for (int i = 1; i < DEVIATION; i++)
                {
                    if (IsPaired(masters, slaves, masterIndex, slaveIndex + i, COUNT))
                    {
                        slaveIndex += i;
                        return true;
                    }
                    if (IsPaired(masters, slaves, masterIndex + i, slaveIndex, COUNT))
                    {
                        masterIndex += i;
                        return true;
                    }
                }

                masterIndex++;
            }

            return false;
        }

        /// <summary>
        /// Approximatly synchronises frames. Returns a pair of indexes marking the first matched pair equal or greater than the passed indexes.
        /// If no match is found one or both of the returned indexes will be out of range of the associated lists. 
        /// </summary>
        /// <param name="masters">List of master frame references to match.</param>
        /// <param name="slaves">List of slave frame references to match.</param>
        /// <param name="masterIndex">Master index. Passed as start of search, returns at start of run or out of range if there is no match</param>
        /// <param name="slaveIndex">Slave index. Passed as start of search, returns at start of run or out of range if there is no match</param>
        /// <returns>True if a match was found.</returns>
        static bool RoughSync(List<FrameRef> masters, List<FrameRef> slaves, ref int masterIndex, ref int slaveIndex)
        {
            while (masterIndex < masters.Count && slaveIndex < slaves.Count)
            {
                int comp = masters[masterIndex].CompareTo(slaves[slaveIndex]);
                if (comp < 0) //master frame is earliest
                {
                     masterIndex = masters.BinarySearch(slaves[slaveIndex]);
                    if (masterIndex >= 0) //found a match
                       return true;
                    else //no matches, masterIndex set to the complement of the nearest frame after the start of the slaves
                       masterIndex = ~masterIndex; //get index from complement
                }
                else if (comp > 0) //slave frame is earliest
                {
                    slaveIndex = slaves.BinarySearch(masters[masterIndex]);
                    if (slaveIndex >= 0) //found a match
                        return true;
                    else //as above except for slaveIndex
                        slaveIndex = ~slaveIndex;
                }
                else //master and slave match
                {
                    return true;
                }
            }
            return false;
        }
            
        /// <summary>
        /// Returns true if the passed indexes mark the start of a run
        /// </summary>
        static bool IsPaired(List<FrameRef> masters, List<FrameRef> slaves, int masterIndex, int slaveIndex, int count)
        {
            var seq = new SequenceIterator(masters, slaves, masterIndex, slaveIndex);
            while (seq.Next() && 0 < --count) ;
            return count == 0 ;
        }

        /// <summary>
        /// Pairs up frames in a run and removes the run sequence from the source lists
        /// </summary>
        static void PairUpRun(List<FrameRef> masters, List<FrameRef> slaves, ref int masterIndex, ref int slaveIndex, List<FramePair> pairs)
        {
            var run = new SequenceIterator(masters, slaves, masterIndex, slaveIndex);

            while (run.Next())
                pairs.Add(run.Current);

            run.TrimPaired();
        }



        static int Compare(FrameRef a, FrameRef b)
        {
            int c = a.CompareTo(b);
            if (c == 0 && a != b)
            {
                ulong d = a.CameraTick - b.CameraTick;

                //if (d > 0x40000000)
                //    d -= 0x100000000;
                //else if (-d > 0x40000000)
                //    d += 0x100000000;

                return (int)d;
            }
            return c;
        }




        #endregion





    }

    public class OnFrameBatchEventArgs : EventArgs
    {
        public OnFrameBatchEventArgs(List<FramePair> mapFrames, List<FramePair> fullFrames)
        {
            MapFrames = mapFrames;
            FullFrames = fullFrames;
        }
        public List<FramePair> MapFrames { get; private set; }
        public List<FramePair> FullFrames { get; private set; }
    }

    public class OnProgressChangeEventArgs : EventArgs
    {
        public OnProgressChangeEventArgs(int percentDone) { PercentDone = percentDone; }
        public int PercentDone { get; private set; }
    }

    public class FolderSearchParameters
    {
        public FolderSearchParameters(
            string folder,
            string folderPattern = "_*",
            string masterPattern = "M*.*",
            string slavePattern = "S*.*",
            SearchOption searchOption = SearchOption.AllDirectories)
        {
            RootFolder = folder;
            FolderSearchPattern = folderPattern;
            MasterFrameSearchPattern = masterPattern;
            SlaveFrameSearchPattern = slavePattern;
            SearchOption = searchOption;
        }
        public string RootFolder { get; private set; }
        public string FolderSearchPattern { get; private set; }
        public string MasterFrameSearchPattern { get; private set; }
        public string SlaveFrameSearchPattern { get; private set; }
        public SearchOption SearchOption { get; private set; }
    }
}
