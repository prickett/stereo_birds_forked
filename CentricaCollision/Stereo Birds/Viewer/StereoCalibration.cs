﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Createc.StereoBirds;
using Createc.Net.Rotation;
using Createc.Net.Vector;
using Createc.Net.Imaging;


namespace Createc.StereoBirds.Viewer
{


    [Serializable]
    public class StereoCalibration
    {
        //TimeStamp systemTime;
        double aor;
        Coord2d masterCentre;
        Coord2d slaveCentre;
        Vector3d slaveOffset;
        Matrix3x3 toSlaveMatrix;
        [NonSerialized()]
        Matrix3x3 toMasterMatrix;
        double error;


        internal StereoCalibration(TimeStamp systemTime)
        {
            //this.systemTime = systemTime;
            SystemTime = systemTime;
        }

        public StereoCalibration()
        {

        }

        public StereoCalibration(TimeStamp systemTime, int width, int height, Vector3d slaveOffset)//Coord2d[] masterPoints, Coord2d[] slavePoints)
        {
            //this.systemTime = systemTime;
            SystemTime = systemTime;
            this.slaveOffset = slaveOffset;
            masterCentre = slaveCentre = new Coord2d(width * 0.5, height * 0.5); 
            aor = (Math.PI / Math.Sqrt(width*width + height * height)) * .955;
        }

        public void Calibrate(List<CoordPair> points)
        {
            Console.WriteLine(" ({0:f3},{1:f3})", masterCentre.X, masterCentre.Y);
            toSlaveMatrix = FindCentre(ref masterCentre, ref slaveCentre, aor, points);
            toMasterMatrix = toSlaveMatrix.Transpose_();
            Console.Write(" ({0:f3},{1:f3}) - ", masterCentre.X, masterCentre.Y);
            error = CalibrationError(masterCentre, slaveCentre, aor, points);
            Console.WriteLine(" ({0:f3},{1:f3})\terror {2:f3}", masterCentre.X, masterCentre.Y, error);

        }

        public TimeStamp SystemTime { get; set; }
        public double AngleOfResolution { get { return aor; } set { aor = value; } }
        public Coord2d MasterCentre { get { return masterCentre; } set { masterCentre = value; } }
        public Coord2d SlaveCentre { get { return slaveCentre; } set { slaveCentre = value; } }
        public Vector3d SlaveOffset { get { return slaveOffset; } set { slaveOffset = value; } }
        public Matrix3x3 Matrix { get { return toSlaveMatrix; } set {toSlaveMatrix = value;} }
        public double Error { get { return error; } set { error = value; } }
        
        [OnDeserialized()]
        internal void OnDeserializedMethod(StreamingContext context)
        {
            toMasterMatrix = toSlaveMatrix.Transpose_();
        }

       









        public Coord2d ToSlavePt(Coord2d masterPt)
        {
            var q = pixel2normal(masterPt, masterCentre, aor);
            return normal2pixel(toSlaveMatrix.Hit(q), slaveCentre, aor);
        }
        public Coord2d ToMasterPt(Coord2d slavePt)
        {
            var q = pixel2normal(slavePt, slaveCentre, aor);
            return normal2pixel(toMasterMatrix.Hit(q), masterCentre, aor);
        }

        public void Debug()
        {
            Console.Write(" SystemTime: {0}:{1}\n", SystemTime.sec, SystemTime.nsec);
            Console.Write(" AOR:        {0}\n", aor);
            Console.Write(" MCentre:    ({0:f3} x {1:f3})\n",masterCentre.X,masterCentre.Y);
            Console.Write(" SCentre:    ({0:f3} x {1:f3})\n",slaveCentre.X,slaveCentre.Y);
            Console.Write(" SOffset:    ({0:f3}, {1:f3}, {2:f3})\n",slaveOffset.x,slaveOffset.y,slaveOffset.z);
            Console.Write(" Error:      {0}\n", error);
            toSlaveMatrix.Debug();  
        }


        //As Alan and John have said, your three-dimensional lines may not intersect. In general you can find the two points, 
        //A0 and B0, on the respective lines A1A2 and B1B2 which are closest together and determine if they coincide or else
        //see how far apart they lie.

        //The following is a specific formula using matlab for determining these closest points A0 and B0 in terms of vector
        //cross products and dot products. Assume that all four points are represented by three-element column vectors or by
        //three-element row vectors. Do the following:

        //nA = dot(cross(B2-B1,A1-B1),cross(A2-A1,B2-B1));
        //nB = dot(cross(A2-A1,A1-B1),cross(A2-A1,B2-B1));
        //d = dot(cross(A2-A1,B2-B1),cross(A2-A1,B2-B1));
        //A0 = A1 + (nA/d)*(A2-A1);
        //B0 = B1 + (nB/d)*(B2-B1);

        //As Alan mentioned, it is evident by its nature that A0 = A1+t*(A2-A1) with t = nA/d a scalar, must fall on the line
        //A1A2, and similarly for B0 = B1+s*(B2-B1) with scalar s = nB/d. You can easily verify that vector A0-B0 is orthogonal
        //to both A2-A1 and B2-B1, which establishes that A0 and B0 are the closest possible pair of points on the two lines. 
        //This can be done by evaluating dot(A0-B0,A2-A1) and dot(A0-B0,B2-B1) for various random four-point sets and showing
        //that each dot product is zero, allowing of course for very small round-off errors.

        //The one case where the above code becomes indeterminate occurs when the two lines are parallel, in which case there
        //are infinitely many possible pairs for A0 and B0. An arbitrary plane orthogonal to the lines will cut them in a valid
        //pair A0 and B0. For such a case you will get a zero divided by zero situation in the above code which produces a NaN.


        public Vector3d LineInterSection3D(Coord2d master, Coord2d slave, out double fit)
        {
            //offset *= -1.0;

            var mv = pixel2normal(master, masterCentre,aor) * 1000.0;
            var sv = toMasterMatrix.Hit(pixel2normal(slave, slaveCentre,aor) * 1000.0).Subtract(slaveOffset);
            var ms = slaveOffset * -1.0;
            var x = mv.Cross(sv);

            //offset *= -1.0;

            double m = mv.Cross(ms).Dot(x);
            double s = sv.Cross(ms).Dot(x);
            double d = mv.Cross(sv).Dot(x);

            var mi = mv * (m / d);
            var si = slaveOffset.Add(sv * (s / d));

            fit = mi.Subtract(si).DistanceSquared();
            return mi.Add(si) * 0.5;
        }


        static void LineIntersect3D(Vector3d a1, Vector3d a2, Vector3d b1, Vector3d b2, out Vector3d a0, out Vector3d b0)
        {
            a2 = a2.Subtract(a1);
            b2 = b2.Subtract(b1);
            var ab = a1.Subtract(b1);
            var x = a2.Cross(b2);

            var na = b2.Cross(ab).Dot(x);
            var nb = a2.Cross(ab).Dot(x);
            var d = a2.Cross(b2).Dot(x);

            a0 = a1.Add(a2 * (na / d));
            b0 = b1.Add(b2 * (nb / d));
        }




        #region statics

        static Vector3d pixel2normal(Coord2d src, Coord2d centre, double aor)
        {
            double x, y, z, d;

            x = src.X - centre.X;
            y = src.Y - centre.Y;

            d = Math.Sqrt(x * x + y * y);
            if (d != 0)
            {
                var m = Math.Sin(d * aor) / d;
                z = Math.Cos(d * aor);
                x *= m;
                y *= m;
            }
            else
                z = 1;

            return new Vector3d(x, y, z);
        }

        static private Coord2d normal2pixel(Vector3d src, Coord2d centre, double aor)
        {
            double d = src.x * src.x + src.y * src.y;
            if (d != 0)
            {
                d = Math.Acos(src.z) / (Math.Sqrt(d) * aor);
            }
            return new Coord2d(
                centre.X + src.x * d,
                centre.Y + src.y * d);
        }

        //static Matrix3x3 FindRotation(Coord2d a, Coord2d b, double aor, List<CoordPair> points)
        //{
        //    int count = points.Count;
        //    Matrix3x3 matrix;
        //    if (count < 3)
        //        throw new ArgumentException("Calibration requires at least three points from each frame");

        //    float[] masterPts = new float[3 * count];
        //    float[] slavePts = new float[3 * count];

        //    //Convert the passed points to 3d vectors and add to each
        //    for (int i = 0; i < count; i++)
        //    {
        //        Vector3d v;

        //        v = pixel2normal(points[i].Master, a, aor);
        //        int j = i * 3;
        //        masterPts[j + 0] = (float)v.x;
        //        masterPts[j + 1] = (float)v.y;
        //        masterPts[j + 2] = (float)v.z;

        //        v = pixel2normal(points[i].Slave, b, aor);
        //        slavePts[j + 0] = (float)v.x;
        //        slavePts[j + 1] = (float)v.y;
        //        slavePts[j + 2] = (float)v.z;
        //    }

        //    // make our call to openCV SVD
        //    matrix = Unmanaged.FindRotation(masterPts, slavePts);

        //    return matrix;
        //}

        static Matrix3x3 FindRotation(Coord2d a, Coord2d b, double aor, List<CoordPair> points)
        {
            int count = points.Count;
            Matrix3x3 matrix;
            if (count < 3)
                throw new ArgumentException("Calibration requires at least three points from each frame");

            double[] masterPts = new double[3 * count];
            double[] slavePts = new double[3 * count];

            //Convert the passed points to 3d vectors and add to each
            for (int i = 0; i < count; i++)
            {
                Vector3d v;

                v = pixel2normal(points[i].Master, a, aor);
                int j = i * 3;
                masterPts[j + 0] = v.x;
                masterPts[j + 1] = v.y;
                masterPts[j + 2] = v.z;

                v = pixel2normal(points[i].Slave, b, aor);
                slavePts[j + 0] = v.x;
                slavePts[j + 1] = v.y;
                slavePts[j + 2] = v.z;
            }

            // make our call to openCV SVD
            matrix = Unmanaged.FindRotation(masterPts, slavePts);

            Console.WriteLine("dll\n{0}, {1}, {2},\n{3}, {4}, {5},\n{6}, {7}, {8}",
                matrix.m11, matrix.m12, matrix.m13,
                matrix.m21, matrix.m22, matrix.m23,
                matrix.m31, matrix.m32, matrix.m33);
 
            return matrix;
        }

        static Matrix3x3 FindCentre(ref Coord2d masterCentre, ref Coord2d slaveCentre, double aor, List<CoordPair> points, double distance = 64)
        {            
            double error, min = double.MaxValue;
            Coord2d mBest = masterCentre, sBest = slaveCentre;
            Coord2d[] offsets = getOffsets();
            min = CalibrationError(mBest, sBest, aor, points);

            do
            {
                Coord2d mc = mBest;
                Coord2d sc = sBest;
                bool found = false;
                foreach (var offset in offsets)
                {
                    double x = offset.X * distance, y = offset.Y * distance;
                    Coord2d gmc = new Coord2d(mc.X - x, mc.Y - y);
                    Coord2d gsc = new Coord2d(sc.X + x, sc.Y + y);
                    if ((error = CalibrationError(gmc, gsc, aor, points)) < min)
                    {
                        min = error;
                        mBest = gmc;
                        sBest = gsc;
                        found = true;
                    }
                }

                if (!found)
                    distance *= 0.5;

            } while (distance > 0.01);


            masterCentre = mBest;
            slaveCentre = sBest;
            return FindRotation(masterCentre, slaveCentre, aor, points);
        }

        static double CalibrationError(Coord2d a, Coord2d b,double aor, List<CoordPair> points)
        {
            //double e =  RotationError(FindRotation(a,b,aor, points), a,b,aor, points);
            //Console.WriteLine("e:{0}", e);
            return RotationError(FindRotation(a, b, aor, points), a, b, aor, points); 
        }

        static double RotationError(Matrix3x3 matrix, Coord2d a, Coord2d b, double aor, List<CoordPair> points)
        {
            if (points == null || points.Count == 0)
                return double.PositiveInfinity;
            double error = 0;
            foreach (var p in points)
            {
                var v = pixel2normal(p.Master, a,aor);
                var t = normal2pixel(matrix.Hit(v), b,aor);
                double dx = t.X - p.Slave.X;
                double dy = t.Y - p.Slave.Y;
                error += dx * dx + dy * dy;
            }
            return error / points.Count;
        }


        static Coord2d[] getOffsets()
        {
            double h = Math.Sqrt(0.75);
            return new Coord2d[] {           
                new Coord2d(1,0),               
                new Coord2d(0.5,h),               
                new Coord2d(-0.5,h),               
                new Coord2d(-1,0),               
                new Coord2d(-0.5,-h),               
                new Coord2d(0.5,-h)
            };
        }

        #endregion

    }

    static class Matrix3x3Extension
    {
        public static double Determinate(this Matrix3x3 m)
        {
            return
                (m.m11 * m.m22 * m.m33) +
                (m.m21 * m.m32 * m.m13) +
                (m.m31 * m.m12 * m.m23) -
                (m.m31 * m.m22 * m.m13) -
                (m.m21 * m.m12 * m.m33) -
                (m.m11 * m.m32 * m.m23);
        }

        public static Matrix3x3 Inverse(this Matrix3x3 m)
        {
            return new Matrix3x3(
                m.m22 * m.m33 - m.m23 * m.m32,
                m.m13 * m.m32 - m.m12 * m.m33,
                m.m12 * m.m23 - m.m13 * m.m22,
                m.m23 * m.m31 - m.m21 * m.m33,
                m.m11 * m.m33 - m.m13 * m.m31,
                m.m13 * m.m21 - m.m11 * m.m23,
                m.m21 * m.m32 - m.m22 * m.m31,
                m.m12 * m.m31 - m.m11 * m.m32,
                m.m11 * m.m22 - m.m12 * m.m21)
                * (1.0 / Determinate(m));
        }

        public static Matrix3x3 Transpose_(this Matrix3x3 m)
        {
            return new Matrix3x3(
                m.m11, m.m21, m.m31,
                m.m12, m.m22, m.m32,
                m.m13, m.m23, m.m33);
        }

        public static void Debug(this Matrix3x3 m)
        {
            Console.Write("{0:f5}\t{1:f5}\t{2:f5}\n{3:f5}\t{4:f5}\t{5:f5}\n{6:f5}\t{7:f5}\t{8:f5}\n",
                m.m11, m.m21, m.m31, m.m12, m.m22, m.m32, m.m13, m.m23, m.m33);
        }


    }

    public static class Vector3dExtension
    {
        public static Vector3d Subtract(this Vector3d a, Vector3d b)
        {
            return new Vector3d(a.x - b.x, a.y - b.y, a.z - b.z);
        }
        public static Vector3d Add(this Vector3d a, Vector3d b)
        {
            return new Vector3d(a.x + b.x, a.y + b.y, a.z + b.z);
        }
    }



    
}
