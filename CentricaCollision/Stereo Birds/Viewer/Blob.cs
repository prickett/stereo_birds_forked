using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Createc.Imaging.Thresholds
{
    public class Blob
    {
        public readonly static Blob Empty = new Blob(); 
        private int xMin;
        private int xMax;
        private int yMin;
        private int yMax;
        private int xSum;
        private int ySum;
        private int area;

        public Blob()
        {
            init();
        }

        public Blob(Blob other)
        {
            xMin = other.xMin;
            xMax = other.xMax;
            yMin = other.yMin;
            yMax = other.yMax;
            xSum = other.xSum;
            ySum = other.ySum;
            area = other.area;
        }

        public void Add(int y, int xStart, int xStop)
        {
            if (y < yMin)
                yMin = y;

            if (y > yMax)
                yMax = y;

            if (xStart < xMin)
                xMin = xStart;

            if (xStop > xMax)
                xMax = xStop;

            int w = xStop - xStart;
            ySum += w * y;
            xSum += w * xStart + ((w * (w - 1)) >> 1);
            area += w;
        }
        
        public void Absorb(Blob other)
        {
            if (other == this||other.area == 0)
                return;

            if (other.yMin < yMin)
                yMin = other.yMin;

            if (other.yMax > yMax)
                yMax = other.yMax;

            if (other.xMin < xMin)
                xMin = other.xMin;

            if (other.xMax > xMax)
                xMax = other.xMax;

            ySum += other.ySum;
            xSum += other.xSum;
            area += other.area;

            other.init();
        }

        public void Offset(int x, int y)
        {
            xMin += x;
            xMax += x;
            yMin += y;
            yMax += y;
            xSum += area * x;
            ySum += area * y;
        }


        private void init()
        {
            xMin = int.MaxValue;
            yMin = int.MaxValue;
            xMax = 0;
            yMax = 0;
            xSum = 0;
            ySum = 0;
            area = 0;
        }



        public int X
        {
            get { return xMin; }
        }
        public int Y
        {
            get { return yMin; }
        }
        public int XMax
        {
            get { return xMax; }
        }
        public int YMax
        {
            get { return yMax; }
        }
        public int Width
        {
            get { return xMax - xMin; }
        }
        public int Height
        {
            get { return yMax - yMin + 1; }
        }
        public int Area
        {
            get { return area; }
        }
        public float CX
        {
            get { return (float)xSum / area; }
        }
        public float CY
        {
            get { return (float)ySum / area; }
        }
        public PointF Center
        {
            get { return new PointF(CX, CY); }
        }
        public Rectangle Bounds
        {
            get { return new Rectangle(xMin, yMin, Width, Height); }
        }
    }
}
