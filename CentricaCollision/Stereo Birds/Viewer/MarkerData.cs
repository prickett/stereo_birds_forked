﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using CustomControls;

using Createc.Net.Vector;
using Createc.Net.Imaging;
using Createc.Imaging.Thresholds;
using Createc.StereoBirds;
using System.IO;

namespace Createc.StereoBirds.Viewer
{
    public class Marker
    {
        public long Id { get; set; }
        public Coord2d Location { get; set; }
        public Marker(long id, Coord2d location)
        {
            Id = id;
            Location = location;
        }
    }

    public class MarkerData
    {


        private const int RADIUS = 8;
        private long id;
        private List<Coord2d> currentMarkers;
        private FramePair currentFramePair;        
        private SortedDictionary<FramePair, List<Coord2d>> currentDictionary;
        private SortedDictionary<FramePair, List<Marker>> markerDictionary;
        private Color[] palette;

        public MarkerData()
        {
            id = 0;
            currentMarkers = null;
            currentDictionary = new SortedDictionary<FramePair, List<Coord2d>>();
            markerDictionary = new SortedDictionary<FramePair, List<Marker> >();
            palette = new Color[]{
                Color.Orange,     
                Color.Yellow,
                Color.Green,
                Color.Cyan,
                Color.Blue,
                Color.Magenta};
            Clear();
        }

        public List<Coord2d> CurrentMarkers { get { return currentMarkers; } } 

        public void MoveMarker(int index, float x, float y)
        {
            if (currentMarkers != null && index < currentMarkers.Count)
            {
                var p = currentMarkers[index];
                currentMarkers[index] = new Coord2d(p.X + x, p.Y + y);
            }
        }

        public int GrabIndex { get; set; }
        public Point GrabPoint { get; set; }
        public int HighLightIndex { get; set; }

        private double SquareDistance(Coord2d a, Coord2d b)
        {
            double x = a.X - b.X;
            double y = a.Y - b.Y;
            return x * x + y * y;
        }
        
        public void Clear()
        {
            currentDictionary.Clear();
            if (currentMarkers != null)
            {
                currentMarkers.Clear(); 
            }
            HighLightIndex = -1;
            GrabIndex = -1;
        }

        public void Commit()
        {
            int max_count = 0;
            foreach (var kvp in currentDictionary)
            {
                List<Marker> markers = null;
                if (!markerDictionary.TryGetValue(kvp.Key, out markers))
                {
                    markers = new List<Marker>();
                    markerDictionary.Add(kvp.Key, markers);
                }
                else if(markers == null)
                {
                    Console.WriteLine("Null markers");
                    break;
                }

                max_count = Math.Max(max_count, kvp.Value.Count);
                for (int i = 0; i < kvp.Value.Count; ++i )
                {
                    markers.Add(new Marker(id + i, kvp.Value[i]));
                }
            }
            id += max_count;
            Console.WriteLine("id is now {0}", id);
            currentDictionary.Clear();
            currentMarkers = null;
            HighLightIndex = -1;
            GrabIndex = -1;

        }

        public void DeleteMarker(int index)
        {
            if (currentMarkers != null && index >= 0 && index < currentMarkers.Count)
            {
                currentMarkers.RemoveAt(index);
            }
        }

        public int HitTest(Coord2d location, float distance = RADIUS)
        {
            int index = -1;
            if (currentMarkers != null)
            {
                float min = distance * distance;
                int i = 0;
                //Console.Write("Hit distance squared {0}\n", min);
                foreach (var pt in currentMarkers)
                {
                    if (SquareDistance(location, pt) < min)
                    {
                        index = i;
                        //Console.Write(".");
                        break;
                    }
                    i++;
                }
            }
            return index;
        }

        public void Render(ZoomBox view, Graphics graphics)
        {    
            const int R = RADIUS;          
            const int D = R * 2;       

            if (currentMarkers != null)
            { 

                int i = 0;
                var font = SystemFonts.DefaultFont;

                foreach (var pt in currentMarkers)
                {
                    var p = view.ToControlSpace(new PointF((float)pt.X,(float)pt.Y));
                    float w = i != HighLightIndex ? 1f : 2f;
                    var colour = palette[i % palette.Length];
                    i++;
                    using (var pen = new Pen(colour, w))
                    {
                        graphics.DrawEllipse(pen, p.X - R, p.Y - R, D, D);
                        graphics.DrawLine(pen, p.X - R, p.Y - R, p.X + R, p.Y + R);
                        graphics.DrawLine(pen, p.X - R, p.Y + R, p.X + R, p.Y - R);
                    }
                    using (var brush = new SolidBrush(colour))
                    {
                        graphics.DrawString(i.ToString(), font, brush, p.X + R, p.Y + R);
                    }
                }
            }

            List<Marker> markers = null;
            markerDictionary.TryGetValue(currentFramePair, out markers);

            if(markers!=null)
            {
                var pen = new Pen(Color.Red, 1);
                var brush = new SolidBrush(Color.Red);
                var font = SystemFonts.DefaultFont;
              
                foreach (var m in markers)
                {
                    PointF pt = new PointF((float)m.Location.X, (float)m.Location.Y);
                    var p = view.ToControlSpace(pt);               
                    graphics.DrawEllipse(pen, p.X - R, p.Y - R, D, D);
                    graphics.DrawLine(pen, p.X - R, p.Y, p.X + R, p.Y);
                    graphics.DrawLine(pen, p.X, p.Y + R, p.X , p.Y - R);
                    graphics.DrawString(m.Id.ToString(), font, brush, p.X + R, p.Y + R);                   
                }

                pen.Dispose();
                brush.Dispose();                
            }
        }


        public int Add(double x, double y)
        {
            return Add(new Coord2d(x,y));
        }

        public int Add(Coord2d point)
        {
            int index = -1;
            if( currentMarkers == null )
            {
                currentMarkers = new List<Coord2d>(0);
                currentDictionary.Add(currentFramePair, currentMarkers);
            }
            index = currentMarkers.Count;
            currentMarkers.Add(point);

            return index;
        }

        public FramePair FrameReference
        {
            get
            {
                return currentFramePair;
            }
            set
            {
                currentFramePair = value;                
                currentDictionary.TryGetValue(currentFramePair, out currentMarkers);
                HighLightIndex = -1;
                GrabIndex = -1;

                //List<Coord2d> markers = null;
                //if (!markerDictionary.TryGetValue(value, out markers))
                //{
                //    markers = new List<Coord2d>(0);
                //    markerDictionary.Add(value, markers);
                //}


                //currentMarkers = markers;
            }
        }


        public static void SaveMarkerData(string path, MarkerData master, MarkerData slave, StereoCalibrationSet calibrationSet)
        {
            using (var sw = new StreamWriter(path))
            {
                SaveMarkerData(sw, master, slave,calibrationSet);
            }
        }


        public static void SaveMarkerData(StreamWriter writer, MarkerData master, MarkerData slave, StereoCalibrationSet calibrationSet)
        {

            //FramePair[] slavePairs = slave.markerDictionary.Keys.ToArray();
            foreach (var kvp in master.markerDictionary)
            {
                FramePair pair = kvp.Key;
                List<Marker> master_markers = kvp.Value;
                List<Marker> slave_markers = null;
                slave.markerDictionary.TryGetValue(kvp.Key, out slave_markers);
                if(slave_markers != null)
                {
                    var calib = calibrationSet.FindNearest(kvp.Key.SystemTime);
                    int ct = Math.Min(master_markers.Count, slave_markers.Count);

                    if (ct > 0 && calib != null)
                    {
                        writer.WriteLine(string.Format("\"{0}\\{1}\", {2}.{3:000}, {4}, {5}",
                            Path.GetDirectoryName(pair.Master.Path),
                            Path.GetFileName(pair.Master.Path),
                            pair.SystemTime.sec,
                            pair.SystemTime.nsec / 1000000,
                            pair.FrameNumber,
                            ct));

                        for (int i = 0; i < ct; i++)
                        { 
                            double fit;
                            var v = calib.LineInterSection3D(master_markers[i].Location, slave_markers[i].Location, out fit);
                            writer.Write(string.Format("{0}, ", master_markers[i].Id));
                            writer.Write(string.Format("{{{0},{1}}} ,{{{2},{3}}}",
                                    (float)master_markers[i].Location.X,
                                    (float)master_markers[i].Location.Y,
                                    (float)slave_markers[i].Location.X,
                                    (float)slave_markers[i].Location.Y));                           
                            writer.Write(string.Format(", {{{0},{1},{2}}}, {3}",
                                (float)v.x, (float)v.y, (float)v.z, fit)); 
                            writer.WriteLine();
                        }

                       
                    }
                }



                //int ct = Math.Max(master_markers.Count, slave_markers.Count);

                //var calib = calibrationSet.FindNearest(kvp.Key.SystemTime);

                //if (ct > 0)
                //{
                //    writer.WriteLine(string.Format("\"{0}\\{1}\", {2}.{3:000}, {4}, {5}",
                //        Path.GetDirectoryName(pair.Master.Path),
                //        Path.GetFileName( pair.Master.Path),
                //        pair.SystemTime.sec,
                //        pair.SystemTime.nsec/1000000,
                //        pair.FrameNumber,
                //        ct));


                //    for (int i = 0; i < ct; i++)
                //    {
                //        writer.Write(string.Format("{0}, ", i ));
                //        writer.Write(string.Format("{{{0},{1}}} ,{{{2},{3}}}",
                //                (float)masterPts[i].X,
                //                (float)masterPts[i].Y,
                //                (float)slavePts[i].X,
                //                (float)slavePts[i].Y));

                //        double fit;
                //        if (calib != null)
                //        {
                //            var v = calib.LineInterSection3D(masterPts[i], slavePts[i], out fit);
                //            writer.Write(string.Format(", {{{0},{1},{2}}}",
                //                (float)v.x, (float)v.y, (float)v.z));
                //         }               

                //        writer.WriteLine();                  
                //    }
                //}
                // }

            }


        }


    }




}
