﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


using System.IO;
using System.Threading;
using CustomControls;

using Createc.Net.Imaging;
using Createc.Imaging.Thresholds;
using Createc.StereoBirds;

namespace Createc.StereoBirds.Viewer
{
    public partial class MainForm : Form
    {
        MapFileRenderer renderer;
        StereoCalibrationSet calibrationSet;
        MapFileBirdSearch birdSearch;
        MapFileAcquisition aquisition;
        Queue<FramePair> calibrationFrames;

        Pair<Createc.Net.Imaging.Image> currentImagePair;
        FramePair currentFramePair;

        //bool isCalibrating = false;

        #region initialisation and cleanup

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            calibrationSet = UserData.LoadCalibrationSet();
            calibrationFrames = new Queue<FramePair>();
            calibrationSet.Debug();

            aquisition = loadFilesPanel.MapFileAcquisition;

            playBackPanel.MapFileAquisition = aquisition;
            playBackPanel.CalibrationSet = calibrationSet; 

            birdSearch = new MapFileBirdSearch(calibrationSet, loadFilesPanel.MapFileAcquisition);
            //birdSearch.OnBirdPossible += OnBirdPossible;
            //birdSearch.OnProgressChange += OnBirdSearchProgressChange;
            //birdSearch.OnFinish += OnBirdSearchFinish;
            //birdSearch.OnRequestCalibration += OnRequestCalibration;
            //birdSearch.OnConfirmCalibration += OnConfirmCalibration;

            renderer = new MapFileRenderer();
            //renderer = new MapFileRenderer(MapFileRenderer.RenderModes.Bayer);

            aquisition.OnFrameBatch += OnAquisitionBatch;
            aquisition.OnFrameBatch += playBackPanel.OnAquisition;
            

            Show(loadFilesPanel);
            //Show(calibrationPanel);
            //Show(confirmCalibrationPanel);
            //Show(playBackPanel);
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            aquisition.OnFrameBatch -= OnAquisitionBatch;
            aquisition.OnFrameBatch -= playBackPanel.OnAquisition; 
            UserData.SaveCalibrationSet(calibrationSet);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (birdSearch.Busy || aquisition.Busy)
            {
                var result = MessageBox.Show("The application is busy.\nDo you want to force quit?",
                    "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

                if (result == DialogResult.OK)
                {
                    aquisition.Quit(); //close any active threads
                    birdSearch.Quit();
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        #endregion

        #region aquisition

        void OnAquisitionBatch(object sender, OnFrameBatchEventArgs e)
        {
            Console.WriteLine("frame batch ({0} {1}) ({2} {3})",
                e.MapFrames.Count, 
                e.FullFrames.Count,
                aquisition.MapFrameCount,
                aquisition.FullFrameCount );

            foreach (var fr in e.FullFrames)
            {
                Console.WriteLine("AquisitionBatch {0}",fr.Type);                
                calibrationFrames.Enqueue(fr); 
            }

            if(!calibrationPanel.Visible && !confirmCalibrationPanel.Visible)
            {
                Console.WriteLine("try next calibration...");
                TryNextCalibration();
            }
        }

        bool TryNextCalibration()
        {
            bool ret = false;
            Console.WriteLine("calibration frames {0}", calibrationFrames.Count);
            if (calibrationFrames.Count > 0)
            {
                currentFramePair = calibrationFrames.Dequeue();
                currentImagePair = renderer.CreateImagePair(currentFramePair);

                Console.WriteLine(currentFramePair.Master.Path);

                var set = calibrationSet.FindNearest(currentFramePair.SystemTime);
                if (set != null)
                {
                    confirmCalibrationPanel.Confirm(set, currentImagePair);
                    Show(confirmCalibrationPanel);
                }
                else
                {
                    calibrationPanel.CalibrateImagePair(currentImagePair, currentFramePair);
                    Show(calibrationPanel);
                }
                ret = true;
            }
            return ret;
        }

        #endregion

        #region keyboard

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            bool handled = true;
            switch (e.KeyCode)
            {                
                case Keys.F1:
                    Show(calibrationPanel);                    
                    break;
                case Keys.F2:
                    Show(playBackPanel);
                    break;
                case Keys.F3:
                    Show(confirmCalibrationPanel);
                    break;
                case Keys.F4:
                    Show(loadFilesPanel);
                    break;
                case Keys.F5:
                    Show(birdConfirmPanel);
                    break;
                //case Keys.F12:
                //    doStuffHackThing();                    
                //    break;
                //case Keys.F11:
                //    doThingHackStuff();
                //    break;
                default:
                    handled = false;
                    break;
            }
            e.Handled = handled;
        }

        /// <summary>
        /// Saves out all full frames (legacy debug) 
        /// </summary>
        void SaveFullFramesAsBmp()
        {

            Console.WriteLine("Got {0} full frames :)", aquisition.FullFrameCount);
            string targetFolder = Utility.GetFolderFromDialog("Select folder to save full frames",true);
            var ffl =aquisition.FullFrames;
            ffl.Reset();
            int count =0;
            
            while(ffl.MoveNext())
            {
                var fp = ffl.Current;
                string format = string.Format("{0}\\{1}_{2}_{3:D4}.bmp" ,targetFolder,fp.SystemTime,"{0}",count);
                File.Move(fp.Master.Path,string.Format(format, "M"));
                File.Move(fp.Slave.Path, string.Format(format, "S"));
                count++;
            }

        }

        /// <summary>
        /// Saves out all full frames (legacy debug) 
        /// </summary>
        void SaveFullFramesAsPng()
        {
            string sourceFolder = Utility.GetFolderFromDialog("Select full frame folder");
            if(sourceFolder==null)
                return;

            string targetFolder = Utility.GetFolderFromDialog("Select folder to save finished frames", true);            
            if(targetFolder==null)
                return;

            var files = Directory.GetFiles(sourceFolder, "*.bmp");
            Array.Sort(files);

            var backBmp = new Bitmap(1920, 1080, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            var g = Graphics.FromImage(backBmp);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            var renderer = new MapFileRenderer();

            var mr = new Rectangle(160, 0, 720, 1080);
            var sr = new Rectangle(1040, 0, 720, 1080);

            for (int i = 0; i < files.Length; i+=2)
            {
                var m = Path.GetFileNameWithoutExtension(files[i]);
                var s = Path.GetFileNameWithoutExtension(files[i+1]);
                if (m.IndexOf('M') >= 0 && s.IndexOf('S') >= 0)
                {
                    var mim = renderer.FullFrameImage(new FrameRef(files[i]));
                    var sim = renderer.FullFrameImage(new FrameRef(files[i + 1]));
                    renderImage(g, mim, mr);
                    renderImage(g, sim, sr);
                    backBmp.Save(
                        targetFolder + string.Format("\\Sept2012_{0:D4}.png", i >> 1)
                        , System.Drawing.Imaging.ImageFormat.Png);
                    Console.WriteLine("{0}x{1}", sim.Width, sim.Height);
                }
                else
                {
                    Console.WriteLine("error saving PNG");
                }
 
            }

        }

        void renderImage(Graphics g, Createc.Net.Imaging.Image im, Rectangle rect)
        {
            var typ = System.Runtime.InteropServices.GCHandleType.Pinned;
            var him = System.Runtime.InteropServices.GCHandle.Alloc(im.Buffer,typ);
            var fmt = System.Drawing.Imaging.PixelFormat.Format32bppRgb;//.Format24bppRgb; 
            using(var bmp = new Bitmap(im.Width,im.Height,im.Stride,fmt,him.AddrOfPinnedObject()))
            {
                g.DrawImage(bmp, rect);
            }
            him.Free();
        }

        private void Show(UserControl panel)
        {
            foreach (var ctrl in Controls)
            {
                var uc = (UserControl)ctrl;
                uc.Visible = panel == uc;
                //Console.WriteLine("{0}", uc.ToString());
            }
            //calibrationPanel.Visible = panel is CalibrationPanel;
            //confirmCalibrationPanel.Visible = panel is VerifyCalibrationPanel;
            //birdConfirmPanel.Visible = panel is BirdConfirmPanel;
            //loadFilesPanel.Visible = panel is LoadFilesPanel;
        }


        #endregion

        #region bird search

        private void birdSearchCancelButton_Click(object sender, EventArgs e)
        {
            birdSearch.Quit();
        }

        private void OnBirdSearchFinish(object sender, EventArgs e)
        {
            Console.WriteLine("finished birdsearch");
            //aquisitionProgressBar.Value = 0;
            //SetAquisitionButtons(false);
        }

        private void OnBirdSearchProgressChange(object sender, OnProgressChangeEventArgs e)
        {
            //if (!birdConfirmPanel.Visible)
                //Show(birdConfirmPanel);
            //birdSearchProgressBar.Value = Math.Min(100, e.PercentDone);
        }

        private void OnBirdPossible(object sender, OnBirdPossibleEventArgs e)
        {

            //fileFinderlabel.Text = string.Format("{0} pairs processed", aquisition.PairedCount);
            //Console.WriteLine("gui got batch {0}", e.FrameBatch.Count());
        }


        void OnConfirmCalibration(object sender, OnConfirmCalibrationArgs e)
        {
        }

        void OnRequestCalibration(object sender, OnRequestCalibrationArgs e)
        {
            Console.WriteLine("Calibration Request for {0}", e.FullFramePair.SystemTime);
            calibrationPanel.Render(e.FullFramePair);
            Show(calibrationPanel);
        }

        private void birdSearchButton_Click(object sender, EventArgs e)
        {
            //var b = birdSearchButton;
            //var bp = birdSearch.GetNextBirdPossible();
            //if (bp != null)
            //{
            //    renderFramePair(bp.FramePair);
            //    var mm = calibrationPanel.MasterMarkers;
            //    var sm = calibrationPanel.SlaveMarkers;
            //    double h = calibrationPanel.MasterView.Image.Height;
            //    foreach (var p in bp.Hits)
            //    {
            //        mm.Add(new Coord2d(p.Master.Y, h - p.Master.X));
            //        sm.Add(new Coord2d(p.Slave.Y, h - p.Slave.X));
            //    }
            //    calibrationPanel.Invalidate();
            //}
        }

        #endregion

        private void calibrationPanel_OnCalibration(object sender, OnCalibrationArgs e)
        {
            confirmCalibrationPanel.Confirm(e.Calibration, currentImagePair);
            Show(confirmCalibrationPanel);
        }

        private void calibrationPanel_OnCalibrationCancel(object sender, EventArgs e)
        {
            if (!TryNextCalibration())
                Show(playBackPanel);
        }

        private void confirmCalibrationPanel_OnCalibrationFail(object sender, OnCalibrationArgs e)
        {
            calibrationPanel.CalibrateImagePair(currentImagePair, currentFramePair);
            Show(calibrationPanel);
        }

        private void confirmCalibrationPanel_OnCalibrationPass(object sender, OnCalibrationArgs e)
        {            
            calibrationSet.Add(e.Calibration);
            if (!TryNextCalibration())
                Show(playBackPanel);
        }


  
    }
}
