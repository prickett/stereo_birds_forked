﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using CustomControls;

namespace Createc.StereoBirds.Viewer
{
    public partial class GraphicsOptionsBox : UserControl
    {
        public delegate void OnInterpolationModeChangeHandler(object sender, OnInterpolationModeChangeEventArgs e);
        public event OnInterpolationModeChangeHandler OnInterpolationModeChange;

        public delegate void OnSmoothingModeChangeHandler(object sender, OnSmoothingModeChangeEventArgs e);
        public event OnSmoothingModeChangeHandler OnSmoothingModeChange;

        InterpolationMode interpolation;
        SmoothingMode smoothing;

        public GraphicsOptionsBox()
        {
            InitializeComponent();

            //use the option button tags to store the associated values
            nearestNeighbourButton.Tag = InterpolationMode.NearestNeighbor;
            bilinearButton.Tag = InterpolationMode.Bilinear;
            bicubicButton.Tag = InterpolationMode.Bicubic;
            highQualityBicubicButton.Tag = InterpolationMode.HighQualityBilinear;
            highQualityBilinearButton.Tag = InterpolationMode.HighQualityBicubic;

        }


        public GraphicsOptionsBox(InterpolationMode interpolationMode, SmoothingMode smoothingMode)
            : this()
        {
            InterpolationMode = interpolationMode;
            SmoothingMode = smoothingMode;
        }


        public InterpolationMode InterpolationMode
        {
            get { return interpolation; }
            set
            {
                switch (value)
                {
                    case InterpolationMode.High:
                        value = InterpolationMode.Bicubic;
                        goto case InterpolationMode.Bicubic;
                    case InterpolationMode.Bilinear:
                        bilinearButton.Checked = true;
                        break;
                    case InterpolationMode.Bicubic:
                        bicubicButton.Checked = true;
                        break;
                    case InterpolationMode.NearestNeighbor:
                        nearestNeighbourButton.Checked = true;
                        break;
                    case InterpolationMode.HighQualityBilinear:
                        highQualityBilinearButton.Checked = true;
                        break;
                    case InterpolationMode.HighQualityBicubic:
                        highQualityBicubicButton.Checked = true;
                        break;
                    default:
                        value = InterpolationMode.NearestNeighbor;
                        goto case InterpolationMode.NearestNeighbor;
                }
                interpolation = value;
                raiseOnInterpolationChange();
            }
        }

        public SmoothingMode SmoothingMode
        {
            get { return smoothing; }
            set
            {
                antiAliasCheck.Checked = value == SmoothingMode.HighQuality || value == SmoothingMode.AntiAlias;
                smoothing = value != SmoothingMode.Invalid ? value : SmoothingMode.None;
                raiseOnSmoothingChange();
            }
        }


        void option_CheckedChanged(object sender, EventArgs e)
        {
            interpolation = (InterpolationMode)((RadioButton)sender).Tag;
            raiseOnInterpolationChange();
        }

        void antiAliasCheck_CheckedChanged(object sender, EventArgs e)
        {
            smoothing = antiAliasCheck.Checked ? SmoothingMode.AntiAlias : SmoothingMode.None;
            raiseOnSmoothingChange();
        }

        void raiseOnSmoothingChange()
        {
            var handler = OnSmoothingModeChange;
            if (handler != null)
                handler(this, new OnSmoothingModeChangeEventArgs(smoothing));
        }
        void raiseOnInterpolationChange()
        {
            var handler = OnInterpolationModeChange;
            if (handler != null)
                handler(this, new OnInterpolationModeChangeEventArgs(interpolation));
        }
    }

    public class OnInterpolationModeChangeEventArgs : EventArgs
    {
        public OnInterpolationModeChangeEventArgs(InterpolationMode interpolationMode) { InterpolationMode = interpolationMode; }
        public InterpolationMode InterpolationMode { get; private set; }
    }
    public class OnSmoothingModeChangeEventArgs : EventArgs
    {
        public OnSmoothingModeChangeEventArgs(SmoothingMode smoothingMode) { SmoothingMode = smoothingMode; }
        public SmoothingMode SmoothingMode { get; private set; }
    }
}
