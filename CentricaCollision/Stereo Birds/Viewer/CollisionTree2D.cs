﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Createc.Imaging.Thresholds
{
    class CollisionTree2D<T>
    {

    }



    class QuadTreeNode<T>
    {
        private List<T> values;
        private QuadTreeNode<T>[] children;
        //private readonly Common tree;
         private int bit;
        private int min;

        public QuadTreeNode(int worldSize,int objectSize,bool foo)
        {
            //tree = new Common();

            int bit = 1;
            while (bit < worldSize)
                bit <<= 1;
        }

        private QuadTreeNode(int bit, int min)
        {
            this.bit = bit;
            this.min = min;
            if(bit>min) 
                children = new QuadTreeNode<T>[4];
        }

        public List<T> HitTest(int x, int y)
        {
            if (bit > min)
            {
                return children[getIndex(x, y)].HitTest(x, y);
            }
            else 
            {
                return values;
            }
        }

        public void Add(int ax, int ay, int bx, int by, T value)
        {
            int i = getIndex(ax, ay);
            if (bit > min & i == getIndex(bx, by))
            {
                if (children[i] == null)
                    children[i] = new QuadTreeNode<T>(bit >> 1, min);

                children[i].Add(ax, ay, bx, by, value);
            }
            else
            {
                if (values == null)
                    values = new List<T>();

                values.Add(value);
            }
        }

        public void Add(int x, int y, T value)
        {
            if (bit > min)
            {
                int i = getIndex(x, y);
                if (children[i] == null)
                    children[i] = new QuadTreeNode<T>(bit >> 1, min);

                children[i].Add(x, y, value);
            }
            else
            {
                if (values == null)
                    values = new List<T>();

                values.Add(value);
            }
        }




        private int getIndex(int x,int y)
        {
            return (((x & bit) - 1) & 1) | (((y & bit) - 1) & 2);
        }

        //private class Common
        //{
        //    public int Min;
        //}

    }






    class OneWayLinkNode<T> :IEnumerable<T>
    {
        private OneWayLinkNode<T> next;
        private T value;

        public OneWayLinkNode(T value)
        {
            this.value = value;
            next = null;
        }

        public OneWayLinkNode<T> Next 
        { 
            get { return Next; } 
        }
        public T Value
        { 
            get { return value; } 
        }

        public void Add(T value)
        {
            var node = new OneWayLinkNode<T>(value);
            node.next = next;
            next = node;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new OneWayLinkNodeEnumerator<T>(this);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new OneWayLinkNodeEnumerator<T>(this);
        }
    }

    class OneWayLinkNodeEnumerator<T> : IEnumerator<T>
    {
        private OneWayLinkNode<T> node;
        private T value;

        public OneWayLinkNodeEnumerator(OneWayLinkNode<T> node)
        {
            this.node = node;
        }
        
        public T Current
        {
            get { return value; }
        }

        public void Dispose() { }

        object System.Collections.IEnumerator.Current
        {
            get { return value; }
        }

        public bool MoveNext()
        {
            if (node != null)
            {
                value = node.Value;
                node = node.Next;
                return true;
            }
            return false;
        }

        public void Reset()
        {
            throw new NotImplementedException();
        }
    }

}
