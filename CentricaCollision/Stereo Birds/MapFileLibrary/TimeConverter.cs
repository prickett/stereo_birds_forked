﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Createc.StereoBirds
{
    public static class TimeConverter
    {
        private static readonly DateTime SYSTEMTIME0 = new DateTime(1970, 1, 1);
        public static DateTime ToDateTime(TimeStamp systemTime)
        {
            
            return SYSTEMTIME0 + TimeSpan.FromSeconds(systemTime.Seconds());
        }
        public static TimeStamp ToSystemTime(DateTime time)
        {
            TimeSpan span = time-SYSTEMTIME0;
            double tot = span.TotalSeconds;
            double secs = Math.Floor(tot);
            double nsecs = (tot-secs)*1000000000;
            return new TimeStamp((uint)secs,(uint)nsecs  );
        }
    }
}
