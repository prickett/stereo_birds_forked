﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;


namespace Createc.StereoBirds
{
    public class MapFileHeader
    {
        public const int HEADERSIZE = 68;
        public MapFileHeader(BinaryReader reader)
        {
            int pos = (int)reader.BaseStream.Position;
            if (reader.BaseStream.Length - pos > HEADERSIZE)
            {
                
                MagicNumber = reader.ReadUInt32();
                Width = reader.ReadUInt32();
                Height = reader.ReadUInt32();
                BitsPerPixel = reader.ReadUInt16();
                BayerPattern = reader.ReadUInt16();

                CameraID = reader.ReadBytes(16);//reader.ReadUInt32();
                Exposure = reader.ReadUInt32();
                FrameStatus = reader.ReadUInt32();
                CameraTick = reader.ReadUInt64();
                uint sec = reader.ReadUInt32();
                uint nsec = reader.ReadUInt32();
                SystemTime = new TimeStamp(sec,nsec);
                FrameCount = reader.ReadUInt32();
                
                MapWidth = reader.ReadUInt16();
                MapHeight = reader.ReadUInt16();
                MapUnitSize = reader.ReadInt32(); 
                MapStride = reader.ReadInt32();
                MapOffset = reader.ReadInt32() + pos;
                MapCount = reader.ReadInt32();
                DataOffset = reader.ReadInt32() + pos;
                MaskOffset = reader.ReadInt32() + pos;
            }
        }
        public uint MagicNumber { get; private set; }
        public uint Width { get; private set; }
        public uint Height { get; private set; }
        public int BitsPerPixel { get; private set; }
        public int BayerPattern { get; private set; }

        public byte[] CameraID { get; private set; }
        public uint Exposure { get; private set; }
        public uint FrameStatus { get; private set; }
        public ulong CameraTick { get; private set; }
       // public uint SystemTick { get; private set; }
        public TimeStamp SystemTime { get; private set; }
        //public ulong SystemTime { get; private set; }
        public uint FrameCount { get; private set; }
        //public int HeaderSize { get { return HEADERSIZE; } }

        public int MapWidth { get; private set; }
        public int MapHeight { get; private set; }
        public int MapUnitSize { get; private set; }
        public int MapStride { get; private set; }
        public int MapOffset { get; private set; }
        public int MapCount { get; private set; }

        public int DataOffset { get; private set; }
        public int MaskOffset { get; private set; }

   
    }
    public class MapFile : IDisposable//:IEnumerable<Location>
    {
        private const uint MAGICNUMBER = 0xC0FFEE;

        byte[] map;
        byte[] bayer;
        byte[] mask;
        int[] image;
        int[,] offsets;

        Stream stream;
        BinaryReader reader;
        MapFileHeader header;
        FrameRef frameRef;

        public bool isOpen { get { return stream != null; } }
        public MapFileHeader Header { get { return header; } }
        public FrameRef FrameReference { get { return frameRef; } }

        static readonly int[] bitCtLut;
        static MapFile()
        {
            //maskLut = buildBrightnessLUT(3f);
            bitCtLut = buildBitCountLUT();
        }

        public MapFile()
        {
        }

        public MapFile(FrameRef frameRef)
        {
            Open(frameRef);
        }

        public bool Open(FrameRef frameRef)
        {
            if (frameRef != null)
            {
                loadHeader(frameRef);
                return frameRef.Status == FrameStatus.Verified;
            }
            else
            {
                return false;
            }
        }

        private void handleLoadException(Exception e)
        {
            if (e is ArgumentException || 
                e is FileNotFoundException || 
                e is UnauthorizedAccessException  || 
                e is DirectoryNotFoundException)
            {
                frameRef.Status = FrameStatus.Bad_Path;
            }
            else if (e is IOException)
            {
                frameRef.Status = FrameStatus.Bad_File;
            }
            else
            {
                Console.WriteLine("Error loading Mapfile. {0}", e);
            }
        }
        private void loadHeader(FrameRef frameRef)
        {
            //if (!isOpen || frameRef != this.frameRef)
            //{
            Close();
            clear();

            this.frameRef=frameRef;

            try
            {
                frameRef.Status = FrameStatus.UnVerified;
                stream = new FileStream(frameRef.Path, FileMode.Open, FileAccess.Read);
                reader = new BinaryReader(stream);
                reader.BaseStream.Seek(frameRef.Offset, SeekOrigin.Begin);
                header = new MapFileHeader(reader);                
            }
            catch (Exception e)
            {
                handleLoadException(e);
            } 
            //}

            if (header != null && header.MagicNumber == MAGICNUMBER)//!!!!More checking here!!!
            {
                frameRef.Status = FrameStatus.Verified;
            }
            else if(frameRef.Status == FrameStatus.UnVerified)
            {
                frameRef.Status = FrameStatus.Bad_File;
            }
        }


        public byte[] GetMap()
        {
            if (map == null && isOpen)
            {
                reader.BaseStream.Seek(header.MapOffset, SeekOrigin.Begin);
                map = reader.ReadBytes(mapSize());
            }
            return map;
        }

        public byte[] GetBayer()
        {
            if (bayer == null && isOpen)
            {
                reader.BaseStream.Seek(header.DataOffset, SeekOrigin.Begin);
                bayer = reader.ReadBytes(bayerSize());
            }
            return bayer;
        }

        public int[] GetImage()
        {
            if (image == null && GetBayer() != null)
            {
                int alpha = 255 << 24;
                int byrW = header.MapUnitSize;
                int rgbW = byrW >> 1;
                int ct = bayer.Length>>2;
                int[] img = new int[ct];
                for (int i = 0,j=0; i < ct;)
                {
                    for (int x = i + rgbW; i < x; i++, j+=2)
                    {                        
                        int red = bayer[j];
                        int green = bayer[j + 1];
                        int blue = bayer[j + byrW];
                        green += bayer[j + byrW + 1];

                        img[i] = blue | green << 7 | red << 16 | alpha;
                    }
                    j += byrW; //skip a row
                }
                image = img;
            }
            return image;
        }

        public byte[] GetMask()
        {
            if (mask==null && isOpen)
            {
                reader.BaseStream.Seek(header.MaskOffset, SeekOrigin.Begin);
                mask = reader.ReadBytes(maskSize());
            }
            return mask;
        }

        public int[,] GetBayerOffsetsYX()
        {
            if (isOpen)
            {
                if (offsets == null)
                    offsets = buildBayerOffsetsYX();
                return offsets;
            }
            return null;
        }

        private int mapSize()
        {
            return ((header.MapWidth + 7) >> 3) * header.MapHeight;
        }
        private int bayerSize()
        { 
            return (int)(header.MaskOffset - header.DataOffset); 
        }
        private int maskSize()
        {
            return bayerSize() >> 1;
        }

        public int GetBayerRegion(ref byte[] target, int x, int y, int w, int h)
        {
            //int mSz = header.MapUnitSize;
            int tw = header.MapUnitSize;
            int th = tw;
            int TileSz = tw * th;
            int lastY = header.MapHeight - 1;

            byte[] bayer = GetBayer();
            int[,] offsets = GetBayerOffsetsYX();

            int szy = y + h;
            int szx = x + w;
            int t = 0;
            int sz = w * h * TileSz;
            int stride = tw * w;

            if (target == null || target.Length < sz)
                target = new byte[sz];
            else
                Array.Clear(target, 0, sz);

            for (int yy = y; yy < szy; yy++)
            {
                t = (yy - y) * TileSz * w;
                if (yy == lastY)
                    th = ((int)header.Height - lastY * tw);
                for (int xx = x; xx < szx; xx++, t += tw)
                {
                    int i = offsets[yy, xx];
                    if (i >= 0)
                        copyTile(bayer, i, target, t, stride, tw,th);
                }
            }
            return sz;

        }

        public void GetMaskRegion(ref byte[] target,int x,int y,int w,int h)
        {
            //int uSz = (int);
            int tw = header.MapUnitSize >> 1;
            int th = tw;
            int maskTileSz = tw * th;
            int halfHeight = (int)header.Height >> 1;
            int lastY = header.MapHeight - 1;

            byte[] mask = GetMask();
            int[,] offsets = GetBayerOffsetsYX();

            int szy = y + h;
            int szx = x + w;
            int t = 0;
            int sz=w*h*maskTileSz;
            int stride = tw*w;

            if (target==null || target.Length < sz)
                target = new byte[sz];
            else
                Array.Clear(target, 0, sz);

            for (int yy = y; yy < szy; yy++)
            {
                t = (yy - y) * maskTileSz * w;
                if (yy==lastY)
                    th = (halfHeight - lastY * tw);
                for (int xx = x; xx < szx; xx++, t += tw)
                {
                    int i = offsets[yy,xx]>>2;//divide by four
                    if (i >= 0)
                        copyTile(mask, i, target, t, stride, tw, th);
                }
            }
        }

        private static void copyTile(byte[] source, int srcOffset, byte[] target, int tgtOffset, int tgtStride, int width,int height)
        {
            if (source.Length > srcOffset + (width * height))
            {
                for (int y = 0; y < height; y++)
                {
                    Array.Copy(source, srcOffset, target, tgtOffset, width);
                    srcOffset += width;
                    tgtOffset += tgtStride;
                }
            }
        }



        //private int[,] buildBayerOffsetsYX()
        //{
        //    int usz = (int)header.MapUnitSize;
        //    //int mw = (header.MapWidth + 7) & -8;
        //    int mw = header.MapWidth;
        //    int mh = header.MapHeight;

        //    int[,] offsets = new int[mh, mw];
        //    int last = header.MapHeight - 1;
        //    int sz = usz * usz;
        //    int offset = 0;
            

        //    byte[] map = GetMap();
        //    //mw--;//adjust by 1
        //    for (int y = 0, i = 0; y < mh; y++)
        //    {

        //        if(y==last)
        //            sz=usz * mh - (int)header.Height;

        //        for (int x = 0; x < mw; x += 8, i++)
        //        {
        //            int bits = map[i];
        //            int ct = x + 8 > mw ? mw-x : 8;
        
        //            int b = 0, n = 1;
        //            while (b < ct)
        //            {
        //                if ((n & bits) != 0)
        //                {
        //                    offsets[y, x + b] = offset;
        //                    offset += sz;
        //                }
        //                else
        //                {
        //                    offsets[y, x + b] = -1;
        //                }
        //                n <<= 1;
        //                b++;
        //            }
        //        }
        //    }
            

        //    //foreach (var loc in new MapEnumerator(map, mw))
        //    //{
        //    //    int i = loc.Offset;
        //    //    offset += loc.Y < last ? sz : lsz;
        //    //    offsets[loc.Y,loc.X] = offset;
        //    //}
        //    return offsets;
        //}

        private int[,] buildBayerOffsetsYX()
        {
            int usz = (int)header.MapUnitSize;
            //int mw = (header.MapWidth + 7) & -8;
            int mw = header.MapWidth;
            int mh = header.MapHeight;

            int[,] offsets = new int[mh, mw];
            //int last = header.MapHeight - 1;
            //int sz = usz * usz;
            int offset = 0;

            //Console.WriteLine("image {0}x{1}", header.Width, header.Height);
            //Console.WriteLine("map {0}x{1}", mw, mh);
            //Console.WriteLine("scaled map {0}x{1}", mw*usz, mh*usz);
            byte[] map = GetMap();

            for (int y = 0, i = 0; y < mh; y++)
            {

                int th = (y + 1 < mh)? 
                    usz : (int)header.Height - y * usz;

                for (int x = 0; x < mw; x += 8, i++)
                {
                    int bits = map[i];
                    int ct = x + 8 > mw ? mw - x : 8;

                    int b = 0, n = 1;
                    while (b < ct)
                    {
                        if ((n & bits) != 0)
                        {
                            offsets[y, x + b] = offset;
                            offset += th*usz;
                        }
                        else
                        {
                            offsets[y, x + b] = -1;
                        }
                        n <<= 1;
                        b++;
                    }
                }
            }

            return offsets;
        }

        public int MapTileCount()
        {
            return countTiles();
        }

        private int countTiles()
        {
            GetMap();
            int ct = 0;
            foreach (byte byt in map)
                ct += bitCtLut[byt];
            return ct;
        }


 
        //keep below for debugging

        //public override string ToString()
        //{
        //    string format = "magicNumber = {0:x}\nwidth = {1}\nheight = {2}\nbpp = {3}\nbayer pattern = {4}\n";
        //    format += "gridWidth = {5}\ngridHeight = {6}\ngridSize = {7}\nGrid offset = {8}\nData Offset = {9}\nMask Offset = {10}\n";
        //    format += "Camera Id = {11}\nExposure = {12}\nframeStatus = {13}\ncameraTick = {14}\nsystemTick = {15}\nsystemTime = {16}\nFrameNumber = {17}";
        //    return string.Format(format,
        //        header.magicNumber,
        //        header.width,
        //        header.height,
        //        header.bitsPerPixel,
        //        header.bayerPattern,
        //        header.mapWidth,
        //        header.mapHeight,
        //        header.mapUnitSize,
        //        header.gridOffset,
        //        header.dataOffset,
        //        header.maskOffset,
        //        header.cameraID,
        //        header.exposure,
        //        header.frameStatus,
        //        header.cameraTick,
        //        header.systemTick,
        //        header.systemTime,
        //        header.FrameCount);
        //}

        private void clear()
        {
            map = null;
            mask = null;
            bayer = null;
            image = null;
            header = null;
            offsets = null;
        }

        public void Close()
        {
            if (reader != null)
            {
                reader.Dispose();
                reader = null;
            }

            if (stream != null)
            {
                stream.Dispose();
                stream = null;
            }
        }

   
        private static int[] buildBitCountLUT()
        {
            int[] lut = new int[256];
            for (int i = 0; i < 256; i++)
            {
                int n = i, ct = 0;
                while (n != 0)
                {
                    n &= n - 1;
                    ct++;
                }
                lut[i] = ct;
            }
            return lut;
        }


        public void Dispose()
        {
            Close();
            clear();
        }


        T ByteArrayToStructure<T>(byte[] bytes) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(),
                typeof(T));
            handle.Free();
            return stuff;
        }
    }

    struct Location
    {
        public int X { get; private set; }
        public int Y { get; private set; }
        public int Offset { get; private set; }
        public Location(int x, int y, int offset)
            :this()
        {
            X = x;
            Y = y;
            Offset = offset;
        }
    }
    [StructLayout(LayoutKind.Explicit)]
    struct FileHeaderStructure
    {
        [FieldOffset(0)]
        public uint magicNumber;
        [FieldOffset(4)]
        public uint width;
        [FieldOffset(8)]
        public uint height;
        [FieldOffset(12)]
        public ushort bitsPerPixel;
        [FieldOffset(14)]
        public ushort bayerPattern;

        [FieldOffset(16)]
        public ushort mapWidth;
        [FieldOffset(18)]
        public ushort mapHeight;
        [FieldOffset(20)]
        public uint mapUnitSize;
        [FieldOffset(24)]
        public uint gridOffset;
        [FieldOffset(28)]
        public uint dataOffset;
        [FieldOffset(32)]
        public uint maskOffset;

        [FieldOffset(36)]
        public uint cameraID;
        [FieldOffset(40)]
        public uint exposure;
        [FieldOffset(44)]
        public uint frameStatus;
        [FieldOffset(48)]
        public uint cameraTick;
        [FieldOffset(52)]
        public uint systemTick;
        [FieldOffset(56)]
        public ulong systemTime; //64bit
        [FieldOffset(64)]
        public uint FrameCount;
    }
        

    class MapEnumerator:IEnumerator<Location>,IEnumerable<Location>
    {
        int width;
        int height;

        int index;
        int y;
        int x;
        int b;
        int offset;
        int bits;
        byte[] map;

        public MapEnumerator(byte[] map, int width)
        {
            this.map = map;
            this.width = width;
            int stride = (width + 7) >> 3; //width in bytes
            height = map.Length / stride;
            //count = map.Length * 8;
            Reset();
        }
                
        public bool MoveNext()
        {
            b++;
            while (y < height)
            {
                while (b < 8 & (bits & (1 << b)) == 0)
                    b++;

                if (b < 8) //we found a bit
                {
                    offset++;
                    return true;
                }
                else
                {
                    b = 0;
                    bits = 0;
                    while (y < height & bits == 0)
                    {
                        do
                        { 
                            index++; 
                            x += 8; 
                        } while (x < width && (bits = map[index]) == 0);

                        if (bits != 0)
                            break;

                        x = 0;
                        y++;
                    }
                }
            }
            return false;
        }

        public void Reset()
        {
            x = 0;
            y = 0;
            bits = 0;
            b = -1;
            offset = -1;
            index = -1;
        }

        public Location Current
        {
            get { return new Location(x+b, y, offset); }
        }
        object System.Collections.IEnumerator.Current
        {
            get { return new Location(x+b, y, offset); }
        }

        public void Dispose(){}




        public IEnumerator<Location> GetEnumerator()
        {
            return this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this;
        }
    }



}
