﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Createc.StereoBirds
{
    public struct FramePair : IComparable<FramePair>
    {
        private FrameRef master;
        private FrameRef slave;

        public FramePair(FrameRef master, FrameRef slave)
        {
            if (master == null || slave == null)
                throw new ArgumentNullException();
            if (master.Type != slave.Type)
                throw new ArgumentException("FrameRef types must match");
            this.master = master;
            this.slave = slave;
        }

        public static readonly FramePair Empty = new FramePair();

        public bool IsPair
        {
            get { return master != null && slave != null; }
        }

        public FrameRef Master
        {
            get { return master; }
        }
        public FrameRef Slave
        {
            get { return slave; }
        }
        public TimeStamp SystemTime
        {
            get { return master != null ? master.SystemTime : slave != null ? slave.SystemTime : new TimeStamp(); }
        }
        public long FrameNumber
        {
            get { return master != null ? master.FrameNumber : slave != null ? slave.FrameNumber : 0; }
        }

        public FrameType Type
        {
            get { return master.Type; }
        }

        public int CompareTo(FramePair other)
        {
            FrameRef o = other.master != null ? other.master : other.slave;
            FrameRef t = this.master != null ? this.master : this.slave;

            if (t != null)
                return t.CompareTo(o);
            else if (o != null)
                return -o.CompareTo(t);
            else
                return 0;
        }
    }
}
