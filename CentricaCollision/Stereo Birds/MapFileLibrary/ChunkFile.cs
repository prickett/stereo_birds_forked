﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Runtime.InteropServices;


namespace Createc.StereoBirds
{
    public class ChunkFileHeader
    {
        public const int HEADERSIZE = 32;
        public ChunkFileHeader(BinaryReader reader)
        {
            if (reader.BaseStream.Length > HEADERSIZE)
            {
                MagicNumber = reader.ReadUInt32();
                StartTime = reader.ReadUInt32();
                EndTime = reader.ReadUInt32();
                FirstFrame = reader.ReadUInt32();
                LastFrame = reader.ReadUInt32();
                MasterFullFrameOffset = reader.ReadUInt32();
                MasterFullFrameSize = reader.ReadUInt32();
                SlaveFullFrameOffset = reader.ReadUInt32();
                SlaveFullFrameSize = reader.ReadUInt32();
                Count = reader.ReadUInt32();
                //Console.WriteLine("got header\n {0} {1} {2} {3} {4}", MasterFullFrameOffset, SlaveFullFrameOffset, MasterFullFrameSize, SlaveFullFrameSize, Count);
            }
        }


        public uint MagicNumber { get; private set; }
        public uint StartTime { get; private set; }
        public uint EndTime { get; private set; }
        public uint FirstFrame { get; private set; }
        public uint LastFrame { get; private set; }
        public uint MasterFullFrameOffset { get; private set; }
        public uint MasterFullFrameSize { get; private set; }
        public uint SlaveFullFrameOffset { get; private set; }
        public uint SlaveFullFrameSize { get; private set; }
        public uint Count { get; private set; }

        //public int HeaderSize { get { return HEADERSIZE; } }
    }

    public class ChunkFile : IDisposable
    {
        public const int MAGICNUMBER = 0xF00D5AC;

        Stream stream;
        BinaryReader reader;
        ChunkFileHeader header;
        string path;

        FramePair fullFrames;
        FramePair[] frames;

        public bool isOpen { get { return stream != null; } }
        public ChunkFileHeader Header { get { return header; } }
        public string Path { get { return path; } }


        public ChunkFile()
        {
        }

        public ChunkFile(string path)
        {
            Open(path);
        }



        public bool Open(string path)
        {
            MapFile file = new MapFile();

            if (path != null && loadHeader(path)
                && reader.BaseStream.Length > ChunkFileHeader.HEADERSIZE + header.Count * sizeof(uint) * 2
                && header.Count>0 ) // Note chunk will fail to load if it contains no frames
            {

                frames = new FramePair[header.Count];
                
                FrameRef master, slave;
                for (int i = 0; i < header.Count; ++i)
                {
                    master = new FrameRef(true, FrameType.Map, path.ToString(), reader.ReadUInt32());
                    slave = new FrameRef(false, FrameType.Map, path.ToString(), reader.ReadUInt32());
                    master.Confirm(file);
                    slave.Confirm(file);
                    frames[i] = new FramePair(master, slave);

                    //Console.WriteLine("{0} {1} {2} {3} {4}", frames[i].IsPair, TimeConverter.ToDateTime( frames[i].SystemTime), frames[i].Master.CameraTick, frames[i].Slave.CameraTick, frames[i].Type);
                } 

                var m = frames[0].Master;
                var s = frames[0].Slave;
                master = new FrameRef(true, m.CameraTick, m.FrameNumber, m.SystemTime, FrameType.Fullframe, path.ToString(), header.MasterFullFrameOffset, (int)header.MasterFullFrameSize);
                slave = new FrameRef(false, s.CameraTick, s.FrameNumber, s.SystemTime, FrameType.Fullframe, path.ToString(), header.SlaveFullFrameOffset, (int)header.SlaveFullFrameSize);

                fullFrames = new FramePair(master, slave);

                return true;
            }
            else
            {
                return false;
            }
        }

        private void handleLoadException(Exception e)
        {
            if (e is ArgumentException || 
                e is FileNotFoundException || 
                e is UnauthorizedAccessException  || 
                e is DirectoryNotFoundException)
            {
                //frameRef.Status = FrameStatus.Bad_Path;
            }
            else if (e is IOException)
            {
                //frameRef.Status = FrameStatus.Bad_File;
            }
            else
            {
                Console.WriteLine("Chunk file load exception: {0}",e);
            }
        }

        private bool loadHeader(string path)
        {
            Close();
            clear();

            this.path=path;

            try
            {
                stream = new FileStream(path, FileMode.Open, FileAccess.Read);
                reader = new BinaryReader(stream);
                header = new ChunkFileHeader(reader);                
            }
            catch (Exception e)
            {
                handleLoadException(e);
            }

            return (header != null && header.MagicNumber == MAGICNUMBER);

        }

        public FramePair GetFullFramePair()
        {
            return fullFrames;
        }

        public List<FramePair> GetFramePairs()
        {
            return new List<FramePair>(frames);
        }










 

        private void clear()
        {
            header = null;
            frames = null;
            //fullFrames = null;
        }
        public void Close()
        {
            if (reader != null)
            {
                reader.Dispose();
                reader = null;
            }

            if (stream != null)
            {
                stream.Dispose();
                stream = null;
            }
        }

   


        public void Dispose()
        {
            Close();
            clear();
        }


        T ByteArrayToStructure<T>(byte[] bytes) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(bytes, GCHandleType.Pinned);
            T stuff = (T)Marshal.PtrToStructure(handle.AddrOfPinnedObject(),
                typeof(T));
            handle.Free();
            return stuff;
        }
    }


   
        

 
}
