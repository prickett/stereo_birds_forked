﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;

namespace Createc.StereoBirds
{
    public class Scheduler
    {
        const int MUTEX_TIMEOUT = 3000;
        const int RESOLUTION = 200;//00;
        LinkedList<ScheduledTask> schedule;
        System.Timers.Timer timer;
        Mutex accessMutex = new Mutex();
        List<Thread> workforce = new List<Thread>();

        public Scheduler()
        {
            schedule = new LinkedList<ScheduledTask>();
            timer = new System.Timers.Timer();
            timer.Elapsed += new ElapsedEventHandler(this.OnTick);
            timer.Interval = RESOLUTION;
            timer.Enabled = true;

            var delay = TimeSpan.FromMinutes(10);
            Add(new ScheduledTask(DateTime.Now + delay, houseKeeping, delay));
        }

        public Scheduler(IEnumerable<ScheduledTask> tasks)
            : this() { Add(tasks); }

        public void ShowTasks()
        {
            Console.WriteLine("\nSchedule at {0}", DateTime.Now);
            foreach (var task in schedule)
                Console.WriteLine("{1}\t{2}\t{0}", task.Task.Method.Name, task.Due, task.Interval);
            Console.WriteLine();
        }

        private void houseKeeping()
        {
            if (Lock())
            {
                var working = new List<Thread>();
                //for (int i = workforce.Count - 1; i >= 0; i--)//can't use for each because the collection might change <-- can now using a mutex
                foreach(var worker in workforce)
                {
                    if (worker.IsAlive)
                        working.Add(worker);
                }
                workforce = working;
                Unlock();
            }
        }




        private void OnTick(object source, ElapsedEventArgs e)
        {
            if (schedule.Count != 0)
            {
                while (schedule.First.Value.Due < DateTime.Now)
                {
                    if (Lock())
                    {
                        var node = schedule.First;
                        var item = node.Value;
                        var worker = new Thread(new ThreadStart(item.Task));

                        //worker.IsAlive

                        schedule.Remove(node);
                        worker.Start();
                        workforce.Add(worker);

                        Console.WriteLine("\n{0}\t{1}", DateTime.Now, item.Task.Method.Name);

                        Unlock();

                        if (item.Reschedule())//if the task can be rescheduled...
                            Add(item); // ...it is added back to the schedule

                        
                    }
                }
            }
        }

        public void Add(ScheduledTask task)
        {
            //null tasks serve no purpose
            if (task == null)
                throw new ArgumentNullException();

            //If the task is due in the past reschedule it
            if (task.Due < DateTime.Now)
                task.Reschedule();
            //if the task due in the past can't be resheduled it will fire at on the next tick

            
            if (Lock()) //get mutex 
            {
                //start by checking if the new item should go at the start
                if (schedule.Count == 0 || task.Due <= schedule.First.Value.Due)
                {
                    schedule.AddFirst(task);
                }
                else //having checked the start search from the back without any need for boundry checks
                {    //(we have already done them)                
                    var node = schedule.Last;
                    while (task.Due < node.Value.Due)
                        node = node.Previous;
                    schedule.AddAfter(node, task);
                }
                Unlock();
            }
        }
        public void Add(IEnumerable<ScheduledTask> tasks)
        {
            foreach (var task in tasks)
                Add(task);
        }

        public void Quit()
        {
            schedule.Clear();
            foreach (var worker in workforce)
            {
                if (worker.IsAlive)
                    worker.Abort();
            }
        }



 
        private bool Lock(int timeOut = MUTEX_TIMEOUT)
        {
            bool locked = accessMutex.WaitOne(timeOut);
            if (!locked)
                Console.WriteLine("Mutex timed out after {0}ms",timeOut);
            return locked;
        }
        private void Unlock()
        {
            accessMutex.ReleaseMutex();
        }       
    }

    public class ScheduledTask
    {
        public delegate void TaskDelegate();
        //public object Param;

        public ScheduledTask(DateTime due, TaskDelegate task, TimeSpan interval)
        {
            if (task == null)
                throw new ArgumentNullException("task");

            if (interval < TimeSpan.Zero)
                throw new ArgumentOutOfRangeException("interval", "cannot be less than zero");

            //if (due < DateTime.Now && interval == TimeSpan.Zero)
            //    throw new ArgumentOutOfRangeException("due");

            Due = due;
            Task = task;
            Interval = interval;
        }

        public ScheduledTask(DateTime due, TaskDelegate task)
            : this(due, task, TimeSpan.Zero) { }

        public ScheduledTask(TaskDelegate task)
            : this(DateTime.Now, task, TimeSpan.Zero) { }

        public DateTime Due { get; private set; }
        public TaskDelegate Task { get; private set; }
        public TimeSpan Interval { get; private set; }

        public bool Reschedule()
        {
            TimeSpan dif = DateTime.Now - Due;
            var tic = Interval.Ticks;
            if (dif >= TimeSpan.Zero && tic > 0)
            {
                Due += new TimeSpan(((dif.Ticks + tic - 1) / tic) * tic);
            }
            return Interval > TimeSpan.Zero;
        }
    }
}

