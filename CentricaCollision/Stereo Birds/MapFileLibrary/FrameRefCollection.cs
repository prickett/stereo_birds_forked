﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Createc.StereoBirds
{
    public class FrameRefCollection:IEnumerable<FramePair>,IEnumerator<FramePair>
    {
        private List<FrameRef> masters;
        private List<FrameRef> slaves;
        private int masterIndex=-1;
        private int slaveIndex=-1;

        public int Count
        {
            get { return Math.Min(masters.Count, slaves.Count); }
        }

        public FrameRefCollection(List<FrameRef> masterFrames, List<FrameRef> slavesFrames)
        {
            masters = masterFrames;
            slaves = slavesFrames;
        }

        public void ReportDroppedSlave()
        {
            Console.WriteLine("slave drop {0}", slaves[slaveIndex].Path);
            nudgeFrameNumbers(slaves,slaveIndex);

        }

        public void ReportDroppedMaster()
        {
            Console.WriteLine("master drop {0}", masters[masterIndex].Path);
            nudgeFrameNumbers(masters, masterIndex);
        }

        private void nudgeFrameNumbers(List<FrameRef> frames,int startIndex)
        {
            int i= startIndex;
            long last = -1;
            while(frames[i].FrameNumber>last)
            {
                frames[i].NudgeFrameNumber();
                last = frames[i].FrameNumber;
                i++;
            }
            slaveIndex--;
            masterIndex--;
            MoveNext();
        }

        public IEnumerator<FramePair> GetEnumerator()
        {
            return this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this;
        }

        public FramePair Current
        {
            get { return new FramePair(masters[masterIndex], slaves[slaveIndex]); }
        }

        public void Dispose()
        {
            //remove any references 
            masters = null;
            slaves = null;
        }

        object System.Collections.IEnumerator.Current
        {
            get { return new FramePair(masters[masterIndex], slaves[slaveIndex]); }
        }

        public bool MoveNext()
        {
            int mCt = masters.Count;
            int sCt = slaves.Count;
            masterIndex++;
            slaveIndex++;
            if (masterIndex < mCt && slaveIndex < sCt)
            {
                while (masterIndex < mCt && masters[masterIndex].CompareTo(slaves[slaveIndex]) < 0)
                {
                    masterIndex++;
                }
                while (slaveIndex < sCt && slaves[slaveIndex].CompareTo(masters[masterIndex]) < 0)
                {
                    slaveIndex++;
                }
            }
            return masterIndex < mCt && slaveIndex < sCt;
        }

        public void Reset()
        {
            masterIndex = -1;
            slaveIndex = -1;
        }
    }


    //class FrameCollectionEnumerator : IEnumerator<FramePair>
    //{
    //    private List<FrameRef> masterFrames;
    //    private List<FrameRef> slaveFrames;
    //    private int masterIndex=-1;
    //    private int slaveIndex=-1;

    //    public FrameCollectionEnumerator(List<FrameRef> masters, List<FrameRef> slaves)
    //    {
    //        masterFrames = masters;
    //        slaveFrames = slaves;
    //    }

    //    public FramePair Current
    //    {
    //        get { return new FramePair(masterFrames[masterIndex], slaveFrames[slaveIndex]); }
    //    }

    //    public void Dispose()
    //    {
    //        //remove any references 
    //        masterFrames = null;
    //        slaveFrames = null;
    //    }

    //    object System.Collections.IEnumerator.Current
    //    {
    //        get { return new FramePair(masterFrames[masterIndex], slaveFrames[slaveIndex]); }
    //    }

    //    public bool MoveNext()
    //    {
    //        int mCt = masterFrames.Count;
    //        int sCt = slaveFrames.Count;
    //        masterIndex++;
    //        slaveIndex++;
    //        while (masterIndex < mCt && masterFrames[masterIndex].CompareTo(slaveFrames[slaveIndex]) < 0)
    //        {
    //            masterIndex++;
    //        }
    //        while (slaveIndex < sCt && slaveFrames[slaveIndex].CompareTo(masterFrames[masterIndex]) < 0)
    //        {
    //            slaveIndex++;
    //        }
    //        return masterIndex < mCt && slaveIndex < sCt;
    //    }

    //    public void Reset()
    //    {
    //        masterIndex = -1;
    //        slaveIndex = -1;
    //    }


        
    //}
}
