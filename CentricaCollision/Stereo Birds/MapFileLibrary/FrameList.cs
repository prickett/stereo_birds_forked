﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Createc.StereoBirds
{
    
    public class FrameList : IEnumerator<FramePair>,IEnumerable<FramePair>
    {
        private List<FrameRef> masterFrames;
        private List<FrameRef> slaveFrames;
        private int masterIndex;
        private int slaveIndex;
        private List<int> pairIndexer = new List<int>();
        private List<int> frameIndexer = new List<int>();
        private List<FramePair> frames = new List<FramePair>();
        private List<int> indexer;
        private int index;

        public FrameList(List<FrameRef> masters, List<FrameRef> slaves)
        {
            pairFrames(masters, slaves);
            indexer = pairIndexer;
        }

        public FrameList(FrameList other)
        {
            pairIndexer = other.pairIndexer;
            frameIndexer = other.frameIndexer;
            frames = other.frames;
            indexer = other.indexer;
            index = other.index;

            
        }

        public bool SkipUnMatched
        {
            get { return indexer == pairIndexer; }
            set
            {
                if (SkipUnMatched != value)
                {
                    if (value)
                    {
                        indexer = pairIndexer;
                    }
                    else
                    {
                        indexer = frameIndexer;
                    }
                }
            }
        }

        public void MoveTo(long systemTime)
        {
            var fr = new FrameRef(systemTime);
            var fp = new FramePair(fr, fr);

            int i = frames.BinarySearch(fp);
            if (i < 0)
                i = -i - 1;

            i = indexer.BinarySearch(i);
            if (i < 0)
                i = -i - 1;

            if(i>=0)
                index=i;
        }

        /// <summary>
        /// Fix out of sync frames
        /// really awful kludge solution to avoid changing to much code; 
        /// </summary>
        public void ReportFrameSkip()
        {
            int i = indexer[index];
            //check if we have a previous frame,
            //we are using the pair list, 
            //we are not at the start of a run
            if(i != 0 && Current.IsPair && Current.FrameNumber > 1)           
            {
                const long MEAN_TIC_BETWEEN_FRAMES = 18730910; //based on rough sample of 50
                const long CAMERA_TIC_TOLERANCE = MEAN_TIC_BETWEEN_FRAMES >> 2;//<-- any difference below this (4682727) is ignored 
                const long ROLLOVER_VERIFY = MEAN_TIC_BETWEEN_FRAMES << 5;//<-- any difference as gross as this indicates rollover
                const long BIT32 = 1 << 32;//rollover error

                var master = new MapFile();
                var slave = new MapFile();
                long oTic=0, nTic = 0;
                long oNum, nNum = -10000; //preset with an impossible frame number < -maximum allowed count difference
                var prev = frames[i - 1];
                var curr = frames[i];

                if (master.Open(prev.Master) && slave.Open(prev.Slave))
                {
                    oTic = (long)master.Header.CameraTick - slave.Header.CameraTick;
                    oNum = master.Header.FrameCount;
                }

                if (master.Open(curr.Master) && slave.Open(curr.Slave))
                {
                    nTic = (long)master.Header.CameraTick - slave.Header.CameraTick;
                    nNum = master.Header.FrameCount;
                }

                long dif = nTic - oTic;
                if (dif >= ROLLOVER_VERIFY)
                {
                    dif -= BIT32;
                }
                else if (-dif >= ROLLOVER_VERIFY)
                {
                    dif += BIT32;
                }

                dif/=CAMERA_TIC_TOLERANCE;
                if (dif > 0)//slave drop ??
                {
                    while (prev.FrameNumber < curr.FrameNumber)
                    {

                        i++;
                        prev = curr;
                        curr = frames[i];
                        if (master.Open(curr.Master) && slave.Open(curr.Slave))
                        {
                            oTic = nTic;
                            nTic = (long)master.Header.CameraTick - slave.Header.CameraTick;
                            nNum = master.Header.FrameCount;
                        }
                    }
                }
                else if (dif < 0) //master drop??
                {
                }
            }
        }

        //class FramePairComparer : IComparer<FramePair>
        //{
        //    public FramePairComparer(long systemTime,int[] indexer, List<FramePair> frames)
        //    {
        //    }
        //    //public int Compare(int x, int y)
        //    //{
        //    //    throw new NotImplementedException();
        //    //}

        //    public int Compare(FramePair x, FramePair y)
        //    {
        //        throw new NotImplementedException();
        //    }
        //}


        private void pairFrames(List<FrameRef> masters, List<FrameRef> slaves)
        {
            masters.Sort();//these should always be sorted....
            slaves.Sort();//...but there is no harm in making sure

            int m = 0;
            int s = 0;
            int msz = masters.Count;
            int ssz = slaves.Count;

            while (s < ssz & m < msz)
            {
                while (m < msz && masters[m].CompareTo(slaves[s]) < 0)
                {
                    frameIndexer.Add(frames.Count);
                    frames.Add(new FramePair(masters[m++], null));
                }
                while (s < ssz && slaves[s].CompareTo(masters[m]) < 0)
                {
                    frameIndexer.Add(frames.Count);
                    frames.Add(new FramePair(null, slaves[s++]));
                }
                while (s < ssz & m < msz && slaves[s].CompareTo(masters[m]) == 0)
                {
                    pairIndexer.Add(frames.Count);
                    frameIndexer.Add(frames.Count);
                    frames.Add(new FramePair(masters[m++], slaves[s++]));
                }
            }


            //verifyFrames();//!!!!!!!!!!Kludge


        }

        private void verifyFrames()
        {
            //although already paired early files are vunerable to dropped frames

            const long MEAN_TIC_BETWEEN_FRAMES = 18730910; //based on rough sample of 50
            const long CAMERA_TIC_TOLERANCE = MEAN_TIC_BETWEEN_FRAMES>>1;//<-- any difference below this is ignored

            var master = new MapFile();
            var slave = new MapFile();
            int ct = frames.Count;

            //long tic1,tic0 =0;
            long oTic, nTic = 0;
            long oNum, nNum = -10000; //preset with an impossible frame number < -maximum allowed count difference
            for (int i = 0; i < ct; i++)
            {
                var fp = frames[i];

                if (fp.IsPair && master.Open(fp.Master) && slave.Open(fp.Slave)) //both files have readable headers
                {
                    //tic1 = tic0;
                    //tic0 = master.Header.CameraTick;
                    oNum = nNum;
                    nNum = master.Header.FrameCount;
                    oTic = nTic;
                    nTic = (long)master.Header.CameraTick - slave.Header.CameraTick;
                    long dif = (nTic - oTic) / CAMERA_TIC_TOLERANCE;

                    //Both Cameras will have different tick counts but the difference
                    //between tick counts should remain similar between consecutive frames.
                    //If we encounter a jump in this tick difference we can assume that 
                    //a frame has been dropped.
                    if (nNum - oNum == 1 && dif != 0)//only consecutive frames can be tested
                    {
                        int j = pairIndexer[i];
                        long fn = nNum;
                        if (dif < 0)//slave has dropped a frame
                        {

                            Console.WriteLine("slave drop {0}_{1}", fp.SystemTime,fp.FrameNumber);
                            //var carry = fp.Slave;                   
                            //frames[j++] = new FramePair(fp.Master, null);
                            //while (j < frames.Count && frames[j].FrameNumber - fn >0 && frames[j].IsPair)
                            //{
                            //    var carried = carry;
                            //    carry = frames[j].Slave;
                            //    frames[j] = new FramePair(frames[j].Master, carried);
                            //    fn = frames[j].FrameNumber;
                            //    j++;
                            //}
                        }
                        else //master has dropped a frame
                        {
                            Console.WriteLine("master drop {0}_{1}",fp.SystemTime,fp.FrameNumber);
                            //var carry = fp.Master;
                            //frames[j++] = new FramePair(null,fp.Slave);
                            //while (j < frames.Count && frames[j].FrameNumber - fn > 0 && frames[j].IsPair)
                            //{
                            //    var carried = carry;
                            //    carry = frames[j].Master;
                            //    frames[j] = new FramePair(carried,frames[j].Slave);
                            //    fn = frames[j].FrameNumber;
                            //    j++;
                            //}
                        }

                        // Console.WriteLine("{0}\t{1}\t\t\t{2}\t{3} {4} {5}", nTic, oTic, master.Header.CameraTick, slave.Header.CameraTick, fp.FrameNumber, frames[pairIndexer[i - 1]].FrameNumber);
                    }
                }
                else
                {
                    Console.WriteLine("bad pair!");
                }
            }

            frameIndexer.Clear();
            pairIndexer.Clear();

            for (int i = 0; i < frames.Count; i++)
            {
                if (frames[i].IsPair)
                    pairIndexer.Add(i);
                frameIndexer.Add(i);
            }


        }

        public int Count
        {
            get { return indexer.Count; }
        }
        public int PairedFrameCount
        {
            get { return pairIndexer.Count; }
        }
        public int TotalFrameCount
        {
            get { return frameIndexer.Count; }
        }


        public FramePair Current
        {
            get { return frames[indexer[index]]; }
        }


        object System.Collections.IEnumerator.Current
        {
            get { return frames[indexer[index]]; }
        }

        public bool MoveNext()
        {
            index++;
            return index < indexer.Count;
        }

        public void Reset()
        {
            index = -1;
        }

        public void Dispose()
        {

        }





        public IEnumerator<FramePair> GetEnumerator()
        {
            return this;
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this;
        }
    }
}
