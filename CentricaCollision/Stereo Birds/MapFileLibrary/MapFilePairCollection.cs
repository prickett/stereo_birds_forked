﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using System.Windows.Forms;

using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters;
using Createc.StereoBirds;

namespace Createc.StereoBirds
{
    public class MapFilePairCollection : IEnumerable<MapFilePair>
    {
        private List<FrameRef> masters;
        private List<FrameRef> slaves;
        public MapFilePairCollection(List<FrameRef> masterFrames, List<FrameRef> slavesFrames)
        {
            masters = masterFrames;
            slaves = slavesFrames;
        }
        public int PotentialCount
        {
            get { return Math.Min(masters.Count, slaves.Count); }
        }


        public IEnumerator<MapFilePair> GetEnumerator()
        {
            //MapFileFrameEnumerator is the better choice however it accepts a collection of framePair (a pair of frameRefs) 
            // the jump enumerator was to fix the syncing issue we were experiencing
            return new MapFileFrameJumpEnumerator(masters, slaves); //new MapFileEnumerator(masters, slaves); //
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return new MapFileFrameJumpEnumerator(masters, slaves); //new MapFileEnumerator(masters, slaves);
        }
    }

    public class MapFilePair
    {
        public MapFile Master { get; set; }
        public MapFile Slave { get; set; }
        public MapFilePair(MapFile master, MapFile slave)
        {
            Master = master;
            Slave = slave;
        }
    }



    public class MapFileFrameJumpEnumerator : MapFileEnumerator
    {
        private const long MEAN_TIC_BETWEEN_FRAMES = 18730910; //based on rough sample of 50
        private const long CAMERA_TIC_TOLERANCE = 1000;//<--trying just over 20x mean ticDif// (MEAN_TIC_BETWEEN_FRAMES *3)/4;//<-- any difference below this is ignored CRUCIAL VALUE
        private const long ROLLOVER_VERIFY = MEAN_TIC_BETWEEN_FRAMES << 5;
        private const long BIT32 = 1L << 32;

        private MapFile master;
        private MapFile slave;

        private List<FrameRef> masters;
        private List<FrameRef> slaves;
        private List<IntPair> frameMemory;

        private int masterIndex = -1;
        private int slaveIndex = -1;

        private long oldTicDif, newTicDif = 0;
        private long oldFrmNum, newFrmNum = -1;//-10000; //preset with an impossible frame number < -maximum allowed count difference

        private int counter = 0;

        private const int TIC_SAMPLE_BITS = 3;
        private const int TIC_SAMPLE_SIZE = 1 << TIC_SAMPLE_BITS;
        private const int TIC_SAMPLE_MASK = TIC_SAMPLE_SIZE - 1;
        private long[] slaveTics = new long[TIC_SAMPLE_SIZE];
        private long[] masterTics = new long[TIC_SAMPLE_SIZE];

        private long ticDifTot = 0;
        private long ticDifCt = 0;
        private long ticDifMean = 0;
        private long ticDifMax = int.MinValue;
        private long ticDifMin = int.MaxValue;

        public  MapFileFrameJumpEnumerator(List<FrameRef> masterFrames, List<FrameRef> slavesFrames)
            :base()
        {
            masters = masterFrames;
            slaves = slavesFrames;
            master = new MapFile();
            slave = new MapFile();
            frameMemory = new List<IntPair>();
        }


        public override  bool MovePrev()
        {
            int ub = frameMemory.Count - 1;
            if (ub>0)
            {
                masterIndex = frameMemory[ub-1].A;
                slaveIndex = frameMemory[ub-1].B;
                master.Open(masters[masterIndex]);
                slave.Open(slaves[slaveIndex]);
                frameMemory.RemoveAt(ub);
                return true;
            }
            return false;
        }



        public override bool MoveNext()
        {
            bool success = false;

            while (!success && moveNextFramePair())//this will only loop in the event of a detected frame drop
            {
                while ((!master.Open(masters[masterIndex]) || !slave.Open(slaves[slaveIndex])) && moveNextFramePair())
                {
                    //if either file can't be opened and there are pairs left, keep moving to the next pair. 
                }

                if (master.isOpen && slave.isOpen) //if we have a pair we can open
                {
                    //get the framenumber difference between this and the last valid pair 
                    oldFrmNum = newFrmNum;
                    newFrmNum = master.FrameReference.FrameNumber;
                    long numDIf = newFrmNum - oldFrmNum;

                    //get the difference of camera clock pair difference between this and the last valid pair 
                    int index = counter & TIC_SAMPLE_MASK;
                    long sTic = (long)slave.Header.CameraTick;
                    long mTic = (long)master.Header.CameraTick;

                    oldTicDif = newTicDif;
                    newTicDif = sTic - mTic;
                    masterTics[index] = mTic;
                    slaveTics[index] = sTic;

                    long ticDif = newTicDif - oldTicDif;

                    //catch and correct camera clock rollover (happens ~ every 225 frames)
                    correctOverflow(ref ticDif);

                    //check if the difference exceeds our tolerance
                    int offset;
                    if (counter > TIC_SAMPLE_SIZE && oldFrmNum < newFrmNum &&
                        (ticDif > CAMERA_TIC_TOLERANCE | ticDif < -CAMERA_TIC_TOLERANCE) &&
                        (offset = bestFitTics(index)) != 0)
                    {
                        Console.WriteLine(offset);
                        //decrement the indexes 
                        slaveIndex--;
                        masterIndex--;
                        counter = 0;
                        nudgeFrameNumber(slaves, slaveIndex, -offset);
                    }
                    else
                    {
                        success = true; //bonza! got there in the end, found a valid matched pair
                        counter++;
                        frameMemory.Add(new IntPair(masterIndex, slaveIndex));
                    }
                }
            }
            return success;
        }

        private int bestFitTics(int sampleIndex)
        {
            var offsets = new int[] { 0, 1, -1, 2, -2, 3, -3, 4, -4 };
            long min = int.MaxValue;
            int best = 0;
            foreach (var offset in offsets)
            {
                long result = testFitTics(sampleIndex, offset);
                if (result < min)
                {
                    min = result;
                    best = offset;
                }
            }
            return best;
        }

        private void correctOverflow(ref long n)
        {
            if (n > ROLLOVER_VERIFY)
                n -= BIT32;
            else if (n < -ROLLOVER_VERIFY)
                n += BIT32;
        }

        private long testFitTics(int sampleIndex, int offset)
        {
            int firstSample = (sampleIndex + 1 + Math.Max(0, -offset)) & TIC_SAMPLE_MASK;
            int count = TIC_SAMPLE_SIZE - Math.Abs(offset);

            long od, d, dd, t = 0, sq = 0;//,dmn = long.MaxValue, dmx = long.MinValue;

            d = masterTics[firstSample & TIC_SAMPLE_MASK] - slaveTics[(firstSample + offset) & TIC_SAMPLE_MASK];
            for (int i = 1; i < count; i++)
            {
                od = d;
                int j = (i + firstSample) & TIC_SAMPLE_MASK;
                int k = (j + offset) & TIC_SAMPLE_MASK;
                d = masterTics[j] - slaveTics[k];
                dd = d - od;
                correctOverflow(ref dd);
                t += dd;
            }
            long m = t / count;

            d = masterTics[firstSample & TIC_SAMPLE_MASK] - slaveTics[(firstSample + offset) & TIC_SAMPLE_MASK];
            for (int i = 1; i < count; i++)
            {
                od = d;
                int j = (i + firstSample) & TIC_SAMPLE_MASK;
                int k = (j + offset) & TIC_SAMPLE_MASK;
                d = masterTics[j] - slaveTics[k];
                dd = (d - od) - m;
                correctOverflow(ref dd);
                sq += dd * dd;
            }

            return sq / count;

        }

        private long mean(long[] values)
        {
            long t = 0;
            foreach (var n in values)
                t += n;
            return t / values.Length;
        }


        /// <summary>
        /// increments the framenumbers of a section of frames, stops 
        /// incrementing when the framenumbers indicate a new sequence
        /// </summary>
        /// <param name="frames">the frames to increment</param>
        /// <param name="startindex">the index of the first frame to be incremented</param>
        private void nudgeFrameNumber(List<FrameRef> frames, int startindex, int nudge)
        {
            int i = startindex;
            int ct = frames.Count;
            long n = 0;

            while (i < ct && frames[i].FrameNumber > n)
            {
                n = frames[i].FrameNumber;
                frames[i].NudgeFrameNumber(nudge);
                i++;
            }
        }











        //public void ReportDroppedSlave()
        //{
        //    Console.WriteLine("slave drop {0}", slaves[slaveIndex].Path);
        //    nudgeFrameNumbers(slaves,slaveIndex);

        //}

        //public void ReportDroppedMaster()
        //{
        //    Console.WriteLine("master drop {0}", masters[masterIndex].Path);
        //    nudgeFrameNumbers(masters, masterIndex);
        //}

        public override MapFilePair Current
        {
            get
            {
                //files will be open first call for this index, any calls after are uncertain
                //check and open files if necessary
                if (master != null && !master.isOpen && masterIndex > 0)
                    master.Open(masters[masterIndex]);
                if (slave != null && !slave.isOpen && slaveIndex > 0)
                    slave.Open(slaves[slaveIndex]);
                return new MapFilePair(master, slave);
            }
        }

        public override void Dispose()
        {
            //remove any references and close files
            masters = null;
            slaves = null;
            master.Dispose();
            slave.Dispose();
        }

        //object System.Collections.IEnumerator.Current
        //{
        //    get { return new MapFilePair(master, slave); }
        //}


        private bool moveNextFramePair()
        {
            bool success = false;
            int mCt = masters.Count;
            int sCt = slaves.Count;
            masterIndex++;
            slaveIndex++;

            if (slaveIndex < sCt)
            {
                while (masterIndex < mCt && masters[masterIndex].CompareTo(slaves[slaveIndex]) < 0)
                {
                    masterIndex++;
                }
            }
            if (masterIndex < mCt)
            {
                while (slaveIndex < sCt && slaves[slaveIndex].CompareTo(masters[masterIndex]) < 0)
                {
                    slaveIndex++;
                }
            }
            if (masterIndex < mCt && slaveIndex < sCt)
            {
                success = true;
            }

            return success;
        }

        public override void Reset()
        {
            masterIndex = -1;
            slaveIndex = -1;
            counter = 0;
        }

        private struct IntPair
        {
            public IntPair(int a, int b)
            {
                A = a;
                B = b;
            }
            public int A;
            public int B;
        }
    }

    public class MapFileFrameEnumerator : MapFileEnumerator
    {
        private FramePair[] frameArray;
        private MapFilePair frame;
        private int index;
        private int loadedIndex;

        public MapFileFrameEnumerator(IEnumerable<FramePair> frames)
        {
            frameArray = frames.ToArray(); //bad move? inits slower, runs faster
            loadedIndex = index = -1;
        }

        public override MapFilePair Current
        {            
            get
            { 
                if(index != loadedIndex && index >= 0)
                {
                    var pair = frameArray[index];
                    frame = new MapFilePair(new MapFile(pair.Master),new MapFile(pair.Slave));
                    loadedIndex = index;
                }
                return frame; 
            }
        }

        public override void Dispose()
        {
            if (frame != null)
            {
                frame.Master.Dispose();
                frame.Slave.Dispose();
            }
        }

        public override bool MoveNext()
        {
            if (index < frameArray.GetUpperBound(0))
            {
                index++;
                return true;
            }
            return false;
        }

        public override bool MovePrev()
        {
            if (index > 1)
            {
                index--;
                return true;
            }
            return false;
        }

        //public bool MoveTo(int index)
        //{

        //    return 
        //}

        public override void Reset()
        {
            index = -1;
        }     
    }

    /// <summary>
    /// An abstract class providing an enumerator that allows a backwards move
    /// </summary>
    public abstract class MapFileEnumerator : IEnumerator<MapFilePair>
    {
        public abstract bool MovePrev();
        //IEnumerator implementation
        public abstract bool MoveNext(); 
        public abstract MapFilePair Current { get; }
        public abstract void Reset();
        public abstract void Dispose();
        object System.Collections.IEnumerator.Current { get { return Current; } }
    }
}
