﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Createc.StereoBirds
{
    [Serializable]
    public struct TimeStamp : IComparable<TimeStamp>
    {
        public TimeStamp(uint seconds = 0, uint nanoseconds = 0)
        {
            sec = seconds;
            nsec = nanoseconds;
        }
        public int CompareTo(TimeStamp other)
        {
            if (sec < other.sec)
                return -1;
            else if (sec > other.sec)
                return 1;
            else if (nsec < other.nsec)
                return -1;
            else if (nsec > other.nsec)
                return 1;
            else
                return 0;
        }

        public override string ToString()
        {
            return string.Format("{0}_{1:000}", sec, nsec / 1000000);
        } 

        public double Seconds()
        {
            return sec + nsec * 0.000000001;
        }

        public uint sec;// {get; set;}
        public uint nsec;// { get; set; }
    }


    public enum FrameType
    {
        None,
        Map,
        Fullframe,
        Multiframe, // special type that contains a sequence of paired frames
        Unknown
    }
    public enum FrameStatus
    {
        UnVerified,
        Verified,
        Bad_Path,
        Bad_File
    }


    public class FrameRef:IComparable<FrameRef>
    {
        public bool IsMaster { get; private set; }
        public bool IsSlave { get; private set; }
        public ulong CameraTick { get; set; }
        public long FrameNumber { get; private set; }
        public TimeStamp SystemTime { get; private set; }
        public FrameType Type { get; private set; }
        public string Path { get; private set; }
        public long Offset { get; private set; }
        /// <summary>
        /// Size of the full frame file (not used with map file)
        /// </summary>
        public int Size { get; private set; } 
        public FrameStatus Status { get; set; }

        public void NudgeFrameNumber(int nudge)
        {
            FrameNumber+=nudge;
        }

        public FrameRef(bool isMaster, ulong cameraTick, long frameNumber, TimeStamp systemTime, FrameType type, string path, long offset =0 ,int size=0, FrameStatus status = FrameStatus.UnVerified)
        {
            IsMaster = isMaster;
            IsSlave = !isMaster;
            CameraTick = cameraTick;
            FrameNumber = frameNumber;
            SystemTime = systemTime;
            Type = type;
            Path = path;
            Offset = offset;
            Size = size;
            Status = status;
        }

        public FrameRef(bool isMaster,  FrameType type, string path, long offset = 0, FrameStatus status = FrameStatus.UnVerified)
        {
            IsMaster = isMaster;
            IsSlave = !isMaster;
            Type = type;
            Path = path;
            Offset = offset;
            Status = status;
        }


        public FrameRef(TimeStamp systemTime)
        {
            Type = FrameType.None;
            SystemTime = systemTime;
            FrameNumber = 0;
        }

        public FrameRef(string path)
        {
            
            var extension = System.IO.Path.GetExtension(path);

            switch (extension)
            {
                case ".chunk":
                    Type = FrameType.Multiframe;
                    ParseMultiFrame(path);
                    break;
                case ".map":
                    Type = FrameType.Map;
                    ParseMapPath(path);
                    break;
                case ".bmp":
                case ".jpg":
                    Type = FrameType.Fullframe;
                    ParseFullFramePath(path);
                    break;
                default:
                    Type = FrameType.Unknown;
                    Parse(path);
                    break;
            }   
            
        }


        private void ParseMultiFrame(string path)
        {
            var filename = System.IO.Path.GetFileNameWithoutExtension(path);
            IsMaster = false;
            IsSlave = false;
            Path = path;

            string result;
            int offset = 0;
            long number = 0;
            uint unsigned = 0;

            result = InnerString(filename, '_', '_', ref offset); // ignore the start time of the acquisition session 
            --offset;

            FrameNumber = offset >= 0 &&
                (result = InnerString(filename, '_', '_', ref offset)) != null &&
                long.TryParse(result, out number)
                ? number : -1;

            SystemTime = offset != -1 &&
                uint.TryParse(filename.Substring(offset), out unsigned) ?
                new TimeStamp(unsigned) : new TimeStamp();
        }

        /// <summary>
        /// finds the system time and frame number from a given path
        /// </summary>
        /// <param name="filename"></param>
        private void ParseMapPath(string path)
        {
            var filename = Parse(path);

            int offset = 0;
            string result;
            long number = 0;
            uint unsigned = 0;

            SystemTime = (result = InnerString(filename, '_', '_', ref offset))!=null && 
                uint.TryParse(result, out unsigned)
                ? new TimeStamp(unsigned) : new TimeStamp();

            FrameNumber = offset != -1 && 
                long.TryParse(filename.Substring(offset), out number) ?
                number : -1;
        }

        /// <summary>
        /// finds the system time and frame number from a given path
        /// </summary>
        /// <param name="filename"></param>
        private void ParseFullFramePath(string path)
        {
            var filename = Parse(path);
            int offset = 0;
            string result;
            long number = 0;
            uint unsigned = 0;
            
            SystemTime =
                (result = InnerString(filename, '_', '_', ref offset))!= null &&
                uint.TryParse(result, out unsigned) ?
                new TimeStamp(unsigned) : new TimeStamp();

            FrameNumber = offset != -1 && 
                long.TryParse(filename.Substring(offset), out number) ?
                number : -1;
            
        }

        private string Parse(string path)
        {
            var filename = System.IO.Path.GetFileNameWithoutExtension(path);
            IsMaster = (filename.ToLower().IndexOf('m') == 0);
            IsSlave = (filename.ToLower().IndexOf('s') == 0);
            Path = path;
            return filename;
        }

        private static string InnerString(string text, char opener, char closer,ref int offset)
        {
            int p, q;
            if ((p = text.IndexOf(opener,offset) + 1) >= 1 && (q = text.IndexOf(closer, p)) >= 1)
            {
                offset = q + 1; // was + 2 ??
                return text.Substring(p, q - p);
            }
            else
            {
                offset = -1;
                return null;
            }
        }


        public int CompareTo(FrameRef other)
        {
            return other != null? SystemTime.CompareTo(other.SystemTime):1;
        }

        //public int CompareTo(FrameRef other)
        //{
        //    if (other == null)
        //        return 1;
        //    long sDif = SystemTime - other.SystemTime;
        //    if (Math.Abs(sDif) < 120)
        //    {
        //        long fDif = FrameNumber - other.FrameNumber;
        //        if (Math.Abs(fDif) < 1000 || sDif == 0)
        //            sDif = fDif;
        //    }
        //    if ((sDif & -2147483648) != 0)
        //        sDif >>= 32;
        //    return (int)sDif;
        //}

        //public int CompareToAndVerify(FrameRef other)
        //{
        //    if (other == null)
        //        return 1;
        //    int sDif = (int)(SystemTime - other.SystemTime);
        //    if (Math.Abs(sDif) < 20)
        //    {
        //        int fDif = (int)(FrameNumber - other.FrameNumber);
        //        if (Math.Abs(fDif) < 100)
        //            return fDif;
        //    }
        //    return sDif;
        //}

        /// <summary>
        /// Confirms that a map file frame reference can be loaded 
        /// </summary>
        /// <param name="file">An optional Mapfile object to (re)use</param>
        /// <returns></returns>
        public bool Confirm(MapFile file)
        {
            if (file == null)
                file = new MapFile();
            if (file.Open(this))
            {
                MapFileHeader h = file.Header;
                CameraTick = h.CameraTick;
                FrameNumber = h.FrameCount;
                SystemTime = h.SystemTime;
                Type = FrameType.Map;
                return true;
            }
            return false;
        }
    }
}
