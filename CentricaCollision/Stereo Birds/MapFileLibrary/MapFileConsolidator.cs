﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;

//using Createc.Threading;


namespace Createc.StereoBirds
{
    public class MapFileConsolidator
    {
        private int counter = 0;
        public DirectoryInfo SourceFolder { get; private set; }
        public DirectoryInfo TargetFolder { get; private set; }
        public SearchOption SearchOption { get; set; }
        public int TransferedCount { get { return counter; } }

        public MapFileConsolidator(string sourceFolder, string targetFolder, SearchOption searchOption = SearchOption.TopDirectoryOnly)
            : this(new DirectoryInfo(sourceFolder), new DirectoryInfo(targetFolder), searchOption) { }

        public MapFileConsolidator(DirectoryInfo sourceFolder, DirectoryInfo targetFolder, SearchOption searchOption = SearchOption.TopDirectoryOnly)
        {
            SourceFolder = sourceFolder;
            TargetFolder = targetFolder;
            SearchOption = searchOption;
        }


        public void SortFiles()
        {

            //get an enumerator of FrameRefs to all files in the root directory
            var justBefore = DateTime.Now - TimeSpan.FromMinutes(3);
            string duplicateFolder = TargetFolder + "Duplicates\\";

 
            var frameRefs = Directory.EnumerateFiles(SourceFolder.FullName, "*.*", SearchOption)
                .Where(f => File.GetLastWriteTime(f) < justBefore)
                .Select(f => new FrameRef(f));

            foreach (var fr in frameRefs)
            {
                if (fr.Type != FrameType.Unknown && fr.SystemTime.sec > 0)
                {
                    ulong time = fr.SystemTime.sec; // !! ignoring nanosec

                    string d0 = string.Format("_{0}0000000", time / 10000000);  //115 days 17 hours
                    string d2 = string.Format("_{0}00000", time / 100000);  //27 hours 46 minutes
                    string d4 = string.Format("_{0}000", time / 1000);  //16 minutes 40 seconds ~ 3000 - 6000 files
                    string folder = string.Format("{0}{1}\\{2}\\{3}\\", TargetFolder, d0, d2, d4);
                    string filename = Path.GetFileName(fr.Path);
                    string targetPath = folder + filename;

                    if (!Directory.Exists(folder))
                    {
                        Console.WriteLine("{0} Creating folder: {1}", DateTime.Now, folder);
                        Directory.CreateDirectory(folder);
                    }

                    if (fr.Path.ToLower() != targetPath.ToLower())// just in case...
                    {
                        //Console.WriteLine(File.GetLastWriteTime(fr.Path));
                        if (File.Exists(targetPath))//we have likely already tried and failed moving this file before it was written
                        {
                            Console.WriteLine("what??");
                        }

                        try
                        {
                            File.Move(fr.Path, targetPath);
                            counter++;
                        }
                        catch (IOException e)//try this, it won't swallow all exceptions
                        {
                            Console.Write(e);
                            if (File.Exists(targetPath) && File.Exists(fr.Path))
                            {
                                Console.Write('!');
                                var ftgt = new FileInfo(targetPath);
                                var fsrc = new FileInfo(fr.Path);
                                if (ftgt.Length > fsrc.Length)
                                {
                                    Console.Write('?');
                                    targetPath += ".dup";
                                }
                                else
                                {
                                    try
                                    {
                                        File.Delete(targetPath);
                                    }
                                    catch (IOException ie)
                                    {
                                        Console.WriteLine(ie);
                                    }
                                }
                            }
                            else
                            {
                                Console.Write(e);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Fail! we are copying to the same place we a copying from.");
                    }
                }
            }
        }

        static bool CanOpenFile(string filename)
        {
            bool retval = false;
            FileStream fs = null;
            try
            {
                fs = new FileStream(filename, FileMode.Open, FileAccess.Read,FileShare.None);
                fs.Dispose();
                fs =null;
                retval = true;
            }
            catch(IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (fs != null)
                    fs.Dispose();
            }
            return retval;
        }

    }
}
