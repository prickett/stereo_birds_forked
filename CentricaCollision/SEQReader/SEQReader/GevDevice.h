#pragma once

//
// To receives images using a PvPipeline, full recovery management
//

#include <stdio.h>
#include <conio.h>

#include <PvDeviceFinderWnd.h>
#include <PvDevice.h>
#include <PvPipeline.h>
#include <PvBuffer.h>
#include <PvStream.h>
#include <PvStreamRaw.h>

#include <mutex>
#include <condition_variable>

#include "Img.h"
#include "StereoHandler.h"

#define MSG_RESTOREACQ ( 1000 )
#define MSG_STOPACQ ( 1003 )
#define MSG_QUIT ( 1004 )


//
// GevDevice Camera Class
//

class GevDevice : protected PvDeviceEventSink
{
public:
	GevDevice();
	~GevDevice();

	bool Connect(const char * device_identifier, StereoHandler * saveBuffer,  GevDevice * other = NULL, bool isMaster = false);
	bool Run();
	void Quit();

protected:
	bool OpenStream();
	void CloseStream();
	bool StartAcquisition();
	bool StopAcquisition();
	void AcquisitionLoop();
	void WrapUp();
	void HandleMsgQueue();

	

	// Inherited from PvDeviceEventSink
    void OnLinkDisconnected( PvDevice *aDevice );
    void OnLinkReconnected( PvDevice *aDevice );

private:
	PvString device_ip_;
	PvDevice device_;
	PvStream stream_;
	PvPipeline pipeline_;

	// Communication link GenICam parameters
    PvGenBoolean *gen_link_recovery_;

	// Device GenICam parameters
	PvGenInteger *gen_transport_layer_params_locked_;
	PvGenInteger *gen_payload_size_;
	PvGenInteger *gen_width_;
	PvGenInteger *gen_height_;
	PvGenCommand *gen_command_start_;
	PvGenCommand *gen_command_stop_;

	// Stream GenICam parameters
	PvGenInteger *gen_count_;
	PvGenFloat *gen_framerate_;
	PvGenFloat *gen_bandwidth_;
    PvGenCommand *gen_command_reset_timestamp_;
	PvGenEnum *gen_aquisition_mode_;

	// exposure/gain parameters
	PvGenBoolean *gen_auto_gain_enable_;
	PvGenInteger *gen_auto_gain_max_;
	PvGenEnum *gen_auto_gain_speed_;
	PvGenBoolean *gen_auto_exposure_enable_;
	PvGenInteger *gen_auto_exposure_max_;
	PvGenEnum *gen_auto_exposure_speed_;
	PvGenInteger *gen_auto_luminance_level_;
	PvGenEnum  *gen_auto_luminance_type_;
	PvGenInteger  *gen_auto_offset_x_;
	PvGenInteger  *gen_auto_offset_y_;
	PvGenInteger  *gen_auto_width_;
	PvGenInteger  *gen_auto_height_;

	// exposure/gain status
	PvGenInteger *gen_auto_gain_;
	PvGenInteger *gen_auto_exposure_;
	PvGenInteger *gen_auto_luminance_;



	std::mutex mutex_;
	std::condition_variable condition_;
	bool ready_; // used to sync cameras
	std::list<int> message_queue_;	

    bool quit_;
	bool is_master_;
	
	int8_t camera_id_[16];

	GevDevice * other_;
	StereoHandler *  stereo_handler_;
};

