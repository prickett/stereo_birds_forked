#pragma once

//#include "PvApi.h"
#include "Img.h"
#include "SIMDHelper.h"

#define CHANNELOFINTEREST 1

namespace ImgManip
{
	typedef struct{
		unsigned long X;
		unsigned long Y;
		unsigned long Width;
		unsigned long Height;
		unsigned long FrameCount;
		unsigned long CameraTimestamp;
		unsigned long SystemTimestamp;
		void * Context;
		unsigned char *Channels[4];
	} tSplitBayer;


	static void Swap(__m128i* a, __m128i* b)
	{
		__m128i* t = a;
		a=b;
		b=t;
	}

	inline static void AbsDif(__m128i* srcA, __m128i* srcB, __m128i* tgt)
	{
		*tgt = _mm_or_si128( _mm_subs_epu8(*srcA,*srcB) , _mm_subs_epu8(*srcB,*srcA));
	}

	//populates a buffer with the absolute difference of two buffers
	static void AbsoluteDifference(__m128i* srcA, __m128i* srcB, __m128i* tgt, const unsigned long count)
	{
		for( unsigned long int i = 0; i<count; i++, tgt++, srcA++, srcB++)
		{
			//*tgt = _mm_sad_epu8(*srcA,*srcB); //<-- thought it would work! but sadly no :(

			//subtracts, saturates then combines tgt=(a-b)|(b-a)
			*tgt = _mm_or_si128(_mm_subs_epu8(*srcA,*srcB), _mm_subs_epu8(*srcB,*srcA));
		}
	}

		//populates a buffer with the absolute difference of two buffers
	static void TernaryDifference(__m128i* srcA, __m128i* srcB, __m128i* srcC, __m128i* tgt, const unsigned long count)
	{
		for(unsigned long i = 0; i<count; i++, tgt++, srcA++, srcB++, srcC++)
		{
			//subtracts, saturates then combines d=(a-b)|(b-a)
			__m128i d = _mm_or_si128(_mm_subs_epu8(*srcA,*srcB), _mm_subs_epu8(*srcB,*srcA));

			*tgt = _mm_subs_epu8(d,*srcB);
		}
	}

	//populates a buffer with the difference of two buffers
	static void Difference(__m128i* srcA, __m128i* srcB, __m128i* tgt, const unsigned long count)
	{
		const __m128i HIMASK = _mm_set1_epi8(0xfe);
		const __m128i MIDVAL = _mm_set1_epi8(0x7f);

		for(unsigned long i = 0; i<count; i++, tgt++, srcA++, srcB++)
		{
			//no support for a 8 bit shift... so using 128 bit right shift with a mask to stop underflow
			__m128i a =_mm_srli_epi64( _mm_and_si128(*srcA,HIMASK),1 );
			__m128i b =_mm_srli_epi64( _mm_and_si128(*srcB,HIMASK),1 );

			*tgt = _mm_add_epi8 (MIDVAL, _mm_sub_epi8 (a,b));
		}
	}

	static void SplitScanLine(__m128i* src, __m128i* tgtA, __m128i* tgtB, unsigned long width)
	{
		const __m128i EVNBYTS=_mm_set1_epi16(0xff); 
		const __m128i ODDBYTS =_mm_set1_epi16(0xff00);

		__m128i lo,hi; //to store source values
		unsigned long count = width >> 5; 

		//Spot the difference with the following two loops
		if(((unsigned long long) src & 0xf) == 0) //aligned source!
		{
			for(unsigned long i=0;i<count;i++)
			{
				//load 256 bits from the aligned buffer
				lo = _mm_load_si128 (src++); 
				hi = _mm_load_si128 (src++);

				//mask the even bytes and pack into the first output channel
				*tgtA++ = _mm_packus_epi16(
					_mm_and_si128(lo,EVNBYTS),
					_mm_and_si128(hi,EVNBYTS));

				//mask and shift the odd bytes then pack into the second output channel
				*tgtB++ = _mm_packus_epi16(
					_mm_srli_epi16(_mm_and_si128(lo,ODDBYTS),8), 
					_mm_srli_epi16(_mm_and_si128(hi,ODDBYTS),8));
			}
		}
		else //unaligned source!
		{
			for(unsigned long i=0;i<count;i++)
			{
				//load 256 bits from the unaligned buffer
				lo = _mm_loadu_si128 (src++); 
				hi = _mm_loadu_si128 (src++);

				//mask the even bytes and pack into the first output channel
				*tgtA++ = _mm_packus_epi16(
					_mm_and_si128(lo,EVNBYTS),
					_mm_and_si128(hi,EVNBYTS));

				//mask and shift the odd bytes then pack into the second output channel
				*tgtB++ = _mm_packus_epi16(
					_mm_srli_epi16(_mm_and_si128(lo,ODDBYTS),8), 
					_mm_srli_epi16(_mm_and_si128(hi,ODDBYTS),8));
			}
		}
	}

	static void SplitBayer(Img::ImageRef* in, tSplitBayer* out)
	{
		unsigned long w = in->width;
		unsigned long h = in->height;
		unsigned long stride = w >> 5; //destination stride
		unsigned char* byr = (unsigned char*)in->image; 

		//set up destination pointers
		__m128i *chan0 =  (__m128i*)out->Channels[0];
		__m128i *chan1 =  (__m128i*)out->Channels[1];
		__m128i *chan2 =  (__m128i*)out->Channels[2];
		__m128i *chan3 =  (__m128i*)out->Channels[3];

		out->X=0;
		out->Y=0;
		out->Width = w;
		out->Height = h;
		//out->FrameCount = in->FrameCount;
		//out->CameraTimestamp = in->TimestampLo;
		//out->SystemTimestamp = 0xffffffff;
		//out->Context = in->Context[0];

		for (unsigned long y= 0;y<h;y+=2)
		{
			SplitScanLine((__m128i*)(byr + w * (y)),   chan0, chan1, w);
			SplitScanLine((__m128i*)(byr + w * (y+1)), chan2, chan3, w);
			chan0+=stride;
			chan1+=stride;
			chan2+=stride;
			chan3+=stride;
		}

		//This is roughly the equivalent without using intrinsics
		//int index= 0;
		//for (unsigned long y= 0;y<h;y+=2)
		//{
		//	unsigned char* t = byr + w * y;
		//	unsigned char* u = t + w;
		//	for (int x=0;x<w;x+=2)
		//	{
		//		out->Channels[0][index] = *t++;
		//		out->Channels[1][index] = *t++;
		//		out->Channels[2][index] = *u++;
		//		out->Channels[3][index] = *u++;
		//		index++;
		//	}
		//}

	}


};

