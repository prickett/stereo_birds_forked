#pragma once
#include <intrin.h>
#include <xmmintrin.h>
#include <emmintrin.h>
#include <iostream>
#include <iomanip>

class SIMDHelper
{
public:
	static bool IsSSE2Available();
	static int GetLogicalCoreCount();

	static bool IsAligned(void * p);

	//debugging functions
	static void Cout_m128_sp(__m128 *n);
	static void Cout_m128i_i16x(__m128i *n);
	static void Cout_m128i_u8(__m128i *n);
	static void Cout_m128i_i16(__m128i *n);
	static void Cout_m128i_i32(__m128i *n);

};



