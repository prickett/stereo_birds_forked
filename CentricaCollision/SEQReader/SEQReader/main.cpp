
#define NOMINMAX // get rid of the evil min max macros

#include <opencv2\world.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\imgproc.hpp>
#include <Hermes.h>
#include "StereoHandler.h"
#include "demosaic.h"
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>

//#define CHECK_MOSAIC 
//uncomment the above line to compare the original image to the simulated image 
//(Proof that the mosaicing is not degrading the data)

void process_seq(const char* filename)
{

	bool quit = false;
	HImage* frame = HImage::CreateHImage();
	HSequence sequence;

	//Open the sequence
    if(!sequence.Open(filename))
	{
		printf("Failed to open %s",filename);
		return;
	}

	const std::string winname ="seq";
	cv::namedWindow(winname,cv::WINDOW_AUTOSIZE);

	const unsigned count = sequence.GetAllocatedFrames();
		
	printf("sequence\n");
	printf("%d frames\n", sequence.GetAllocatedFrames());
	printf("%d x %d frame size\n", sequence.GetImageWidth(), sequence.GetImageHeight());
	printf("%d format\n", sequence.GetImageFormat());

	eHImageFormat format =  sequence.GetImageFormat();
	
	const unsigned magic_wtf_offset = 0;
	for(unsigned i=65; i<= count && !quit; ++i)
	{
		if(!sequence.Read(frame, i))
			continue;

		long ts = frame->GetTimestamp();
		ushort ms = frame->GetTimestampMS();
		ushort us = frame->GetTimestampUS();
		printf("%u  ms: %u  us: %u\n",ts,ms,us);

		if(format == H_IMAGE_BGR)
		{
			cv::Mat im(frame->GetImageHeight(), frame->GetImageWidth(), CV_8UC3, (uchar*)frame->GetRawImageData() + magic_wtf_offset);
			cv::flip(im,im,0);	
			cv::imshow(winname, im.t());
		}
		else if(format == H_IMAGE_MONO_BAYER)
		{
			cv::Mat im(frame->GetImageHeight(), frame->GetImageWidth(), CV_8UC1, (uchar*)frame->GetRedChannel());
			cv::Mat bgr(im.rows,im.cols,CV_8UC3);
			demosaic(im,bgr,GRBG);
			cv::resize(bgr,bgr,cv::Size(),0.2,0.2,CV_INTER_AREA);
			cv::Rect roi(500,500,256,256);
			cv::imshow(winname, bgr);
		}			
			
		int key =  cv::waitKey(0);
		quit = tolower(key)=='q';
	}
   
    sequence.Close();

}

bool process_seq(
	const char* left_seq_path, 
	const char* right_seq_path, 
	const char* target_folder, 	
	int offset_right = 0)
{
	HSequence seqL, seqR;

	if(!seqL.Open(left_seq_path) || !seqR.Open(right_seq_path))
	{
		fprintf(stderr,"Failed to open seq file\n");
		return false;
	}

	eHImageFormat format = seqL.GetImageFormat();

	if( format != H_IMAGE_BGR && format !=  H_IMAGE_MONO_BAYER )
	{
		fprintf(stderr,"Only BGR and MONO BAYER formats supported\n");
		return false;
	}

	if(seqR.GetImageFormat() != format  )
	{
		fprintf(stderr,"The image format of each stream must match\n");
		return false;
	}

	int width = seqL.GetImageWidth();
	int height = seqL.GetImageHeight();

	if(width!=seqR.GetImageWidth() || height!=seqR.GetImageHeight())
	{
		fprintf(stderr,"Image dimensions do not match\n");
		return false;
	}
	printf("%u x %u\n",width,height);
	StereoHandler stereo_handler(target_folder, "SPC", 540);
	HImage* frame = HImage::CreateHImage();

	const unsigned countL = seqL.GetAllocatedFrames();
	const unsigned countR = seqR.GetAllocatedFrames();
		
	MapFile::FrameInfo info;
	memset(info.cameraID,0,sizeof(info.cameraID));
	info.exposure = 0;
	info.frameStatus = 0;
	info.cameraTick = 0;
	info.systemTime.sec = 0;
	info.systemTime.nsec = 0;
	info.frameNumber = 0;
	info.bayerPattern = GRBG; 
	info.IsMaster = 1; // 1/0;

	cv::Mat bayer(height, width ,CV_8UC1, cv::Scalar(0)); 
	cv::Mat bgr;

	//cv::Mat felix = cv::imread("felix40x40.png",CV_LOAD_IMAGE_GRAYSCALE);
	//cv::imshow("felix", felix);

	auto handle_frame = [&](void)
	{	
		if(format == H_IMAGE_MONO_BAYER)
		{
			cv::Mat im(height, width, CV_8UC1, (uchar*)frame->GetRawImageData());
			im.copyTo(bayer);
		}
		else			
		{
			cv::Mat im(height, width, CV_8UC3, (uchar*)frame->GetRawImageData());
			mosaic(	im, bayer, GBRG); // create one channel bayer pattern from bgr input

#ifdef CHECK_MOSAIC //following few lines compare the original bgr to the faked bayer demosaiced
			cv::Mat chk(height, width, CV_8UC3);
			demosaic(bayer, chk, GBRG); // pattern flipped horizontally from header
			int sz = 256;
			cv::Rect sqr((width-sz)/2,(height-sz)/2,sz,sz); 
			cv::imshow("original",im(sqr));
			cv::imshow("check",chk(sqr));
#endif
		}

		cv::flip(bayer, bayer, -1);
		return Img::ImageRef(bayer.ptr(0), bayer.cols, bayer.rows, bayer.cols, bayer.elemSize() * CHAR_BIT);
		//return Img::ImageRef(felix.ptr(0), felix.cols, felix.rows,felix.step, bayer.elemSize() * CHAR_BIT);
	};

#ifdef CHECK_MOSAIC	
	cv::namedWindow("original",cv::WINDOW_NORMAL);
	cv::namedWindow("check",cv::WINDOW_NORMAL);
#endif

	int index = std::max(1-offset_right, 1);
	int end_index = std::min(countL, countR-offset_right) ;
	int step = 1;



	for(;index <= end_index; ++index)
	{
		if(!seqL.Read(frame, index))
			break;

		long sec = frame->GetTimestamp();
		ushort msec = frame->GetTimestampMS();
		ushort usec = frame->GetTimestampUS();

		info.systemTime.sec = sec;
		info.systemTime.nsec = 1000000 * msec + 1000 * usec;
		info.cameraTick = 1000000ULL * sec + info.systemTime.nsec / 1000;
		info.frameNumber = index;

		info.IsMaster = 0;
		strcpy(reinterpret_cast<char*>(&info.cameraID[0]) ,"Left Slave");
		stereo_handler.Add( handle_frame(), info );

		if(!seqR.Read(frame, index+offset_right))
			break;

		info.IsMaster = 1;
		strcpy(reinterpret_cast<char*>(&info.cameraID[0]) ,"Right Master");
		stereo_handler.Add( handle_frame(), info );
		
		int key = cv::waitKey(1); 
		if(tolower(key)=='q')
			break;	

	}
}

int main(int argc, char* argv[])
{
	using namespace std;
	const char* path = argc>1? argv[1] : "files.txt";

	ifstream input(path);    
	string cam1_folder;
	string cam2_folder;
	string target_folder;
	string file_name; 

	getline(input, cam1_folder);
	getline(input, cam2_folder);
	getline(input, target_folder);

	printf("camera one: %s\n", cam1_folder.c_str());
	printf("camera two: %s\n", cam2_folder.c_str());
	printf("target folder: %s\n", target_folder.c_str());

    while( getline( input, file_name ) ) 
	{
		printf("%s\n", file_name.c_str());
        process_seq(
			(cam1_folder + "\\" + file_name).c_str(),
			(cam2_folder + "\\" + file_name).c_str(),
			target_folder.c_str(), 0);
    }
		
	return 0;
}