SEQReader combines two Norpix SEQ streams into one or more chunk files that encapsulate a stereo stream.

The SEQReader executable expects to find a text file called files.txt in the same folder. This text file lists the required folders and files and will need to be adjusted to suit.

The first line should be the folder of the footage from camera one.
The second line, the folder of the footage from camera two.
The third line, the output folder
The lines that follow should list the individual file names.

example...

D:\Capture\C1
D:\Capture\C2
D:\Chunk
15-48-11.000.seq
15-52-08.000.seq

Once the text file is correct double click SEQReader.exe to begin generating *.chunk files. Each *.seq file will take a few minutes.




