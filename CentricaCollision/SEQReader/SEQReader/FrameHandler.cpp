
#include "FrameHandler.h"
#include <algorithm>


// opencv is used only for renderering frames to screen
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>


#define MAGICNUMBER 0xC0FFEE
#define CHANNELOFINTEREST 0
#define EXPOSURE_CUTOFF 100000
//#define EXPOSURE_CUTOFF 200000
#define EXPOSURE_CUTOFF_FULLFRAME 500000
//2,148,113,636




//FrameHandler::FrameHandler(void) 





std::unique_ptr<MapFile> FrameHandler::HandleFrame(const Img::ImageRef& bayer, const MapFile::FrameInfo& camera)
{
	uint32_t width = bayer.width / 2;
	uint32_t height = bayer.height / 2;
	MapFile * mapFile = nullptr;

	if (image_.width != width || image_.height != height)
	{
		counter_ = 0; // counter must be reset

		//image_ is used to describe all 5 buffers
		image_.width = width;
		image_.height = height;

		
		//image_.stride = (image_.width + 15) & -16; //align scanlines to 128 bits
		image_.stride = (image_.width + 7) & -8; //align scanlines to 64 bits
		image_.bpp = 8;
		image_.data = nullptr;

		printf("creating buffers: %d x %d (%d)\n", width,height,image_.stride);

		int sz = image_.size();
		blur_ = std::vector<uninitialized<uint8_t>>(sz);
		mask_ = std::vector<uninitialized<uint8_t>>(sz);
		previous_blur_ = std::vector<uninitialized<uint8_t>>(sz);
		previous_mask_ = std::vector<uninitialized<uint8_t>>(sz);
		channel_ = std::vector<uninitialized<uint8_t>>(sz);		
	}

	if (channel_.empty())
		return nullptr;

	std::swap(mask_,previous_mask_);
	std::swap(blur_,previous_blur_);	

	//extract chosen channel from  bayer 
	image_.data = &channel_[0].value; //set the image buffer	
	Img::ExtractChannel(bayer, image_ , CHANNELOFINTEREST);

	int size = image_.size();
	int simdSize = size >> 4; // the simd instructions work on 16 bytes at a time 
	int blurOrder = 5; // 1<<5 sized kernel
	int mapOrder = 3; // 1<<3 = 8 but we are sampling a quarter size image so the resulting tile size is double this
	int thresholdOrder = 5; // any bits greater than and including 1<<thresholdOrder-1 pass the threshold test


	memcpy(&blur_[0].value, &channel_[0].value, size);
	image_.data = &blur_[0].value; //set the image buffer	
	Img::BoxBlur(&image_, blurOrder);	

	//get a mask of high frequency areas
	Img::AbsoluteDifference(
		(__m128i*)&channel_[0].value ,
		(__m128i*)&blur_[0].value,
		(__m128i*)&blur_[0].value,
		simdSize);


	if(counter_ > 1)
	{
		Img::TernaryDifference(
			(__m128i*) &blur_[0].value,
			(__m128i*) &previous_blur_[0].value,
			(__m128i*) &previous_mask_[0].value,
			(__m128i*) &mask_[0].value,
			simdSize);

		// !!!! HACKS !!!! fill entire mask !!!
		cv::Mat(image_.height,image_.width,CV_8UC1,&mask_[0].value,image_.stride) = cv::Scalar(255); /// HACKS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		if (camera.exposure > EXPOSURE_CUTOFF) // removed
		{
			printf("warning: exposure %d\n", camera.exposure);
		}

		image_.data = &mask_[0].value;
		int mapsize = Img::MapSize(image_, mapOrder);
		unsigned char *map = new unsigned char[mapsize];

		// this is the function that would need to be altered if nearest neighbours want to be included in the tile map
		Img::MapThresholds(image_, mapOrder, map, thresholdOrder); //4 = 16, 5 = 32, 6 = 64...
		//Img::MapDebug(image_, mapOrder, map);

		mapFile = new MapFile(bayer, image_, map, mapOrder+1, camera);

		delete [] map;
		
	}
	else if(counter_  > 0)
	{	
		Img::AbsoluteDifference(
			(__m128i*) &blur_[0].value, 
			(__m128i*) &previous_blur_[0].value,
			(__m128i*) &mask_[0].value,
			simdSize);				
	}


	Render(camera);

	//if(mapFile)
	//{
	//	MapFile::Header *h = mapFile->GetHeader();	
	//	uchar *buf = mapFile->data() + h->dataOffset;
	//	int rows = h->mapUnit*8;
	//	int cols = h->mapUnit;
	//	cv::Mat im(	rows,cols,	CV_8UC1,	buf,	cols);
	//	cv::resize(im,im,cv::Size(im.cols*5,im.rows*5),0,0,CV_INTER_NN);
	//	cv::imshow("peachy",im);
	//}	



	//every 1024 frames and if the exposure is low enough we save a full frame bitmap
	//if((counter_ & 1023) == 0  && camera.exposure < EXPOSURE_CUTOFF_FULLFRAME) 
	//{
	//	SaveFullFrame(bayer,camera);
	//}

	counter_++;

	return std::unique_ptr<MapFile>(mapFile);
}


// opencv code for rendering, this is the only reference to opencv librarys
void FrameHandler::Render(const MapFile::FrameInfo& camera)
{
	std::string winname(camera.IsMaster? "master" : "slave");
	uint8_t* buffer = &channel_[0].value; //blur_; //channel_; //
	cv::Mat image(image_.height, image_.width, CV_8UC1, buffer, image_.stride);
	cv::namedWindow(winname, cv::WINDOW_NORMAL);// Create a window for display.
	cv::imshow(winname, image);
	//cv::waitKey(1); // minimal pause !! now using waitKey in main loop
}

bool FrameHandler::SaveFullFrame(const Img::ImageRef& bayer, const MapFile::FrameInfo& camera)
{
	bool result;
	char path[64];
	FILE *file;
		
	sprintf(path,"C:/Capture/%s_%u_%06u.bmp",
		camera.IsMaster ? "M" : "S",
		time(NULL),
		camera.frameNumber);

	if ((file = fopen(path,"w+b"))!=0)
	{
		result = Img::Bitmap::SaveAsGreyscale(bayer,file);
		fclose(file);
	}

	return result;
}


bool FrameHandler::SaveFrame(const Img::ImageRef& bayer, const Img::ImageRef& mask, unsigned char *map, int order, const  MapFile::FrameInfo& camera)
{
	//Img::ImageRef bayer(
	//	(unsigned char*)frame->ImageBuffer,
	//	frame->Width,
	//	frame->Height,
	//	frame->Width,
	//	frame->BitDepth);

	//tCamera *camera = (tCamera*)frame->Context[0]; 

	int mapSize = Img::MapSize(bayer,order);
	int outBufSize = Img::MappedBufferSize(bayer,order,map);
	int maskBufSize = Img::MappedBufferSize(mask,order-1,map);
	camera.frameNumber; //starts at 2

	unsigned char *outBuf = new unsigned char[outBufSize];
	unsigned char *maskBuf = new unsigned char[maskBufSize];
	
	Img::MapToBuffer(bayer,order,map,outBuf);
	Img::MapToBuffer(mask,order-1,map,maskBuf);

	MapFile::Header header;
	header.magicNumber = MAGICNUMBER;
	header.width= bayer.width;
	header.height= bayer.height;
	header.bitsPerPixel= (short)bayer.bpp;
	header.bayerPattern= -1;//frame->BayerPattern;

	header.mapWidth = Img::MapWidth(bayer,order);
	header.mapHeight= Img::MapHeight(bayer,order);
	header.mapUnit = 1<<order;
	header.mapOffset= sizeof(header);//!!!!!!!!!!!! realign
	header.dataOffset=header.mapOffset + mapSize;//!!!!!!!!!! realign
	header.maskOffset=header.dataOffset + outBufSize;

	
	memcpy(header.cameraID, camera.cameraID, std::min(sizeof(header.cameraID), sizeof(camera.cameraID)));
	header.exposure = camera.exposure;
	header.frameStatus	= camera.frameStatus;
	header.cameraTick = camera.cameraTick;
	//header.systemTick = mTimer.GetTicks() & 0xffffffff;
	header.systemTime = camera.systemTime;// time(NULL);
	header.FrameCount = camera.frameNumber;//-2 because we don't save first 2 frames// counter_;///??????? was internal now passed with cameraInfo

	char path[64];
	FILE *file;
	long long written = 0;
		
	sprintf(path,"C:/Capture/%s_%u_%06u.map",
		camera.IsMaster ? "M" : "S",
		time(NULL),
		camera.frameNumber);

	if ((file = fopen(path,"w+b"))!=0)
	{
		written += fwrite(&header, sizeof(header), 1, file);
		written += fwrite(map, mapSize, 1, file);
		written += fwrite(outBuf, outBufSize, 1, file);
		written += fwrite(maskBuf, maskBufSize, 1, file);
		fclose(file);
	}
	else
	{
		printf("can't open file %s\n",path);
	}

	delete [] outBuf;
	delete [] maskBuf;

	return written == 4 || (written == 2 && outBufSize == 0);
}
