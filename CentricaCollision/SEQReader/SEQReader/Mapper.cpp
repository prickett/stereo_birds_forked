


#include "Img.h"
//#define SANITYCHECKING // activate debug checks
using namespace Img;





	int downsize(int length,int order)
	{
		return (length + (1<<order) - 1) >> order;
	}


	int Img::MapWidth(const Img::ImageRef& img, int order)
	{
		return downsize(img.width, order);
	}
	int Img::MapHeight(const Img::ImageRef& img, int order)
	{
		return downsize(img.height, order);
	}
	int Img::MapStride(const Img::ImageRef& img, int order)
	{
		return (MapWidth(img, order) + 7) >> 3;
	}
	int Img::MapSize(const Img::ImageRef& img, int order)
	{
		return MapStride(img, order) * MapHeight(img, order);
	}
	int Img::MapBitCount(const Img::ImageRef& img, int order, unsigned char *map)
	{
		int unit = 1<<order;
		int w = MapWidth(img,order);
		int h = MapHeight(img,order);
		int count = 0;

		for (int y=0,i=0; y<h; y++)
		{
			int ct = 8;
			for (int x=0; x<w; x+=8, i++)
			{			
				if(x+8>w)
				{
					ct = w-x;
				}
				int n = map[i];
				for(int b=0; b<ct; b++)
				{
					if((n&1)==1) 
						count++;
					n>>=1;
				}
			}
		}
		return count;
	}
	int Img::MappedBufferSize(const Img::ImageRef& img, int order, unsigned char *map)
	{
		return MapBitCount(img,order,map) << order*2;
	}




	int Img::MapDebug(const Img::ImageRef& img, int order, unsigned char *map)
	{
		int unit = 1<<order;
		int w = MapWidth(img,order);
		int h = MapHeight(img,order);

		printf("<map>\n");

		for (int x=0; x<w; x++)
			printf("_");
		printf("\n");

		for (int y=0,i=0; y<h; y++)
		{
			int ct = 8;
			for (int x=0; x<w; x+=8, i++)
			{			
				if(x+8>w)
				{
					ct = w-x;
				}
				int n = map[i];
				for(int b=0;b<ct;b++)
				{
					printf("%s",((n&1)==0) ? " ":".");
					n>>=1;
				}
			}
			printf("|\n");

		}
		printf("<\\map>\n");
		return 0;
	}






//copies the pixels from the image reference to the target, removing any padding
int copyImage(ImageRef* image, unsigned char* target)
{

#ifdef SANITYCHECKING
	if (image->bpp != 8)
		return 0 & printf("image not 8 bpp (%u)\n",image->bpp);	
#endif
	
	unsigned char *scanline = image->data;
	int stride = image->stride;
	int width = image->width;
	int height =image->height;
	int copied = 0;

	for(int y=0; y<height; y++)//for(int y=0; y<image->height; y++)
	{
		memcpy(target,scanline,width);
		scanline+=stride;
		target+=width;
		copied+=width;
	}
	return copied;
}

////returns true if any part of the passed rectangle is over the threshold shift
//bool copyRect(ImageRef* image, int threshold)
//{
//
//#ifdef SANITYCHECKING
//	bool good = false;
//
//	if ((threshold & -8)!=0)
//		printf("threshold out of 0 to 7 range\n");
//	else if (image->alignment()<8)
//		printf("%s: image not 64 bit aligned (%u)\n",__FUNCTION__,image->alignment());	
//	else if (image->bpp != 8)
//		printf("image not 8 bpp (%u)\n",image->bpp);	
//	else
//		good=true;
//
//	if(!good)
//		return false;
//#endif
//
//	int width64,stride64,remainder;
//	width64 = image->width>>3; 
//	remainder = image->width & 7;
//	stride64 = image->stride >> 3;
//	
//	const unsigned long long ONE = 0x101010101010101;
//	long long mask8, *scan8,*pix8;
//	char mask, *pix;
//	scan8 = (long long*)image->image;
//
//	mask = ~((1<<threshold) - 1);
//	mask8 = (ONE*(unsigned char)mask);
//
//
//	for(int y=0; y<image->height; y++)//for(int y=0; y<image->height; y++)
//	{
//		pix8 = scan8;
//		scan8 += stride64;
//		for(int i=0; i<width64; i++, pix8++)//for(int i=0; i<image->width; i++)
//		{
//			if(*pix8 & mask8)
//			{
//				return true;
//			}
//		}
//		if(remainder)
//		{
//			pix = (char*) pix8;
//			for(int i=0; i<remainder; i++, pix++)//for(int i=0; i<image->width; i++)
//			{
//				if(*pix & mask)
//				{
//					return true;
//				}
//			}
//
//		}
//	}
//	return false;
//}





//returns true if any part of the passed rectangle is over the threshold shift
bool thresholdRect(ImageRef* image, int threshold)
{
	
#ifdef SANITYCHECKING
	bool good = false;

	if ((threshold & -8)!=0)
		printf("threshold out of 0 to 7 range\n");
	else if (image->alignment()<8)
		printf("%s: image not 64 bit aligned (%u)\n",__FUNCTION__,image->alignment());	
	else if (image->bpp != 8)
		printf("image not 8 bpp (%u)\n",image->bpp);	
	else
		good=true;

	if(!good)
		return false;
#endif

	int width64,stride64,remainder;
	width64 = image->width / 8; 
	remainder = image->width & 7;
	stride64 = image->stride / 8;
	
	const unsigned long long ONE = 0x101010101010101;
	long long mask8, *scan8,*pix8;
	char mask, *pix;
	scan8 = (long long*)image->data;

	mask = ~((1<<threshold) - 1);		
	mask8 = (ONE*(unsigned char)mask);


	for(int y=0; y<image->height; y++)
	{
		pix8 = scan8;
		scan8 += stride64;
		for(int i=0; i<width64; i++, pix8++)
		{
			if(*pix8 & mask8)
			{
				return true;
			}
		}
		if(remainder)
		{
			pix = (char*) pix8;
			for(int i=0; i<remainder; i++, pix++)
			{
				if(*pix & mask)
				{
					return true;
				}
			}

		}
	}
	return false;
}

int Img::MapToBuffer(const Img::ImageRef& image, int order, unsigned char *map, unsigned char *target)
{
	int unit = 1<<order;
	int w = (image.width+unit-1) / unit;
	int h = (image.height+unit-1) / unit;
	int stride = image.stride * unit; // stride covers multiple rows

	//printf("%u x %u (%u)\n", image.width,image.height,image.stride);

	ImageRef gridsqr = image; //get a local copy of the image reference 
	gridsqr.height = unit;
	unsigned char *scanline = image.data;

	for (int y=0, i=0; y<h; y++)
	{
		gridsqr.height = y+1<h? unit : image.height - y*unit;
		gridsqr.data = scanline;
		gridsqr.width = unit;
		scanline += stride;

		for (int x=0; x<w; x+=8,i++)
		{
			int ct = x+8 > w? w-x : 8;
			int n = map[i];
			for (int b=0;b<ct;b++)
			{
				if((n&(1<<b))!=0)
					target += copyImage(&gridsqr,target);
				gridsqr.data += unit;
			}
		}
	}
	return 0;
}


int Img::MapToBufferX(const Img::ImageRef& image, int order, unsigned char *map, unsigned char *target)
{
	int unit = 1<<order;
	int w = (image.width+unit-1) / unit;
	int h = (image.height+unit-1) / unit;
	int stride = image.stride * unit;
	

	ImageRef gridsqr = image; //get a local copy of the image reference 
	gridsqr.height = unit;
	unsigned char *scanline = image.data;

	for (int y=0, i=0; y<h; y++)
	{ 
		// if necessary adjust height for the last row
		gridsqr.height = y+1<h? unit : image.height - y*unit; 
		gridsqr.data = scanline;
		gridsqr.width = unit;
		scanline += stride;

		int x, n, ct = 8;
		for (x=0; x<w; x+=8, i++)
		{
			ct = x+8 >= w? w-x-1 : 8;
			n = map[i];
			for (int b=0;b<ct;b++)
			{

				if((n&(1<<b))!=0)
					target += copyImage(&gridsqr,target);
				gridsqr.data += unit;
			}
		}
		// handle end of row which might not be aligned
		gridsqr.width = image.width - (x+ct-8)*unit;
		if(gridsqr.width > 0 && (n&(1<<ct))!=0)
			target += copyImage(&gridsqr,target);

	}
	return 0;
}

int Img::MapThresholds(const ImageRef& image, int order, unsigned char *map, int threshold)
{

#ifdef SANITYCHECKING

	bool good = false;
	if ((threshold & -8)!=0)
		printf("threshold must be in the range of 0 to 7");
	else if (image.alignment()<8)
		printf("%s: image not 64 bit aligned (%u bit aligned)\n",__FUNCTION__,image.alignment()*8);	
	else if (image.bpp != 8)
		printf("image must be 8 bpp (%u bpp)\n",image.bpp);	
	else
		good=true;

	if(!good)
		return false;
#endif

	int unit = 1 << order;
	int w = (image.width+unit-1) / unit;
	int h = (image.height+unit-1) / unit;
	int stride = image.stride * unit;

	//memset(map,0,mapsize(image->width,image->height,order));

	ImageRef gridsqr = image; //get a copy of image reference
	unsigned char *scanline = image.data;
	gridsqr.height = unit;

	for (int y=0, i=0; y<h; y++)
	{
		gridsqr.height = y+1<h? unit : image.height - y*unit;
		gridsqr.data = scanline;
		gridsqr.width = unit;

		scanline += stride;

		for (int x=0; x<w; x+=8,i++)
		{
			int ct = x+8 > w? w-x : 8;

			//gridsqr.width=wr;!!!!!!!!!!!!!!!!!!!!
		
			int n = 0;
			for (int b=0;b<ct;b++)
			{
				if(thresholdRect(&gridsqr, threshold))
					n|=(1<<b);
				gridsqr.data += unit;
			}
			map[i] = n;
		}
	}


	return 0;

}
