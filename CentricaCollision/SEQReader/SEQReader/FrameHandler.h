#pragma once

#include <memory>
#include "Img.h" 
#include "MapFile.h"
#include "MemoryPool.h" // used for uninitialised<T>


class FrameHandler
{
public:	
	//FrameHandler(int bayerWidth,int bayerHeight);
	//FrameHandler(void); 
	//~FrameHandler(void);

	std::unique_ptr<MapFile> HandleFrame(const Img::ImageRef& bayer, const MapFile::FrameInfo& camera);

	
	uint32_t GetWidth(void){ return image_.width; }
	uint32_t GetHeight(void){ return image_.height; }
	uint32_t GetStride(void){ return image_.stride; }

private:
	//FrameHandler(FrameHandler const &);
	//FrameHandler& operator=(FrameHandler const &);
	void Render(const MapFile::FrameInfo& camera);

	bool SaveFrame(const Img::ImageRef& bayer, const Img::ImageRef& mask, unsigned char *map, int order, const MapFile::FrameInfo& camera);
	bool SaveFullFrame(const Img::ImageRef& bayer, const MapFile::FrameInfo& camera);

	unsigned int counter_;
	Img::ImageRef image_;
	std::vector< uninitialized<uint8_t> > channel_;
	std::vector< uninitialized<uint8_t> > blur_;
	std::vector< uninitialized<uint8_t> > previous_blur_;
	std::vector< uninitialized<uint8_t> > mask_;
	std::vector< uninitialized<uint8_t> > previous_mask_;
};

