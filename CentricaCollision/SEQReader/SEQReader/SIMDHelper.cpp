
#include "SIMDHelper.h"


using namespace std;

bool SIMDHelper::IsSSE2Available()
{
	//Check for SSE2
	unsigned sse2_id = 67108864;
	int CPUInfo[4] = {-1};

	__cpuid(CPUInfo, 1);
	return ((CPUInfo[3] & sse2_id) == sse2_id);
}

int SIMDHelper::GetLogicalCoreCount()
{
	int CPUInfo[4] = {-1};

	__cpuid(CPUInfo, 1);
	return (CPUInfo[1] >> 16) & 0xff;
}

bool SIMDHelper::IsAligned(void * p)
{
	return (((unsigned long long)p) & 15)==0;
}


	void SIMDHelper::Cout_m128_sp(__m128 *n)
	{
		for (int i = 0; i<4; i++)
		{
			cout <<  n->m128_f32[i] << " |"; 
		}
		cout << endl;
	}

	void SIMDHelper::Cout_m128i_i16x(__m128i *n)
	{
		cout << hex << setfill('0');
		for (int i = 0; i<8; i++)
		{
			cout << setw(4) << n->m128i_i16[i] << " |"; 
		}
		cout << dec << endl;
	}

	void SIMDHelper::Cout_m128i_u8(__m128i *n)
	{
		cout << hex << setfill('0');
		for (int i = 0;i<16;i+=2)
		{
			cout << setw(2) << (int)n->m128i_u8[i] << ' ' << setw(2) << (int)n->m128i_u8[i+1] << '|'; 
		}
		cout << dec << endl;
	}

	void SIMDHelper::Cout_m128i_i16(__m128i *n)
	{
		cout << setfill(' ');
		for (int i = 0;i<8;i++)
		{
			cout << setw(4) << n->m128i_i16[i] <<  '|'; 
		}
		cout << endl;
	}

	void SIMDHelper::Cout_m128i_i32(__m128i *n)
	{
		cout << setfill(' ');
		for (int i = 0;i<4;i++)
		{
			cout << setw(4) << n->m128i_i32[i] <<  '|'; 
		}
		cout << endl;
	}
