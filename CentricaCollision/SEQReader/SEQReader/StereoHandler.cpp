
#include "StereoHandler.h"

#include <iostream>
#include <sstream>

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
//#define MAXIMUM_DIFFERENCE 10000
//#define PURGE_COUNT 5

const std::string StereoHandler::temp_suffix_ = ".current";
const std::string StereoHandler::suffix_ = ".chunk";

using namespace std;

StereoHandler::StereoHandler(const char * save_folder, const char * save_prefix, int frames_per_file)
	:frame_count_(0)
	,file_count_(0)
	,frames_per_file_(frames_per_file)
	,master_tic_(0)
	,slave_tic_(0)
	,file_(0)
	,offsets_(frames_per_file*2)
	//,file_offset_table_(0)
{
	memset(&header_, 0, sizeof(header_));

	// note: boost::filesystem might be a better choice for file handling
	string folder(save_folder);

	// ensure we have a trailing backslash
	if (!folder.empty() && folder.back() != '\\')
		folder.push_back('\\');

	// use a windows api call to check the directory is valid
	DWORD att = GetFileAttributesA(folder.c_str());
	if (att == INVALID_FILE_ATTRIBUTES || (att & FILE_ATTRIBUTE_DIRECTORY) == 0)
		throw std::runtime_error("Bad path\n");

	stringstream ss;
	ss << folder;	

	// if not present add a trailing back slash
	if (!folder.empty() && folder.back() != '\\') 
		ss << '\\';

	// if provided add the save prefix
	if (save_prefix) 
		ss << save_prefix << '_';


	save_prefix_ = ss.str();
}



StereoHandler::~StereoHandler(void)
{
	flush();

	for (BayerMapPair& f : master_list_)
		pool_.Release(f.bayer_data);

	for (BayerMapPair& f : slave_list_)
		pool_.Release(f.bayer_data);
}

bool StereoHandler::handle(const Img::ImageRef& bayer, const MapFile::FrameInfo& frame_info, FrameHandler& handler, std::list<BayerMapPair>& frames)
{
	BayerMapPair frame;

	frame.map_file = move(handler.HandleFrame(bayer, frame_info));

	if (!frame.map_file) // if the frame handler has not recieved enough files it returns null
		return false;

	frame.bayer_data = pool_.Acquire();

	if (!frame.bayer_data.data())
	{
		printf("Memory pool out of blocks\n");
		return false;
	}

	frame.bayer = bayer;
	frame.bayer.data = frame.bayer_data.data();
	memcpy(frame.bayer.data, bayer.data, bayer.size());

	frames.emplace_back(move(frame));

	return true;
}

void StereoHandler::Add(const Img::ImageRef& bayer, const MapFile::FrameInfo& frame_info)//, std::unique_ptr<MapFile>&& map_file)
{
	try
	{ 
	if (bayer.size() == 0)
		return;

	// if required initialise the memory pool and frame handlers
	if (bayer.size() != pool_.BlockSize())
	{ 
		lock_guard<mutex> lock(access_mutex_);	
		pool_ = Pool<uint8_t>(bayer.size(), POOL_SIZE);
	}

	FrameHandler& handler = frame_info.IsMaster ? master_handler_ : slave_handler_;
	list<BayerMapPair>& frames = frame_info.IsMaster ? master_list_ : slave_list_;
	BayerMapPair frame;

	frame.map_file = move(handler.HandleFrame(bayer, frame_info));

	if (!frame.map_file) // if the frame handler has not recieved enough files it returns null
		return;

	// scope for mutex
	{ 
		lock_guard<mutex> lock(access_mutex_);

		// if there are no buffers available we have to free one
		if (pool_.AvailableCount() == 0)
		{
			if (master_list_.size() > slave_list_.size())
			{
				printf("popping a master frame.\n");
				pool_.Release(master_list_.front().bayer_data);
				master_list_.pop_front();
			}
			else if (!slave_list_.empty())
			{
				printf("popping a slave frame.\n");
				pool_.Release(slave_list_.front().bayer_data);
				slave_list_.pop_front();
			}
			else
			{
				printf("Unable to free memory block.\n");
				return;
			}
		}

		frame.bayer_data = pool_.Acquire();
	}


	if (!frame.bayer_data.data())
	{
		printf("Memory pool out of blocks\n");
		return;
	}

	frame.bayer = bayer;
	frame.bayer.data = frame.bayer_data.data();
	memcpy(frame.bayer.data, bayer.data, bayer.size());

	{
		lock_guard<mutex> lock(access_mutex_);
		frames.emplace_back(move(frame));
		pairAndSave();		
	}
	}
	catch (exception& e)
	{
		printf(e.what());
	}
	
}

	
//returns the difference between the passed tick and the current tick
uint64_t getDifAndSwap(MapFile* mapFile, uint64_t &tic)
{
	uint64_t t = tic;
	tic = mapFile->GetHeader()->cameraTick;	
	return tic - t; // overflow does not affect difference
}




// finalise file
void StereoHandler::flush(void)
{
	if (file_)
	{
		string old_name = save_path_ + temp_suffix_;
		string new_name = save_path_ + suffix_;
		
		fseek(file_, 0, SEEK_SET);
		fwrite(&header_, sizeof(header_), 1, file_); // rewrite the updated header	
		fwrite(&offsets_[0], sizeof(offsets_[0]), header_.count*2, file_); // write the frame offsets	
		fclose(file_);
		file_ = 0;		
		
		rename(old_name.c_str(), new_name.c_str());
	}

}

bool StereoHandler::savePairedFrame(BayerMapPair& master_frame, BayerMapPair& slave_frame)
{
	if (!file_)
	{
		stringstream ss;

		if (file_count_ == 0) // first run add in initial timestamp to the save prefix
		{
			ss << save_prefix_ << master_frame.map_file->GetHeader()->systemTime.sec << '_';
			save_prefix_ = ss.str();
			ss = stringstream();
		}

		// construct filename from prefix, file count and timestamp 
		ss << save_prefix_
			<< setfill('0') << setw(6) << file_count_ + 1 << "_"
			<< master_frame.map_file->GetHeader()->systemTime.sec;

		save_path_ = ss.str();

		//temp suffix is used whilst the file is being written
		string filename = save_path_ + temp_suffix_; 

		file_ = fopen(filename.c_str(), "w+b");	

		if (!file_)
			return false;
	
		cout << save_path_ << endl;
		
		++file_count_;

		memset(&header_, 0, sizeof(header_));
		header_.magic_number = StereoHandler::MAGIC_NUMBER;
		header_.start_time = master_frame.map_file->GetHeader()->systemTime.sec;
		header_.first_frame = frame_count_;

		fwrite(&header_, sizeof(header_), 1, file_);

		// save the master full frame
		header_.master_fullframe_offset = sectorAlign(sizeof(header_) + sizeof(uint32_t) * frames_per_file_ * 2);
		fseek(file_, header_.master_fullframe_offset, SEEK_SET);
		Img::Bitmap::SaveAsGreyscale(master_frame.bayer, file_);
		header_.master_fullframe_size = ftell(file_) - header_.master_fullframe_offset;

		// save the slave full frame
		header_.slave_fullframe_offset = sectorAlign(ftell(file_));
		fseek(file_, header_.slave_fullframe_offset, SEEK_SET);
		Img::Bitmap::SaveAsGreyscale(slave_frame.bayer, file_);
		header_.slave_fullframe_size = ftell(file_) - header_.slave_fullframe_offset;

	}

	bool success = true;
	uint32_t pos , index = header_.count * 2;

	// write the master map to the file
	offsets_[index++] = pos = sectorAlign(ftell(file_));
	fseek(file_, pos, SEEK_SET);
	master_frame.map_file->GetHeader()->FrameCount = frame_count_;
	success &= master_frame.map_file->Save(file_);
	
	// write the slave map to the file
	offsets_[index++] = pos = sectorAlign(ftell(file_));
	fseek(file_, pos, SEEK_SET);
	slave_frame.map_file->GetHeader()->FrameCount = frame_count_;
	success &= slave_frame.map_file->Save(file_);

	// fill out the end time and last frame fields for every frame in lieu of a possible flush
	header_.end_time = master_frame.map_file->GetHeader()->systemTime.sec;
	header_.last_frame = frame_count_;

	++header_.count;
	++frame_count_; // increment the frame count

	if ((header_.count  >= frames_per_file_) | (pos > MAX_FILESIZE))
	{
		flush();
	}

	return success;
}


bool StereoHandler::saveDroppedFrame(MapFile* frame)//std::unique_ptr<MapFile> &frame)
{
	frame->GetHeader()->FrameCount = frame_count_;
	return frame->Save();
}


void StereoHandler::pairAndSave()
{
	uint64_t mtic, stic, mdif, sdif, dif;
	while (!master_list_.empty() && !slave_list_.empty())
	{
		BayerMapPair& mframe = master_list_.front();
		BayerMapPair& sframe = slave_list_.front();

		//get the last saved tick values
		mtic = master_tic_;
		stic = slave_tic_;

		//get the difference between last saved and current tick values
		mdif = getDifAndSwap(mframe.map_file.get(), mtic); //mtic returns as the current tick
		sdif = getDifAndSwap(sframe.map_file.get(), stic); //stic ditto

		// get the difference of the differences
		// !! this logic did not sync at start
		//if (frame_count_)
		//  dif = abs(int64_t(mdif - sdif));
		//else
		//  dif=0;


		dif = abs(int64_t(mdif - sdif));

		//cout << "(" << dif <<")" << endl;
		if (dif < DROP_THRESHOLD) // close enough, these frames are paired
		{
			savePairedFrame(mframe, sframe); 			

			pool_.Release(sframe.bayer_data);
			slave_list_.pop_front();

			pool_.Release(mframe.bayer_data);
			master_list_.pop_front();

			master_tic_ = mtic;
			slave_tic_ = stic;
		}
		else if (mdif>sdif) //master camera has dropped a frame
		{
			cout << "master drop (" << dif << ")\n";

			pool_.Release(slave_list_.front().bayer_data);			
			slave_list_.pop_front();			
		}
		else //slave camera has dropped a frame
		{
			cout << "slave drop (" << dif << ")\n";

			pool_.Release(master_list_.front().bayer_data);
			master_list_.pop_front();
		}
	}
}


void StereoHandler::purgeFrames(std::deque<std::unique_ptr<MapFile>> &frames)
{
	while (frames.size() > 0)
	{
		saveDroppedFrame(frames.front().get());
		frames.pop_front();
	}
}


uint32_t StereoHandler::sectorAlign(uint32_t n)
{
	return (n + SECTOR_ALIGNMENT - 1) & -SECTOR_ALIGNMENT;
}
