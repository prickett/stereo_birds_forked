

#include "Img.h"
#define SANITYCHECKING // activate debug checks



using namespace Img;

long LSB(long n);
long long LSB(long long n);

Img::ImageRef::ImageRef(void)
	: data(nullptr), width(0), height(0), stride(0), bpp(0)
{}

Img::ImageRef::ImageRef(unsigned char* image, long width, long height, long stride, long bpp)
	: data(image), width(width), height(height), stride(stride), bpp(bpp)
{}


long Img::ImageRef::alignment(void) const
{
	long long a = LSB((long long) this->data);
	long s = LSB(this->stride);

	return static_cast<long>( a<s ? a:s );
}

long Img::ImageRef::size(void) const
{
	return this->stride*this->height;
}






bool checkKernel(unsigned char *kernel, int count, int sum)
{
	for(int i = 0;i<count;i++)
		sum-=*kernel++;
	return sum == 0;
}


void BoxBlur1D(unsigned char *target,int width,int height, int incr, int stride,int order)
{
	//unsigned char *pix;
	int sum = 0;
	
	int kSz = 1 << order;
	int kMask = kSz -1;
	int kHlf = kSz>>1;

	int * kernel  = (int*) _alloca( kSz );

	for(int y = 0;y<height;y++)
	{
		//pix = target;
		//target += stride;
		//int k=0;

		////initialise the kernel using mirror edge treatment
		//sum = 0;		
		//for(int i=0; i<kHlf; i++)
		//{
		//	sum += kernel[kHlf - (i+1)] = kernel[kHlf + i] = pix[i];
		//}
		//sum<<=1;//double the sum

		////middle section, where the work is done
		//for(int i = 0; i < leadOut; i += incr, k = (k+1) & kMask)
		//{
		//	sum -= kernel[k];
		//	sum += kernel[k] = pix[i+kHlf];
		//	pix[i] = sum >> order;
		//}

		////lead out using mirror edge treatment
		//for(int i = leadOut, j = width-1 ; i<width; i++, j--)
		//{
		//	int k = i & kMask;
		//	sum -= kernel[k];
		//	sum += kernel[k] = pix[j];
		//	pix[i]= sum >> order;
		//}
	}

	printf("not written yet");
	



}

//Performs an in place quick box blur on the passed image.
//order is the power of 2 used to size the kernel.
//the algorithm uses mirror edge treatment
void Img::BoxBlur(ImageRef *image, int order)
{
//#ifdef SANITYCHECKING
//	bool good = false;
//	if(image->bpp != 8)
//		printf("images must be 8 bpp");
//	//else if(image->alignment() < 8)
//	//	printf("image must be at least 64 bit alighned");
//	else
//		good = true;
//	if (!good)
//		return;
//#endif
//	printf("HEre bE DrAgoNs!!!!\n");

	unsigned char *pix, *row;	

	int width = image->width;
	int height = image->height;
	int stride = image->stride;

	int kSz = 1<<order; //always a power of two
	int kMask = kSz -1;
	int kHlf = kSz>>1;
	//int* kernel = new int[kSz];
	unsigned char * kernel  = (unsigned char*) _alloca( kSz );

	int leadOut;  

	int sum;
	

	//horizontal blur
	row = image->data;
	leadOut = width - kHlf;
	
	//printf("\n%u\t%u\t%u\t%u\n",kSz,kHlf,kMask,leadOut);
	for(int y = 0; y < height; y++)
	{
		pix = row;
		row+=stride;
	
		//initialise the kernel using mirror edge treatment
		sum = 0;		
		for(int i=0; i<kHlf; i++)//example order 3 kernel -> 32100123
		{
			sum += kernel[kHlf - (i+1)] = kernel[kHlf + i] = pix[i];
		}
		sum<<=1;//double the sum

		if(!checkKernel(kernel,kSz,sum))
			printf("bad kernel 1");

		//middle section, where the work is done
		for(int i = 0;i<leadOut;i++)
		{
			int k = i & kMask;
			sum -= kernel[k];
			sum += kernel[k] = pix[i+kHlf];
			pix[i] = sum >> order;
		}

		if(!checkKernel(kernel,kSz,sum))
			printf("bad kernel 2");

		//lead out using mirror edge treatment
		for(int i = leadOut, j = width-1 ; i<width; i++, j--)
		{
			int k = i & kMask;
			sum -= kernel[k];
			sum += kernel[k] = pix[j];
			pix[i]= sum >> order;
		}

		if(!checkKernel(kernel,kSz,sum))
			printf("bad kernel 3");
	}

	//vertical blur
	row = image->data;
	leadOut = height - kHlf;
	for(int x = 0; x < width; x++)
	{
		pix = row;
		row++;
	
		//initialise the kernel using mirror edge treatment
		sum = 0;		
		for(int i=0; i<kHlf; i++)//example order 3 kernel -> 32100123
		{
			sum += kernel[kHlf - (i+1)] = kernel[kHlf + i] = pix[i*stride];
		}
		sum<<=1;//double the sum

		//middle section, where the work is done
		for(int i = 0; i<leadOut;i++)
		{
			int k = i & kMask;
			sum -= kernel[k];
			sum += kernel[k] = pix[(i+kHlf)*stride];
			pix[i*stride] = sum >> order;
		}

		//lead out using mirror edge treatment
		for(int i = leadOut, j = height-1 ; i<height; i++, j--)
		{
			int k = i & kMask;
			sum -= kernel[k];
			sum += kernel[k] = pix[j*stride];
			pix[i*stride]= sum >> order;
		}
	}




	//delete [] kernel; //free our temporary kernel
	//no need to delete a stack allocated variable 
}




void Img::ExtractChannel(const ImageRef& bayer, ImageRef& target, int channel)
{
#ifdef SANITYCHECKING

	bool good = false;
	if(bayer.width  > target.width*2 || bayer.height  > target.height*2)
		printf("target buffer too small\n");	
	//else if(bayer->bpp != 8 || target->bpp != 8)
	//	printf("images must be 8 bpp\n");
	else
		good = true;
	if (!good)
		return;
#endif

	unsigned long width, height, stride;
	unsigned char *src, *tgt, *srcRow, *tgtRow;
	srcRow = bayer.data + (channel & 1) +  ((channel & 2)>>1) * bayer.stride;
	tgtRow = target.data;

	height = bayer.height / 2;
	width = bayer.width / 2;
	stride = bayer.stride * 2;

	for (unsigned int y = 0;y< height;y++)
	{
		src=srcRow;
		srcRow += stride;
		tgt=tgtRow;
		tgtRow += target.stride;
		for (unsigned int x=0; x<width; x++, src+=2,tgt++)
		{
			*tgt = *src;
		}
	}
}

inline void Img::AbsDif(__m128i* srcA, __m128i* srcB, __m128i* tgt)
{
	*tgt = _mm_or_si128( _mm_subs_epu8(*srcA,*srcB) , _mm_subs_epu8(*srcB,*srcA));
}




//populates a buffer with the absolute difference of two buffers
void Img::AbsoluteDifference(__m128i* srcA, __m128i* srcB, __m128i* tgt,  unsigned long count)
{
	if (((int)srcA & 15) != 0 || ((int)srcB & 15) != 0 || ((int)tgt & 15) != 0)
	{
		printf("bad buffers\n");
		return;
	}

	for(unsigned int i = 0; i<count; i++, tgt++, srcA++, srcB++)
	{
		//*tgt = _mm_sad_epu8(*srcA,*srcB); //<-- thought it would work! but sadly no :(

		//subtracts, saturates then combines tgt=(a-b)|(b-a)
		*tgt = _mm_or_si128(_mm_subs_epu8(*srcA,*srcB), _mm_subs_epu8(*srcB,*srcA));
	}
}

	//populates a buffer with the absolute difference of two buffers
void Img::TernaryDifference(__m128i* srcA, __m128i* srcB, __m128i* srcC, __m128i* tgt, unsigned long count)
{
	for(unsigned int i = 0; i<count; i++, tgt++, srcA++, srcB++, srcC++)
	{
		//subtracts, saturates then combines d=(a-b)|(b-a)
		__m128i d = _mm_or_si128(_mm_subs_epu8(*srcA,*srcB), _mm_subs_epu8(*srcB,*srcA));

		*tgt = _mm_subs_epu8(d,*srcC);
	}
}

//populates a buffer with the saturated difference of two buffers
void Img::Difference(__m128i* srcA, __m128i* srcB, __m128i* tgt,  unsigned long count)
{
	//const __m128i HIMASK = _mm_set1_epi8(0xfe);
	//const __m128i MIDVAL = _mm_set1_epi8(0x7f);

	for(unsigned int i = 0; i<count; i++, tgt++, srcA++, srcB++)
	{
		*tgt = _mm_subs_epu8(*srcA,*srcB);
		////no support for a 8 bit shift... so using 128 bit right shift with a mask to stop underflow
		//__m128i a =_mm_srli_epi64( _mm_and_si128(*srcA,HIMASK),1 );
		//__m128i b =_mm_srli_epi64( _mm_and_si128(*srcB,HIMASK),1 );

		//*tgt = _mm_add_epi8 (MIDVAL, _mm_sub_epi8 (a,b));
	}
}

void Img::SplitScanLine(__m128i* src, __m128i* tgtA, __m128i* tgtB, unsigned long width)
{
	const __m128i EVNBYTS=_mm_set1_epi16(0xff); 
	const __m128i ODDBYTS =_mm_set1_epi16((short)0xff00);

	__m128i lo,hi; //to store source values
	unsigned long count = width >> 5; 

	// Spot the difference with the following two loops
	// The second loop uses slower compound unaligned read functions 
	if(((unsigned long long) src & 0xf) == 0) //aligned source!
	{
		for(unsigned int i=0;i<count;i++)
		{
			//load 256 bits from the aligned buffer
			lo = _mm_load_si128 (src++); 
			hi = _mm_load_si128 (src++);

			//mask the even bytes and pack into the first output channel
			*tgtA++ = _mm_packus_epi16(
				_mm_and_si128(lo,EVNBYTS),
				_mm_and_si128(hi,EVNBYTS));

			//mask and shift the odd bytes then pack into the second output channel
			*tgtB++ = _mm_packus_epi16(
				_mm_srli_epi16(_mm_and_si128(lo,ODDBYTS),8), 
				_mm_srli_epi16(_mm_and_si128(hi,ODDBYTS),8));
		}
	}
	else //unaligned source!
	{
		for(unsigned int i=0;i<count;i++)
		{
			//load 256 bits from the unaligned buffer
			lo = _mm_loadu_si128 (src++); 
			hi = _mm_loadu_si128 (src++);

			//mask the even bytes and pack into the first output channel
			*tgtA++ = _mm_packus_epi16(
				_mm_and_si128(lo,EVNBYTS),
				_mm_and_si128(hi,EVNBYTS));

			//mask and shift the odd bytes then pack into the second output channel
			*tgtB++ = _mm_packus_epi16(
				_mm_srli_epi16(_mm_and_si128(lo,ODDBYTS),8), 
				_mm_srli_epi16(_mm_and_si128(hi,ODDBYTS),8));
		}
	}
}
//
//void Img::SplitTheBayer(tPvFrame* in, SplitBayer* out)
//{
//	unsigned long w = in->Width;
//	unsigned long h = in->Height;
//	unsigned long stride = w >> 5; //destination stride
//	unsigned char* byr = (unsigned char*)in->ImageBuffer; 
//
//	//set up destination pointers
//	__m128i *chan0 =  (__m128i*)out->Channels[0];
//	__m128i *chan1 =  (__m128i*)out->Channels[1];
//	__m128i *chan2 =  (__m128i*)out->Channels[2];
//	__m128i *chan3 =  (__m128i*)out->Channels[3];
//
//	out->X=0;
//	out->Y=0;
//	out->Width = w;
//	out->Height = h;
//	out->FrameCount = in->FrameCount;
//	out->CameraTimestamp = in->TimestampLo;
//	out->SystemTimestamp = 0xffffffff;
//	out->Context = in->Context[0];
//
//	for (unsigned long y= 0;y<h;y+=2)
//	{
//		SplitScanLine((__m128i*)(byr + w * (y)),   chan0, chan1, w);
//		SplitScanLine((__m128i*)(byr + w * (y+1)), chan2, chan3, w);
//		chan0+=stride;
//		chan1+=stride;
//		chan2+=stride;
//		chan3+=stride;
//	}
//
//	//This is roughly the equivalent without using intrinsics
//	//int index= 0;
//	//for (unsigned long y= 0;y<h;y+=2)
//	//{
//	//	unsigned char* t = byr + w * y;
//	//	unsigned char* u = t + w;
//	//	for (int x=0;x<w;x+=2)
//	//	{
//	//		out->Channels[0][index] = *t++;
//	//		out->Channels[1][index] = *t++;
//	//		out->Channels[2][index] = *u++;
//	//		out->Channels[3][index] = *u++;
//	//		index++;
//	//	}
//	//}
//
//}




template <class T>
void Swap(T* a, T* b)
{
	T* swp = a;
	a = b;
	b = swp;
}


long LSB(long n)
{
	return (n & n-1)^n;
}
long long LSB(long long n)
{
	return (n & n-1)^n;
}

