﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace CustomControls
{
    public class ZoomBox : PictureBox
    {
        //Overrides PictureBox OnPaint to render the Image at a specified Offset and Zoom.
        //Provides methods to translate between pixel space and control space
        public event EventHandler ViewChanged;

        private Point grab = Point.Empty;
        private Point _offset = Point.Empty;
        private Image _image = null;
        private float _zoom = 1f;


        public ZoomBox()
        {

            using (var g = CreateGraphics())
            {
                InterpolationMode = g.InterpolationMode;
                SmoothingMode = g.SmoothingMode;
            }
            InitializeComponent();
        }

        public InterpolationMode InterpolationMode { get; set; }
        public SmoothingMode SmoothingMode { get; set; }

        protected override void OnPaint(PaintEventArgs pe)
        {
            if (Image != null)
            {
                var g = pe.Graphics;
                g.SmoothingMode = SmoothingMode;
                g.InterpolationMode = InterpolationMode;
                g.DrawImage(Image, _offset.X, _offset.Y,
                    (int)(Image.Width * _zoom),
                    (int)(Image.Height * _zoom));
            }
            base.OnPaint(pe);
        }

        //take Image for ourselves, ensures base never has an image to render
        new public Image Image
        {
            get { return _image; }
            set
            {
                _image = value;
                SetZoom(Width >> 1, Height >> 1, _zoom);
                Invalidate();
            }
        }



        public Point Offset
        {
            get { return _offset; }
            set
            {
                _offset = value;
                if (Image != null)
                {
                    _offset.X = confineOffset(value.X, Width, (int)(Image.Width * _zoom));
                    _offset.Y = confineOffset(value.Y, Height, (int)(Image.Height * _zoom));
                    OnZoomChanged(new EventArgs());
                }

            }
        }

        public void ZoomToFit()
        {
            if (Image != null && Width>0 && Height >0)
            {
                float rw = (float)Width / Image.Width;
                float rh = (float)Height / Image.Height;
                float zm = Math.Min(rw,rh);
                SetZoom(0,0,zm);
                Invalidate();
            }
        }

        public virtual void OnZoomChanged(EventArgs e)
        {
            var handler = ViewChanged;
            if (handler != null) //using a local copy to avoid possible race condition
            {  
                handler(this, e);
            }
        }

        public float Zoom
        {
            get { return _zoom; }
            set
            {
                SetZoom(Width >> 1, Height >> 1, value);
            }
        }

        private void SetZoom(int x, int y, float value)
        {
            value = Math.Max(float.Epsilon, value);
            float delta = -value / _zoom + 1f;
            _zoom = value;

            x = _offset.X + (int)((x - _offset.X) * delta);
            y = _offset.Y + (int)((y - _offset.Y) * delta);
            Offset = new Point(x, y); //this calls Invalidate            
        }



        protected override void OnResize(EventArgs e)
        {
            SetZoom(Width >> 1, Height >> 1, _zoom);
            Invalidate();
            base.OnResize(e);
        }

        protected override void OnMouseEnter(EventArgs e)
        {
            base.Focus();
            base.OnMouseEnter(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle)
            {
                grab = e.Location;
            }
            base.OnMouseDown(e);
        }


        protected override void OnMouseWheel(MouseEventArgs e)
        {
            if (Image != null)
            {
                float mult = 1f + Math.Abs((float)e.Delta / 1000f);
                if (e.Delta > 0)
                    mult = 1f / mult;

                SetZoom(e.X, e.Y, _zoom * mult);
                OnZoomChanged(new EventArgs());
                Invalidate();
            }
            base.OnMouseWheel(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (e.Button.HasFlag(MouseButtons.Middle))
            {
                var pt = new Point(_offset.X + e.X - grab.X, _offset.Y + e.Y - grab.Y);
                Offset = pt; //Offset setter might adjust pt...
                grab.X = _offset.X + e.X - pt.X; //...adjusted grab to suit
                grab.Y = _offset.Y + e.Y - pt.Y;
                OnZoomChanged(new EventArgs());
                Invalidate();
            }
            base.OnMouseMove(e);
        }

        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Middle && Image != null)
            {
                _zoom = 1f;
                _offset = Point.Empty;
                OnZoomChanged(new EventArgs());
                Invalidate();
            }
            base.OnMouseDoubleClick(e);
        }

        //protected override void OnMouseUp(MouseEventArgs e)
        //{
        //    if (e.Button.HasFlag(MouseButtons.Middle))
        //    {
        //        Cursor = Cursors.Default;
        //    }
        //    base.OnMouseUp(e);
        //}


        public PointF ToPixelSpace(Point controlSpace)
        {
            return new PointF(
                (controlSpace.X - _offset.X) / _zoom,
                (controlSpace.Y - _offset.Y) / _zoom);
        }
        public Point ToControlSpace(PointF pixelSpace)
        {
            return new Point(
                (int)(pixelSpace.X * _zoom) + _offset.X,
                (int)(pixelSpace.Y * _zoom) + _offset.Y);
        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
        }

        private static int confineOffset(int value, int container, int contained)
        {
            if (contained >= container)
                return saturate(value, container - contained, 0);
            else
                return (container - contained) >> 1;
        }
        private static int saturate(int value, int min, int max)
        {
            return value < min ? min : value > max ? max : value;
        }
    }

    //public class View
    //{
    //    public float Zoom { get; set; }
    //    public int X { get; set; }
    //    public int Y { get; set; }
    //    public Point Location { get { return new Point(X, Y); } }
    //    //private View()
    //    //{
    //    //    Zoom = 1f;
    //    //    X = 0;
    //    //    Y = 0;
    //    //}
    //}
}

