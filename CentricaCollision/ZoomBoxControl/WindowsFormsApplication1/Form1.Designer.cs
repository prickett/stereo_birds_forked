﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.zoomBox1 = new CustomControls.ZoomBox();
            ((System.ComponentModel.ISupportInitialize)(this.zoomBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(25, 22);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // zoomBox1
            // 
            this.zoomBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zoomBox1.Image = ((System.Drawing.Image)(resources.GetObject("zoomBox1.Image")));
            this.zoomBox1.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Bilinear;
            this.zoomBox1.Location = new System.Drawing.Point(0, 0);
            this.zoomBox1.Name = "zoomBox1";
            this.zoomBox1.Offset = new System.Drawing.Point(78, 88);
            this.zoomBox1.Size = new System.Drawing.Size(284, 313);
            this.zoomBox1.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            this.zoomBox1.TabIndex = 2;
            this.zoomBox1.TabStop = false;
            this.zoomBox1.Zoom = 1F;
            this.zoomBox1.Click += new System.EventHandler(this.zoomBox1_Click);
            this.zoomBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.zoomBox1_Paint);
            this.zoomBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.zoomBox1_MouseMove);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 313);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.zoomBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.zoomBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private CustomControls.ZoomBox zoomBox1;
    }
}

