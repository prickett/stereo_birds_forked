﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        static Random rand = new Random();
        List<string> imagepaths;

        public Form1()
        { 
            imagepaths = new List<string>();
            string folder = @"C:\Users\Public\Pictures\Sample Pictures\";
            imagepaths.Add(folder + "Desert.jpg");
            imagepaths.Add(folder + "Penguins.jpg");
            imagepaths.Add(folder + "Koala.jpg");
            imagepaths.Add(folder + "Chrysanthemum.jpg");
            imagepaths.Add(folder + "Hydrangeas.jpg");
            imagepaths.Add(folder + "Jellyfish.jpg");
            imagepaths.Add(folder + "Lighthouse.jpg");
            imagepaths.Add(folder + "Tulips.jpg");
            
            InitializeComponent();


        }


        private void button1_Click(object sender, EventArgs e)
        {
            Image old = zoomBox1.Image;
            zoomBox1.Image = new Bitmap(imagepaths[rand.Next(imagepaths.Count)]);
            if (old != null)
                old.Dispose();//is this the right thing to do?
        }

        private void zoomBox1_MouseMove(object sender, MouseEventArgs e)
        {
            var ps = zoomBox1.ToPixelSpace(e.Location);
            var cs = zoomBox1.ToControlSpace(ps);
            Text = string.Format("{0} - {1}", ps.ToString(), cs.ToString());
                

        }

        private void zoomBox1_Click(object sender, EventArgs e)
        {
            zoomBox1.Offset = Point.Add(zoomBox1.Offset, new Size(34, 12));
        }

        private void zoomBox1_Paint(object sender, PaintEventArgs e)
        {
            float zm = zoomBox1.Zoom;
            var pt = zoomBox1.ToControlSpace(new PointF(54,54));
            var sz = (Size)new Size((int)(32f*zm),(int)(32f*zm));
            Console.WriteLine("{0} {1}", pt, sz);
            e.Graphics.DrawRectangle(Pens.Red, new Rectangle(pt,sz));
        }
    }
}
