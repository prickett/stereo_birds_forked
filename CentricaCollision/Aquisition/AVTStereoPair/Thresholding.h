#pragma once

#include "PvApi.h"
#include "SIMDHelper.h"
#include <list>
#include <vector>

#define CHANNELOFINTEREST 1

namespace Thresholding
{
	using namespace std;

	typedef struct{
		unsigned long Y;
		unsigned long Id;
		unsigned long Xs;
		unsigned long Xe;
	} tXSeq;

	bool IsOverlaping(tXSeq &a, tXSeq &b)
	{
		return a.Xs < b.Xe && b.Xs < a.Xe; 
	}

	static void Threshold(unsigned char *pix, const unsigned long width, const unsigned long height,const unsigned char threshold)
	{
		bool over = false;

		vector<unsigned long> blobIds;
		vector<tXSeq> slices;
		tXSeq current;
		
		unsigned long id = 0;
		unsigned long i;
		unsigned long size = width*height;
		unsigned long chaser = 0;


		for (unsigned long y = 0; y<height; y++)
		{
			over = false;
			for (unsigned long x = 0; x<width; x++, pix++)
			{
				if(over != *pix >= threshold)//we have a threshold change
				{
					over = !over; 
					if(over) //gone over threshold
					{
						current.Y = y;
						current.Xs = x;
					}
					else //gone under threshold
					{
						current.Xe = x;

						if(slices[chaser].Y + 1 < y)
							printf("logic fail");

						//make sure we are on the right row
						if(slices[chaser].Y + 1 == y) 
						{
							//catch up on X
							while(slices[chaser].Xe <= current.Xs)
								chaser++;

							if(IsOverlaping(current, slices[chaser]))
							{
								current.Id = slices[chaser].Id; //use the id of the overlapping slice
								chaser++;
								while(IsOverlaping(current, slices[chaser]))
								{
									unsigned long id = slices[chaser].Id;
									while(blobIds[id]!=id)
									{
										id = blobIds[id];
									}
									blobIds[id] = current.Id; //keep track of id changes
									slices[chaser].Id = current.Id; 
									chaser++;
								}
							}
							else //a new blob
							{
								unsigned long id = blobIds.size();
								blobIds.push_back(id);
								current.Id = id;
							}
						}
						slices.push_back(current);
					}
				}
			}

			//increment chaser until we arrive at the current row
			while(slices[chaser].Y < y)
			{
				chaser++;
			}

		}


	}


	void Consolidate(std::vector<tXSeq> slices, std::vector<unsigned long> blobIds)
	{
		unsigned long sz = slices.size();
		for (int i=0;i<sz;i++)
		{
			unsigned long id = slices[i].Id;
			//unsigned long id1st = id;
			//unsigned long idFinal;
			while(id !=blobIds[id])
			{
				id = blobIds[id];
			}
			//idFinal = id;
			//id=id1st;
			//while(id != blobIds[id])
			//{
			//	unsigned long tmp = id;
			//	id = blobIds[id];
			//	blobIds[tmp]=idFinal;
			//}



			slices[i].Id = id;
		}
	}


	


};

