#include "StdAfx.h"

#include <list>
#include "Threading.h"

std::list<HANDLE> threadQueue;

void  Threading::Create(LPTHREAD_START_ROUTINE func, LPVOID param)
{
	Threading::CloseFinished();
	threadQueue.push_back(CreateThread(0,0,func,param,0,0));
	printf("%u threads\n",threadQueue.size());
}

void Threading::CloseFinished(void)
{
	for(std::list<HANDLE>::iterator hq = threadQueue.begin(); hq!=threadQueue.end();)
	{
		std::list<HANDLE>::iterator ho = hq++;
		if( WaitForSingleObject(*ho, 0) == WAIT_OBJECT_0)
		{
			printf("popping %u\n",*ho);
			CloseHandle(*ho);
			threadQueue.erase(ho);
		}
	}
}

void Threading::CloseAll(void)
{
	for(std::list<HANDLE>::iterator hq = threadQueue.begin(); hq!=threadQueue.end();)
	{
		std::list<HANDLE>::iterator ho = hq++;
		unsigned long ret;
		if (ret = WaitForSingleObject(*ho,INFINITE) == WAIT_OBJECT_0)
		{
			printf("final popping %u (%u)\n",*ho,CloseHandle(*ho));
			threadQueue.erase(ho);
		}
		else
		{
			printf("ERROR! WaitForSingleObject returned %u with handle %u \n",ret,*ho);
		}
	}
	threadQueue.clear();
}

