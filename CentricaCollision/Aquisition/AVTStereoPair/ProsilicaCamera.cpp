#include "StdAfx.h"
#include "ProsilicaCamera.h"

char * ProsilicaCamera::ErrorDescriptions[] =  {  
	"No error",  
	"Unexpected camera fault",
	"Unexpected fault in PvApi or driver",
    "Camera handle is invalid",
    "Bad parameter to API call",
    "Sequence of API calls is incorrect",
    "Camera or attribute not found",
    "Camera cannot be opened in the specified mode",
    "Camera was unplugged",
    "Setup is invalid (an attribute is invalid)",
    "System/network resources or memory not available",
    "1394 bandwidth not available",
    "Too many frames on queue",
    "Frame buffer is too small",
    "Frame cancelled by user",
    "The data for the frame was lost",
    "Some data in the frame is missing",
    "Timeout during wait",
    "Attribute value is out of the expected range",
    "Attribute is not this type (wrong access function)",
    "Attribute write forbidden at this time",
    "Attribute is not available at this time",
    "A firewall is blocking the traffic (Windows only)"
};

#define XFRAMECOUNT 8

typedef struct 
{
    unsigned long   UID;
    tPvHandle       Handle;
    tPvFrame        Frames[XFRAMECOUNT];
    tPvUint32       Counter;
	bool			Available;
	bool			WaitingForFrame;
} tCamera;

ProsilicaCamera::ProsilicaCamera(unsigned long uid)
{

}

ProsilicaCamera::~ProsilicaCamera(void)
{

}

const unsigned long& ProsilicaCamera::Uid() const
{
	return _uid;
}
void ProsilicaCamera::Uid(const unsigned long& uid)
{
	_uid = uid;
}

const tPvHandle& ProsilicaCamera::Handle() const
{
	return _handle;
}
void ProsilicaCamera::Handle(const tPvHandle& handle)
{
	_handle = handle;
}

const tPvUint32& ProsilicaCamera::Counter() const
{
	return _counter;
}
void ProsilicaCamera::Counter(const tPvUint32& counter)
{
	_counter = counter;
}




//const std::string& Student::name() const {
//    return name_;
//}
// 
//void Student::name(const std::string& name) {
//    name_ = name;
//}