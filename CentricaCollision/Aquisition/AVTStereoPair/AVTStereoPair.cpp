// AVTStereoPair.cpp : Defines the entry point for the console application.


#include "stdafx.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <string>
#include <vector>
#include <time.h>
#include "SanityChecking.h"
#include "PrecisionTimer.h"
#include "Img.h"
#include "Threading.h"
#include "PVApiWrapper.h"

//5006123 5006124 <--old uids
//123510 124401 <--ge4900c

#define FRAMECOUNT 16
#define LEADIN 8
#define WATCHDOGTIMEOUT 327

struct tCamera
{
    unsigned long   UID;
    tPvHandle       Handle;
    tPvFrame        Frames[FRAMECOUNT];
	Img::ImageRef	Channels[FRAMECOUNT];
	Img::ImageRef	Subtraction[FRAMECOUNT];
	Img::ImageRef	ChannelSub;
	Img::ImageRef	ChannelSum;
    tPvUint32       Counter;
	bool			Available;
};

struct tSaveParam
{
	tCamera*camera;
	tPvFrame*frame;
};

bool GAbort;
CPrecisionTimer GTimer; 
PVApiWrapper* GPVApi;
tCamera GCameras[2];
tCamera* GMaster = &GCameras[0];
tCamera* GSlave = &GCameras[1];
bool GMasterBack;
bool GSlaveBack;
time_t GWatchdogTime;

bool SaveFrame(tPvFrame *frame, Img::ImageRef * mask, unsigned char *map, int order);

// CTRL+C handler
BOOL WINAPI CtrlCHandler(DWORD dwCtrlType)
{

	//Available events for a switch
	//CTRL_C_EVENT
	//CTRL_CLOSE_EVENT
	//CTRL_BREAK_EVENT
	//CTRL_LOGOFF_EVENT
	//CTRL_SHUTDOWN_EVENT

    printf("\nInterrupt received (%x)\n",dwCtrlType);    
	if(dwCtrlType==CTRL_C_EVENT)GAbort = TRUE;
	//GAbort=true;//!!!!!!!!!!
    return true;
}

void softTrigger()
{
	if(!GAbort)
	{
		if(GPVApi->SoftTriggerAsync(GMaster->Handle))
		{
			GWatchdogTime = time(NULL); //having this line here puts the watch dog in a tight loop when SoftTriggerAsync fails	
			GMasterBack=false;
			GSlaveBack=false;
		}
	}

}
bool DeallocateFrameBuffers(tCamera* camera)
{
	printf("%*************u\n",camera->UID);
	for(int i =0; i<FRAMECOUNT; i++)
	{
		tPvFrame* frm = &camera->Frames[i];

		if (frm->ImageBuffer)
			delete [] frm->ImageBuffer;

		if(camera->Channels[i].image)
			delete [] camera->Channels[i].image;

		if(camera->Subtraction[i].image)
			delete [] camera->Subtraction[i].image;
		
		frm->ImageBufferSize = 0;
		frm->Context[0] = NULL;
		frm->Context[1] = NULL;
	}

	if(camera->ChannelSum.image)
		delete [] camera->ChannelSum.image;
	if(camera->ChannelSub.image)
		delete [] camera->ChannelSub.image;
	return true;
}

bool AllocateFrameBuffers(tCamera* camera)
{	
	unsigned long framW,framH,framSz;
	framW = GPVApi->GetSensorWidth(camera->Handle);
	framH = GPVApi->GetSensorHeight(camera->Handle);
	framSz = GPVApi->GetFrameSize(camera->Handle);

	long chanW,chanH,chanSz;
	chanW = ((framW+31)>>5)<<4;
	chanH = ((framH+31)>>5)<<4;//(framH +1)>>1;
	chanSz = chanW*chanH;

	//printf("%u %u %u\n",chanW,chanH,chanSz);

#ifdef SANITYCHECKING
	if (framSz != framW * framH)
		printf("unexpected pixel format!!");
#endif

	
	for(int i =0; i<FRAMECOUNT; i++)
	{
		tPvFrame* frame = &camera->Frames[i];

		frame->ImageBuffer = new char[framSz];
		
 		camera->Channels[i] = Img::ImageRef(new unsigned char[chanSz],chanW,chanH,chanW,8);
		camera->Subtraction[i] = Img::ImageRef(new unsigned char[chanSz],chanW,chanH,chanW,8);

		if (frame->ImageBuffer!=NULL && camera->Channels[i].image != NULL && camera->Subtraction[i].image != NULL )
		{
			frame->ImageBufferSize = framSz;
			frame->Context[0] = camera;
			frame->Context[1] =(void*) i;
		}
		else
		{
			printf("failed to allocate buffers");
			DeallocateFrameBuffers(camera);
			return false;
		}

	}
	camera->ChannelSub = Img::ImageRef(
		new unsigned char[chanSz], 
		chanW, chanH, chanW, 8);

	camera->ChannelSum = Img::ImageRef(
		(unsigned char*)new unsigned short[chanSz],
		chanW, chanH, chanW * sizeof(unsigned short), 16);

	printf("buffer alignment %u %u %u\n", camera->ChannelSub.alignment(), camera->ChannelSum.alignment(), camera->Channels[1].alignment());

	return camera->ChannelSum.image != NULL;
}




// wait until ctrl+C abort
void WaitForEver()
{
	while(!GAbort)
		Sleep(100);//Sleep(500); ///!!!!!!Experiment!!
}

long getGlobalTime(void)
{
	return GTimer.GetMilliseconds();

	//eturn CPrecisionTimer::Global.GetMilliseconds();
}



// Event callback.  This is called by PvApi when camera event(s) occur.
void _stdcall CameraEventCB(void* Context, tPvHandle handle, const tPvCameraEvent* EventList,unsigned long	EventListLength)
{
	return; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	char id = handle == GMaster->Handle? 'M':'S';
	if(handle == GMaster->Handle)
	{
		printf("                                  [%u]M ",GTimer.GetMilliseconds());
	}
	else
	{
		printf("  [%u]S ",GTimer.GetMilliseconds());
	}
	//multiple events may have occurred for this one callback
	for (unsigned long i = 0; i < EventListLength; i++)
	{
		//printf("    [%u] %c ",getGlobalTime(),id);
		switch (EventList[i].EventId) {
			case 40000:
				printf("EventAcquisitionStart ");
				break;
			case 40001:
				printf("EventAcquisitionEnd ");
				break;
			case 40002:
				printf("EventFrameTrigger ");
				break;
			case 40003:
				printf("EventExposureEnd ");
				//////SoftTrigger(); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
				//if(handle == GSlave->Handle)
				//{
				//	printf("**trigger** ");
				//	softTrigger();
				//}
				//if(handle == GMaster->Handle)
				//{
				//	GMasterBack=true;
				//}
				//else
				//{
				//	GSlaveBack=true;
				//}
				//if (GMasterBack==true & GSlaveBack==true)
				//{
				//	printf("trigger %s\n",id);
				//	softTrigger();
				//}
				break;
			case 40004:
				printf("EventAcquisitionRecordTrigger ");
				break;
			case 40010:
				printf("EventSyncIn1Rise ");
				break;
			case 40011:
				printf("EventSyncIn1Fall ");
				break;
			case 40012:
				printf("EventSyncIn2Rise ");
				break;
			case 40013:
				printf("EventSyncIn2Fall ");
				break;
			case 40014:
				printf("EventSyncIn3Rise ");
				break;
			case 40015:
				printf("EventSyncIn3Fall ");
				break;
			case 40016:
				printf("EventSyncIn4Rise ");
				break;
			case 40017:
				printf("EventSyncIn4Fall ");
				break;
			case 65534:
				printf("EventOverflow error ");
				break;
			default:
				printf("Event unknown %u",EventList[i].EventId);
				break;
		}
	}
	printf("\n");
}



	bool SaveFrameX(tCamera *camera, tPvFrame *frame)
	{
		char path[64];
		FILE *file;
		long long written = 0;
		int w = frame->Width;
		int h = frame->Height;
		int sz = w*h;
		
		sprintf(path,"C:/Capture/%s_%u_%06u.raw",
			camera==GMaster ? "M" : "S",
			time(NULL),
			camera->Counter);

		if (fopen_s(&file,path,"w+b") == 0)
		{
			written += fwrite(&w,1,4,file);
			written += fwrite(&h,1,4,file);
			written += fwrite(frame->ImageBuffer, 1, sz,file);
			fclose(file);
			printf("saved %s\n",path);
		}
		else
		{
			printf("can't open file %s\n",path);
		}

		return 0;//written == sz + 8;
	}

	bool SaveRawImage(tCamera *camera, Img::ImageRef *image)
	{
		char path[64];
		FILE *file;
		long long written = 0;
		
		int sz = image->width * image->height;
		
		sprintf(path,"C:/Capture/%s_%u_%06u.raw",
			camera==GMaster ? "M" : "S",
			time(NULL),
			camera->Counter);

		if ((file = fopen(path,"w+b"))!=0)
		{
			written += fwrite(&image->width,1,4,file);
			written += fwrite(&image->height,1,4,file);
			written += fwrite(image->image, 1, sz,file);
			fclose(file);
			//printf("saved %s\n",path);
		}
		else
		{
			printf("can't open file %s\n",path);
		}

		return 0;//written == sz + 8;
	}




	//bool saveAsBitmap(tCamera *camera, tPvFrame *frame)
	//{
	//	char path[128];
	//	FILE *file;
	//	long long written = 0;
	//	bool retval = false;

	//	Img::ImageRef bayer((unsigned char*)frame->ImageBuffer, 
	//		frame->Width, 
	//		frame->Height, 
	//		frame->Width,
	//		8);
	//	
	//	sprintf(path,"C:/Capture/%s_%u_%06u_%06u_[%u].bmp",
	//		camera==GMaster ? "M" : "S",
	//		time(NULL),
	//		GTimer.GetMilliseconds(),
	//		camera->Counter,
	//		GPVApi->GetExposure(camera->Handle));


	//	if ((file = fopen(path,"w+b")) != 0)
	//	{		
	//		if (retval = Img::Bitmap::SaveAsGreyscale(&bayer,file))
	//			printf(".");//printf("saved \"%s\"\n",path);
	//		else
	//			printf("something went wrong writing %s\n",path);

	//		if(fclose(file)!=0)
	//			printf("could not close file",path);
	//	}	
	//	else
	//	{
	//		printf("can't open file %s\n",path);
	//	}
	//		
	//	return retval;
	//}

	bool saveAsBitmap(char* path, Img::ImageRef *imageRef)
	{
		FILE *file;
		bool retval = false;

    	if ((file = fopen(path,"w+b")) != 0)
		{		
			if (retval = Img::Bitmap::SaveAsGreyscale(imageRef,file))
				printf("succesfully saved \"%s\"\n",path);
			else
				printf("something went wrong writing %s\n",path);

			if(fclose(file)!=0)
				printf("could not close file",path);
		}	
		else
		{
			printf("can't open file %s\n",path);
		}
			
		return retval;
	}


	bool saveFullFrame(tCamera *camera, tPvFrame *frame)
	{
		char path[128];
		bool retval = false;

		Img::ImageRef bayer((unsigned char*)frame->ImageBuffer, 
			frame->Width, 
			frame->Height, 
			frame->Width,
			8);
		
		sprintf(path,"C:/Capture/%s_%u_%06u_%06u_[%u].bmp",
			camera==GMaster ? "M" : "S",
			time(NULL),
			GTimer.GetMilliseconds(),
			camera->Counter,
			GPVApi->GetExposure(camera->Handle));

		return saveAsBitmap(path,&bayer);
	}


	unsigned long _stdcall ProcessFrameThreadFunc(void* param)
	{
		return 0;
	}
	unsigned long _stdcall SaveFrameThreadFunc(void* param)
	{
		Img::ImageRef *image = (Img::ImageRef*) param;
		Img::write_JPEG_file("c:\\Capture\\boo.jpg",85,image);

		return 0;
	}
	unsigned long _stdcall SaveBitmapThreadFunc(void* param)
	{
		tSaveParam *sp = (tSaveParam*)param;
		saveFullFrame(sp->camera,sp->frame);
		return 0;
	}




	void subBlur(Img::ImageRef *target, Img::ImageRef *temp, int order)
	{	
		int sz = target->size();
		memcpy(temp->image , target->image, sz);
		Img::BoxBlur(temp,order);		
		Img::AbsoluteDifference((__m128i*)target->image,(__m128i*)temp->image,(__m128i*)target->image,sz>>4);
	}

// Frame completed callback executes on seperate driver thread.
// One callback thread per camera. If a frame callback function has not 
// completed, and the next frame returns, the next frame's callback function is queued. 
// This situation is best avoided (camera running faster than host can process frames). 
// Spend as little time in this thread as possible and offload processing
// to other threads or save processing until later.
//
// Note: If a camera is unplugged, this callback will not get called until PvCaptureQueueClear.
// i.e. callback with pFrame->Status = ePvErrUnplugged doesn't happen -- so don't rely
// on this as a test for a missing camera. 

//Indirectly called for each Frame callback
bool HandleFrame(tCamera* camera, tPvFrame* frame, tPvFrameCallback callback)
{
	//camera->WaitingForFrame = false;
	
	

	//printf("Handle frame function (frame back status: %i)\n",GFramesBack);
	//Do something with the frame.
	//E.g. display to screen, shift pFrame->ImageBuffer location for later usage , etc
	//Here we display FrameCount and Status
	if (frame->Status != ePvErrSuccess)
	{
		if (frame->Status == ePvErrDataMissing)
		{
			//Possible improper network card settings. See GigE Installation Guide.
			printf(" (Frame: %u dropped packets) ", frame->FrameCount);
		}
		else if (frame->Status == ePvErrCancelled)
		{
			printf(" (Frame cancelled %u) ", frame->FrameCount);
		}
		else
		{
			printf(" (Frame: %u Error: %u) ", frame->FrameCount, frame->Status);
		}
	}
	
	// if frame hasn't been cancelled...
    if((frame->Status != ePvErrCancelled) & !GAbort)
	{

		long exposure = GPVApi->GetExposure(camera->Handle);

		//fill an image reference structure for the bayer
		Img::ImageRef bayer(
			(unsigned char*)frame->ImageBuffer, 
			frame->Width, 
			frame->Height, 
			frame->Width,
			8);

		int curr = (int)frame->Context[1];//<--this is where we store the frame index
		int prev = (curr + FRAMECOUNT - 1) % FRAMECOUNT;//<--find the index of the previous frame

		Img::ImageRef *current = &camera->Channels[curr];
		Img::ImageRef *temp = &camera->ChannelSub; //*************************
		Img::ImageRef *previous = &camera->Channels[prev];
		Img::ImageRef *prevtarget = &camera->Subtraction[prev];
		Img::ImageRef *target = &camera->Subtraction[curr];

		Img::ExtractChannel(&bayer, current ,CHANNELOFINTEREST);

		int count = current->size()>>4;

		subBlur(current,temp,5);
	
		if(camera->Counter > 1)
		{
			Img::TernaryDifference(
				(__m128i*) current->image, 
				(__m128i*) previous->image, 
				(__m128i*) prevtarget->image,
				(__m128i*) target->image,
				count);
		}
		else if(camera->Counter > 0)
		{			
			Img::AbsoluteDifference(
				(__m128i*) current->image, 
				(__m128i*) previous->image, 
				(__m128i*) target->image,
				count);				
		}

		if(camera->Counter > 0 && exposure < 100000)
		{
			int order = 3;
			int mapsize = Img::MapSize(target, order);//(chan0->width>>3)*chan0->height;
			unsigned char *map = new unsigned char[mapsize];

			Img::MapThresholds(target,order,map,5);//4 = 16, 5 = 32, 6 = 64...

			SaveFrame(frame,target,map,order+1);

			delete [] map;
		}
		else
		{
			printf("exposure above max (%u)\n",exposure);
		}
		
		//if((camera->Counter & 255) == 0) //every 256 frames
		//	Threading::Create(SaveFrameThreadFunc,current);//save a full frame jpeg
		if((camera->Counter & 1023) == 32 && exposure < 500000) //every 1024 frames ~5 minutes
		{
			//tSaveParam param;
			//param.camera=camera;
			//param.frame=frame;
			saveFullFrame(camera,frame);
			//saveAsBitmap(camera,frame);
		}

		//...requeue the frame
		PvCaptureQueueFrame(camera->Handle,frame,callback);
	}

	//camera->Counter++;
	return frame->Status == ePvErrSuccess;
}




void _stdcall MasterFrameDoneCB(tPvFrame* frame)
{
	softTrigger();//be cafeful with softtrigger here, we have 4 seconds to complete!!
	HandleFrame(GMaster, frame, MasterFrameDoneCB);

	GMaster->Counter++;
	GSlave->Counter++;
}
void _stdcall SlaveFrameDoneCB(tPvFrame* frame)
{
	HandleFrame(GSlave, frame, SlaveFrameDoneCB);
}



bool OpenCamera(tCamera *camera)
{
	
	if(GPVApi->OpenCamera(camera->UID, camera->Handle) &&
	   AllocateFrameBuffers(camera) &&
	   GPVApi->RegisterEventsCallback(camera->Handle, CameraEventCB)) 
	{
		camera->Available = true;
		return true;
	}
	return false;
}


// callback function called on seperate thread when a registered camera event received
void __stdcall PlugCB(void* context, tPvInterface Interface, tPvLinkEvent Event, unsigned long uniqueId)
{
	if(!GAbort)
	{
		if (uniqueId == GMaster->UID)
		{
			printf("Master found\n");
			OpenCamera(GMaster);
		}
		else if(uniqueId == GSlave->UID)
		{
			printf("Slave found\n");
			OpenCamera(GSlave);
		}
		else
		{
			printf("Unexpected camera %u plugged\n",uniqueId);
			return;
		}

		if (GMaster->Available && GSlave->Available)//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		{
			GPVApi->StartAsMaster(GMaster->Handle, GMaster->Frames, FRAMECOUNT, MasterFrameDoneCB);
			GPVApi->StartAsSlave( GSlave->Handle,  GSlave->Frames,  FRAMECOUNT, SlaveFrameDoneCB);
			softTrigger();
			//GPVApi->SoftTriggerAsync(GMaster->Handle);
		}
	}
}

// callback function called on seperate thread when a registered camera event received
void __stdcall UnplugCB(void* context, tPvInterface Interface, tPvLinkEvent Event, unsigned long uniqueId)
{
	if (uniqueId == GMaster->UID)
	{
		printf("Master lost\n");
		GMaster->Available=false;
	}
	else if(uniqueId == GSlave->UID)
	{
		printf("Slave lost\n");
		GSlave->Available=false;
	}

}






int _tmain(int argc, _TCHAR* argv[])
{

	//CPrecisionTimer::Global.Start();
	
	//Initialise globals
	memset(GCameras,0,sizeof(GCameras));
	GAbort = FALSE;
	GPVApi = &PVApiWrapper::getInstance(); //Singletom API wrapper, this will fail if the API can't be initialised
		
	//grab any camera uids passed in the command line arguments
	if (argc>1)
		GMaster->UID = _wtoi(argv[1]);
	if (argc>2)
		GSlave->UID = _wtoi(argv[2]);

	//!!!!!!!!!!!!!!!!!!
	//GMaster->UID = 5006123;
	//GSlave->UID = 5006124;

	//!!!!!!!!!!!!!!!!!!!!
	//TEST THAT BLOODY JPEG ENCODER
	//////////////////////
	//Img::ImageRef image(NULL,300,300,300,8);
	////image.width=300;
	////image.height=300;
	////image.stride=image.width;
	//printf("%u\n",image.width);
	//printf("%u\n",image.height);
	//printf("%u\n",image.stride);

	//image.image=(unsigned char*)malloc(image.width*image.height);
	//for(int y=0,i=0;y<image.height;y++)
	//{
	//	for(int x=0;x<image.width;x++,i++)
	//	{
	//		image.image[i]=y;
	//	}
	//}
	//Img::write_JPEG_file("c:\\Capture\\boo.jpg",85,&image);
	////Threading::Create(SaveFrameThreadFunc,&image);//save a full frame jpeg


	//free(image.image);
	//return 0;
	//!!!!!!!!!!!!!!!!!!


	//exit if we have not been told which cameras to use
	if (GMaster->UID == 0 || GSlave->UID == 0)
	{
		printf("Usage: AVTStereoPair [Left camera unique id] [Right camera unique id]\n\n",argv[0]);
		//return 0;
	}
	printf("\"%u\" \"%u\"\n",GMaster->UID,GSlave->UID);

	// set the CTRL-C handler
    SetConsoleCtrlHandler(&CtrlCHandler, TRUE);

	printf("Ctrl+C to break.\n\nWaiting for cameras...\n");

 
	//exit if we can't register callbacks
	if (!GPVApi->RegisterPnpCallbacks(PlugCB,UnplugCB,NULL))
	{
		printf("Failed to register link callbacks\nAborting!\n");
		return 0;
	}


	//initialise GWatchdogTime with current time 
	GWatchdogTime = time(NULL);

	//keep thread alive until user exit
	while(!GAbort)
	{
		if((time(NULL) - GWatchdogTime) > WATCHDOGTIMEOUT)
		{
			printf("Watchdog kick start\n");
			softTrigger();
		}
		Sleep(1000);//Sleep(500); ///!!!!!!Experiment!!
	}

	printf("\n1st pause...\n");
	Sleep(500);
     
	//unregister callbacks
	GPVApi->UnregisterPnpCallbacks(PlugCB,UnplugCB);

	printf("\n2nd pause...\n");
	Sleep(500);

	GPVApi->StopCamera(GMaster->Handle);
	GPVApi->StopCamera(GSlave->Handle);

	Sleep(500);

	GPVApi->UnregisterEventsCallback(GMaster->Handle,CameraEventCB);
	GPVApi->UnregisterEventsCallback(GSlave->Handle,CameraEventCB);

	Sleep(500);

	Threading::CloseAll();

	GPVApi->CloseCamera(GMaster->Handle);
	GPVApi->CloseCamera(GSlave->Handle);

	DeallocateFrameBuffers(GMaster);
	DeallocateFrameBuffers(GSlave);

	Sleep(500);

 	return 0;
}




//following is the file saving bit
#define MAGICNUMBER 0xC0FFEE


enum tBayerPattern
{
	eBayerRGGB = 0, // First line RGRG, second line GBGB...
	eBayerGBRG = 1, // First line GBGB, second line RGRG...
	eBayerGRBG = 2, // First line GRGR, second line BGBG...
	eBayerBGGR = 3, // First line BGBG, second line GRGR...
};   


	
struct FileHeader
{
	unsigned long magicNumber;
	unsigned long width;
	unsigned long height;
	unsigned short bitsPerPixel;
	unsigned short bayerPattern;

	unsigned short mapWidth;
	unsigned short mapHeight;
	unsigned long mapUnit;
	unsigned long mapOffset;
	unsigned long dataOffset;
	unsigned long maskOffset;

	unsigned long cameraID;
	unsigned long exposure;
	unsigned long frameStatus;
	unsigned long cameraTick;
	unsigned long systemTick;
	__time64_t systemTime; //64bit 
	unsigned long FrameCount;
};


bool SaveFrame(tPvFrame *frame, Img::ImageRef *mask, unsigned char *map, int order)
{
	Img::ImageRef bayer(
		(unsigned char*)frame->ImageBuffer,
		frame->Width,
		frame->Height,
		frame->Width,
		frame->BitDepth);

	tCamera *camera = (tCamera*)frame->Context[0]; 

	int mapSize = Img::MapSize(&bayer,order);
	int outBufSize = Img::MappedBufferSize(&bayer,order,map);
	int maskBufSize = Img::MappedBufferSize(mask,order-1,map);

	unsigned char *outBuf = new unsigned char[outBufSize];
	unsigned char *maskBuf = new unsigned char[maskBufSize];
	
	Img::MapToBuffer(&bayer,order,map,outBuf);
	Img::MapToBuffer(mask,order-1,map,maskBuf);

	FileHeader header;
	header.magicNumber = MAGICNUMBER;
	header.width= bayer.width;
	header.height= bayer.height;
	header.bitsPerPixel= bayer.bpp;
	header.bayerPattern=frame->BayerPattern;

	header.mapWidth = Img::MapWidth(&bayer,order);
	header.mapHeight= Img::MapHeight(&bayer,order);
	header.mapUnit = 1<<order;
	header.mapOffset= sizeof(header);//!!!!!!!!!!!! realign
	header.dataOffset=header.mapOffset + mapSize;//!!!!!!!!!! realign
	header.maskOffset=header.dataOffset + outBufSize;

	header.cameraID = camera->UID;
	header.exposure = GPVApi->GetExposureValue(camera->Handle);
	header.frameStatus	= (long)frame->Status;
	header.cameraTick = frame->TimestampLo;
	header.systemTick = GTimer.GetTicks() & 0xffffffff;
	header.systemTime = time(NULL);
	header.FrameCount = camera->Counter;

	char path[64];
	FILE *file;
	long long written = 0;
		
	sprintf(path,"C:/Capture/%s_%u_%06u.map",
		camera == &GCameras[0] ? "M" : "S",
		time(NULL),
		camera->Counter);

	if ((file = fopen(path,"w+b"))!=0)
	{
		written += fwrite(&header, sizeof(header), 1, file);
		written += fwrite(map, mapSize, 1, file);
		written += fwrite(outBuf, outBufSize, 1, file);
		written += fwrite(maskBuf, maskBufSize, 1, file);
		fclose(file);
		printf("saved %s \n",path);
	}
	else
	{
		printf("can't open file %s\n",path);
	}

	delete [] outBuf;
	delete [] maskBuf;

	return written == 4;
}



bool LoadFrame(char* path, Img::ImageRef *bayer, Img::ImageRef *mask, unsigned char *map)
{
	FileHeader header;

	//char path[64];
	FILE *file;
	long long read = 0;
		

	if ((file = fopen(path,"r+b")) != 0 &&
		(read = fread(&header, sizeof(header), 1, file)) == 1 &&
		header.magicNumber == MAGICNUMBER)
	{
			int mapSz,mapOff;
			int maskSz,maskOff;
			int bayerSz,bayerOff;

			mapSz = ((header.mapWidth + 7) >> 3) * header.mapHeight;
			map = new unsigned char[mapSz];

			bayerSz = (int)(header.maskOffset - header.dataOffset);
			bayer->image = new unsigned char[bayerSz];
			bayer->bpp = 8;
			bayer->width = bayer->stride = header.mapUnit;
			bayer->height = bayerSz;

			maskSz = bayerSz >> 2;
			mask->image = new unsigned char[mapSz];
			mask->bpp = 8;
			mask->width = mask->stride = header.mapUnit>>1;
			mask->height = maskSz;

			read += fread(map,mapSz,1,file);
			read += fread(bayer->image,bayerSz,1,file);
			read += fread(mask->image,maskSz,1,file);
	}
	else
	{
		printf("can't open file %s\n",path);
	}


	return read == 4;
}