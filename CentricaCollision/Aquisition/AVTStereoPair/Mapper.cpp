#include "stdafx.h"
#include "Img.h"

using namespace Img;





	int downsize(int length,int order)
	{
		return (length + (1<<order) - 1) >> order;
	}


	int Img::MapWidth(Img::ImageRef* img, int order)
	{
		return downsize(img->width, order);
	}
	int Img::MapHeight(Img::ImageRef* img, int order)
	{
		return downsize(img->height, order);
	}
	int Img::MapStride(Img::ImageRef* img, int order)
	{
		return (MapWidth(img, order) + 7) >> 3;
	}
	int Img::MapSize(Img::ImageRef* img, int order)
	{
		return MapStride(img, order) * MapHeight(img, order);
	}
	int Img::MapBitCount(Img::ImageRef* img, int order, unsigned char *map)
	{
		int unit = 1<<order;
		int w = MapWidth(img,order);
		int h = MapHeight(img,order);
		int count = 0;

		for (int y=0,i=0; y<h; y++)
		{
			int ct = 8;
			for (int x=0; x<w; x+=8, i++)
			{			
				if(x+8>w)
				{
					ct = w-x;
				}
				int n = map[i];
				for(int b=0; b<ct; b++)
				{
					if((n&1)==1) 
						count++;
					n>>=1;
				}
			}
		}
		return count;
	}
	int Img::MappedBufferSize(Img::ImageRef* img, int order, unsigned char *map)
	{
		return MapBitCount(img,order,map) << order*2;
	}




	int Img::MapDebug(Img::ImageRef* img, int order, unsigned char *map)
	{
		int unit = 1<<order;
		int w = MapWidth(img,order);
		int h = MapHeight(img,order);

		printf("<map>\n");

		for (int x=0; x<w; x++)
			printf("_");
		printf("\n");

		for (int y=0,i=0; y<h; y++)
		{
			int ct = 8;
			for (int x=0; x<w; x+=8, i++)
			{			
				if(x+8>w)
				{
					ct = w-x;
				}
				int n = map[i];
				for(int b=0;b<ct;b++)
				{
					printf("%s",((n&1)==0) ? " ":".");
					n>>=1;
				}
			}
			printf("|\n");

		}
		printf("<\\map>\n");
		return 0;
	}






//copies the pixels from the image reference to the target, removing any padding
int copyImage(ImageRef* image, unsigned char* target)
{

#ifdef SANITYCHECKING
	if (image->bpp != 8)
		return 0 & printf("image not 8 bpp (%u)\n",image->bpp);	
#endif



	unsigned char *scanline = image->image;
	int stride = image->stride;
	int width = image->width;
	int height =image->height;
	int copied = 0;

	for(int y=0; y<height; y++)//for(int y=0; y<image->height; y++)
	{

		memcpy(target,scanline,width);
		scanline+=stride;
		target+=width;
		copied+=width;
	}
	return copied;
}

////returns true if any part of the passed rectangle is over the threshold shift
//bool copyRect(ImageRef* image, int threshold)
//{
//
//#ifdef SANITYCHECKING
//	bool good = false;
//
//	if ((threshold & -8)!=0)
//		printf("threshold out of 0 to 7 range\n");
//	else if (image->alignment()<8)
//		printf("%s: image not 64 bit aligned (%u)\n",__FUNCTION__,image->alignment());	
//	else if (image->bpp != 8)
//		printf("image not 8 bpp (%u)\n",image->bpp);	
//	else
//		good=true;
//
//	if(!good)
//		return false;
//#endif
//
//	int width64,stride64,remainder;
//	width64 = image->width>>3; 
//	remainder = image->width & 7;
//	stride64 = image->stride >> 3;
//	
//	const unsigned long long ONE = 0x101010101010101;
//	long long mask8, *scan8,*pix8;
//	char mask, *pix;
//	scan8 = (long long*)image->image;
//
//	mask = ~((1<<threshold) - 1);
//	mask8 = (ONE*(unsigned char)mask);
//
//
//	for(int y=0; y<image->height; y++)//for(int y=0; y<image->height; y++)
//	{
//		pix8 = scan8;
//		scan8 += stride64;
//		for(int i=0; i<width64; i++, pix8++)//for(int i=0; i<image->width; i++)
//		{
//			if(*pix8 & mask8)
//			{
//				return true;
//			}
//		}
//		if(remainder)
//		{
//			pix = (char*) pix8;
//			for(int i=0; i<remainder; i++, pix++)//for(int i=0; i<image->width; i++)
//			{
//				if(*pix & mask)
//				{
//					return true;
//				}
//			}
//
//		}
//	}
//	return false;
//}

	int Img::MapToBuffer(Img::ImageRef* image, int order, unsigned char *map, unsigned char *target)
	{

		//int Img::MapThresholds(ImageRef* image,int order, unsigned char *map, int threshold)//int MapThresholds2(ImageRef* img, ImageRef* map, int threshold)// 

	int unit = 1<<order;
	int w = (image->width+unit-1) >> order;
	int bw = (w+7)>>3;
	int wr =  image->width & (unit-1);

	int h = (image->height+unit-1) >> order;
	int hr = image->height & (unit-1);


	int stride = image->stride<<order;

	ImageRef gridsqr = *image; //get a local copy of the image reference 
	gridsqr.height = unit;
	unsigned char *scanline = image->image;

	for (int y=0, i=0; y<h; y++)
	{
		if(y+1==h && hr!=0)//alter height of last row if fractional
			gridsqr.height = hr;

		gridsqr.image = scanline;
		gridsqr.width = unit;
		scanline += stride;

		int ct = 8;
		for (int x=0; x<w; x+=8,i++)
		{
			if( x+8>w)
			{
				ct=w-x;
				gridsqr.width=wr;
			}
			int n = map[i];
			for (int b=0;b<ct;b++)
			{
				if((n&(1<<b))!=0)
				{
					target += copyImage(&gridsqr,target);
				}
				gridsqr.image += unit;
			}
			map[i] = n;
		}
	}


	return 0;

}


void debugMap(unsigned char *map,int width,int height,int order)
{


}







//returns true if any part of the passed rectangle is over the threshold shift
bool thresholdRect(ImageRef* image, int threshold)
{

#ifdef SANITYCHECKING
	bool good = false;

	if ((threshold & -8)!=0)
		printf("threshold out of 0 to 7 range\n");
	else if (image->alignment()<8)
		printf("%s: image not 64 bit aligned (%u)\n",__FUNCTION__,image->alignment());	
	else if (image->bpp != 8)
		printf("image not 8 bpp (%u)\n",image->bpp);	
	else
		good=true;

	if(!good)
		return false;
#endif

	int width64,stride64,remainder;
	width64 = image->width>>3; 
	remainder = image->width & 7;
	stride64 = image->stride >> 3;
	
	const unsigned long long ONE = 0x101010101010101;
	long long mask8, *scan8,*pix8;
	char mask, *pix;
	scan8 = (long long*)image->image;

	mask = ~((1<<threshold) - 1);
	mask8 = (ONE*(unsigned char)mask);


	for(int y=0; y<image->height; y++)//for(int y=0; y<image->height; y++)
	{
		pix8 = scan8;
		scan8 += stride64;
		for(int i=0; i<width64; i++, pix8++)//for(int i=0; i<image->width; i++)
		{
			if(*pix8 & mask8)
			{
				return true;
			}
		}
		if(remainder)
		{
			pix = (char*) pix8;
			for(int i=0; i<remainder; i++, pix++)//for(int i=0; i<image->width; i++)
			{
				if(*pix & mask)
				{
					return true;
				}
			}

		}
	}
	return false;
}
//int _MapRow(ImageRef* img, unsigned char *map, int threshold)
//{
//		sqr.data = scn;
//		sqr.width = unit;
//		scn+=img->stride;
//		for (int x=0; x<w; x++)
//		{
//			if (thresholdRect(&sqr,threshold))
//			{
//				map[y * s + (x>>3)] |= 1 << x & 7;
//			}
//			sqr.data+=unit;
//		}
//		sqr.width=wr;
//		if (thresholdRect(&sqr,threshold))
//		{
//			map[y * s + (w>>3)] |= 1 <  w & 7;
//		}
//}
int Img::MapThresholds(ImageRef* image,int order, unsigned char *map, int threshold)//int MapThresholds2(ImageRef* img, ImageRef* map, int threshold)// 
{

#ifdef SANITYCHECKING
	bool good = false;
	if ((threshold & -8)!=0)
		printf("threshold must be in the range of 0 to 7");
	else if (image->alignment()<8)
		printf("%s: image not 64 bit aligned (%u)\n",__FUNCTION__,image->alignment());	
	else if (image->bpp != 8)
		printf("image not 8 bpp (%u)\n",image->bpp);	
	else
		good=true;

	if(!good)
		return false;
#endif

	int unit = 1<<order;
	int w = (image->width+unit-1) >> order;
	int bw = (w+7)>>3;
	int wr =  image->width & (unit-1);

	int h = (image->height+unit-1) >> order;
	int hr = image->height & (unit-1);

	//memset(map,0,mapsize(image->width,image->height,order));

	int stride = image->stride<<order;

	ImageRef gridsqr = *image; //get a copy of image
	gridsqr.height = unit;
	unsigned char *scanline = image->image;

	for (int y=0, i=0; y<h; y++)
	{
		if(y+1==h && hr!=0)//alter height of last row if fractional
			gridsqr.height = hr;

		gridsqr.image = scanline;
		gridsqr.width = unit;
		scanline += stride;

		int ct = 8;
		for (int x=0; x<w; x+=8,i++)
		{
			if( x+8>w)
			{
				ct=w-x;
				gridsqr.width=wr;
			}
	
			int n = 0;
			for (int b=0;b<ct;b++)
			{
				if(thresholdRect(&gridsqr, threshold))
					n|=(1<<b);
				gridsqr.image += unit;
			}
			map[i] = n;
		}
	}


	return 0;

}
