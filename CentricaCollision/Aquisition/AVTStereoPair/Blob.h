#pragma once

#include <list>

#define uint unsigned long

class Blob
{
public:

	typedef struct{
		unsigned short y;
		unsigned short x0;
		unsigned short x1;
	} Slice;

	typedef struct{
		std::list<Blob>::iterator blobIterator;
		Blob::Slice slice;
	} BlobSlice;

	static inline bool IsOverlapping(const Blob::Slice &above, const Blob::Slice &below);
	static std::list<Blob> ThresholdBlobs(unsigned char *pix, unsigned short width,unsigned short height,unsigned char threshold);

	Blob(void);
	~Blob(void);

	void Add(Slice &slice);
	void Absorb(Blob &blob);
	void Debug(void);
	unsigned short XMin(void);
	unsigned short YMin(void);
	unsigned short XMax(void);
	unsigned short YMax(void);
	unsigned long Area(void);
	float CenterX(void);
	float CenterY(void);
	std::list<Slice> Slices(void);

private:
	//class Impl;
	//Impl* impl;
	unsigned short xMin;
	unsigned short xMax;
	unsigned short yMin;
	unsigned short yMax;
	unsigned long area;
	unsigned long xSum;
	unsigned long ySum;
	std::list<Slice> slices; 
};
