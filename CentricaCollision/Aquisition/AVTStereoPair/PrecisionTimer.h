
#ifndef _PRECISIONTIMER_H_
#define _PRECISIONTIMER_H_

#include <windows.h>

class CPrecisionTimer
{
private:
	long long freq, start;

public:

	CPrecisionTimer()
	{
		QueryPerformanceFrequency((LARGE_INTEGER*) &freq);
		Start();
	}

	inline void Start()
	{
		QueryPerformanceCounter((LARGE_INTEGER*) &start);
	}

	inline unsigned long long GetTicks()
	{
		// Return performance count...
		long long end;
		QueryPerformanceCounter((LARGE_INTEGER*) &end);
		return end - start;
	}

	inline double GetSeconds()
	{
		// Return duration in seconds...
		return (double) GetTicks() / freq;
	}

	inline unsigned long GetMilliseconds()
	{
		// Return duration in milliseconds...
		return (long)((GetTicks() * 1000) / freq);
	}



};

#endif // _PRECISIONTIMER_H_
