#pragma once
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

namespace Threading
{
	void Create(LPTHREAD_START_ROUTINE func, LPVOID param); 
	void CloseFinished(void);
	void CloseAll(void);
};

