#pragma once

//#include <time.h>
#include "SanityChecking.h"
#include "PvApi.h"
#include "SIMDHelper.h"
#include "jpeglib.h"


#define CHANNELOFINTEREST 1

namespace Img
{
	struct ImageRef
	{
		ImageRef(void);
		ImageRef(unsigned char* image, long width, long height, long stride, long bpp);
		unsigned char* image;
		long width;
		long height;
		long stride;
		long bpp;
		long alignment(void);
		long size(void);
	};



	int MapWidth		(Img::ImageRef* image, int order);
	int MapHeight		(Img::ImageRef* image, int order);
	int MapStride		(Img::ImageRef* image, int order);
	int MapSize			(Img::ImageRef* image, int order);
	int MapThresholds	(Img::ImageRef* image, int order, unsigned char *map, int threshold);
	int MapBitCount		(Img::ImageRef* image, int order, unsigned char *map);
	int MapDebug		(Img::ImageRef* image, int order, unsigned char *map);
	int MappedBufferSize(Img::ImageRef* image, int order, unsigned char *map);
	int MapToBuffer		(Img::ImageRef* image, int order, unsigned char *map, unsigned char *target);



	void BoxBlur(ImageRef * img, int order);

	inline void AbsDif(__m128i* srcA, __m128i* srcB, __m128i* tgt);

	//void ExtractChannel(unsigned char *bayerSrc, unsigned char *channel, int channel);
	void ExtractChannel(ImageRef *bayer, ImageRef *target, int channel);

	//populates a buffer with the absolute difference of two buffers
	void AbsoluteDifference(__m128i* srcA, __m128i* srcB, __m128i* tgt, unsigned long count);

	//populates a buffer with the absolute difference of two buffers
	void TernaryDifference(__m128i* srcA, __m128i* srcB, __m128i* srcC, __m128i* tgt, unsigned long count);

	//populates a buffer with the difference of two buffers
	void Difference(__m128i* srcA, __m128i* srcB, __m128i* tgt,  unsigned long count);

	void SplitScanLine(__m128i* src, __m128i* tgtA, __m128i* tgtB, unsigned long width);

	//void SplitTheBayer(tPvFrame* in, SplitBayer* out);
	void write_JPEG_file (char * filename, int quality, Img::ImageRef *image);

	namespace Bitmap
	{
		/* Note: the magic number has been removed from the bmpfile_header structure
		since it causes alignment problems
			struct bmpfile_magic should be written/read first
		followed by the
			struct bmpfile_header
		[this avoids compiler-specific alignment pragmas etc.]
		*/
 
		struct bmpfile_magic {
		  unsigned char magic[2];
		};
		const unsigned short MAGICBYTES = 0x4D42;
 
		struct bmpfile_header {
		  unsigned long filesz;
		  unsigned short creator1;
		  unsigned short creator2;
		  unsigned long bmp_offset;
		};

		struct bitmap_info_header{
		  unsigned long header_sz;
		  long width;
		  long height;
		  unsigned short nplanes;
		  unsigned short bitspp;
		  unsigned long compress_type;
		  unsigned long bmp_bytesz;
		  long hres;
		  long vres;
		  unsigned long ncolors;
		  unsigned long nimpcolors;
		};
		
		//enum bmp_compression_method{
		//  BI_RGB = 0,
		//  BI_RLE8,
		//  BI_RLE4,
		//  BI_BITFIELDS, //Also Huffman 1D compression for BITMAPCOREHEADER2
		//  BI_JPEG,      //Also RLE-24 compression for BITMAPCOREHEADER2
		//  BI_PNG
		//};

		bool SaveAsGreyscale(ImageRef *greyscale, FILE *file);
		bool Load(char * filename, ImageRef &out);
		bool Info(char * filename, ImageRef &out);
	}

};