#pragma once

#include <PvApi.h>
//#define WIN32_LEAN_AND_MEAN
//#include <Windows.h>

class ProsilicaCamera
{
public:
	ProsilicaCamera(unsigned long uid);
	~ProsilicaCamera(void);

	unsigned long getUid();
	tPvHandle getHandle();
	tPvUint32 getCounter();

	
	//properties
	const unsigned long& Uid() const;
	void Uid(const unsigned long& uid);

	const tPvHandle& Handle() const;
	void Handle(const tPvHandle& handle);
	
	const tPvUint32& Counter() const;
	void Counter(const tPvUint32& counter);

private:
	static char * ErrorDescriptions[]; 

	unsigned long _uid;
	tPvHandle _handle;
	tPvUint32 _counter;
};


