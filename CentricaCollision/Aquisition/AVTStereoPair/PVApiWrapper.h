#pragma once
#include <PvApi.h>

class PVApiWrapper
{
public:
	static PVApiWrapper& getInstance()
	{
		static PVApiWrapper instance; //Singleton instance
		return instance;
	}
		
	enum CameraEvents
	{
		EventAcquisitionStart = 40000,
		EventAcquisitionEnd = 40001,
		EventFrameTrigger = 40002,
		EventExposureEnd = 40003,
		EventAcquisitionRecordTrigger = 40004,
		EventSyncIn1Rise = 40010,
		EventSyncIn1Fall = 40011,
		EventSyncIn2Rise = 40012,
		EventSyncIn2Fall = 40013,
		EventSyncIn3Rise =40014,
		EventSyncIn3Fall=40015,
		EventSyncIn4Rise=40016,
		EventSyncIn4Fall=40017,
		EventOverflow=65534,
	};


	 bool SoftTriggerAsync(tPvHandle handle);

	// close camera, free memory.
	 bool OpenCamera(unsigned long uniqueId, tPvHandle &handle);
	 bool CloseCamera(tPvHandle handle);

	 bool StartAsMaster(tPvHandle handle, tPvFrame* frames, unsigned long count,tPvFrameCallback callback);
	 bool StartAsSlave(tPvHandle handle, tPvFrame* frames, unsigned long count,tPvFrameCallback callback);
	 bool StopCamera(tPvHandle handle);


	 //bool AllocateFrameBuffers(tPvHandle handle, tPvFrame* frames, unsigned long count);
	 //bool DeallocateFrameBuffers( tPvFrame* frames, unsigned long count);
	 unsigned long GetExposure(tPvHandle handle);
	 unsigned long GetFrameSize(tPvHandle handle);
	 unsigned long GetSensorWidth(tPvHandle handle);
	 unsigned long GetSensorHeight(tPvHandle handle);

	 unsigned long GetExposureValue(tPvHandle handle);


	 bool RegisterEventsCallback(tPvHandle handle, tPvCameraEventCallback callback);
	 bool UnregisterEventsCallback(tPvHandle handle, tPvCameraEventCallback callback);

	 bool RegisterPnpCallbacks(tPvLinkCallback plugCB, tPvLinkCallback unplugCB, void * context);
	 bool UnregisterPnpCallbacks(tPvLinkCallback plugCB, tPvLinkCallback unplugCB);

	 bool SetCentralDSPSubregion(tPvHandle handle,int percentage);
	 bool SetDSPSubregion(tPvHandle handle, float top, float bottom,float left, float right);

private:
	PVApiWrapper(void);
	~PVApiWrapper(void);

	//Not implemented
	PVApiWrapper(PVApiWrapper const&); 
	void operator = (PVApiWrapper const&);



};

