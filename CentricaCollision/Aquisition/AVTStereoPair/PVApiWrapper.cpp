#include "StdAfx.h"
#include "PVApiWrapper.h"

char* ErrorDescriptions[] = {  
	"No error",  
	"Unexpected camera fault",
	"Unexpected fault in PvApi or driver",
    "Camera handle is invalid",
    "Bad parameter to API call",
    "Sequence of API calls is incorrect",
    "Camera or attribute not found",
    "Camera cannot be opened in the specified mode",
    "Camera was unplugged",
    "Setup is invalid (an attribute is invalid)",
    "System/network resources or memory not available",
    "1394 bandwidth not available",
    "Too many frames on queue",
    "Frame buffer is too small",
    "Frame cancelled by user",
    "The data for the frame was lost",
    "Some data in the frame is missing",
    "Timeout during wait",
    "Attribute value is out of the expected range",
    "Attribute is not this type (wrong access function)",
    "Attribute write forbidden at this time",
    "Attribute is not available at this time",
    "A firewall is blocking the traffic (Windows only)"
};

#define PVAPICALL(pvApiCall, description) {tPvErr e = (pvApiCall); if(e != ePvErrSuccess) printf("Error! %s %s:%d %d '%s'\n",(description), __FUNCTION__,__LINE__,e,ErrorDescriptions[e]); }


PVApiWrapper::PVApiWrapper()
{
	tPvErr err = PvInitialize();
	PVAPICALL(err ,"PvInitialize");
	if(err)
	{
		printf("FATAL ERROR! Can't initialise API, Exiting");
		exit (EXIT_FAILURE);
	}
	printf("Initialised API\n");
}

PVApiWrapper::~PVApiWrapper(void)
{
	PvUnInitialize();
	printf("Uninitialised API\n");
}


bool PVApiWrapper::SoftTriggerAsync(tPvHandle handle)
{
	tPvErr err = PvCommandRun(handle,"FrameStartTriggerSoftware");
	PVAPICALL(err,"PvCommandRun FrameStartTriggerSoftware");
	return err == ePvErrSuccess;
}

unsigned long PVApiWrapper::GetFrameSize(tPvHandle handle)
{
	unsigned long frameSize = 0;
	tPvErr err = PvAttrUint32Get(handle,"TotalBytesPerFrame", &frameSize);
	PVAPICALL(err,"PvAttrUint32Get TotalBytesPerFrame");
	return frameSize;
}
unsigned long PVApiWrapper::GetSensorWidth(tPvHandle handle)
{
	unsigned long value = 0;
	tPvErr err = PvAttrUint32Get(handle,"SensorWidth", &value);
	PVAPICALL(err,"PvAttrUint32Get SensorWidth");
	return value;
}
unsigned long PVApiWrapper::GetSensorHeight(tPvHandle handle)
{
	unsigned long value = 0;
	tPvErr err = PvAttrUint32Get(handle,"SensorHeight", &value);
	PVAPICALL(err,"PvAttrUint32Get SensorHeight");
	return value;
}
unsigned long PVApiWrapper::GetExposureValue(tPvHandle handle)
{
	unsigned long value = 0;
	tPvErr err = PvAttrUint32Get(handle,"ExposureValue", &value);
	PVAPICALL(err,"PvAttrUint32Get ExposureValue");
	return value;
}


bool PVApiWrapper::OpenCamera(unsigned long uniqueId, tPvHandle &handleOut)
{
	tPvHandle handle;
	tPvErr err;

	err = PvCameraOpen(uniqueId, ePvAccessMaster, &handleOut);
	PVAPICALL(err,"PvCameraOpen");
	if (err != ePvErrSuccess) return false;
	return true;
}

bool PVApiWrapper::CloseCamera(tPvHandle handle)
{
	tPvErr err = PvCameraClose(handle);
	PVAPICALL(err,"PvCameraClose");
	return err == ePvErrSuccess;
}

bool PVApiWrapper::RegisterEventsCallback(tPvHandle handle, tPvCameraEventCallback callback)
{
	tPvErr err;	
	//err = PvCameraEventCallbackRegister(handle,callback,handle)

	tPvUint32 bitmask = 0;

	//Find all events available for the passed camera
	for(int i = 0,n=1; i<30 ;i++, n*=2)
	{
		if (PvAttrUint32Set(handle, "EventsEnable1", n) == ePvErrSuccess)
		{
			printf("*");
			bitmask |= n;
		}
		else
		{
			printf(".");
		}
	}
	printf("\n");

	//Subscribe to all available events
	err = PvAttrUint32Set(handle,"EventsEnable1",bitmask);
	PVAPICALL(err,"EventsEnable1");
	

	//Sanity check to see if we were successful subscribing
	PvAttrUint32Get(handle,"EventsEnable1", &bitmask);
	printf("Events set. EventsEnable1 bitmask: %u\n", bitmask);

    //register callback function
	if ((err = PvCameraEventCallbackRegister(handle,callback,handle)) != ePvErrSuccess)
    {
		printf("PvCameraEventCallbackRegister err: %u\n", err);
        return false;
    }     
	return true;
}

// unsetup event channel
bool PVApiWrapper::UnregisterEventsCallback(tPvHandle handle, tPvCameraEventCallback callback)
{
    // wait so that the "AcquisitionEnd" [from CameraStop()] can be received on the event channel
   // Sleep(1000);??

	// clear all events
	tPvErr err;
	err = PvAttrUint32Set(handle,"EventsEnable1",0);
	PVAPICALL(err,"PvAttrUint32Set EventsEnable1");

    // unregister callback function
	err = PvCameraEventCallbackUnRegister(handle,callback);
	PVAPICALL(err,"PvAttrUint32Set EventsEnable1");

	return err == ePvErrSuccess;
}

bool PVApiWrapper::StopCamera(tPvHandle handle)
{
	tPvErr err;
	
	//stop camera receiving triggers
	err = PvCommandRun(handle,"AcquisitionStop");
	PVAPICALL(err,"PvCommandRun AcquisitionStop");
	if (err != ePvErrSuccess) return false;
	
    //clear queued frames. will block until all frames dequeued
	err = PvCaptureQueueClear(handle);
	PVAPICALL(err,"PvCaptureQueueClear");
	if (err != ePvErrSuccess) return false;

	//stop driver stream
	err = PvCaptureEnd(handle);
	PVAPICALL(err,"PvCaptureEnd");
	if (err != ePvErrSuccess) return false;

	return true;
}

bool PVApiWrapper::SetCentralDSPSubregion(tPvHandle handle,int percentage)
{
	tPvErr err;

	tPvUint32 w = PVApiWrapper::getInstance().GetSensorWidth(handle);
	tPvUint32 h = PVApiWrapper::getInstance().GetSensorHeight(handle);
	tPvUint32 y =( h-((h*percentage)/100))/2;
	tPvUint32 x =( w-((w*percentage)/100))/2;

	printf("sensor size = %ux%u\nleft and right border = %u\ntop and bottom border = %u\n",w,h,x,y);

	err = PvAttrUint32Set(handle,"DSPSubregionTop", y);
	PVAPICALL(err,"DSPSubregionTop");
	if (err != ePvErrSuccess) return false;

	err = PvAttrUint32Set(handle,"DSPSubregionBottom", h-y);
	PVAPICALL(err,"DSPSubregionBottom");
	if (err != ePvErrSuccess) return false;
	
	err = PvAttrUint32Set(handle,"DSPSubregionLeft", x);
	PVAPICALL(err,"DSPSubregionLeft");
	if (err != ePvErrSuccess) return false;

	err = PvAttrUint32Set(handle,"DSPSubregionRight", w-x);
	PVAPICALL(err,"DSPSubregionRight");
	if (err != ePvErrSuccess) return false;
	
	
	return true;

}
unsigned long PVApiWrapper::GetExposure(tPvHandle handle)
{
	//set the initial exposure to a very short value
	tPvErr err;
	tPvUint32 val = -1;
	err = PvAttrUint32Get(handle,"ExposureValue", &val);
	PVAPICALL(err,"ExposureValue");
	return  val;
}

 bool PVApiWrapper::SetDSPSubregion(tPvHandle handle, float top, float bottom,float left, float right)
 {
	tPvErr err;
	bool success = true;
	float w = PVApiWrapper::getInstance().GetSensorWidth(handle);
	float h = PVApiWrapper::getInstance().GetSensorHeight(handle);

	top*=h;
	bottom*=h;
	left*=w;
	right*=w;

	err = PvAttrUint32Set(handle,"DSPSubregionTop", (int)top);
	PVAPICALL(err,"DSPSubregionTop");
	success &= (err == ePvErrSuccess);

	err = PvAttrUint32Set(handle,"DSPSubregionBottom", (int)bottom);
	PVAPICALL(err,"DSPSubregionBottom");
	success &= (err == ePvErrSuccess);
	
	err = PvAttrUint32Set(handle,"DSPSubregionLeft", (int)left);
	PVAPICALL(err,"DSPSubregionLeft");
	success &= (err == ePvErrSuccess);

	err = PvAttrUint32Set(handle,"DSPSubregionRight", (int)right);
	PVAPICALL(err,"DSPSubregionRight");
	success &= (err == ePvErrSuccess);

	printf("DSP Subregion: Top=%u, Bottom=%u, Left=%u, Right=%u\n",(int)top, (int)bottom, (int)left, (int)right);

	return success;
 }

bool StartCamera(tPvHandle handle, tPvFrame* frames, unsigned long count, tPvFrameCallback callback)
{
	//FrameStartTriggerOverlap

	tPvErr err;
	err = PvCaptureAdjustPacketSize(handle, 8228);
	PVAPICALL(err,"PvCaptureAdjustPacketSize 8228")
	if (err != ePvErrSuccess) return false;

	err = PvCaptureStart(handle);
	PVAPICALL(err,"PvCaptureStart")
	if (err != ePvErrSuccess) return false;


 
	//set the sub region metering to the center of the image 
	PVApiWrapper::getInstance().SetDSPSubregion(handle,0.1f,0.9f,0.2f,0.9f);

	//set the initial exposure to a very short value
	err = PvAttrUint32Set(handle,"ExposureValue", 1000);
	PVAPICALL(err,"ExposureValue");
	if (err != ePvErrSuccess) return false;

	tPvUint32 val;
	err = PvAttrUint32Get(handle,"ExposureAutoMax", &val);
	printf("ExposureAutoMax = %u\n",val);

	val= 625;//can't set any lower it seems
	err = PvAttrUint32Set(handle,"ExposureAutoMin", val);
	PVAPICALL(err,"ExposureAutoMin");
	err = PvAttrUint32Get(handle,"ExposureAutoMin", &val);	
	printf("ExposureAutoMin = %u\n",val);

	err = PvAttrUint32Get(handle,"ExposureAutoOutliers", &val); 
	printf("ExposureAutoOutliers = %u\n",val);

	err = PvAttrUint32Get(handle,"ExposureAutoTarget", &val);
	printf("ExposureAutoTarget = %u\n",val);

	err = PvAttrUint32Get(handle,"ExposureAutoRate", &val);
	printf("ExposureAutoRate = %u\n",val);

		//Exposure settings
	err = PvAttrEnumSet(handle,"ExposureAutoAlg","Mean");
	PVAPICALL(err,"PvAttrEnumSet ExposureAutoAlg Mean");
	if (err != ePvErrSuccess) return false;

	err = PvAttrEnumSet(handle,"ExposureMode","Auto");
	PVAPICALL(err,"PvAttrEnumSet ExposureMode Mode");
	if (err != ePvErrSuccess) return false;

		// queue frames with FrameDoneCB callback function. Each frame can use a unique callback function
	// or, as in this case, the same callback function.

	for(int i=0; i<count ; i++)
	{  
		err = PvCaptureQueueFrame(handle, &frames[i], callback);
		PVAPICALL(err,"PvCaptureQueueFrame");
		if (err != ePvErrSuccess)
		{
			//clear any frames that have already been queued
			PvCaptureQueueClear(handle);
			// stop driver capture stream
			PvCaptureEnd(handle);
			return false;
		}
	}
	printf("frames buffered and queued\n");
	return true;
}

bool PVApiWrapper::StartAsMaster(tPvHandle handle, tPvFrame* frames, unsigned long count, tPvFrameCallback callback)
{
	if(!StartCamera(handle,frames,count, callback))
		return false;

	tPvErr err;
	err = PvAttrEnumSet(handle,"FrameStartTriggerMode","Software");
	PVAPICALL(err,"PvAttrEnumSet FrameStartTriggerMode Software");
	if (err != ePvErrSuccess) return false;

	err = PvAttrEnumSet(handle,"SyncOut1Mode","Exposing");
	PVAPICALL(err,"PvAttrEnumSet SyncOut1Mode Exposing");
	if (err != ePvErrSuccess) return false;

	//err = PvAttrEnumSet(handle,"SyncOut1Mode","FrameTrigger");
	//PVAPICALL(err,"PvAttrEnumSet SyncOut1Mode FrameTrigger");
	//if (err != ePvErrSuccess) return false;

	err = PvAttrEnumSet(handle,"AcquisitionMode","Continuous");
	PVAPICALL(err,"PvAttrEnumSet AcquisitionMode Continuous");
	if (err != ePvErrSuccess) return false;

	err = PvCommandRun(handle,"AcquisitionStart");
	PVAPICALL(err,"PvCommandRun AcquisitionStart");
	if (err != ePvErrSuccess) return false;

	/// Close camera on fail!!!!

	return true;
}

bool PVApiWrapper::StartAsSlave(tPvHandle handle, tPvFrame* frames, unsigned long count,tPvFrameCallback callback)
{
	if(!StartCamera(handle,frames ,count,callback))
		return false;

	tPvErr err;

	err = PvAttrEnumSet(handle,"FrameStartTriggerMode","SyncIn1");
	PVAPICALL(err,"PvAttrEnumSet FrameStartTriggerMode SyncIn1");
	if (err != ePvErrSuccess) return false;

	err = PvAttrEnumSet(handle,"FrameStartTriggerEvent","EdgeRising");
	PVAPICALL(err,"PvAttrEnumSet FrameStartTriggerEvent EdgeRising");
	if (err != ePvErrSuccess) return false;
		
	//??
	err = PvAttrEnumSet(handle,"SyncOut1Mode","AcquisitionTriggerReady");
	PVAPICALL(err,"PvAttrEnumSet SyncOut1Mode AcquisitionTriggerReady");
	if (err != ePvErrSuccess) return false;
	
	err = PvAttrEnumSet(handle,"AcquisitionMode","Continuous");
	PVAPICALL(err,"PvAttrEnumSet AcquisitionMode Continuous");
	if (err != ePvErrSuccess) return false;

	err = PvCommandRun(handle,"AcquisitionStart");
	PVAPICALL(err,"PvCommandRun AcquisitionStart");
	if (err != ePvErrSuccess) return false;

	
	/// Close camera on fail!!!!

	return true;
}

bool PVApiWrapper::RegisterPnpCallbacks(tPvLinkCallback plugCB, tPvLinkCallback unplugCB, void * context)
{
	tPvErr err;

	err = PvLinkCallbackRegister(plugCB, ePvLinkAdd, context);
	PVAPICALL(err, "PvLinkCallbackRegister  ePvLinkAdd");
	if (err != ePvErrSuccess) return false;
	
	err = PvLinkCallbackRegister(unplugCB, ePvLinkRemove, context);
	PVAPICALL(err, "PvLinkCallbackRegister  ePvLinkRemove");
	if (err != ePvErrSuccess) return false;

	return true; 
}

bool PVApiWrapper::UnregisterPnpCallbacks(tPvLinkCallback plugCB,tPvLinkCallback unplugCB)
{
	tPvErr err;

	err = PvLinkCallbackUnRegister(plugCB, ePvLinkAdd);
	PVAPICALL(err, "PvLinkCallbackUnRegister  ePvLinkAdd"); 
	if (err) return false;
	
	err = PvLinkCallbackUnRegister(unplugCB, ePvLinkRemove);
	PVAPICALL(err, "PvLinkCallbackUnRegister  ePvLinkRemove");
	if (err) return false;

	return true; 
}

