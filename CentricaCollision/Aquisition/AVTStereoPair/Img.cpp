
#include "stdafx.h"
#include "Img.h"

using namespace Img;

long LSB(long n);
long long LSB(long long n);

Img::ImageRef::ImageRef(void)
	: image(NULL), width(0), height(0), stride(0), bpp(0)
{}

Img::ImageRef::ImageRef(unsigned char* image, long width, long height, long stride, long bpp)
	: image(image), width(width), height(height), stride(stride), bpp(bpp)
{}


long Img::ImageRef::alignment(void)
{
	long long a = LSB((long long) this->image);
	long s = LSB(this->stride);
	return a<s ? a:s;
}

long Img::ImageRef::size(void)
{
	return this->stride*this->height;
}



void Img::write_JPEG_file (char * filename, int quality, ImageRef* image)
{
  /* This struct contains the JPEG compression parameters and pointers to
   * working space (which is allocated as needed by the JPEG library).
   * It is possible to have several such structures, representing multiple
   * compression/decompression processes, in existence at once.  We refer
   * to any one struct (and its associated working data) as a "JPEG object".
   */
  struct jpeg_compress_struct cinfo;
  /* This struct represents a JPEG error handler.  It is declared separately
   * because applications often want to supply a specialized error handler
   * (see the second half of this file for an example).  But here we just
   * take the easy way out and use the standard error handler, which will
   * print a message on stderr and call exit() if compression fails.
   * Note that this struct must live as long as the main JPEG parameter
   * struct, to avoid dangling-pointer problems.
   */
  struct jpeg_error_mgr jerr;
  /* More stuff */
  FILE * outfile;		/* target file */
  JSAMPROW row_pointer[1];	/* pointer to JSAMPLE row[s] */
  int row_stride;		/* physical row width in image buffer */

  /* Step 1: allocate and initialize JPEG compression object */

  /* We have to set up the error handler first, in case the initialization
   * step fails.  (Unlikely, but it could happen if you are out of memory.)
   * This routine fills in the contents of struct jerr, and returns jerr's
   * address which we place into the link field in cinfo.
   */
  cinfo.err = jpeg_std_error(&jerr);
  /* Now we can initialize the JPEG compression object. */
  jpeg_create_compress(&cinfo);

  /* Step 2: specify data destination (eg, a file) */
  /* Note: steps 2 and 3 can be done in either order. */

  /* Here we use the library-supplied code to send compressed data to a
   * stdio stream.  You can also write your own code to do something else.
   * VERY IMPORTANT: use "b" option to fopen() if you are on a machine that
   * requires it in order to write binary files.
   */
  if ((outfile = fopen(filename, "wb")) == NULL) {
    fprintf(stderr, "can't open %s\n", filename);
    exit(1);
  }
  jpeg_stdio_dest(&cinfo, outfile);

  /* Step 3: set parameters for compression */

  /* First we supply a description of the input image.
   * Four fields of the cinfo struct must be filled in:
   */

  //cinfo.image_width = image->width/3;	/* image width and height, in pixels */
  //cinfo.image_height = image->height;
  //cinfo.input_components = 3;		/* # of color components per pixel */
  //cinfo.in_color_space =  JCS_RGB;//JCS_GRAYSCALE; 	/* colorspace of input image */

  cinfo.image_width = image->width;	/* image width and height, in pixels */
  cinfo.image_height = image->height;
  cinfo.input_components = 1;		/* # of color components per pixel */
  cinfo.in_color_space =  JCS_GRAYSCALE; 	/* colorspace of input image */
  /* Now use the library's routine to set default compression parameters.
   * (You must set at least cinfo.in_color_space before calling this,
   * since the defaults depend on the source color space.)
   */
  jpeg_set_defaults(&cinfo);
  /* Now you can set any non-default parameters you wish to.
   * Here we just illustrate the use of quality (quantization table) scaling:
   */

  jpeg_set_quality(&cinfo, 75, FALSE);
  //jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);

  /* Step 4: Start compressor */
 
  /* TRUE ensures that we will write a complete interchange-JPEG file.
   * Pass TRUE unless you are very sure of what you're doing.
   */
  jpeg_start_compress(&cinfo, TRUE);

  /* Step 5: while (scan lines remain to be written) */
  /*           jpeg_write_scanlines(...); */

  /* Here we use the library's state variable cinfo.next_scanline as the
   * loop counter, so that we don't have to keep track ourselves.
   * To keep things simple, we pass one scanline per call; you can pass
   * more if you wish, though.
   */
  row_stride = image->stride;	/* JSAMPLEs per row in image_buffer */

  while (cinfo.next_scanline < cinfo.image_height) {
    /* jpeg_write_scanlines expects an array of pointers to scanlines.
     * Here the array is only one element long, but you could pass
     * more than one scanline at a time if that's more convenient.
     */
    row_pointer[0] = & image->image[cinfo.next_scanline * row_stride];
    (void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
  }

  /* Step 6: Finish compression */

  jpeg_finish_compress(&cinfo);
  /* After finish_compress, we can close the output file. */
  fclose(outfile);

  /* Step 7: release JPEG compression object */

  /* This is an important step since it will release a good deal of memory. */
  jpeg_destroy_compress(&cinfo);

  /* And we're done! */
}



//returns true if any part of the passed rectangle is over the threshold shift
//bool ThresholdRect(Img::ImageRef* image, int threshold)
//{
//
//#ifdef SANITYCHECKING
//	bool good = false;
//
//	if ((threshold & -8)!=0)
//		printf("threshold out of 0 to 7 range\n");
//	else if (image->alignment()<8)
//		printf("image not 64 bit aligned (%u)\n",image->alignment());	
//	else if (image->bpp != 8)
//		printf("source image must be 8bpp (%u)\n",image->bpp);	
//	else
//		good=true;
//
//	if(!good)
//		return false;
//#endif
//
//	int width64,stride64;
//	width64 = image->width>>3; 
//	stride64 = image->stride >> 3;
//	
//	const long long ONE = 0x101010101010101;
//	long long mask, *scan,*pix,*end;
//	scan = (long long*)image->image;
//	mask = (ONE<<threshold)-ONE;
//
//	for(int y=0; y<image->height; y++)//for(int y=0; y<image->height; y++)
//	{
//		pix = scan += stride64;
//		for(int i=0; i<width64; i++, pix++)//for(int i=0; i<image->width; i++)
//		{
//			if(*pix & mask)
//			{
//				return true;
//			}
//		}
//	}
//	return false;
//}

//int Img::MapThresholds(Img::ImageRef* img,int order, unsigned char *map, int threshold)//int MapThresholds2(ImageRef* img, ImageRef* map, int threshold)// 
//{
	//int unit = 1<<order;
	//int w = img->width >> order;
	//int wr = img->width - (w << order);

	//int h = (img->height+unit-1) >> order;
	//int hr = unit + img->height - (h << order);
	//int s = img->stride;
	//
	//memset(map,0,w*h);//<<---this might need a fiddle

	//Img::ImageRef sqr;
	//sqr.height = unit;
	//sqr.stride=img->stride;
	//sqr.bpp = img->bpp;
	//unsigned char *scn=img->image;

	//for (int y=0,i=0,x; y<h; y++)
	//{
	//	if(y+1==h)//last row alter height if fractional
	//		sqr.height = hr;
	//	sqr.image = scn;
	//	sqr.width = unit;
	//	scn+=img->stride;
	//	for (x=0; x<w; x++,i++)
	//	{
	//		if (ThresholdRect(&sqr,threshold)) //handle every full unit of the row
	//		{
	//			map[i] |= 1 << x & 7;
	//		}
	//		sqr. image += unit;
	//	}
	//	sqr.width=wr; 
	//	if (ThresholdRect(&sqr,threshold)) //handle any fractional remainder unit 
	//	{
	//		map[i++] |= 1 << x & 7;
	//	}
	//	
	//}


	//return 0;

//}



//void Img::Swap(__m128i* a, __m128i* b)
//	{
//		__m128i* t = a;
//		a=b;
//		b=t;
//	}


bool checkKernel(unsigned char *kernel, int count, int sum)
{
	for(int i = 0;i<count;i++)
		sum-=*kernel++;
	return sum == 0;
}


void BoxBlur1D(unsigned char *target,int width,int height, int incr, int stride,int order)
{
	unsigned char *pix;
	int sum = 0;
	
	int kSz = 1 << order;
	int kMask = kSz -1;
	int kHlf = kSz>>1;

	int * kernel  = (int*) _alloca( kSz );

	for(int y = 0;y<height;y++)
	{
		//pix = target;
		//target += stride;
		//int k=0;

		////initialise the kernel using mirror edge treatment
		//sum = 0;		
		//for(int i=0; i<kHlf; i++)
		//{
		//	sum += kernel[kHlf - (i+1)] = kernel[kHlf + i] = pix[i];
		//}
		//sum<<=1;//double the sum

		////middle section, where the work is done
		//for(int i = 0; i < leadOut; i += incr, k = (k+1) & kMask)
		//{
		//	sum -= kernel[k];
		//	sum += kernel[k] = pix[i+kHlf];
		//	pix[i] = sum >> order;
		//}

		////lead out using mirror edge treatment
		//for(int i = leadOut, j = width-1 ; i<width; i++, j--)
		//{
		//	int k = i & kMask;
		//	sum -= kernel[k];
		//	sum += kernel[k] = pix[j];
		//	pix[i]= sum >> order;
		//}
	}

	printf("not written yet");
	



}

//Performs an in place quick box blur on the passed image.
//order is the power of 2 used to size the kernel.
//the algorithm uses mirror edge treatment
void Img::BoxBlur(ImageRef *image, int order)
{
//#ifdef SANITYCHECKING
//	bool good = false;
//	if(image->bpp != 8)
//		printf("images must be 8 bpp");
//	//else if(image->alignment() < 8)
//	//	printf("image must be at least 64 bit alighned");
//	else
//		good = true;
//	if (!good)
//		return;
//#endif
//	printf("HEre bE DrAgoNs!!!!\n");

	unsigned char *pix, *row;	

	int width = image->width;
	int height = image->height;
	int stride = image->stride;

	int kSz = 1<<order; //always a power of two
	int kMask = kSz -1;
	int kHlf = kSz>>1;
	//int* kernel = new int[kSz];
	unsigned char * kernel  = (unsigned char*) _alloca( kSz );

	int leadOut;  

	int sum;
	

	//horizontal blur
	row = image->image;
	leadOut = width - kHlf;
	
	//printf("\n%u\t%u\t%u\t%u\n",kSz,kHlf,kMask,leadOut);
	for(int y = 0; y < height; y++)
	{
		pix = row;
		row+=stride;
	
		//initialise the kernel using mirror edge treatment
		sum = 0;		
		for(int i=0; i<kHlf; i++)//example order 3 kernel -> 32100123
		{
			sum += kernel[kHlf - (i+1)] = kernel[kHlf + i] = pix[i];
		}
		sum<<=1;//double the sum

		if(!checkKernel(kernel,kSz,sum))
			printf("bad kernel 1");

		//middle section, where the work is done
		for(int i = 0;i<leadOut;i++)
		{
			int k = i & kMask;
			sum -= kernel[k];
			sum += kernel[k] = pix[i+kHlf];
			pix[i] = sum >> order;
		}

		if(!checkKernel(kernel,kSz,sum))
			printf("bad kernel 2");

		//lead out using mirror edge treatment
		for(int i = leadOut, j = width-1 ; i<width; i++, j--)
		{
			int k = i & kMask;
			sum -= kernel[k];
			sum += kernel[k] = pix[j];
			pix[i]= sum >> order;
		}

		if(!checkKernel(kernel,kSz,sum))
			printf("bad kernel 3");
	}

	//vertical blur
	row = image->image;
	leadOut = height - kHlf;
	for(int x = 0; x < width; x++)
	{
		pix = row;
		row++;
	
		//initialise the kernel using mirror edge treatment
		sum = 0;		
		for(int i=0; i<kHlf; i++)//example order 3 kernel -> 32100123
		{
			sum += kernel[kHlf - (i+1)] = kernel[kHlf + i] = pix[i*stride];
		}
		sum<<=1;//double the sum

		//middle section, where the work is done
		for(int i = 0; i<leadOut;i++)
		{
			int k = i & kMask;
			sum -= kernel[k];
			sum += kernel[k] = pix[(i+kHlf)*stride];
			pix[i*stride] = sum >> order;
		}

		//lead out using mirror edge treatment
		for(int i = leadOut, j = height-1 ; i<height; i++, j--)
		{
			int k = i & kMask;
			sum -= kernel[k];
			sum += kernel[k] = pix[j*stride];
			pix[i*stride]= sum >> order;
		}
	}




	//delete [] kernel; //free our temporary kernel
	//no need to delete a stack allocated variable 
}




void Img::ExtractChannel(ImageRef *bayer, ImageRef *target, int channel)
{
#ifdef SANITYCHECKING
	bool good = false;

	if(bayer->width  > target->width*2 || bayer->height  > target->height*2)
		printf("target buffer too small\n");	
	//else if(bayer->bpp != 8 || target->bpp != 8)
	//	printf("images must be 8 bpp\n");
	else
		good = true;
	if (!good)
		return;
#endif

	unsigned long width, height, stride;
	unsigned char *src, *tgt, *srcRow, *tgtRow;
	srcRow = bayer->image + (channel & 1) +  ((channel & 2)>>1) * bayer->stride;
	tgtRow = target->image;

	height = bayer->height >> 1;
	width = bayer->width >> 1;
	stride = bayer->stride<<1;

	for (int y = 0;y< height;y++)
	{
		src=srcRow;
		srcRow += stride;
		tgt=tgtRow;
		tgtRow += target->stride;
		for (int x=0;x<width;x++, src+=2,tgt++)
		{
			*tgt = *src;
		}
	}
}

inline void Img::AbsDif(__m128i* srcA, __m128i* srcB, __m128i* tgt)
{
	*tgt = _mm_or_si128( _mm_subs_epu8(*srcA,*srcB) , _mm_subs_epu8(*srcB,*srcA));
}




//populates a buffer with the absolute difference of two buffers
void Img::AbsoluteDifference(__m128i* srcA, __m128i* srcB, __m128i* tgt,  unsigned long count)
{
	if (((int)srcA & 15) != 0 || ((int)srcB & 15) != 0 || ((int)tgt & 15) != 0)
	{
		printf("bad buffers\n");
		return;
	}

	for(int i = 0; i<count; i++, tgt++, srcA++, srcB++)
	{
		//*tgt = _mm_sad_epu8(*srcA,*srcB); //<-- thought it would work! but sadly no :(

		//subtracts, saturates then combines tgt=(a-b)|(b-a)
		*tgt = _mm_or_si128(_mm_subs_epu8(*srcA,*srcB), _mm_subs_epu8(*srcB,*srcA));
	}
}

	//populates a buffer with the absolute difference of two buffers
void Img::TernaryDifference(__m128i* srcA, __m128i* srcB, __m128i* srcC, __m128i* tgt, unsigned long count)
{
	for(int i = 0; i<count; i++, tgt++, srcA++, srcB++, srcC++)
	{
		//subtracts, saturates then combines d=(a-b)|(b-a)
		__m128i d = _mm_or_si128(_mm_subs_epu8(*srcA,*srcB), _mm_subs_epu8(*srcB,*srcA));

		*tgt = _mm_subs_epu8(d,*srcC);
	}
}

//populates a buffer with the saturated difference of two buffers
void Img::Difference(__m128i* srcA, __m128i* srcB, __m128i* tgt,  unsigned long count)
{
	//const __m128i HIMASK = _mm_set1_epi8(0xfe);
	//const __m128i MIDVAL = _mm_set1_epi8(0x7f);

	for(int i = 0; i<count; i++, tgt++, srcA++, srcB++)
	{
		*tgt = _mm_subs_epu8(*srcA,*srcB);
		////no support for a 8 bit shift... so using 128 bit right shift with a mask to stop underflow
		//__m128i a =_mm_srli_epi64( _mm_and_si128(*srcA,HIMASK),1 );
		//__m128i b =_mm_srli_epi64( _mm_and_si128(*srcB,HIMASK),1 );

		//*tgt = _mm_add_epi8 (MIDVAL, _mm_sub_epi8 (a,b));
	}
}

void Img::SplitScanLine(__m128i* src, __m128i* tgtA, __m128i* tgtB, unsigned long width)
{
	const __m128i EVNBYTS=_mm_set1_epi16(0xff); 
	const __m128i ODDBYTS =_mm_set1_epi16(0xff00);

	__m128i lo,hi; //to store source values
	unsigned long count = width >> 5; 

	//Spot the difference with the following two loops
	if(((unsigned long long) src & 0xf) == 0) //aligned source!
	{
		for(int i=0;i<count;i++)
		{
			//load 256 bits from the aligned buffer
			lo = _mm_load_si128 (src++); 
			hi = _mm_load_si128 (src++);

			//mask the even bytes and pack into the first output channel
			*tgtA++ = _mm_packus_epi16(
				_mm_and_si128(lo,EVNBYTS),
				_mm_and_si128(hi,EVNBYTS));

			//mask and shift the odd bytes then pack into the second output channel
			*tgtB++ = _mm_packus_epi16(
				_mm_srli_epi16(_mm_and_si128(lo,ODDBYTS),8), 
				_mm_srli_epi16(_mm_and_si128(hi,ODDBYTS),8));
		}
	}
	else //unaligned source!
	{
		for(int i=0;i<count;i++)
		{
			//load 256 bits from the unaligned buffer
			lo = _mm_loadu_si128 (src++); 
			hi = _mm_loadu_si128 (src++);

			//mask the even bytes and pack into the first output channel
			*tgtA++ = _mm_packus_epi16(
				_mm_and_si128(lo,EVNBYTS),
				_mm_and_si128(hi,EVNBYTS));

			//mask and shift the odd bytes then pack into the second output channel
			*tgtB++ = _mm_packus_epi16(
				_mm_srli_epi16(_mm_and_si128(lo,ODDBYTS),8), 
				_mm_srli_epi16(_mm_and_si128(hi,ODDBYTS),8));
		}
	}
}
//
//void Img::SplitTheBayer(tPvFrame* in, SplitBayer* out)
//{
//	unsigned long w = in->Width;
//	unsigned long h = in->Height;
//	unsigned long stride = w >> 5; //destination stride
//	unsigned char* byr = (unsigned char*)in->ImageBuffer; 
//
//	//set up destination pointers
//	__m128i *chan0 =  (__m128i*)out->Channels[0];
//	__m128i *chan1 =  (__m128i*)out->Channels[1];
//	__m128i *chan2 =  (__m128i*)out->Channels[2];
//	__m128i *chan3 =  (__m128i*)out->Channels[3];
//
//	out->X=0;
//	out->Y=0;
//	out->Width = w;
//	out->Height = h;
//	out->FrameCount = in->FrameCount;
//	out->CameraTimestamp = in->TimestampLo;
//	out->SystemTimestamp = 0xffffffff;
//	out->Context = in->Context[0];
//
//	for (unsigned long y= 0;y<h;y+=2)
//	{
//		SplitScanLine((__m128i*)(byr + w * (y)),   chan0, chan1, w);
//		SplitScanLine((__m128i*)(byr + w * (y+1)), chan2, chan3, w);
//		chan0+=stride;
//		chan1+=stride;
//		chan2+=stride;
//		chan3+=stride;
//	}
//
//	//This is roughly the equivalent without using intrinsics
//	//int index= 0;
//	//for (unsigned long y= 0;y<h;y+=2)
//	//{
//	//	unsigned char* t = byr + w * y;
//	//	unsigned char* u = t + w;
//	//	for (int x=0;x<w;x+=2)
//	//	{
//	//		out->Channels[0][index] = *t++;
//	//		out->Channels[1][index] = *t++;
//	//		out->Channels[2][index] = *u++;
//	//		out->Channels[3][index] = *u++;
//	//		index++;
//	//	}
//	//}
//
//}




template <class T>
void Swap(T* a, T* b)
{
	T* swp = a;
	a = b;
	b = swp;
}


long LSB(long n)
{
	return (n & n-1)^n;
}
long long LSB(long long n)
{
	return (n & n-1)^n;
}

