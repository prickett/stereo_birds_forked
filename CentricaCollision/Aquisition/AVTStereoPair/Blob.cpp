#include "StdAfx.h"
#include "Blob.h"


Blob::Blob(void)
{
	area = 0;
	xSum = 0;
	ySum = 0;
	xMax = 0;
	xMin = -1; //set all bits of unsigned type
	yMax = 0;
	yMin = -1;
}


Blob::~Blob(void)
{
}

void Blob::Add(Slice &slice)
{
	//maybe too much in here?
	if(slice.x0 < xMin)
		xMin = slice.x0;

	if(slice.x1 > xMax)
		xMax = slice.x1;

	if(slice.y < yMin)//this won't happen if the order is sequencial
		yMin=slice.y;

	if(slice.y > yMax)
		yMax=slice.y;

	unsigned long w = slice.x1-slice.x0;
	ySum += w * slice.y;
	xSum += w * slice.x0 + ((w*(w-1))>>1);
	area += w;

	slices.push_back(slice);//needed??
}

void Blob::Absorb(Blob &blob)
{
	if(blob.xMin < xMin)
		xMin=blob.xMin;

	if(blob.xMax > xMax)
		xMax=blob.xMax;

	if(blob.yMin <  yMin)//this won't happen if the order is sequencial
		yMin=blob.yMin;

	if(blob.yMax >  yMax)
		yMax=blob.yMax;

	xSum+= blob.xSum;
	ySum+= blob.ySum;
	area+= blob.area;

	slices.splice(slices.end(),blob.slices);

	Blob newblob;
	blob = newblob;

}

void Blob::Debug()
{
	printf("area = %u\n", area);
	printf("x sum = %u\n", xSum);
	printf("y sum = %u\n", ySum);
	printf("x max = %u\n", xMax);
	printf("x min = %u\n", xMin);
	printf("y max = %u\n", yMax);
	printf("y min = %u\n", yMin);
	printf("\n");
	for (std::list<Slice>::iterator it = slices.begin(); it!=slices.end(); ++it)
	{
		printf("%u %u %u\n",it->y,it->x0,it->x1);
	}
}

unsigned short Blob::XMin(void)
{
	return xMin;
}
unsigned short Blob::YMin(void)
{
	return yMin;
}
unsigned short Blob::XMax(void)
{
	return xMax;
}
unsigned short Blob::YMax(void)
{
	return yMax;
}

unsigned long Blob::Area(void)
{
	return area;
}
float Blob::CenterX(void)
{
	return (float)xSum / area;
}
float Blob::CenterY(void)
{
	return (float)ySum / area;
}

std::list<Blob::Slice> Blob::Slices(void)
{
	return slices;
}




//Important to get above and below in the right order, below is further down the buffer
inline bool Blob::IsOverlapping(const Blob::Slice &above, const Blob::Slice &below)
{
	return above.x0 < below.x1 && above.x1 > below.x0 && above.y + 1 == below.y;
}


std::list<Blob> Blob::ThresholdBlobs(unsigned char *pix, unsigned short width,unsigned short height,unsigned char threshold)
{
	std::list<BlobSlice> active;
	std::list<Blob> blobs;
	BlobSlice current;

	unsigned long size = width * height;
	unsigned long i = 0;

	unsigned long lasty=0;

	do
	{
		while(i < size && pix[i] < threshold) //loop until threshold is reached or no values left
			i++;

		if (i < size) //we have gone over the threshold
		{
			//new slice
			unsigned long pos = i;
			current.slice.y = pos / width;
			current.slice.x0 = pos % width;

			unsigned long e = (current.slice.y + 1) * width;
			while(i < e && pix[i] >= threshold) //loop until we go under threshold or the row ends 
				i++;

			//finish new slice
			current.slice.x1 = i - pos + current.slice.x0;
				

			//printf("%u %u %u\n",current.slice.y,current.slice.x0,current.slice.x1);
			
			if (active.size() > 0)
			{
				//pop any slices from the rows before the row previous
				while(active.begin()->slice.y + 1 < current.slice.y)
					active.pop_front();

				//pop any slices to the left of the current slice on the previous row.
				while(active.begin()->slice.y < current.slice.y && active.begin()->slice.x1 <= current.slice.x0)
					active.pop_front();
			}
			
			if(active.size() > 0 && IsOverlapping(active.begin()->slice, current.slice)) //current overlaps a slice on the previous row
			{
				//let the slice on the previous row become the current slices parent
				current.blobIterator = active.begin()->blobIterator;
				current.blobIterator->Add(current.slice);

				//if the slice on the previous row does not extend to the right of the current slice...
				if (active.begin()->slice.x1 <= current.slice.x1)
				{
					//...pop it from the list, the slice cannot overlap any others... 
					active.pop_front();

					//loop through any further slices that the current slice overlaps
					while(active.size() > 0 && IsOverlapping(active.begin()->slice,current.slice))
					{
						//As long as they are not from the same blob any further overlapping slices get absorbed
						std::list<Blob>::iterator absorbed = active.begin()->blobIterator;
						if(absorbed != current.blobIterator) 
						{							
							current.blobIterator->Absorb(*absorbed);//absorb the other blob
							//loop through active slices replaced any references to the absorbed blob !!Heavy!!
							for(std::list<BlobSlice>::iterator bs = active.begin(); bs!=active.end(); ++bs)
							{
								if(bs->blobIterator == absorbed)
									bs->blobIterator = current.blobIterator;
							}
							blobs.erase(absorbed);
						}
					
						//again, if the slice on the previous row finishes further to the right of the current slice...
						if (active.begin()->slice.x1 > current.slice.x1)
						{						
							break; ///...exit the loop, the slice might hit others
						}
						active.pop_front();						
					}
				}
			}
			else //the current slice does not overlap any previous slices
			{				
				Blob blob; //create a new blob
				blob.Add(current.slice); //add the current slice to the blob
				blobs.push_back(blob); //add the blob to the blob list

				current.blobIterator = blobs.end(); //get the iterator for the new blob	
				current.blobIterator--;

				//printf("%f %f\n",current.blobIterator->CenterX(),blob.CenterX());
			}
			active.push_back(current); //add the new slice to the active blob list
			//active.back().blobIterator = current.blobIterator;

		}
		
	} while(i < size);
	return blobs;
}