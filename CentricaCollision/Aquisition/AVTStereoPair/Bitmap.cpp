#include "stdafx.h"
#include "Img.h"

using namespace Img;
using namespace Bitmap;

long sizeScanline(long width, long bpp)
{
	return ((width * bpp + 31) & -32) >> 3;
}

void fillHeader(ImageRef *image, bitmap_info_header *info)
{
	info->header_sz = sizeof(bitmap_info_header);
	info->width = image->width;
	info->height = image->height;
	info->nplanes = 1;
	info->bitspp = image->bpp;
	info->compress_type = 0; //<-- defaults to no compression
	info->bmp_bytesz = sizeScanline(image->width,image->bpp) * image->height;
	info->hres = 2835;
	info->vres = 2835;
	if(image->bpp == 8)
		info->ncolors = 256;
	if(image->bpp == 1)
		info->ncolors = 2;
	else
		info->ncolors = 0;
	info->nimpcolors = 0;
}

bool Img::Bitmap::Info(char * filename, ImageRef &out)
{
	bool result = false;
	FILE *file = fopen(filename,"r+b");
	if(file != NULL)
	{
		unsigned short mbs;
		bmpfile_header bfh;
		bitmap_info_header bih;
		int read;

		read+=fread(&mbs, sizeof(short), 1, file);
		read+=fread(&bfh, sizeof(bmpfile_header), 1, file);
		read+=fread(&bih, sizeof(bitmap_info_header), 1, file);

		if(mbs == MAGICBYTES && bih.header_sz == sizeof(bitmap_info_header))
		{
			out.width=bih.width;
			out.height=bih.height;
			out.stride=sizeScanline(bih.width,bih.bitspp);
			out.bpp=bih.bitspp;
			out.image = NULL;
			result = true;
		}
		fclose(file);
	}
	return result;
}


bool Img::Bitmap::Load(char * filename, ImageRef &out)
{
	int read = 0;
	bool result = false;
	//printf(filename);
	FILE *file = fopen(filename,"r+b");
	if(file != NULL)
	{
		unsigned short mbs;
		bmpfile_header bfh;
		bitmap_info_header bih;

		read += fread(&mbs, sizeof(short), 1, file);
		read += fread(&bfh, sizeof(bmpfile_header), 1, file);
		read += fread(&bih, sizeof(bitmap_info_header), 1, file);

		if(mbs == MAGICBYTES &&	bih.header_sz == sizeof(bitmap_info_header))
		{
			int  stride = sizeScanline(bih.width,bih.bitspp);
			out.width=bih.width;
			out.height=bih.height;
			out.stride=(out.width+15) & -16;//stride;//(out.width+15) & -16; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			out.bpp=bih.bitspp;
			if (out.image!=NULL)
			{
				fseek(file, bfh.bmp_offset, SEEK_SET);
				for(int i=out.height-1;i>=0;i--)
				{
					read += fread(out.image + i*out.stride,1,stride,file);
				}
			}
			result = true;
		}
		fclose(file);
	}
	return result;
}
bool Img::Bitmap::SaveAsGreyscale(ImageRef *greyscale, FILE *file)
{

	//Warning this has an error with non aligned scanlines
	if (greyscale->bpp<8)
	{
		printf("alignement = (%u), passed image reference must be least 8bpp\n",greyscale->bpp);
		return false;
	}
		
	unsigned short mb = MAGICBYTES;
	bmpfile_header bfh;
	bitmap_info_header bih;
	unsigned long colour_table[256];
	unsigned long stride = (greyscale->width + 3) & -4;
	unsigned long padSz = stride-greyscale->width;
	unsigned long pad = 0;
	unsigned long written = 0;

	bfh.creator1=0;
	bfh.creator2=0;
	bfh.bmp_offset = 2 + sizeof(bfh) + sizeof(bih) + sizeof(colour_table);

	fillHeader(greyscale,&bih);

	bih.ncolors = 256;
	bih.nimpcolors = 0;

	bfh.filesz = bfh.bmp_offset + bih.bmp_bytesz;

	for(unsigned long i = 0;i<256;i++)
	{
		colour_table[i] = (i * 0x10101u) | 0xff000000u;
	}

	written += fwrite(&mb, 1, sizeof(short),file);//magic bytes
	written += fwrite(&bfh, 1, sizeof(bfh),file);//file header
	written += fwrite(&bih, 1, sizeof(bih),file);//bitmap header
	written += fwrite(colour_table, 1, sizeof(colour_table),file);//colour table

	//Bitmaps are bottom up so write the scanlines backwards
	for(int y=bih.height-1; y>=0; y--)
	{
		unsigned char* scanline = greyscale->image + y * greyscale->stride;
		written += fwrite(scanline,	1, greyscale->width, file);
		written += fwrite(&pad,	1, padSz, file);
	}
	return written == bfh.filesz;
}

